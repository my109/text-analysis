                   Copyright 2009 The New York Times Company


                              17 of 2021 DOCUMENTS


                               The New York Times

                              June 7, 2009 Sunday
                              Late Edition - Final

Paying for Universal Health Coverage

SECTION: Section WK; Column 0; Editorial Desk; EDITORIAL; Pg. 7

LENGTH: 557 words


For Congress and the administration to keep the promise of comprehensive health
care reform, they will have to find the political will to pay for universal
coverage and other investments that are needed right away but will not produce
quick savings. The cost could reach $1.5 trillion over the next decade.

President Obama, who had already proposed some $634 billion in new taxes and
spending cuts, endorsed additional ideas last week. But Congressional Democrats
will almost certainly need to come up with a lot more money -- and that is
likely to mean new taxes.

There are at least two easy ways to duck the problem should Congress choose to
be imprudent. One way out would be to abandon the goal of universal coverage
until after costs have been controlled. That would be unfair to the 46 million
uninsured Americans, who often suffer health damage because they are reluctant
to seek treatment until their plight becomes desperate.

Another way out would be to finance universal coverage by adding to the deficit,
the path that George W. Bush took to pay for his tax cuts for the wealthy. With
deficits already at high levels, Mr. Obama has reasonably insisted that health
care reforms have to be ''deficit neutral'' over a 10-year period, meaning that
any upfront costs must be fully paid for through cost reductions or new revenues
by the end of a decade.

Mr. Obama's budget experts have proposed short-term savings of more than $300
billion in the Medicare and Medicaid programs. They would eliminate unjustified
subsidies given to private plans that participate in Medicare and reduce
payments to home health care providers, drug companies and many hospitals.

Last week, Mr. Obama said he would work with the Senate to find $200 billion to
$300 billion more in Medicare and Medicaid savings. He endorsed one way to
ensure that cuts would actually be made -- saying he was open to giving an
obscure panel, the Medicare Payment Advisory Commission, enormous power to set
Medicare payment rates. That would insulate Congress from lobbying by every
group whose income might be reduced.

Mr. Obama said he was receptive to proposals that would require most Americans
to take out health insurance and most employers, except for small businesses, to
share the cost. Both would pump money into the system and help defray the costs
of reform.

Virtually all experts agree that more revenues will be needed. Mr. Obama's
proposal to limit itemized deductions by wealthy Americans met with a cool
reception but ought to remain on the table. Significant money could be raised by
increasing taxes on sugared drinks, alcohol, tobacco and other products that are
bad for one's health. But more taxes will probably be needed.

Even the liberal-leaning Center on Budget and Policy Priorities suggested last
week that Congress is unlikely to be able to pay for universal coverage unless
it takes the unpopular step of limiting the tax exclusion for the value of the
health insurance provided by an employer. It is the nation's costliest tax
subsidy, and some experts believe it encourages overuse of medical services.

Congress has heavy lifting ahead. It must foster reforms that are apt to reduce
costs in the long-run (past the 10-year mark) and find a mix of short-term
savings and tax increases to put us on course without driving up the deficit.

URL: http://www.nytimes.com

LOAD-DATE: June 7, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


