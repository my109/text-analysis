                   Copyright 2009 The New York Times Company


                             600 of 2021 DOCUMENTS


                               The New York Times

                           September 15, 2009 Tuesday
                              Correction Appended
                              Late Edition - Final

Trying to Rekindle a Fire: For Obama, a Window Is Closing

BYLINE: By STEPHEN LABATON and JEFF ZELENY; Jack Healy contributed reporting.

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 1086 words


President Obama on Monday sternly admonished the financial industry and
lawmakers to accept his proposals to reshape financial regulation to protect the
nation from a repeat of the excesses that drove Lehman Brothers into bankruptcy
and wreaked havoc on the global economy last year.

But with the markets slowly healing, Mr. Obama's plan to revamp financial rules
faces a diminishing political imperative. Disenchantment by many Americans with
big government, along with growing obstacles from financial industry lobbyists
pressing Congress not to do anything drastic, have also helped to stall his
proposals.

Mr. Obama chastised Wall Street denizens in the audience at Federal Hall, at the
foot of Wall Street. ''Instead of learning the lessons of Lehman and the crisis
from which we are still recovering, they are choosing to ignore them,'' Mr.
Obama said. ''They do so not just at their own peril, but at our nation's.''

The speech came on the first  anniversary of the collapse of Lehman Brothers and
implicitly recognized that time was an enemy of reform. Throughout history, most
major laws to change the financial system arose from the cauldron of a crisis.
Senior officials have acknowledged that as the financial system begins to mend,
a kind of political inertia sets in as lawmakers have less of an incentive to
act boldly.

Hoping to kick-start the legislation, Mr. Obama said: ''I want them to hear my
words. We will not go back to the days of reckless behavior and unchecked excess
at the heart of this crisis, where too many were motivated only by the appetite
for quick kills and bloated bonuses.''

He also warned that ''those on Wall Street cannot resume taking risks without
regard for consequences, and expect that next time, American taxpayers will be
there to break their fall.''

Mr. Obama's speech in New York, like his address to Congress last week on health
care, is part of an attempt to put the plan back on political track. But with
the Senate preoccupied with the president's health care plan and strong
opposition to central provisions of the financial overhaul, the president faces
major hurdles on both the substance and the timetable.

Senior Congressional Democrats had originally planned to have the House complete
its work on the financial overhaul  before turning to the more recalcitrant
Senate. But the tighter time schedule has forced lawmakers to rethink that
approach.

Later this week, Barney Frank, Democrat of Massachusetts and the chairman of the
House Financial Services Committee, is expected to announce a series of hearings
for the coming days before his committee marks up legislation in October. Aides
say he is hoping to get legislation to the floor by the end of next month or
beginning of November.

There is less certainty in the Senate, where Christopher J. Dodd, Democrat of
Connecticut and the chairman of the Senate Banking Committee, has been working
to put together a package that could withstand the threat of a filibuster.

''Failure to act leaves our economy at risk,'' Mr. Dodd said in a statement
after the president's speech. ''We will not allow our efforts to be stalled by
well financed special interests intent on keeping the status quo.''

But Republican opponents of the administration's legislation said that it
suffered from the same problems as the White House's health care plan.

''The president has offered a reform proposal that would grant broad new
authorities to government bureaucrats while intruding in private markets and
restricting personal choice,'' said Spencer Bachus of Alabama, the senior
Republican on the House Financial Services Committee. ''The obvious lesson of
the events of September 2008 is that we need smarter regulation, not more
regulation, not more government bureaucracy, and not more incentives to engage
in harmful business practices.''

One major difficulty is that many of the issues do not break along party lines
-- they tend instead to force disagreements between industries and their various
supporters in Congress. So the fight is as much among Democrats as it is between
parties.

Big institutions and community banks have unified against a central provision of
the plan to create a new consumer finance protection agency. The new agency
would regulate mortgages, credit cards and other forms of consumer debt. The
companies, and their Republican and Democratic allies in Congress, fear that the
new agency would lead to unnecessarily burdensome oversight.

Some top regulators, including Sheila C. Bair, the head of the Federal Deposit
Insurance Corporation, support the creation of the new agency but have said they
would give it less authority than what the president is seeking.

Lawmakers, particularly in the Senate but also in the House, have been skeptical
of a second major plank that would give the Federal Reserve more explicit
authority to monitor the markets for systemwide problems.

Opponents prefer an enlarged role for a council of regulators. The Obama plan
creates such a council, but makes the Fed the first among equals and
acknowledges, as the Treasury secretary, Timothy F. Geithner, has said, that you
cannot put out a fire by committee. Mr. Geithner, who before joining the
administration was head of the Federal Reserve Bank of New York, has said the
administration's plan for the Fed is both incremental and essential.

He and Ben S. Bernanke, chairman of the Fed, have also said that no other agency
is better positioned to play the lead role in monitoring markets for potential
problems that could cascade through the financial system.

But there are lingering questions about the Fed's failure to adopt consumer
protection measures to curb abusive mortgages and other regulations that could
have reduced the risk-taking at large banks under its supervision. And top Fed
officials were slow to grasp the depths of the problems once the crisis began.

In recent weeks, Mr. Frank has been saying that the Fed might have to share the
role of systemic risk regulator, indicating that there was not enough support in
Congress to give the Fed alone the wider authority that the president had been
seeking. Other central elements of the plan have come under assault.

Major Wall Street companies, for example, have also worked to water down the
president's proposal on tightening the rules for derivatives like credit default
swaps. They have also lobbied heavily to make sure that any attempts to impose
tough new restrictions on executive pay do not come to pass.

URL: http://www.nytimes.com

LOAD-DATE: September 15, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: September 16, 2009



CORRECTION: A picture caption on Tuesday referring to an article in Business Day
about President Obama's address to Wall Street executives misstated the timing
of his lunch with former President Bill Clinton. The two men lunched together
after the speech, which was given shortly after noon, not before.

GRAPHIC: PHOTO: President Obama warned of excesses on Monday, speaking to an
audience of financial heavyweights at Federal Hall on Wall Street. (PHOTOGRAPH
BY RUTH FREMSON/THE NEW YORK TIMES) (pg.B1)
In a sea of onlookers, a job seeker's sign stood out Monday in a gathering
outside President Obama's financial speech in Manhattan. (PHOTOGRAPH BY LARRY
DOWNING/REUTERS)
 From left, Barney Frank, Michael R. Bloomberg, Paul Volcker, Christina Romer
and Timothy F. Geithner were among the officials listening to the president's
speech on Wall Street. (PHOTOGRAPH BY RUTH FREMSON/THE NEW YORK TIMES) (pg.B4)

PUBLICATION-TYPE: Newspaper


