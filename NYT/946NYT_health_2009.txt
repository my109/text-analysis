                   Copyright 2009 The New York Times Company


                             946 of 2021 DOCUMENTS


                               The New York Times

                           November 7, 2009 Saturday
                              Late Edition - Final

The World's Best Health Care System?

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 22

LENGTH: 945 words


To the Editor:

As Nicholas D. Kristof points out in ''Unhealthy America'' (column, Nov. 5), the
health statistics that measure average outcomes do not support the claim that
Americans have the ''greatest health care system in the world.''

These numbers come as no surprise in the face of tens of millions of uninsured
or underinsured Americans. Americans are tethered to a health care system that
they neither chose nor control. Where else in the world do some people have to
keep their jobs  to maintain their health coverage?

The majority of Americans who have coverage are not unaware of the many
deficiencies in our health care system; rather they live in fear that any change
in health care that includes those not currently in the system will
significantly diminish their own health care. Unfortunately, the Obama
administration and the Democrats in Congress have done very little to reassure
them that this is not the case.

Charles E. Inturrisi New York, Nov. 5, 2009

The writer is a professor of pharmacology at Weill Cornell Medical College.

To the Editor:

Nicholas D. Kristof promotes the misunderstanding that our dysfunctional health
care delivery system equates to wretched health care and underestimates the
devastation that obesity has had on our country's health.

Comparing infant mortality and life expectancy between the United States and
Canada or Europe does not account for inherent differences in health between
societies that are culturally and economically diverse and those that are more
culturally and economically homogenous.

I have practiced medicine in Europe, Central America and the Middle East. I have
met many wealthy foreigners seeking care at my institution here in the United
States. If our health care is so deplorable, then why do foreigners seek it? If
36 countries are more likely to deliver a healthy baby, then why are our
affluent not flocking elsewhere when the stork is arriving?

The focus of health care reform should be to reform ''the system'' and to
elevate the care of all citizens. Implying that there is widespread practice of
substandard health care is misleading.

Peter J. Allen Chappaqua, N.Y., Nov. 6, 2009

To the Editor:

Nicholas D. Kristof reminds us of public health statistics that have been widely
known to health professionals for years. We lag behind almost all other
industrialized and developed countries in most measures of the health of our
citizens.

Senator Richard Shelby, Republican of Alabama, and his allies would have us
believe we already have the best health system in the world. We do, provided you
belong to a favored group: You're rich and can afford to pay out of pocket,
you're over 65 like me and can't be turned away by Medicare or you have a
generous employer like Senator Shelby that subsidizes its employees so they can
have ''Cadillac'' plans.

Meanwhile more than 46 million of our fellow citizens are uninsured. The best
health care system the world has ever known?

We have forgotten our basic American value of looking out for one another. Would
we support the idea of a fire or police department that provided help only to
those who had a ''Cadillac'' protection plan?  I don't think so.

Only a health care system that provides each and every one of us with the care
we need will be good enough. We need a single-payer, Medicare-for-all system.

Donald Broder Studio City, Calif., Nov. 5, 2009

The writer is a retired medical doctor.

To the Editor:

''Mandates and Affordability'' (editorial, Nov. 1) demonstrates just how
difficult it will be to get universal coverage by mandating that citizens
purchase private health insurance. Congressional proposals leave insurance
companies free to set premiums at whatever level maximizes profits.

In effect, Congress would require every citizen to pay a tax (another name for a
''universal mandate'') set by a private corporation. Surely this could spark a
tax revolt. Moreover, the higher the premiums, the greater the subsidies
necessary, and the larger the hit to the Treasury.

There is an easier, more democratic way. Allow people to buy into Medicare, with
premiums determined by income. Such coverage would be affordable by definition.
If costs exceed premium income, use the money earmarked for subsidies. Those who
prefer (and can afford) comparable private insurance could buy it instead.

Medicare buy-in would bring money into Medicare, instead of draining money out,
and would guarantee good, affordable coverage for all.

Caroline Poplin Bethesda, Md., Nov. 2, 2009

The writer is an internist.

To the Editor:

The public option has been significantly weakened in both the Senate and the
House.

These steps have made elimination of the antitrust exemption for insurance
companies a much more attractive way to introduce real competition in the health
insurance industry. The repeal could give broad latitude to the Justice
Department and Federal Trade Commission to fashion remedies that would suit the
unique circumstance of each market.

For example, additional companies could be invited into markets lacking real
competition, and remedies could include giving the new entrants the same
preferred provider rates that the existing monopolies have, thus eliminating
barriers to entry. Or large regional or national companies could be broken up
into smaller units, enabling existing smaller companies to compete.

Unlike the public option, which is aimed only at the individual market, the
antitrust approach would have an industrywide impact, covering both group and
individual markets.

Carl Mankowitz New York, Oct. 29, 2009

The writer, a medical doctor, is a consultant to health care organizations.

URL: http://www.nytimes.com

LOAD-DATE: November 7, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY OLIVER MUNDAY)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


