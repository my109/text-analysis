                   Copyright 2009 The New York Times Company


                             822 of 2021 DOCUMENTS


                               The New York Times

                           October 15, 2009 Thursday
                              Late Edition - Final

G.O.P. Has a Lightning Rod, and Her Name Is Not Palin

BYLINE: By MONICA DAVEY; Carl Hulse contributed reporting from Washington.

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 1149 words

DATELINE: BUFFALO, Minn.


Representative Michele Bachmann will appear in the 2010 calendar of ''Great
American Conservative Women'' (she will be November). Her likeness has been
transformed into an action figure. And, so far in 2009, she has been interviewed
on a national cable news show every nine days, on average, an analysis by Smart
Politics, a nonpartisan blog affiliated with the University of Minnesota shows.

Not bad for someone less than three years on the job.

Here in Ms. Bachmann's district, and in much of the country, that outsized
celebrity has boiled down to this: They adore her or they loathe her.

As the health care overhaul moves closer to a full debate in Congress, Ms.
Bachmann is under attack from the Democratic National Committee for spreading
''reckless lies'' about the overhaul, one of a handful of Republicans singled
out as part of the committee's ''Call 'Em Out'' campaign. Back home, two
Democrats already say they are seeking Ms. Bachmann's seat in next year's
election, and are raising a lot of money to do so.

''People are struggling to stay in their homes, and she's off trying to be on
Fox News,'' said one of those Democrats, Tarryl L. Clark, a state senator.

Some of Ms. Bachmann's fellow Republicans, meanwhile, are drawing glowing
comparisons between her and Sarah Palin, the former Alaska governor and
Republican candidate for vice president. Sean Hannity, the conservative talk
show host, has introduced Ms. Bachmann as ''the second-most-hated Republican
woman in the country, second to Governor Palin, which is a good position.''

Ms. Bachmann's admirers point to her uncompromising, unvarnished stances against
big spending, big government programs, tax increases and abortions. Her
detractors moan that she opposes anything a Democrat says, and assert that she
has transformed herself into a cable television gadfly.

''This is the thing with her,'' said Brad Biers, a supporter of Ms. Bachmann who
is active in the state Republican Party and lives in her district, probably the
most conservative in the state. ''There's not a whole lot of middle ground. You
either like her or don't.''

What is beyond dispute is that Ms. Bachmann's remarks are seldom dull. She has
encouraged an ''orderly revolution'' against the Democratic establishment and
has suggested that she fears that the Obama administration is trying to do away
with the dollar.

Last week, she called on Minnesota's governor to appoint a special investigator
here to look into Acorn, the Democratic-leaning community group under fire
nationally. In June, she said she would not complete all of her United States
census forms next year because such forms were inappropriately intrusive.

In recent months, Ms. Bachmann has been particularly outspoken against
Democrats' proposals on health care. She has questioned whether Congress has the
constitutional authority to set such policy in the first place. She has
suggested that one proposal's privacy rules for school-based clinics could open
the way for young girls' getting referrals for abortions. And she has said that
illegal immigrants will get access to taxpayer-subsidized health care, one among
a list of comments by Ms. Bachmann that the Democratic National Committee has
attacked as false.

On Capitol Hill, Ms. Bachmann is viewed with disdain by Democrats who see her as
a wacky purveyor of outrageous claims and criticisms. Leading Republicans wince
occasionally at her  appearances on the floor and on television, but they also
see her as someone with telegenic appeal who can energize conservatives and
aggravate Democrats and they are not likely to rein her in.

In Minnesota, some believe the national attention around Ms. Bachmann may hint
at hopes for a higher office. When Gov. Tim Pawlenty announced he would not seek
re-election, some analysts suggested Ms. Bachmann might run, but questioned
whether her positions were too conservative to win statewide.

Asked whether she might consider an office beyond the House, Ms. Bachmann, who
has announced plans to seek re-election, did not address other possibilities.
''I am focused 100 percent on representing Minnesota's Sixth Congressional
District and ensuring their voices are heard in Washington,'' she wrote in an
e-mail response to questions.

Still, Ms. Bachmann, a lawyer, former state senator and mother of five who with
her husband, Marcus, a clinical therapist, has opened their home to 23 foster
children, has not so far objected to the speculation, particularly the
comparisons to Ms. Palin.

''Sarah Palin is a dedicated mother, committed public servant and strong
political figure who has fought hard to protect life, the family budget, and
freedom,'' Ms. Bachmann said, noting that Ms. Palin's book -- not yet out -- was
already selling in enormous numbers, while ''Nancy Pelosi's book sold a paltry
2,737 copies in its first week of sales'' in 2008.

From a distance, it may be hard to picture how Minnesota, the state with the
longest-running streak of picking Democrats for president (since 1976), could
also have elected Ms. Bachmann, who was found in a study by The National Journal
to be the 31st-most-conservative member of the House based on her votes in 2008.

But Ms. Bachmann's district, stretching from the northern suburbs of the Twin
Cities, through middle-income exurbs, farmland and into  St. Cloud, is defined
by social conservatism, an independent, populist streak, and a significant Roman
Catholic population. While the state voted for President Obama, Senator John
McCain, the Republican opponent, won in this district.

In serving the district, a spokeswoman for Ms. Bachmann said, the congresswoman
is most proud of ''providing the best constituent service'' and pushing to
obtain a veterans health clinic.

Still, in Ms. Bachmann's first bid for re-election in 2008, many speculated that
she was in serious trouble. Shortly before the election, she said on
''Hardball'' on MSNBC that she was ''very concerned'' that Mr. Obama ''may have
anti-American views.'' Her Democratic opponent, El Tinklenberg, lost by three
percentage points.

One of her Democratic opponents next year, Maureen Reed, said Ms. Bachmann
''injects fear and anger in people, and people don't solve problems well when
they're fearful and angry.'' Ms. Reed said her goal was to ''dial down the fear,
dial down the anger.''

Constance Carlson, a resident of Buffalo, seemed ready for such a change.

''I try to avoid listening to her,'' Ms. Carlson said of Ms. Bachmann. ''Some of
her comments are just distracting, conspiracy-type stuff.''

''I'd much rather hear her say, 'I don't agree and here's my solution,' '' she
added.

But others here say they wonder whether a Democrat really has a chance.

''She fits this district well,'' said King Banaian, a Bachmann supporter in St.
Cloud. ''I think there are a lot of people who want straight talk and she
appeals to them.''

URL: http://www.nytimes.com

LOAD-DATE: October 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: In 2006, newly elected Representative Michele Bachmann talked
with a reporter while an aide, Andy Parrish, held her purse.(PHOTOGRAPH BY
STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


