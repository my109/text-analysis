                   Copyright 2009 The New York Times Company


                             944 of 2021 DOCUMENTS


                               The New York Times

                           November 7, 2009 Saturday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 940 words


International

U.S. DELAYS REDUCE SUPPLY OF FOOD RATIONS IN SOMALIA

United Nations officials said rations to starving people in Somalia had been
interrupted, partly because the American government has delayed contributions,
fearing they would be diverted to terrorists. PAGE A4

DEAL WIDENS HONDURAN RIFT

The collapse of an accord that only a week ago was celebrated as ending the
four-month political standoff in Honduras has only intensified the disagreements
that have long characterized the crisis. PAGE A4

ZIMBABWE TO BE MONITORED

The international body charged with stopping the illicit trade in diamonds will
monitor, not suspend, Zimbabwe. PAGE A5

YEMENIS AND SAUDIS CLASH

Clashes continued between the Saudi military and Houthi rebels from Yemen on the
border between the two countries. Both sides claimed to have inflicted
casualties and captured enemy soldiers. PAGE A7

2 U.S. SOLDIERS MISSING

Two American soldiers are missing in western Afghanistan, a NATO spokesman said.
A Taliban spokesman said they found the bodies of two drowned soldiers. PAGE A7

IRAQIS RETURN TO STRUGGLES

Many Iraqis who fled sectarian violence return home to unemployment and poor
access to electricity and water, according to a report by a nongovernmental
group. PAGE A7

BRITISH LEADER WARNS KARZAI

Prime Minister Gordon Brown said President Hamid Karzai would lose Afghanistan's
right to international help against the Taliban if he did not root out
corruption. PAGE A8

Obituaries

PAUL C. ZAMECNIK, 96

A molecular biologist, he co-discovered transfer RNA, a molecule that is
essential in the making of protein. PAGE A24

National

ON HEALTH CARE, DEMOCRATS PLAY DOWN ELECTION RESULTS

Interviews on Capitol Hill suggested the elections for governor on Tuesday are
not a big influence even politically vulnerable Democrats as they decide how to
vote on health care legislation. Political Memo by Adam Nagourney. PAGE A11

MAN SHOOTS 6 IN ORLANDO

An engineer opened fire in his former office building in Orlando, killing one
person and injuring five others. He was arrested hours later at his mother's
home.  PAGE A9

CONFUSION OVER TAMIFLU

With guidelines always evolving, there is confusion among doctors about whether
to prescribe the drug Tamiflu for patients with less severe cases of the flu.
PAGE A9

FLU VACCINE TRUCK IS STOLEN

The swine flu vaccine was recovered, and the thieves may not have even known it
was in the truck. But now the doses are no longer considered usable.  PAGE A9

VACCINE ALLOTMENT OUTCRY

Responding to grumbling over some Wall Street banks receiving small shipments of
the swine flu vaccine, a federal health official said that distributing the
vaccine through many outlets was optimal. PAGE A10

New York

NATURAL BIRTHING CENTER CLOSES, DRAWING COMPLAINTS

The Bellevue Birth Center is closed. It was the only natural birthing center
dedicated to poor women on Medicaid. The closing has provoked complaints about
the lack of public notice and the lack of natural-birth options in the city.
PAGE A16

JOB SEEKERS SOCIALIZE

More and more of New York's job seekers are using social events to forge
connections that hold hopes of new careers in the world's emerging economies.
PAGE A17

A Lavish Columbia Dorm A15

Business

CITIGROUP OFFERS OPTIONS TO KEEP BANKERS ON BOARD

Citigroup is giving millions of stock options to more than a quarter of its
workers, hoping to persuade them to stay and help turn around the company. Many
workers' nest eggs vanished after the bank's share price plummeted. PAGE B1

HUNT FOR HOPE IN JOBS DATA

Despite the headline-grabbing unemployment rate (it surged to 10.2 percent in
October), economists sifting through the details of the Labor Department's
report found several reasons to take comfort. PAGE B1

PREP SCHOOLS FACE CUTS

Preparatory high schools, whose endowments shrank as financial markets withered,
may not be able to sustain the level of financial aid they give to incoming
students. PAGE B1

FANNIE MAE PLAN REJECTED

The Obama administration rejected a proposal by Goldman Sachs to buy as much as
$1 billion in Fannie Mae tax credits, saying the deal was a loser for taxpayers.
PAGE B2

A.I.G. SEES SUCCESSIVE GAINS

The American International Group reported its second quarterly profit in a row
as the value of its investments continued to rise. PAGE B3

A Genetic Test for Smokers B1

British Bankers Defend Pay B3

Sports

HUNDREDS OF THOUSANDS CELEBRATE YANKEES' TITLE

The Yankees' 27th World Series title was celebrated with a ticker-tape parade in
Lower Manhattan. The parade ended at City Hall Park, where Mayor Michael R.
Bloomberg gave the team keys to the city. PAGE B10

LOMBARDI ON BROADWAY

The producers of a new play about Vince Lombardi, the legendary coach of the
Green Bay Packers, plan to bring it to Broadway late next year.  PAGE B11

Colts Lose Two Veterans B11

Arts

A VIDEO GAME MAKES A BID TO BEAT THE BIGGER SCREENS

Films have always outclassed games in sophistication, characterization and
visuals. Not anymore. ''Uncharted 2: Among Thieves'' is a big step forward for
gaming. A review by Seth Schiesel. PAGE C1

WANDA, YOU HAVE A SHOW

With ''The Wanda Sykes Show,'' a weekly talk show, beginning Saturday, Ms. Sykes
recalled that her last venture with Fox did not work out exactly as hoped. PAGE
C1

A LONG-DELAYED OPENING

For years the manuscript for ''Free for All,'' a history about (and by) Joseph
Papp and the Public Theater he helped found, lay in the garage of its other
author, Kenneth Turan. PAGE C1

Op-ed

BOB HERBERT PAGE A19

GAIL COLLINS PAGE A19

CHARLES M. BLOW PAGE A19

URL: http://www.nytimes.com

LOAD-DATE: November 7, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


