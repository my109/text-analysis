                   Copyright 2009 The New York Times Company


                             1262 of 2021 DOCUMENTS


                               The New York Times

                            December 25, 2009 Friday
                              Correction Appended
                              Late Edition - Final

A Morning of Democratic Glee and Republican Glumness

BYLINE: By CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 856 words

DATELINE: WASHINGTON


The last time the Senate voted on Christmas Eve, in 1895, it represented a
moment of national reconciliation, as lawmakers agreed to lift a ban on federal
officers who had joined the Confederacy from serving in the post-Civil War
military.

''No Animosity Remaining,'' proclaimed a celebratory front-page headline in The
New York Times the next day.

The same could not be declared about Thursday's vote approving a bitterly
contested health care overhaul at the end of an exhausting 25-day legislative
journey. It was the second-longest consecutive stretch in Senate annals and one
that severely strained the traditions of collegiality that underpin the
institution.

Beneath the bright red jackets, blouses and holiday ties donned to signify the
exceedingly rare occasion of a sunrise session the day before Christmas, real
tension remained over the merits of the proposal passed along hardened party
lines as well as over how the debate unfolded, with its odd postmidnight and
early morning votes.

Democrats and their allies rejoiced at realizing an achievement that had eluded
them for decades, saying they were on the verge of enacting an expansion of
health care coverage that would provide security for millions of Americans. But
the excitement was tempered with the reality for some that they will now have to
persuade skeptical voters that the measure, which has been subjected to a
blizzard of partisan analysis, is in the public's best interests.

''I know there are a lot of people who have an opposing point of view,'' said
Senator Ben Nelson, the Nebraska Democrat who was the key 60th vote for the
bill. ''The use of information and misinformation has really confused the
public. Don't be surprised if you have people who are upset.''

In the minutes leading up to the 7:05 a.m. call of the Senate roll, Republicans
sat glum and scowling at their desks as Senator Harry Reid, the Nevada Democrat
who is the majority leader and architect of the measure, chastised them as
choosing ''to stand on the sidelines rather than participate in great and
greatly needed social change.'' It was not a happy holiday moment for them.

''It is sad,'' said Senator Susan Collins, a Maine Republican who has been
willing to cross party lines to work with Democrats but remains firmly opposed
to the bill. ''I feel that we missed an opportunity.''

Senior Republicans pledged to continue to resist the measure as it headed for
negotiations with the House.

''I guarantee you, the people who voted for this bill are going to get an earful
when they finally get home for the first time since Thanksgiving,'' said Senator
Mitch McConnell of Kentucky, the Republican leader. ''There is widespread
opposition to this monstrosity.''

For all the buildup, the final act was accomplished speedily as senators, ready
to race to the region's airports, assembled with Vice President Joseph R. Biden
Jr. presiding and zipped through the climactic vote in about 15 minutes. The
outcome drew applause as weary Congressional staff members and administration
negotiators looked on. A second vote to approve an increase in federal borrowing
power was quickly disposed of, and in just over 30 minutes, lawmakers were
heading for the doors with their travel bags.

At the White House, President Obama hung around long enough to congratulate his
party, then headed to Hawaii for the holidays with his family.

On Capitol Hill, there was little griping about working right up to the holiday.
''Those kids in Iraq are out there,'' said Senator Jeff Sessions, Republican of
Alabama. ''We have no basis to be whining around here about working.''

Among those in the gallery was Victoria Reggie Kennedy, widow of Senator Edward
M. Kennedy, who was credited by Democrats as inspiring them to push the measure
through after his death last summer.

Senator Paul Kirk, who filled Mr. Kennedy's seat after the Massachusetts
legislature took extraordinary steps to provide a dependable vote for health
care, said it was an emotional moment that left him elated.

''He is having a Merry Christmas up there in heaven, proud of the leadership and
all the work that went into it,'' Mr. Kirk said.

The scores of police officers and Senate staff members required for a daily
session arrived at the Capitol in the dark to prepare for the final vote. Some
opponents of the bill did as well.

''I am here to watch my country get signed away,'' said Sherri Cooper, who
traveled from Georgia to witness the vote, as she searched for an open entrance
to the Capitol.

The 25-day Senate run fell one day short of the record set in 1917, when antiwar
lawmakers were stalling a measure allowing the arming of merchant ships during
World War I.

President Woodrow Wilson, greatly frustrated at the power of a ''little group of
willful men,'' pushed the Senate to create a process to cut off debate, and the
Senate adopted its rule allowing senators to impose cloture and force final
votes.

That power allowed present-day Democrats to use their 60-vote bloc to overcome
Republican objections through a series of cloture votes and push their health
care plan into talks with the House.

URL: http://www.nytimes.com

LOAD-DATE: December 25, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: December 30, 2009



CORRECTION: An article on Friday about the scene in the Senate when it approved
health care legislation on Christmas Eve morning referred incorrectly to a
headline in The New York Times the last time the Senate voted on Christmas Eve
-- in 1895. The headline, ''No Animosity Remaining,'' was on page five of the
Dec. 25, 1895, paper; it was not a ''celebratory front-page headline.''

GRAPHIC: PHOTO: Senators Bob Casey, above foreground, and Ben Nelson, both
Democrats, voted to overhaul health care. Senator Susan Collins, bottom left,
Republican of Maine, voted against the bill.(PHOTOGRAPHS BY STEPHEN CROWLEY/THE
NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


