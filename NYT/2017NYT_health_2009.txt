                   Copyright 2010 The New York Times Company


                             2017 of 2021 DOCUMENTS


                               The New York Times

                              May 31, 2010 Monday
                              Late Edition - Final

Democrats Turn Mantra Of G.O.P. on Its Head

BYLINE: By JOHN HARWOOD

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 867 words


With the American Jobs and Closing Tax Loopholes Act, House Democratic leaders
opened themselves up to both ends of Republicans' familiar ''tax and spend''
barbs.

The bill, passed last week, would raise taxes on Wall Street executives and
multinational corporations. And it would spend more than $100 billion in the
name of creating jobs and extending federal benefits for the jobless.

To overcome opposition, Democrats were forced to make concessions by
substantially trimming spending -- not the higher taxes.

That points to a new reality as Republicans seek inroads against President Obama
and his party in the midterm elections. If they changed the order of their
slogan to reflect its current political throw-weight, Republicans would attack
Democrats for ''spend and tax,'' not the other way around.

''The spending issue has supplanted taxes in the G.O.P. mantra,'' said Neil
Newhouse, a Republican pollster. Democrats acknowledge their vulnerability.

''As a metaphor, it is powerful,'' said Mark Mellman, a strategist for
Democratic leaders. Amid near double-digit unemployment and trillion-dollar
deficits, Mr. Mellman said, Americans link spending with ''long-term economic
decline, lack of discipline, wasted money, money spent bailing out Wall Street
instead of helping Main Street.''

The primacy of spending as a hot-button issue may fade if economic recovery
continues and if Washington moves aggressively to shrink budget deficits. How
much that will help Mr. Obama and his party is another question.

Cuts and Increases

In 1980, Ronald Reagan placed tax cuts at the center of his campaign to oust
President Jimmy Carter and make conservatism the ascendant political force.

In 1992, Ross Perot's denunciation of deficits helped Bill Clinton wrest the
White House from Republican control.

By 2000, economic boom times had eliminated the deficit issue and limited the
drawing power of tax cuts. For his presidential victories, George W. Bush relied
more on appeals to values and national security.

That spending has taken center stage now is no mystery. First, Mr. Bush guided
the budget from surplus to deficit with two tax cuts, new prescription drug
benefits under Medicare and increased security spending after Sept. 11,
including the wars in Iraq and Afghanistan.

Then, Washington responded to economic crisis by writing two gargantuan checks.
In the fall of 2008, Mr. Bush signed a $700 billion bailout for the financial
system. In early 2009, Mr. Obama signed a $787 billion stimulus package in hopes
of kick-starting the economy out of recession.

This year, Mr. Obama added a third mammoth expenditure: a health care overhaul
priced at $938 billion over 10 years.

Never mind that the Congressional Budget Office says tax increases, spending
cuts and cost savings associated with the health bill will actually reduce the
deficit.

Nor does it matter, politically, that the Wall Street bailout will end up
costing taxpayers a fraction of that $700 billion. Or that Democrats have
revived ''pay-go'' procedures earlier abandoned by Republicans, under which
Congress must offset most new entitlement spending or tax cuts with
corresponding spending cuts or tax increases.

Mr. Obama's party is in charge. And the juxtaposition of his activism with the
specter of overreach, as dramatized lately by the debt crisis in Greece, has
turned spending priorities Americans once valued into the political equivalent
of toxic assets.

Arguing Austerity

For many voters, noted Stan Greenberg, a Democratic strategist, ''spending is
debt -- seen as threatening to the long-term economic future.''

An argument now under way inside Mr. Obama's economic team will settle how, and
how quickly, Democrats answer that vulnerability. Lawrence H. Summers, director
of the National Economic Council, insisted last week that the recovery remained
too fragile for an immediate turn toward austerity.

''I cannot agree with those who suggest that it somehow threatens the future to
provide truly temporary, high-bang-for-the-buck jobs and growth measures,'' Mr.
Summers said in a speech. ''Rather, assuring as rapid a recovery as possible
strengthens our future economy, our future prosperity, with many benefits,
including a greater ability to manage our debts.''

Peter R. Orszag, the White House budget director, favors a more aggressive
approach to deficit reduction once a presidential commission reports in
December. That could take some of the sting out of Republican attacks on
spending.

Of course, that could also leave Democrats exposed once again on taxes, an issue
that has been defused lately by Mr. Obama's pledge to focus on only high-income
Americans. Some Democratic economists insist taming long-term deficits will
require setting that promise aside.

For now, Democrats must cope with a shift in attitudes they could not have
imagined a few years ago, when they fretted over winning back so-called values
voters.

While struggling to pass new spending for the unemployed, House Democrats last
week voted to end the Pentagon's ''don't ask, don't tell'' policy and clear the
way for gay men and lesbians to serve openly in the military. That objective
proved easier to achieve.

URL: http://www.nytimes.com

LOAD-DATE: June 3, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama this month with his economic team: Treasury
Secretary Timothy R. Geithner
Peter R. Orszag, White House budget director
Lawrence H. Summers, National Economic Council director
 and Christina D. Romer, Council of Economic Advisers chairwoman.  (PHOTOGRAPH
BY SUSAN WALSH/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


