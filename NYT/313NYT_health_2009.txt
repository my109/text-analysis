                   Copyright 2009 The New York Times Company


                             313 of 2021 DOCUMENTS


                               The New York Times

                             August 14, 2009 Friday
                              Late Edition - Final

Ad Campaign Counterattacks Against Overhaul's Critics

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 566 words


The battle for public opinion over health care intensified further on Thursday
when a strange-bedfellows coalition including drug companies, doctors and a big
labor union, all favoring an overhaul, opened a $12 million television
advertising campaign directed at 12 states.

The advertisements are intended to counter the sharp criticism of President
Obama's overhaul efforts that has emerged at town-hall-style meetings. Change,
they say, would have a positive effect.

Over soothing piano music, the narrator in one commercial tells viewers that
change would mean that they could not be denied coverage for pre-existing
conditions or be dropped from their plans if they get sick, and that they could
still make their own medical decisions, in consultation with their doctors.

''So what does health insurance reform really mean?'' the narrator says.
''Quality, affordable care you can count on.''

The commercials are running in Alaska, Arkansas, Colorado, Indiana, Louisiana,
Maine, Montana, Nebraska, Nevada, North Dakota, South Dakota and Virginia. They
are aimed at bolstering moderate and conservative House Democrats who need some
ammunition to counter the intense campaign against revamping the system. Another
objective is to win over senators from swing states or whose votes are in play.

The new commercials followed by a day the opening of a 20-state,
multimillion-dollar network television campaign by the United States Chamber of
Commerce to try to derail support for a government-run health insurance option.
Such an option, which is a provision of overhaul bills approved by three House
committees and the Senate health committee, could make consumers' premiums
cheaper but would most likely take business away from private insurance
companies, which the chamber represents.

The chamber's commercial portrays Washington's efforts at overhaul as so
expensive that they would all but break the national bank, adding to the deficit
and prompting a tax increase.

The new coalition is called Americans for Stable Quality Care. Its biggest
financial backer is the Pharmaceutical Research and Manufacturers of America, or
PhRMA, which represents the pharmaceutical industry and has promised to spend up
to $150 million in advertising on behalf of Mr. Obama's effort to change the
system.

The coalition also includes the American Medical Association, Families USA, the
Federation of American Hospitals and the Service Employees International Union,
which represents health care workers.

Most of these groups, especially the drug companies, worked against President
Bill Clinton's proposed overhaul in 1993-94. They are generally allied now in
advocating change, although they differ in some of the particulars, including
the contentious matter of a public option, which organized labor supports but
which the hospitals, doctors and drug companies oppose in varying degrees. The
public option is not mentioned in the coalition's commercials.

The groups in the coalition were brought together by the White House in its
continuing effort to broker deals bringing them on board with the president,
although he has yet to back a specific plan.

Some of those deals, especially with the drug companies, have angered the
political left, where critics say that for the sake of harmony, the
administration has compromised too much and reneged on a campaign promise to
rein in drug prices.

URL: http://www.nytimes.com

LOAD-DATE: August 14, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: One of the pro-overhaul commercials being run by a new coalition
including drug companies, doctors and a big labor union.

PUBLICATION-TYPE: Newspaper


