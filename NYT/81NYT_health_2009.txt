                   Copyright 2009 The New York Times Company


                              81 of 2021 DOCUMENTS


                               The New York Times

                              June 26, 2009 Friday
                              Late Edition - Final

California Says It Cannot Afford Overhaul of Inmate Health Care

BYLINE: By THE ASSOCIATED PRESS

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 316 words

DATELINE: SACRAMENTO


The Schwarzenegger administration has rejected a plan intended to end years of
litigation over inmate medical care in the California prison system.

In a letter The Associated Press obtained on Thursday, the state's corrections
secretary, Matthew Cate, tells a court-appointed receiver that the state cannot
afford the $1.9 billion overhaul Mr. Cate agreed to last month.

Mr. Cate and the receiver appointed in federal court, J. Clark Kelso, had agreed
to the outline of a deal intended to overhaul inmate medical care. The federal
courts, which have ruled that the care in California prisons is so poor that it
violates inmates' civil rights, have threatened to take money directly from the
state treasury to fix the system.

But Gov. Arnold Schwarzenegger said in a statement Thursday that California
could not afford the additional cost.

''We cannot agree to spend $2 billion on state-of-the-art medical facilities for
prisoners while we are cutting billions of dollars from schools and health care
programs for children and seniors,'' Mr. Schwarzenegger said.

The governor and lawmakers are considering eliminating or significantly reducing
education, state parks and core social programs to address the $24.3 billion
budget shortfall.

A spokesman for the prison receiver's office, Luis Patino, said the office would
have no direct comment on the rejection of the tentative agreement.

The tentative plan significantly scaled back Mr. Kelso's original plan to revamp
California's prison medical system.

Mr. Cate said Thursday that California remained committed to improving prison
health care, but hoped to do it through legislation signed by the governor in
2007 that provides about $8 billion for prison construction, including $1
billion dedicated to health care improvements.

The tentative plan called for building two prison hospitals to house 3,400
inmates at a cost of $1.9 billion.

URL: http://www.nytimes.com

LOAD-DATE: June 26, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


