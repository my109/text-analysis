                   Copyright 2010 The New York Times Company


                             1932 of 2021 DOCUMENTS


                               The New York Times

                             April 25, 2010 Sunday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1190 words


International

A COUP CHANGES NIGER, BUT NOT CHINA'S ROLE THERE

A few months ago, China was derided in Niger as the financial backbone propping
up an autocratic president. Now, even though that president was overthrown by a
military junta, China's business in Niger seems mostly unchanged. PAGE 6

THE CASE OF THE SORCERER

For more than two years, a Lebanese man has sat in a Saudi Arabian prison,
convicted of sorcery. His head is to be chopped off. The case has brought
international attention to the ultra-religious side of the kingdom, just as it
works to modernize its reputation. PAGE 8

CHINA REPLACES OFFICIAL

Chinese leaders replaced Wang Lequan, the ruling official in the vast western
region of Xinjiang, whose capital was engulfed last summer by the deadliest
ethnic violence in China in decades. PAGE 10

VATICAN SEEKS TRANSPARENCY

The Vatican has begun to remove bishops who have been accused of covering up
accusations of sexual abuse or have committed abuses themselves. A Vatican
spokesman said the church must show that it has ''nothing to hide.'' PAGE 14

British Politician Tries to Rebound 8

Obama Marks Genocide 14

National

CON ARTISTS TAKE ADVANTAGE OF CONFUSION OVER REFORM

Con artists in several states are seizing on the public's financial struggles
and confusion about the recent health care overhaul, the authorities say. State
insurance departments and the secretary of health and human services issued
warnings about such scams. PAGE 16

PRESSING ON FOR SENATE SEAT

Despite the closing of his family's bank, the Democratic state treasurer of
Illinois, Alexi Giannoulias, said he would press on in his tight campaign for
the Senate seat. PAGE 21

TORNADOES IN SOUTHEAST

Tornadoes ripped through the Southeast, killing seven people in Mississippi and
injuring more than a dozen others. Gov. Haley Barbour of Mississippi said there
was ''utter obliteration'' in parts of Yazoo County.  PAGE 26

Metropolitan

SPREADING HIS GOSPEL

Of Warm and Fuzzy

Nice is Lesson 1 at Hospitality Quotient, the consulting business Danny Meyer,
one of the nation's most successful restaurateurs, and his partners started last
month to spread their management style beyond New York and beyond the restaurant
world.  PAGE 1

Sports

YANKEES FOLLOW TRADITION,

With a White House Visit

When the World Series champion Yankees visit the White House on Monday to meet
President Obama, they will continue a tradition, stretching back a century and a
half, of baseball players mixing with commanders in chief.  PAGE 1

LESSONS FOR N.F.L. ROOKIES

Jets players Mark Sanchez, Alan Faneca and Shaun Ellis share their experiences
and offer lessons to incoming first-year players on how to survive a season as
professional football players. William C. Rhoden, Sports of the Times.  PAGE 1

FROM SHORTSTOP TO PITCHER

After eight seasons in the minor leagues and after five organizations had given
up on him, Sergio Santos, 26, is in the major leagues, playing for the Chicago
White Sox in a role that he never, until recently, imagined: as a pitcher.  PAGE
2

Obituaries

ROBERT HICKS, 81

He was one of the last surviving leaders of the Deacons for Defense and Justice,
a paramilitary group that sought to protect black communities in the Jim Crow
South from the K.K.K. But his role went beyond armed defense, struggling to
secure civil rights in Bogalusa, La.  PAGE 30

Sunday Business

IN LUXURY WATCHES, IT IS WHAT'S INSIDE THAT COUNTS

New technology is allowing manufacturers to make luxury watches that are smaller
and lighter, with ever more ingenious mechanical features. Novelties by Anne
Eisenberg. PAGE 5

LOOKING FOR BIG REFORMS

Financial reform is gaining traction in Congress, but the leading proposals
would create a two-tier system with big advantages for the largest banks. Fair
Game by Gretchen Morgenson. PAGE 1

THE FLEET-FOOTED START-UP

Eric Ries and Steven Blank are leading proponents of the ''lean start-up,'' a
fresh approach to creating companies that takes less time and money to try new
ideas and find paying customers. Unboxed by Steve Lohr. PAGE 5

Arts & Leisure

AT CITY BALLET, CREATING MANY FIRSTS

For the past six months, the New York City Ballet has been preparing for the
opening of what promises to be a landmark festival, Architecture of Dance, which
features four commissioned scores and seven premieres by some of the biggest
names in ballet.  PAGE 1

Magazine

THE MAN WASHINGTON WAKES UP TO

Mike Allen's e-mail tipsheet, Playbook, has become the principal early-morning
document for an elite set of political and media strivers. Mr. Allen, a reporter
at Politico, has become the insiders' insider. PAGE 33

THE NATIONAL AGENDA

How did the Brooklyn band the National, with one set of twins, another set of
brothers and a strong-willed lead singer, find its way to a big, new sound?
PAGE 24

Book Review

THE FOUNDING FATHER OF AUTHORITY JOURNALISM

In ''The Publisher: Henry Luce and His American Century,'' Alan Brinkley
restores missing dimensions to Luce, the creator of Time, Life, Fortune and
other magazines. PAGE 1

Sunday Styles

A LITTLE TOO READY FOR HER CLOSE-UP

Filmmakers and casting executives are rethinking Hollywood's attitude toward
plastic surgery. Ten years ago, actresses had surgery to help their careers. Now
it can hurt them. PAGE 1

A YOGA MANIFESTO

There's a brewing resistance to the expense, the cult of personality and the
membership fees in yoga. At the forefront of the movement is Yoga to the People.
PAGE 1

Automobiles

A EURO-STYLE DAVID

Vs. Minivan Goliaths

The Mazda 5 -- the only pure multipurpose vehicle (with sliding doors) on the
market -- is still a minivan, writ small but no less boxy. Behind the Wheel by
Lawrence Ulrich. SPORTSSUNDAY, PAGE 11

Week in Review

A VOLCANO'S ASH ACCENTUATES EUROPE'S STUBBORN BORDERS

Separate responses by European Union members to the eruption of an Icelandic
volcano, which shut down air travel over the continent, point to just how
culturally diverse the continent remains, and how fractious the union is. PAGE 1

Taking DNA and Consent  1

Give Us Your Posh 4

Editorial

FALTERING CANCER TRIALS

The government's system for judging the clinical effectiveness of cancer
treatments, now in ''a state of crisis,'' must be repaired.  Week in Review,
PAGE 11

SHOULD THEY BELIEVE US?

The Group of 8 promised far more aid to developing countries than they are
giving. They need to fill the gap.  Week in Review, PAGE 11

Op-Ed

FRANK RICH

Wall Street's gambling culture resists change, regardless of the legislative
fate of financial regulatory reform.  Week in Review, PAGE 12

NICHOLAS D. KRISTOF

Valentino Achak Deng's new school in southern Sudan provides a glimmer of hope
in a war-torn, poverty-stricken region.  Week in Review, PAGE 13

THE PUBLIC EDITOR

The Times is attracting an increasing number of critics because of its
continuing coverage of the sexual abuse scandal in the Catholic Church.  Week in
Review, PAGE 12

THOMAS L. FRIEDMAN

One way to put all the patriotic energy from the Tea Party movement to good use
would be to go green.   Week in Review, PAGE 12

URL: http://www.nytimes.com

LOAD-DATE: April 25, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


