                   Copyright 2009 The New York Times Company


                             139 of 2021 DOCUMENTS


                               The New York Times

                              July 12, 2009 Sunday
                              Late Edition - Final

Cut Medicare With a Scalpel

BYLINE: By PAUL B. GINSBURG

SECTION: Section WK; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 10

LENGTH: 421 words


THE deal struck by the Obama administration and the hospital industry to reduce
the growth of annual Medicare payment increases by about $100 billion over 10
years deserves scrutiny.  In the weeks before the deal, the hospital industry
predictably  screamed foul at proposals to rein in the growth of those
payments, pointing out that 58 percent of hospitals lost money treating Medicare
patients in 2007.

But what about the 42 percent of hospitals that didn't lose money? Is the
problem that Medicare is a stingy payer? Or that too many hospitals have grown
cavalier about controlling costs and have the market power to demand higher
payment rates from private insurers to stay in the black regardless of Medicare
rates?

A  report in March by the Medicare Payment Advisory Commission makes a good case
for the latter, suggesting there is room to trim the fat from Medicare payments
without hitting bone in many hospitals.

According to the commission, high payments by private insurers -- which exceeded
hospitals' actual costs by 32 percent in 2007 -- resulted in overall hospital
profit/surplus margins of 6 percent in 2007,   the highest since 1997. The
commission points to hospital consolidation and consumer and employer pressure
on insurers to offer a broad choice of hospitals as tilting the balance of power
toward hospitals in price negotiations.

But not all hospitals can increase fees  for  private insurers, especially not
hospitals that treat a relatively large number of Medicaid patients. Not
surprisingly, many of these hospitals did a much better job of holding their
costs down.

The commission found that nonprofit hospitals facing the least financial
pressure,  those with high non-Medicare profits,   spent more per unit of
service. Nonprofit hospitals facing the most financial pressure,  those with
little income outside Medicare,  controlled their costs better.

Worried  that financial pressures might lead to lower quality of care, the
Medicare commission  also examined how hospitals that did a good job of
controlling costs compared on  measures like mortality rates and readmissions.
These hospitals typically had higher quality and lower costs,  and will likely
be the ones hit hardest by across-the board reductions.

The recent deal between Mr. Obama and the hospital industry can hardly be called
''health reform.'' What is clear, however, is that hospital market power, left
unchecked, will pose a formidable obstacle to the president's promise to make
health care more affordable.

URL: http://www.nytimes.com

LOAD-DATE: July 12, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


