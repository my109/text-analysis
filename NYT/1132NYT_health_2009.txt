                   Copyright 2009 The New York Times Company


                             1132 of 2021 DOCUMENTS


                               The New York Times

                            December 6, 2009 Sunday
                              Late Edition - Final

To a Divisive Debate, Now Add Religion

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; PRESCRIPTIONS MAKING SENSE OF THE
HEALTH CARE DEBATE; Pg. 35

LENGTH: 982 words


Should health insurance companies cover prayer as a legitimate medical expense?

The Christian Science church is seeking to insert a measure in the Senate's
health care legislation that would encourage private insurance companies to
cover prayer and spiritual treatment of the sick, even though both the House and
Senate turned down earlier versions.

The push is just one facet of a broader health care debate over whether the
government should encourage -- or use taxpayer money to support -- alternative
therapies and treatments that are not considered ''evidence based.''

Such treatments are no longer on the sidelines. In 2007, more than one in three
adults used some form of so-called complementary and alternative medicine,
according to a federal study, spending $34 billion in out-of-pocket expenses on
techniques like yoga and meditation and on the purchase of herbs, vitamins and
supplements. Some of those treatments and supplies are now covered by some
insurance policies.

The Opponents

Although the Christian Science measure calls for reimbursement for some basic
forms of care for the sick, like helping them eat and bathe, the prayer
component has drawn fierce opposition from various civil liberties groups and
child-protection advocates. They say it would violate the separation of church
and state and could endanger the lives of minors whose parents turn to prayer
rather than medicine when their children are ill.

In the past, some members of the church, formally known as the Church of Christ,
Scientist, have been prosecuted for  allowing children to die instead of
treating them medically. But such cases are rare. And while healing through
prayer remains their central belief, church officials say they do not forbid
conventional medical treatment.

The church has had powerful allies. Earlier this year, its reimbursement measure
passed two committees in the House and one in the Senate.

The Senate health committee acted at the behest of Senator Edward M. Kennedy,
Democrat of Massachusetts, where the church has its headquarters, and Senator
Orrin G. Hatch, a Utah Republican who is a Mormon and has said the measure would
ensure that the health legislation ''does not discriminate against any
religion.''

In the end, critics said it was unconstitutional, and the measure was dropped
from the final House bill and from the Senate version being debated now.

But Russ Gerber, a spokesman for the church, said church officials were still
trying to get it into the Senate bill. He said they were drafting new language
to clarify that no federal money would be involved. ''We're looking for ways to
introduce this as one element of a very large and inclusive health care
initiative,'' Mr. Gerber said.

It is not clear if the church will find a sponsor. Aides to Mr. Hatch said he
did not plan to offer such an amendment. Still, Mr. Gerber said the church was
not discouraged.

''Nothing has come along to say to us, 'Pack up the tent and go home,' '' he
said.

As Now Worded

The church's provision says that any insurer offering health insurance on the
exchanges that would be created in the health legislation could not discriminate
against a health service ''on the basis of its religious or spiritual content.''
Such content would be defined as medical expenses for which the Internal Revenue
Service now allows deductions. The I.R.S. specifically names Christian Science
practitioners, who pray for people at a cost of about $20 a day or care for them
in nonmedical ways, as deductible expenses.

Although the reimbursement measure is narrowly drawn to apply to Christian
Science practitioners, it does not mention the church by name. Sharon Frey,
another press officer for the church, said the measure would probably also cover
spiritual care provided by Native Americans and holistic healing centers.

The new language would specify that only private premiums could pay for a
spiritual care benefit and that no federal money would be used to reimburse
practitioners.

But the changes do not appear to satisfy critics.

''I don't think this gets them out of the Constitutional issue,'' said Barry
Lynn, executive director of Americans United for Separation of Church and State.

''Their claim of nondiscrimination is really a claim for special privilege,'' he
said. ''They want to be more equal when it comes to reimbursement.''

Mr. Lynn added: ''This is federal pressure to effectively force insurance
companies to cover religious healing.''

Rita Swan, a former Christian Scientist who lives in Iowa and whose 15-month-old
son died in 1977 after she and her husband delayed seeking medical treatment for
him, is among those working to keep the measure out of the health bill. She now
leads an organization called Children's Healthcare Is a Legal Duty.

''We don't see that the government has any appropriate function in recommending,
encouraging or requiring insurance companies to pay for prayer,'' she said.

Even if the church's measure fails, the question of reimbursement for
alternative therapies like acupuncture, stress reduction and herbal healing
remains. The Senate bill being debated now includes a somewhat similar
provision, introduced by Senator Tom Harkin, Democrat of Iowa, that says
insurers cannot discriminate against alternative therapies offered by
practitioners who are licensed by their states. This would probably not cover
Christian Science practitioners because in most states they are not licensed.

But practitioners of alternative treatments as well as Christian Science are
seeking more than just insurance reimbursements. They want the legitimacy that
could come by being included in landmark health care legislation.

''We want to make sure,'' Mr. Gerber said, ''that anything as far-reaching as
this health reform initiative isn't leaving off the table something that may
then be left off the table for decades.''

URL: http://www.nytimes.com

LOAD-DATE: December 6, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: A provision proposed by the Christian Science church was backed
by Senators Edward M. Kennedy, top, and Orrin G. Hatch.

PUBLICATION-TYPE: Newspaper


