                   Copyright 2009 The New York Times Company


                              83 of 2021 DOCUMENTS


                               The New York Times

                              June 26, 2009 Friday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1026 words


International

ISRAEL CEDES SOME CONTROL

In 4 West Bank Cities

Israel has agreed to give the Palestinian security forces more freedom of action
in four West Bank cities, Israeli and Palestinian security officials said
Thursday, a move that implies a reduction in Israeli military activity in those
areas as the Western-backed Palestinian forces assert more control.  PAGE A4

MERKEL TO VISIT U.S.

As Chancellor Angela Merkel prepared to depart for her meeting in Washington
with President Obama on Friday, the deaths of three German soldiers in
Afghanistan thrust Berlin's role in the escalating conflict back to the
forefront of political discourse. PAGE A4

INDIA'S ID CARD PLAN

One of India's most successful technology entrepreneurs was tapped by the
government on Thursday to lead an ambitious project to give every citizen an
identification card within three years.  PAGE A6

WAVE OF BOMBINGS IN IRAQ

At least seven bombs exploded around Iraq amid an uptick in violence as American
troops prepared to withdraw from Iraqi cities by Tuesday. PAGE A8

National

6 CONSIDERED AS THREATS

Retained Aviation Licenses

At least six men suspected or convicted of crimes that threaten national
security retained their federal aviation licenses, despite antiterrorism laws
written after the attacks of Sept. 11, 2001, that required license revocation.
PAGE A10

MEETING ON IMMIGRATION

President Obama told a bipartisan group of lawmakers that Congress should begin
debating a comprehensive immigration plan by year's end or early next year, but
Republicans said they would support a measure only if it included an expansion
of guest worker programs.  PAGE A12

PROGRESS ON HEALTH CARE

Senate Democrats said that they had found ways to pare the cost of a health care
bill by more than a third -- to $1 trillion over 10 years -- while still
covering nearly all Americans. PAGE A12

New York

STATE ALLOWS COMPENSATION

For Stem Cell Research

Stem cell researchers in New York can now use public money to pay women who give
their eggs for research, a decision that has opened new possibilities for
science but raised concerns among some bioethicists and opponents of such
research. PAGE A18

LAWMAKERS NEAR DEAL

After more than two weeks of stalemate, Republicans and Democrats in the State
Senate appeared close to a power-sharing deal that would let normal legislative
business resume. PAGE A18

CANDIDATE DEFENDS RECORD

Christopher J. Christie, New Jersey's former United States attorney and now its
Republican candidate for governor, aggressively defended his decision to award
his political allies lucrative contracts to monitor corrupt corporations,
telling a Congressional panel that his actions had saved taxpayers money. PAGE
A18

Business

FED CHAIRMAN DEFENDS ROLE

In Merrill Lynch Sale

Ben S. Bernanke, the chairman of the Federal Reserve, staunchly denied that he
or anyone else at the Fed did anything improper in prodding Bank of America to
complete its takeover of Merrill Lynch last year. PAGE B1

THE BAR CODE TURNS 35

When the bar code was unveiled on June 26, 1974, it was meant to speed up the
grocery checkout line. It has since become both the mundane minutiae of modern
life and a cultural icon of cold efficiency and control. PAGE B1

TUG OF WAR ON DERIVATIVES

As a new regulatory system for derivatives is shaped, the banks will try to
preserve as much of their complexity and secrecy as they can. To the extent they
succeed, it will be the customers, and the financial system, that are at risk.
Floyd Norris, High & Low Finance.  PAGE B1

DEAL TO REDUCE A.I.G. DEBT

The American International Group, the insurance giant bailed out by the federal
government, said that it had reached an agreement to reduce its debt with the
Federal Reserve Bank of New York by $25 billion. PAGE B2

Sports

IN HOME-SCHOOL LEAGUE,

Faith and Football

The ranks of the home-schooled grew to 1.5 million nationally through 2007, and
sports participation has bloomed accordingly. The Glory for Christ football
league of Georgia, comprising home-schoolers and students from private Christian
academies, is something of a Petri dish.  PAGE B10

O'NEAL HEADS TO THE CAVS

For the third time in four years, Shaquille O'Neal is on the move -- this time
to Cleveland. He will rumble across the N.B.A. landscape, forcing coaching
adjustments, reassessments and revisions from coast to coast.  PAGE B10

A LONG SHOT ADVANCES

Melanie Oudin, a 17-year-old American playing in her first Wimbledon, defeated
Yaroslava Shvedova, a big hitter from Kazakhstan ranked 50 spots ahead of her,
to join Venus Williams and Serena Williams, the only other American women in the
third round.  PAGE B11

Weekend

A SEX SYMBOL

Who Aimed Higher

Farrah Fawcett's career traced a familiar Hollywood arc: meteoric fame followed
by years spent trying to overcome it, then, too late, seeking to recapture it.
Alessandra Stanley, Appraisal.  PAGE C1

A COMEDY'S UNLIKELY CHARM

''Hung'' on HBO sounds like a one-joke conceit, but Alessandra Stanley writes
that the show, about a well-endowed male prostitute, is an oddly beguiling
screwball comedy in R-rated clothing.  PAGE C1

BETWEEN PERIL AND PROTOCOL

A. O. Scott writes that ''Hurt Locker,'' a film about an Army unit whose job is
to detect and defuse I.E.D.'s in Iraq, is an adrenaline-soaked tour de force of
suspense and surprise.  PAGE C1

A FRESH TAKE ON A CLASSIC

Daniel Sullivan's staging of Shakespeare's ''Twelfth Night'' at the Delacorte
Theater is the most consistently pleasurable rendition of the play in at least a
decade, Charles Isherwood writes. PAGE C1

Escapes

WHERE PUBLIC ENEMIES WENT FOR PEACE AND QUIET

For bad guys on the lam, Wisconsin was once the refuge of choice. Al Capone,
John Dillinger and Baby Face Nelson  all headed for the state's north woods when
the heat was on. PAGE C29

SKI TOWN IN THE OFF-SEASON

Bordered by the Green Mountains to the west and Northfield Ridge to the east,
Vermont's Mad River Valley reveals itself in warmer weather, when history,
culture and a hyperlocal food scene come to the fore. PAGE C27

Op-ed

PAUL KRUGMAN PAGE A23

DAVID BROOKS PAGE A23

URL: http://www.nytimes.com

LOAD-DATE: June 26, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


