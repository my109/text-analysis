                   Copyright 2010 The New York Times Company


                             1613 of 2021 DOCUMENTS


                               The New York Times

                              March 7, 2010 Sunday
                              Late Edition - Final

Laws for Sale

BYLINE: By MATT BAI.

Matt Bai writes about national politics for the magazine.

SECTION: Section MM; Column 0; Magazine Desk; THE WAY WE LIVE NOW; Pg. 9

LENGTH: 1092 words


Plaintiffs' lawyers must be holding their heads a little higher when they walk
into P.T.A. meetings and neighborhood parties these days, knowing that corporate
lobbyists have overtaken them as the most despised professionals in America.
Lobbyists have never been especially popular, of course; even their most
sympathetic pop-culture portrayal, in the book and better-known movie ''Thank
You for Smoking,'' focused mostly on their moral depravity. As a candidate,
Barack Obama made a point of vowing to banish them from the White House. (This
proved considerably harder to do than it was to say, mostly because it turns out
that lobbyists in Washington are like Baathists in Baghdad; if you send them all
home, there aren't enough people left who know anything about running a
government.) In the past year, though, antilobbyist fervor has grown even more
intense, as hope for a new governing era has given way to frustration with a
divided capital and a dysfunctional Congress.

The outrage reached a fever pitch in January when the Supreme Court ruled that a
Fortune 500 corporation enjoys the same First Amendment right to influence the
political process as the guy who puts a yard sign on his lawn. Responding in The
Nation and The Los Angeles Times, the Harvard law professor Lawrence Lessig
incited an online furor when he accused President Obama of betraying his fellow
liberals by declining to confront the capital's culture of corporate dependence.
''Congress has developed a pathological dependence on campaign cash,'' Lessig
wrote, blaming moneyed interests for knocking the public option out of the
Democratic health care plan. He called for a constitutional convention that
would enact the public financing of all campaigns.

If you know some decent people who also happen to be lobbyists, as I do, then it
is tempting to defend their honor against such an onslaught -- to declare, as
Hillary Clinton did at a forum with bloggers in 2007, that ''lobbyists are
people, too.'' You might point out that industry lobbyists weren't the reason
that Democrats in Congress could not get out of their own way long enough to
pass health care reform last year; after all, pharmaceutical companies spent
something like $100 million in support of the Democratic plan.

But then you read a story about how Toyota spent $25 million on federal
lobbying, or how companies like Microsoft and Dell are mobilizing to preserve a
tax loophole that diverts $15.5 billion from the federal Treasury, and it's hard
to refute Professor Lessig's central premise. The truth is that anyone who
spends any significant time in the political world knows that corporate money,
raised and leveraged by lobbyists, is perverting any notion of good
policymaking, as surely as ice dancing perverts the idea of sport.

The problem with Lessig's indictment and others like it isn't that they are too
hard on lobbyists who try to influence the system. It's that they're too easy on
the politicians who cave to the pressure. Those who denounce the recent Supreme
Court ruling and call for radical reform of the campaign-finance system tend to
present a picture of Washington as a modern Gomorrah, in which irresistible
temptation lurks around every marble column. If you vote the way some lobbyist
wants you to vote on his obscure amendment, you will be rewarded with mountains
of cash in your next campaign; if you don't, that same money may be used to
finance a barrage of negative ads against you. How can we expect mere
legislators to resist such titanic forces, knowing that a vote against powerful
interests might well cost them their seats? If you drain all the fetid money
from the system, well-meaning reformers seem to think, then politicians will
have no incentive to bow down before corporations, and suddenly they will all
behave like the statesmen we would prefer them to be.

The flaw here is that if our senators and congressmen really wanted to be ideal
public servants, they wouldn't need us to protect them from their corporate
patrons. Rather, they would simply do what's right and face the consequences.
That is, after all, what most Americans do when our work requires us to choose
between our principles and our personal gain. Most of us would sooner leave our
jobs than follow orders to defraud clients or falsify records. The guy at the
deli counter probably isn't going to sell you contaminated meat just because his
boss orders him to, any more than your electrician is going to risk burning down
your house by installing faulty alarms.

Most of the 535 senators and members of Congress do not face moral dilemmas
quite this stark (examples of cash changing hands in suitcases are, in fact,
exceedingly rare), but they are forced to choose, constantly, between their
constituents and their own self-preservation. Is it really so outside the bounds
of human nature to expect congressmen to serve the interests of the voters, even
when their own re-elections are in jeopardy?

The political system is imperiled mostly because too many of our politicians
just can't seem to imagine any worse fate in life than losing an election. And
in this way, they have utterly missed the modern ethos of career adventurism.
Unlike our parents, who may have worked at the same firm or factory for 30
years, most of us these days fully expect to cycle through a succession of jobs
during our professional lives. But a lot of lawmakers still cling to their seats
at any cost to conscience or to constituency, as if it were the only job they
could ever see themselves holding -- even though, once they leave Congress, they
can expect to field more offers and make more money than the average voter will
see in a lifetime. It's this outmoded sense of entitlement that lobbyists
skillfully exploit. Our legislators don't necessarily need to be rescued from
the tidal current of corporatism; they just need a little perspective.

None of this is to say that publicly financing our campaigns might not be an
improvement over the current system. (It's a much better idea than, say, term
limits, which limits the freedom of voters rather than expanding it.) But
thinking that you can fix everything that's disappointing about politics by
eliminating lobbyists' money is as misguided as thinking you can win the war on
drugs by burning down all the coca fields, or that you can end crime by
outlawing all the guns. Some amount of corrosive money will always leak into the
system. But to borrow the old N.R.A. bumper sticker, lobbyists don't kill
legislation -- legislators do.

URL: http://www.nytimes.com

LOAD-DATE: March 7, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY DAVID HUME KENNERLY) DRAWING (DRAWING BY HUMAN
EMPIRE)
 GRAPHIC: 13,740 registered LOBBYISTS are active in WASHINGTON. (DATA SOURCE:
THE CENTER FOR RESPONSIVE POLITICS)

PUBLICATION-TYPE: Newspaper


