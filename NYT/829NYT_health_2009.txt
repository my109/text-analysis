                   Copyright 2009 The New York Times Company


                             829 of 2021 DOCUMENTS


                               The New York Times

                            October 16, 2009 Friday
                              Late Edition - Final

Democrats Address Their Own Rifts on Health Care

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 861 words

DATELINE: WASHINGTON


Deep fissures among Senate Democrats became evident on Thursday as lawmakers
moved closer to a floor debate on legislation to remake the health care system.

The divisions involved two issues: whether the government should sell health
insurance, in competition with private insurers, and whether Congress should
offset any of the cost of legislation to increase Medicare payments to doctors.

At a luncheon behind closed doors, Democrats said, liberals made impassioned
pleas for a new government insurance plan, and they challenged the chairman of
the Senate Finance Committee, Max Baucus, Democrat of Montana, to defend his
bill, which has no such public option.

Among the outspoken champions of the public plan were Senators Sherrod Brown,
Democrat of Ohio; Tom Harkin, Democrat of Iowa; and Bernard Sanders of Vermont,
an independent who caucuses with the Democrats.

Senator Ben Nelson, Democrat of Nebraska, said the discussions were only
''slightly less raucous than the town hall meetings'' that erupted in many
states in August.

Senate Democratic leaders have insisted for months that health care legislation
would be fully paid for. But the Senate majority leader, Harry Reid, Democrat of
Nevada, said Thursday that he would try next week to pass a free-standing bill
that would pour $240 billion into Medicare -- to shield doctors from cuts in
their Medicare payments that would otherwise occur, under existing law, in the
next 10 years. ''The slate should be wiped clean,'' Mr. Reid said.

Under the bill, which came as a surprise to members of both parties, none of the
cost would be offset or paid for.

The Medicare bill has  support from doctors, who are potentially crucial allies
in the Democrats' effort to overhaul the health care system. But the bill could
shatter the aura of fiscal responsibility that Senate Democrats have worked to
create in writing their broader health care legislation.

A version of the legislation approved Tuesday by the Senate Finance Committee
would block the Medicare cuts for one year. Mr. Baucus acknowledged that this
did not solve the longer-term problem.

Doctors have insisted on a permanent fix to the current Medicare formula, which
leaves them exposed to cuts each year, with a reduction of 21.5 percent planned
for January. Congress established the formula in 1997, stipulating that the
growth of Medicare spending on doctors' services should not exceed a
''sustainable growth rate'' set by law.

The sponsor of the Medicare bill, Senator Debbie Stabenow, Democrat of Michigan,
said the formula was badly flawed. ''Everybody knows that Congress will step in
to stop these cuts because they are so drastic,'' she said. ''They would hurt
patients.''

But two moderate Democrats, Senators Evan Bayh of Indiana and Kent Conrad of
North Dakota, said they opposed Ms. Stabenow's bill.

''It's not fiscally responsible,'' Mr. Bayh said. ''I could not vote for a bill
that raises the deficit by $240 billion, not at a time when we are already
hemorrhaging red ink. The physicians' issue needs to be addressed, but not in a
way that increases the deficit.''

Mr. Conrad, who is chairman of the Budget Committee, was furious at the
possibility that Congress might adjust payment rates for doctors without
offsetting the cost. ''I don't agree with just adding that amount to the debt,''
Mr. Conrad said, adding, ''I won't vote for it.''

Senator Charles E. Grassley of Iowa, the senior Republican on the Finance
Committee, said: ''There's no doubt that the physician payment formula needs to
be fixed. But it undermines the president's commitment to making sure health
care reform won't add a dime to the deficit when one of the most expensive
problems in the Medicare program is removed from overall reform legislation.''

The American Medical Association said Thursday that it had begun a
multimillion-dollar ad campaign to whip up public support for Ms. Stabenow's
bill.

Many doctors say Medicare payments do not cover their costs, and some have
limited the number of Medicare patients they take. Dr. J. James Rohack,
president of the medical association, said, ''It's time for permanent repeal of
the Medicare physician payment formula.''

Senator Sanders said the debate Thursday ''went up a few decibels higher than it
usually does in caucus meetings.'' He said he had told Mr. Baucus that more than
60 percent of the public and more than 80 percent of Democrats supported
creation of a public insurance plan. ''It's difficult to understand why we can't
give the American people what they want,'' Mr. Sanders said.

Describing the closed-door meeting of the Democratic caucus, Mr. Bayh said:
''There was a passionate discussion of the public option. Voices were raised. It
was a very spirited debate.''

Speaker Nancy Pelosi said Thursday that the House bill would include a public
insurance plan. But Mr. Baucus has said such a plan could not get the 60 votes
needed to overcome a Republican filibuster in the Senate.

In response to questions from colleagues, Mr. Baucus vigorously defended his
bill at the Democratic caucus. Mr. Bayh quoted him as saying: ''Hold on, now. We
are doing the best we can.''

URL: http://www.nytimes.com

LOAD-DATE: October 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: The Senate majority leader, Harry Reid, on Capitol Hill on
Thursday. Senator Max Baucus, right.(PHOTOGRAPH BY CHARLES DHARAPAK/ASSOCIATED
PRESS)

PUBLICATION-TYPE: Newspaper


