                   Copyright 2009 The New York Times Company


                              26 of 2021 DOCUMENTS


                               The New York Times

                            June 10, 2009 Wednesday
                              Late Edition - Final

Clues to the Health Care Puzzle

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 28

LENGTH: 1021 words


To the Editor:

Re ''Paying for Universal Health Coverage'' (editorial, June 7):

If it means raising taxes to pay for universal health coverage, so be it. It
seems to me that tax and spend for universal health care coverage is better for
America than cutting taxes and being forced to borrow. It would be a sign of
substantial progress if politicians could begin to win elections even if they
say they support raising taxes.

Private-sector insurance companies are so caught up in making money that they do
not want to relinquish control of the health care system. Health care is for
people, not profit. If we truly love our neighbors as we love ourselves, we
should be willing to pay for health care for all.

Paul L. Whiteley Sr. Louisville, Ky., June 7, 2009

To the Editor:

The Obama administration doesn't need to raise taxes to fix health care. It
needs to cut the cost dramatically. This can be done only by abandoning the
fee-for-service payment model. The problem with fee-for-service is not merely
that it pays providers to provide service; it pays them to create service as
well. It is this almost limitless ability of doctors to create service that
makes our per capita health care costs twice that of any other developed
country.

If physicians were salaried employees with modest incentives for productivity
and outcomes, we could, in very short order, have affordable health care for
all.

Geoff Berg Warren, R.I., June 7, 2009

The writer is an internist.

To the Editor:

While the politics of how to pay for President Obama's goal of universal health
coverage is fierce, one source of revenue ought to be a slam dunk: limit the tax
exclusion for the value of health insurance for employees at work to a basic
policy.

For more than 50 years, the tax laws have allowed the exclusion to extend to the
most deluxe policies, typically acquired for highly paid employees who are best
able to acquire insurance without any government assistance. Yet if Congress
limited the exclusion to a basic policy, it could raise hundreds of billions of
additional tax dollars over the years to help finance basic coverage for the
uninsured and the underinsured, and it would bring down the costs of insurance
and health care for everyone.

Come on, Congress. Can any one of you really justify giving the biggest tax
subsidies for the most expensive policies for the highest paid executives?

John O. Fox Amherst, Mass., June 8, 2009

The writer is the author of ''If Americans Really Understood the Income Tax''
and a visiting professor at Mount Holyoke College.

To the Editor:

Re ''Spending Disparities Set Off a Fight in the Effort to Overhaul Health Care'
' (news article, June 9):

I applaud President Obama in mandating that White House staff members read a
recent article in The New Yorker by Dr. Atul Gawande highlighting the huge
variance in health spending across our country. I especially applaud President
Obama in encapsulating the issue by stating in effect, ''This is what we've got
to fix.''

But the remedy being proposed on Capitol Hill will not control the bleeding that
plagues Medicare. Cost-shifting by siphoning precious health care dollars from
regions of the country with high health use to regions of the country with lower
health use is a temporary Band-Aid remedy that will not control the hemorrhage
of health care spending.

What is lost in the health care debate is that rehospitalizations among Medicare
beneficiaries are prevalent and costly. Controlling rehospitalizations may be
one curative prescription to control the bleeding.

Pankaj Gupta Edison, N.J., June 9, 2009

The writer, a geriatrician, is medical director of a managed care company.

To the Editor:

During the last year and a half of my mother-in-law's life, her medical care
incurred huge medical expenses as the quality of her life declined. She was in
her late 80s, and many of the hospitalizations and tests she endured were
unnecessary, painful and redundant. Her doctors did not communicate so her
medications were often incompatible.

Thousands of dollars could have been saved if her medical records had been
electronic and available to both the hospitals and the doctors who were treating
her. I believe that in many high-cost areas, making minor changes in patient
care and reducing redundancy would improve costs considerably.

Margie Parko Crossville, Tenn., June 9, 2009

To the Editor:

Re ''Downturn Puts a Chokehold on Those Caring for Family Members'' (news
article, June 7):

The enormous financial and emotional challenges faced by the Szalega and Denk
families and the many tens of millions of other Americans who grapple daily with
caring for a frail elderly parent or relative reveal a patch-quilt long-term
care system that is manifestly unfair. Unless we can figure out how to protect
ourselves against the catastrophic costs of long-term care, the system will
continue to fail society in good economic times and bad.

As the Obama administration gears up for health care reform, it must not forget
also to address the financing of long-term care services for both the elderly
and other vulnerable people with disabilities.

Dennis L. Kodner  Old Westbury, N.Y., June 7, 2009

The writer is a professor of medicine and gerontology at the New York Institute
of Technology.

To the Editor:

''Health Insurers Balk at Changes for Small Business'' (Business Day, June 3)
shows another bit of arrogant greed Americans are supposed to accept more or
less quietly. The article says ''employer-provided medical insurance remains the
bedrock of the nation's health care system.'' Well, it shouldn't be.

Given the likelihood of being badly squeezed, small businesses would do well to
sign on at last for a single-payer health care system that would relieve them of
the burden. The customer-be-damned attitude of the private insurers seems
increasingly more blatant and hurtful.

Perhaps like other outdated industries we've seen, they may be outfoxing
themselves. Americans don't need to take their thumbed noses in addition to
their amply demonstrated inefficiency.

Ron Christensen Brooklyn, June 3, 2009

URL: http://www.nytimes.com

LOAD-DATE: June 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY HARRY CAMPBELL)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


