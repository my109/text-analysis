                   Copyright 2010 The New York Times Company


                             1827 of 2021 DOCUMENTS


                               The New York Times

                            March 31, 2010 Wednesday
                              Late Edition - Final

Insurers to Cover Children's Pre-existing Health Conditions

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 723 words


WASHINGTON -- Under pressure from the White House, health insurance companies
said Tuesday that they would comply with rules to be issued soon by the Obama
administration requiring them to cover children with pre-existing medical
problems.

''Health plans recognize the significant hardship that a family faces when they
are unable to obtain coverage for a child with a pre-existing condition,'' said
Karen M. Ignagni, president of America's Health Insurance Plans, a trade group.
Accordingly, she said, ''we await and will fully comply with'' the rules.

Ms. Ignagni made the commitment in a letter to Kathleen Sebelius, the secretary
of health and human services, who had said she feared that some insurers might
exploit a possible ambiguity in the new health care law to deny coverage to some
sick children.

The White House immediately claimed victory.

In a Twitter message, Robert Gibbs, the White House press secretary, scored the
tug of war as ''Kids 1, insurance 0.''

Major provisions of the law take effect in 2014. Some, including a ban on
''pre-existing condition exclusions'' for children under 19, take effect in
September. The law does not explicitly say that insurers must sell insurance to
families with such children this year, but Democratic Congressional leaders and
White House officials said that was their intent.

To eliminate any ambiguity, Ms. Sebelius said, she will issue rules defining the
scope of the new law.

Under these rules, Ms. Sebelius said, ''children with pre-existing conditions
may not be denied access to their parents' health insurance plan,'' and
''insurance companies will no longer be allowed to insure a child but exclude
treatments for that child's pre-existing condition.''

In response to a question, Nick Papas, a spokesman for the Department of Health
and Human Services, said the rules would require insurers to offer coverage to
children with pre-existing conditions, including those who have never had health
insurance.

Ms. Sebelius said she was pleased that ''insurance companies plan to do the
right thing.''

It was not immediately clear whether the rules would allow insurers to charge
higher premiums to families with children with pre-existing conditions.
Administration officials said they would be monitoring any rate increases.

Some Democrats in Congress want Mr. Obama to take a tough line. If insurers
could raise premiums without limits, they would, in effect, be denying coverage,
Democrats say.

Some insurers said they were unclear about the language of the law, based on
differing accounts, and looked forward to detailed guidance from the government.

''There has been some confusion regarding the elimination of pre-existing
condition evaluation for children in the health care bill,'' said Kristin E.
Binns, a spokeswoman for WellPoint, one of the largest insurers. She said the
company would ''follow the law on this and all matters.''

Insurers said they would accept the administration's reading of the law, even if
they did not fully agree with it, because they wanted to avoid a showdown over
the politically explosive issue of health insurance for sick children.

Several lawyers said Congress could easily clear up any confusion by revising
the law. ''The real solution here is a legislative fix so all players in the
industry can act according to a clear set of rules,'' said William G.
Schiffbauer, a Washington lawyer whose clients include employers and insurance
companies.

Representative Allyson Y. Schwartz, Democrat of Pennsylvania, who helped write a
similar provision in an earlier version of the legislation, said the new law
''requires that all insurers issue insurance to children regardless of health
status, and cover all of their ailments,'' starting in September.

Jeff Smokler, a spokesman for the Blue Cross and Blue Shield Association, said
its member companies were ''fully committed to complying with the new law'' and
accepted the principles set forth by Ms. Sebelius.

Gail K. Boudreaux, executive vice president of the UnitedHealth Group, said she
supported the administration's effort to clarify the law ''to ensure that no
child will be denied access to health insurance because of a pre-existing
condition.''

''We expect that the new regulations will eliminate any uncertainty about the
law's intent,'' Ms. Boudreaux said.

URL: http://www.nytimes.com

LOAD-DATE: March 31, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


