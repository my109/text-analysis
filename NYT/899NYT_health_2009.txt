                   Copyright 2009 The New York Times Company


                             899 of 2021 DOCUMENTS


                               The New York Times

                           October 28, 2009 Wednesday
                              Correction Appended
                              Late Edition - Final

A Drop In the Wrong Bucket

BYLINE: By DAVID LEONHARDT.

E-mail: leonhardt@nytimes.com

SECTION: Section B; Column 0; Business/Financial Desk; ECONOMIC SCENE; Pg. 1

LENGTH: 1190 words


If you wanted to help the economy and you had $14 billion to bestow on any group
of people, which group would you choose:

a) Teenagers and young adults, who have an 18 percent unemployment rate.

b) All the middle-age long-term jobless who, for various reasons, are not
eligible for unemployment benefits.

c) The taxpayers of the future (by using the $14 billion to pay down the
deficit).

d) The group that has survived the Great Recession probably better than any
other,   with stronger income growth, fewer job cuts and little loss of health
insurance.

The Obama administration has chosen option d  --  people in their 60s and
beyond.

The president has proposed sending a $250 check to every Social Security
recipient, which sounds pretty good at first. The checks would be part of his
admirable efforts to stimulate the economy, and older Americans are clearly a
sympathetic group. Next year, they are scheduled to receive no cost-of-living
increase in their Social Security benefits.

Yet that is largely because they received an artificially high 5.8 percent
increase this year. For this reason and others, economists are generally
recoiling at the proposal.

President Obama's own economic advisers raised objections, as my colleague
Jackie Calmeshas reported. Isabel Sawhill of the Brookings Institution told me
she thought the idea was crazy  --  and then noted she was in her 70s. Rosanne
Altshuler, co-director of the Tax Policy Center in Washington, says that the
checks ''seem to be pure pandering to seniors.''

Indeed, the politics are attractive. People over 65 vote in large numbers.
Saying no to them is never easy.

And therein lies a problem that's much larger than one misguided $14 billion
proposal.

With the economy gradually improving, members of Congress and White House
officials are just starting  to think more seriously about the budget deficit.
Fifty-three senators voted down a narrow health care bill last week, with many
citing its potential impact on the budget. On Monday, Christina Romer, the
chairwoman of Mr. Obama's Council of Economic Advisers, gave a speech in which
she said the deficit was ''simply not a problem that can be kicked down the road
indefinitely.''

Just about everybody agrees that solving the deficit depends on reducing the
benefits   that current law has promised to retirees, via Medicare and Social
Security. That's not how people usually put it, of course. They tend to use the
more soothing phrase ''entitlement reform.'' But entitlement reform is just
another way of saying that we can't pay more in benefits than we collect in
taxes.

''If the long-term issue is entitlement reform,'' says Joel Slemrod, a
University of Michigan economist, ''the fact that the political system cannot
say no to $250 checks to elderly people is a bad sign.''

The first Social Security check was mailed in 1940 to Ida May Fuller, a retired
legal secretary in Ludlow, Vt. It was for $22.54. Every month for the next 10
years, Ms. Fuller received a check for that same amount.

The original Social Security legislation had not included an inflation
adjustment, which meant benefits did not keep up with the cost of living. A
decade later, Ms. Fuller's checks were worth about 40 percent less in real terms
than when she started receiving them.

Congress finally increased benefits in 1950 and then continued to do so in fits
and starts, sometimes faster than inflation, sometimes slower and usually in an
election year. President Richard M. Nixon and a Democratic Congress brought some
order to this process in 1972, by automatically tying benefits to the movement
of an inflation index in the previous year.

The changes were part of the transformation, during the middle decades of the
20th century, in how this country treated the elderly. In the 1930s, they had
little safety net and frequently struggled to meet their basic needs. Four
decades later, they were the only group of Americans with guaranteed health care
and a guaranteed income. All in all, it was certainly for the good.

But by the 1970s, you could start to see the early signs of excess. In their
bill, Mr. Nixon and Congress included a little bonus: the increase in Social
Security payments could never be less than 3 percent, no matter what inflation
was. In the 1980s, Congress reduced the floor to zero  --  meaning that benefits
would be held constant if prices fell  --  but the principle remained the same:
heads, it's a tie; tails, Social Security recipients win.

This year, the coin finally came up tails.

With oil prices plunging and other prices falling, last year's high inflation
(which led to the 5.8 percent increase in Social Security payments) has turned
into deflation. Overall prices have dropped 2.1 percent in the last year,
according to the relevant price index.

Social Security payments, however, will remain as they were, which means that
recipients are already set to receive an effective raise, even without Mr.
Obama's $250 checks. No matter what happens with that proposal, 2010 will be the
first year since at least the Nixon era that the buying power of an individual
worker's Social Security goes up.

Compare that to what's happening with minimum-wage workers in Colorado. Their
wage is also tied to inflation, but it has no floor. So it will fall slightly
next year, to keep pace with prices.

Now, I understand that there are arguments on the other side of the issue.
Lawrence Summers, Mr. Obama's top economics aide, pointed out that the stimulus
bill included one-time $250 payments for Social Security recipients, which were
sent out this year, but tax cuts for workers both this year and next year.
''We're correcting an anomaly,'' he told me.

Others will argue that the elderly simply need help. Some have been the victim
of age discrimination.  Too many still live in poverty. All of them are likely
to see their Medicare premiums rise in 2010. This recession has spared no group.

But older Americans really have survived the recession better than most.

Many of them started buying assets years if not decades ago, meaning they were
not the main victims of the stock and housing bubbles. They had a cushion. In
addition, relatively few of them work in manufacturing or construction, the
hardest-hit industries.

Just consider: The real median income of over-65 households rose 3 percent from
2000 to 2008. For households headed by somebody  age 25 to  44, it fell about 7
percent.

Economic policy, like most everything else, is about making choices. Mr. Obama
is choosing  the elderly, rich and poor, to be more worthy of $14 billion in
government checks than struggling workers or schoolchildren. Republicans have
pandered in their own ways, choosing to oppose just about any cut in Medicare
and, in effect,  to stick your grandchildren with an enormous tax bill.

In a way, I understand where the politicians are  coming from. We voters may say
that we are in favor of cutting the deficit, but usually mean it in only the
theoretical sense. Who wants their own benefits cut? For that matter, who is
even willing to have their Social Security checks hold steady?

URL: http://www.nytimes.com

LOAD-DATE: October 28, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: October 30, 2009



CORRECTION: The Economic Scene column on Wednesday, about President Obama's
proposal to send $250 checks to all Social Security recipients, misstated
provisions in a 1970s law and a 1980s law regarding Social Security benefits. A
provision in the 1970s law stipulated that Social Security benefits would not
decline even if the Consumer Price Index did; the bill did not fix the minimum
increase in benefits at 3 percent each year. The 1980s law left that provision
intact; it did not create the provision.

GRAPHIC: PHOTO: Ida May Fuller in 1950, at age 76, when her benefits increased.
She received the first Social Security check in 1940.(PHOTOGRAPH BY ASSOCIATED
PRESS)(pg. B8)  CHART: Recession Survivors:  Older Americans' income rose 3
percent from 2000 to 2008, and income for the next oldest age group, 55 to 64,
rose 2 percent. For all other age groups, income fell.(Source: Census Bureau,
via Haver Analytics)(pg. B8)

PUBLICATION-TYPE: Newspaper


