                   Copyright 2010 The New York Times Company


                             1711 of 2021 DOCUMENTS


                               The New York Times

                             March 21, 2010 Sunday
                              Late Edition - Final

Laugh Lines

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 2

LENGTH: 122 words


DAVID LETTERMAN

Congress is getting ready to vote on President Obama's health care bill. It's
going to be a close vote. The House Democrats say it could be a real tickle
fight.

JIMMY FALLON

President Obama talked about health care reform at a senior center in
Strongsville, Ohio, today. The most common question he got: ''When's bingo?''

C-Span is uploading 23 years of video on the Internet. Or if you want to get the
sensation of watching 23 years of C-Span, just watch two minutes of C-Span.

JAY LENO

You know, I was thinking about this health care problem. If you took all the
money the Republicans have spent to stop health care and all the money Democrats
have spent trying to get health care, we could afford health care.

URL: http://www.nytimes.com

LOAD-DATE: March 21, 2010

LANGUAGE: ENGLISH

GRAPHIC: CARTOONS: Mike Luckovich: Atlanta Journal-Constitution, Creators
Syndicate
Mike Luckovich: Atlanta Journal-Constitution, Creators Syndicate
Mike Peters: Dayton Daily News, King Features Syndicate
 Steve Kelley: The Times-Picayune, Creators Syndicate

PUBLICATION-TYPE: Newspaper


