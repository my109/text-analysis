                   Copyright 2009 The New York Times Company


                             1273 of 2021 DOCUMENTS


                               The New York Times

                            December 27, 2009 Sunday
                              Late Edition - Final

Troublemaker

BYLINE: By LLOYD GROVE.

Lloyd Grove, a former newspaper reporter in Corpus Christi and Dallas, is editor
at large for The Daily Beast.

SECTION: Section BR; Column 0; Book Review Desk; Pg. 17

LENGTH: 1179 words


MOLLY IVINS

A Rebel Life

By Bill Minutaglio and W. Michael Smith

Illustrated. 335 pp. PublicAffairs. $26.95

What fearful fun Molly Ivins would have had with Glenn Beck, the birthers, Sarah
Palin, Representative Joe (''You lie!'') Wilson and, of course, Gov. Rick Perry
of Texas -- he of the secessionist aspirations and wonderful hair. And how
pained she would be, while never losing hope, as she showered President Obama
with tough love over his policies in Afghanistan and Iraq, and his
difference-splitting diffidence on health care reform.

Ivins, who died in 2007 at the age of 62 after a battle with breast cancer, was
that rarest of endangered species, an unreconstructed Texas liberal. As a
newspaper reporter, syndicated columnist and public speaker, she built a body of
work enlivened by political passion and rollicking wit. She lampooned the Texas
Legislature, the business establishment, the power elite and most of all George
W. Bush, whom she immortalized as ''Shrub.'' But, as suggested by Bill
Minutaglio and W. Michael Smith, in ''Molly Ivins: A Rebel Life,'' her greatest
creation was herself.

The authors frame her self-invention as a lifelong mutiny against her bourgeois
upbringing in River Oaks, the same gilded Houston neighborhood where George W.
grew up after his family moved from Midland, and the harsh judgment of her
authoritarian father, a Republican oil company executive with yacht-club airs
and some ugly racial theories, which he freely shared with his daughter. By
August 1992 -- when I briefly crossed paths with Ivins on the Clinton-Gore bus
caravan making its way through the Lone Star State (only briefly, because she
quickly abandoned the press bus after being invited to ride with Bill and
Hillary) -- she had already perfected the persona that was equal parts
cracker-barrel and belletristic.

She was a majestic presence, six feet tall and ruby-haired, raucously laughing,
squinting like a gunslinger and tossing out epigrams with a redneck accent.
Typical Ivins lines: If a certain Dallas Republican congressman's ''I.Q. slips
any lower, we'll have to water him twice a day.'' ''Calling George H. W. Bush
shallow is like calling a dwarf short.'' Pat Buchanan's notorious culture-war
speech at the 1992 Republican convention ''probably sounded better in the
original German.''

It was largely a pose. Ivins had attended a fancy prep school in Houston, and
spoke proficient French after attending a cultural immersion program at the
Chateau du Montcel not far from Paris and spending her junior year abroad in
Paris researching a paper on Charles de Gaulle. She shared her father's
affection for the pricey pastime of sailing, graduated from Smith College just
like her ditzily genteel mother and grandmother, and received a master's degree
in journalism from Columbia University. She even had a brief flirtation with Ayn
Rand, which ended with the motorcycle death of her college boyfriend -- ''the
love of my life,'' Ivins later said -- a wealthy Yale man and ''Atlas Shrugged''
enthusiast of whom her father might have approved.

''She spoke with an East Coast, educated elite diction, inflected by a
junior-year-abroad French accent. She sounded like Jacqueline Kennedy,'' her
close friend Terry O'Rourke told Minutaglio and Smith, recalling the Molly Ivins
of the mid-1960s when they were both reporter-interns at The Houston Chronicle.
''She was the daughter of corporate power and wealth.''

Ivins owned up to her background when it suited her -- once fretting that a
prominent New York editor, with whom she was preparing to dine at an expensive
French restaurant, might dismiss her ''as a hick from Texas.'' But more often
she played the rube. ''I grew up in East Texas,'' she once claimed in a speech
at Smith College, fiddling with the geographical and cultural map to locate
herself in one of the rural mill towns where the girls' high school basketball
teams (against which Ivins had competed as a teenager) ''always wore pink
plastic curlers in their hair during games so they'd look good at the dance
afterwards.''

Her newspaper career began conventionally enough at The Minneapolis Tribune,
where she polished her prose for three years on human interest filler, until her
editors let her write about the city's young radicals. She dated and moved in
with one of them, while flouting the norms of objective journalism by
campaigning for tenants' rights in her spare time.

It wasn't until 1970 that Ivins came into her own. As co-editor of The Texas
Observer, the Austin-based journal of progressive politics whose influence
outdistanced its tiny circulation, she established herself as a leading
authority on the cowboy-booted rascals and occasional idealists who populated
the State Legislature, attracting the attention of national journalistic
heavyweights like David Broder and R. W. Apple Jr., and befriending the
blacklisted television personality John Henry Faulk, one of her early
supporters. She also courted local up-and-comers like the future governor Ann
Richards and the state land commissioner Bob Armstrong, with whom she went
rafting and camping.

Ivins grew especially close (possibly as a lover) to the Democratic state
comptroller Bob Bullock, the most skillful power broker in Texas, a gun-toting,
whip-smart manic-depressive who shared her weakness for hard drinking. As
Ivins's barroom mentor, Bullock bequeathed her a precious perspective on the
inner workings of state politics and government. Decades later, as lieutenant
governor, Bullock played a similar role (minus the sexual tension and alcohol)
with Ivins's nemesis, George W.

Minutaglio, the author of a well-received Bush biography, ''First Son,'' and
Smith, who spent six years working for Ivins as a researcher and gofer, draw on
voluminous private papers and interviews to produce a painfully intimate
portrait of her family dysfunction, struggles with alcoholism, thwarted love
life and professional frustrations -- notably an ill-conceived stint in the late
1970s and early '80s with The New York Times. That job ended shortly after the
legendary editor Abe Rosenthal gave Ivins a dressing-down for her suggestive
phrasing in a story about chickens. Rosenthal chided her for trying to make
Times readers think dirty thoughts. ''Damn if I could fool you, Mr. Rosenthal,''
Ivins quoted herself as replying.

While chockablock with colorful anecdotes and psychological insights, ''Molly
Ivins: A Rebel Life'' isn't convincing as the biography of a significant figure
in journalism. Minutaglio and Smith fall short of making their case that she
was, variously, ''one of the best-known and most influential journalists in
American history'' and ''a Texas Mark Twain.'' Despite publishing some
best-selling collections and several well-regarded magazine articles, and
co-writing two slight volumes about Dubya, Ivins never wrote the big, important
book about Texas that she'd always wanted to. But true to form, she made a joke
of her failings, once wearing a black T-shirt blazoned with the message: ''Don't
Ask About the Book.''

URL: http://www.nytimes.com

LOAD-DATE: December 27, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Molly Ivins at The New York Times. (PHOTOGRAPH COURTESY OF THE
TEXAS OBSERVER/FROM ''MOLLY IVINS'')

DOCUMENT-TYPE: Review

PUBLICATION-TYPE: Newspaper


