                   Copyright 2009 The New York Times Company


                             583 of 2021 DOCUMENTS


                               The New York Times

                           September 13, 2009 Sunday
                              Late Edition - Final

Thousands Attend Broad Protest of Government

BYLINE: By JEFF ZELENY; Theo Emery and Ashley Southall contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 37

LENGTH: 956 words

DATELINE: WASHINGTON


A sea of protesters filled the west lawn of the Capitol and spilled onto the
National Mall on Saturday in the largest rally against President Obama since he
took office, a culmination of a summer-long season of protests that began with
opposition to a health care overhaul and grew into a broader dissatisfaction
with government.

On a cloudy and cool day, the demonstrators came from all corners of the
country, waving American flags and handwritten signs explaining the root of
their frustrations. Their anger stretched well beyond the health care
legislation moving through Congress, with shouts of support for gun rights,
lower taxes and a smaller government.

But as they sang verse after verse of patriotic hymns like ''God Bless
America,'' sharp words of profane and political criticism were aimed at Mr.
Obama and Congress.

Dick Armey, a former House Republican leader whose group Freedomworks helped
organize the protest, stood before the crowd and led the rallying cries in
nearly the same spot where Mr. Obama took his oath of office eight months ago.

''He pledged a commitment of fidelity to the United States Constitution,'' Mr.
Armey said, suggesting that Mr. Obama was in violation of what the founding
fathers intended the size and scope of the government to be.

''Liar! Liar! Liar! Liar!'' the crowd shouted back, echoing the accusation that
Representative Joe Wilson, Republican of South Carolina, hurled at the president
three days earlier during his address to Congress.

The demonstrators numbered well into the tens of thousands, though the police
declined to  estimate  the size of the crowd. Many  came on their own and were
not part of an organization or group. But the magnitude of the rally took the
authorities by surprise, with throngs of people streaming from the White House
to Capitol Hill for more than three hours.

The atmosphere was  rowdy at times, with signs and images  casting  Mr. Obama in
a demeaning light. One sign called him the ''parasite in chief.'' Others likened
him to Hitler. Several people held up preprinted signs saying, ''Bury Obama Care
with Kennedy,'' a reference to the  Massachusetts senator whose body passed by
the Capitol two weeks earlier to be memorialized.

Other signs  did not focus on Mr. Obama, but rather on the government at large,
promoting gun rights, tallying  the national deficit and deploring illegal
immigrants living in the United States.

Still, many  demonstrators  expressed their views without a hint of rage. They
said the size of the crowd  illustrated that their views  were shared by a
broader audience.

''I want Congress to be afraid,'' said Keldon Clapp, 45, an unemployed marketing
representative who recently moved to Tennessee from Connecticut after losing his
job. ''Like everyone else here, I want them to know that we're watching what
they're doing. And they do work for us.''

As Mr. Obama traveled to Minnesota on Saturday to rally support for his health
care plan, he flew over the assembling crowd in Marine One. The helicopter could
be seen flying overhead as the demonstrators marched down Pennsylvania Avenue.

''This is not some kind of radical right-wing group,'' Senator Jim DeMint,
Republican of South Carolina, said in an interview as dozens of people streamed
by him. ''I just hope the Congress, the Senate and the president recognize that
people are afraid of what's going on.''

Mr. DeMint and a  few Republican legislators were the only party leaders on hand
for the demonstration. Republican officials said privately that they were
pleased by the turnout but wary of the anger directed at all politicians. And
most of those who turned out were not likely to have been Obama voters anyway.

Protesters came by bus, car and airplane, arriving here from Texas and
Tennessee, New Mexico and New Hampshire, Ohio and Oregon. The messages on their
signs told of an intense distrust of the government, which several people said
began long before Mr. Obama took office.

For the most part, Democrats stayed silent on Saturday, with the exception of a
small group of counterdemonstrators who gathered behind a roadblock to protest
what they called a ''right-wing rally.'' Many were members of the clergy, who
said they were concerned about misinformation propagated by opponents of health
care legislation.

''We'd like to have an honest debate,'' said Chris Korzen, director of the
nonprofit Catholics United. ''I don't see a lot of substance here.''

While there was no shortage of vitriol among protesters, there was also an air
of festivity. A band of protesters in colonial gear wended through the crowd,
led by a bell ringer in a tricorn hat calling for revolution. A folk singer
belting out a protest ballad on a guitar brought cheers.

In conversations with demonstrators, people identified themselves as
Republicans, libertarians, independents and former Democrats. Several speakers
denounced the Obama administration's health care plan as ''socialism.'' A few
Confederate flags waved in the air, but there were hundreds of American flags
and chants of, ''U.S.A.! U.S.A.! U.S.A.!'' A young girl held a sign saying,
''Don't redistribute the wealth of my Barbies.''

Ruth Lobbs, 57, a schoolteacher from Jacksonville, Fla., said she flew to
Washington on Saturday to protest how she believes the government has violated
the Constitution. She said she did not vote for the president, adding that her
anger has been building for years.

''It's more than Obama -- this isn't a Republican or a Democratic issue,'' Ms.
Lobbs said as she held a yellow flag that declared, ''Don't Tread on Me.''

''I don't know if anything will come of this or not,'' she said, ''but this is a
peaceful way of showing our frustration.''

URL: http://www.nytimes.com

LOAD-DATE: September 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: A crowd marched toward the Capitol as people from around the
country gathered to express their discontent with the government.(PHOTOGRAPH BY
AMANDA LUCIDON FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


