                   Copyright 2009 The New York Times Company


                             207 of 2021 DOCUMENTS


                               The New York Times

                              July 24, 2009 Friday
                              Late Edition - Final

What's On Today

BYLINE: By KATHRYN SHATTUCK

SECTION: Section C; Column 0; Movies, Performing Arts/Weekend Desk; Pg. 20

LENGTH: 709 words


9 P.M. (TLC) WHAT NOT TO WEAR The series gets a mini-makeover of its own with
the arrival of Ted Gibson, a hairstylist to the likes of Anne Hathaway and
Angelina Jolie and at runway shows for Prada and Chanel. With a fondness for
luxurious coifs, he replaces the scissor-happy Nick Arrojo, whose snips have
caused some recipients to weep, not always for joy. Above, Mr. Gibson with his
first client: the flowing-tressed Ariel Stewart, a 26-year-old ''kissing wench''
at the Georgia Renaissance Festival whose wardrobe is stuck in the Dark Ages.
Will Mr. Gibson's hair redo cause Ms. Stewart, a k a Ruby Two-lypps, to pucker
up -- or pout?

7 A.M. (NBC) TODAY Men and plastic surgery, healthy beach towns and -- are we
there yet? -- coping with children while driving. Katy Perry performs.

11 A.M. (ABC) THE VIEWKevin Spacey talks about playing a marijuana-toking
psychiatrist to the stars in his new film, ''Shrink.''

8 P.M. (NBC) THE CHOPPING BLOCK The cleaver comes down on the series and its
head chef, Marco Pierre White, as the remaining two couples remodel their
restaurants, devise menus and polish their services in time for the last
suppers. The guest diners include Piers Morgan of ''America's Got Talent'' and
food critics from throughout the season.

9 P.M. (Syfy) EUREKA Inanimate objects start walking around after Dr. Tess
Fontana (Jamie Ray Newman) reopens Section 5. She suspects poltergeists.

9 P.M. (13, 49) BILL MOYERS JOURNAL Mr. Moyers examines the effect of partisan
messages on health care reform. Trudy Lieberman, the director of the health and
medical reporting program at the City University of New York's Graduate School
of Journalism, and Marcia Angell, a senior lecturer in social medicine at
Harvard Medical School and a former editor in chief of The New England Journal
of Medicine, offer perspective.

9 P.M. (National Geographic) DOG WHISPERER Cesar Millan meets Suzie Dove Miles,
a so-called cat whisperer who has adopted a Doberman named Baby Girl, above with
Mr. Millan. The trouble is, Baby Girl  is afraid of loud noises (like the
fireworks that explode each night over nearby Disneyland), and of the linoleum
floors and open cupboards in Ms. Miles's kitchen. But mostly, Baby Girl is
afraid of cats.

9 P.M. (VH1) LIVE & LOUD FRIDAYS Nickelback performs three of its biggest hits
in this summer concert series, presented by VH1 and Live Nation.

9 P.M. (BBC America) TORCHWOOD: CHILDREN OF EARTH The mini-series ends as the
final sanction begins, leaving Torchwood defenseless and Gwen Cooper (Eve Myles)
alone amid the violence -- with the future of the human race to be decided.

9 P.M. (Animal Planet) WHALE WARS Capt. Paul Watson and his Sea Shepherd crew
are in pursuit of the Nisshin Maru, the largest ship of the Japanese whaling
fleet, when three harpoon boats suddenly materialize out of the fog. Uh oh.

10 P.M. (13) IT'S THE ECONOMY, NEW YORKAndrew Ross Sorkin of The New York Times
interviews Cathie Black, the president of Hearst Magazines and the author of
''Basic Black,'' and Niall Ferguson, the host of ''The Ascent of Money.''

10 P.M. (ABC) OVER A BARREL: THE TRUTH ABOUT OIL  In this ''20/20'' episode
Charles Gibson, left, reports on the price of oil, the dangers involved in its
consumption, and the true benefactors of the business. He travels to Cushing,
Okla., a tiny oil town where the price of a barrel dictates the national
standard, and visits the Gulf Coast, home to a third of the country's oil
refineries. He also interviews industry experts, who explain the link between
speculators and skyrocketing oil prices, and talks with Secretary of Energy
Steven Chu about biofuels and how they can help curb United States dependence on
foreign oil.

10:30 P.M. (13) SOUNDSTAGE The band Fall Out Boy performs ''America's
Suitehearts,''  ''Dance, Dance,''  ''Sugar, We're Goin Down'' and other hits at
WTTW's Grainger Studio in Chicago.

11 P.M. (Comedy Central) FUNNY PEOPLE: LIVEJudd Apatow, Seth Rogen and Adam
Sandler are hosts of this stand-up special, which looks at the making of their
new film, ''Funny People,'' opening next Friday. It also features  routines from
Sarah Silverman, Patton Oswalt, Russell Brand, Brian Posehn, Greg Giraldo and
Maria Bamford. KATHRYN  SHATTUCK

URL: http://www.nytimes.com

LOAD-DATE: July 24, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS

PUBLICATION-TYPE: Newspaper


