                   Copyright 2010 The New York Times Company


                             1696 of 2021 DOCUMENTS


                               The New York Times

                            March 20, 2010 Saturday
                              Late Edition - Final

Nuns Back Bill Amid Broad Rift Over Whether It Limits Abortion Enough

BYLINE: By HELENE COOPER

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 831 words


WASHINGTON -- In breaking publicly with Roman Catholic bishops over the health
care bill, a group of nuns has once again exposed the long-running rift between
liberal and conservative theology in the Catholic Church.

The issue dividing them is whether the Senate version of the legislation goes
far enough in limiting the use of federal subsidies paid for insurance policies
that cover abortion. Progressive Catholics, including the group of more than 50
nuns representing thousands more from various religious orders, said this week
that they would support the Senate bill. The traditionalists, like the United
States Conference of Catholic Bishops, said they would oppose it.

Although both the nuns and the bishops firmly oppose abortion, they have reached
different conclusions about the bill, a divide that is also playing out more
broadly among other groups that oppose abortion.

These include Democrats in the Senate like Bob Casey of Pennsylvania who are
comfortable with the restrictions in the Senate health care bill and Democrats
in the House like Bart Stupak of Michigan who are fighting for tighter
restrictions. That divide is proving central to the outcome of the health care
debate -- Mr. Stupak and his allies were continuing to fight Friday night -- as
Democratic leaders scrambled to come up with the votes  to pass the legislation.

''It is an utter mystery to me'' how religious groups that oppose abortion could
read the same bill so differently, said Sister Simone Campbell, the executive
director of Network, a Catholic social justice lobbying organization that
supports the bill.

Sister Simone, who described herself as anti-abortion, said she did not believe
that the Senate version of the bill would make abortion more widely available.
She did not directly criticize the bishops, but said ''some people could be
motivated by a political loyalty that's outside of caring for the people who
live at the margins of health care in society.''

The Senate bill allows any state to ban the use of federal subsidies to buy
insurance plans that cover abortion. In other states, insurers would be required
to place subsidy money in separate accounts from dollars for private premiums
and use only the private money to pay for abortions.

The bishops argue that the Senate legislation, which the House is expected to
adopt, could nonetheless result in federal money being used for abortions.

''In so doing, it forces all of us to become involved in an act that profoundly
violates the conscience of many, the deliberate destruction of unwanted members
of the human family still waiting to be born,'' said Cardinal Francis George,
the president of the United States Conference of Catholic Bishops, in a written
statement this week.

The nuns who have endorsed the health care bill and their supporters say that is
not so, suggesting that they see no need for the more restrictive language on
abortion in the bill passed by the House last year.

''The bishops' view is extremely purist,'' said Morna Murray, president of
Catholics in Alliance for the Common Good. ''They are among the only ones who
have read the language in this way.''

Representative Dale E. Kildee, an anti-abortion Democrat from Michigan who
decided this week to support the Senate bill, said: ''I will be 81 years old in
September. Certainly at this point in my life, I'm not going to change my mind
and support abortion, and I'm not going to risk my eternal salvation.''

In a conference call on Thursday, Mr. Kildee told reporters, ''We must not lose
sight of what is at stake here: the lives of 31 million American children,
adults and seniors who don't have health insurance.''

The nonprofit group Catholics United has been running television advertisements
in the districts of anti-abortion Democrats like Mr. Stupak challenging the
notion that the legislation will force taxpayers to finance abortions. ''It's
not true,'' the ad says, mentioning the support for the bill from the Catholic
Health Association and Catholic hospitals.

Mr. Stupak hit back at the nuns on Thursday, saying they did not have much
influence.

''With all due respect to the nuns, when I deal or am working on right-to-life
issues, we don't call the nuns,'' he said on the MSNBC program ''Hardball.''

The divide between the nuns and the bishops reflects a larger one nationwide
among those who both oppose abortion and believe that the nation's health care
system must be overhauled.

''When I read the Gospel, where is Jesus? He's healing the lepers,'' Sister
Simone said. ''It's because of his Gospel mandate to do likewise that we stand
up for health care reform.''

As for Mr. Stupak, he is in a fair bit of trouble with nuns for his remarks.

''We have a number of nuns in his district, and they've been calling him,'' said
Sister Regina McKillip, a Dominican nun who lives in Washington. ''Who's been on
the ground, in the field? Who knows the struggles people have to deal with? It's
the sisters.''

URL: http://www.nytimes.com

LOAD-DATE: March 20, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Bart Stupak of Michigan annoyed some nuns who
back the Senate bill by suggesting they lacked influence. (PHOTOGRAPH BY CHIP
SOMODEVILLA/GETTY IMAGES)

PUBLICATION-TYPE: Newspaper


