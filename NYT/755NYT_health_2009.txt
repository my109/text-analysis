                   Copyright 2009 The New York Times Company


                             755 of 2021 DOCUMENTS


                               The New York Times

                             October 5, 2009 Monday
                              Late Edition - Final

A Billion Here, a Billion There

BYLINE: By EDUARDO PORTER

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL NOTEBOOK; Pg. 22

LENGTH: 449 words


Past the initial schadenfreude, it's hard to figure out what to think about the
shrinking of the nation's 400 most gilded fortunes. It is reassuring that the
super-rich can lose money too -- $300 billion in the last year, according to
Forbes, bringing their total down to $1.27 trillion. It's about the same
percentage that was lost by Americans' private pensions, whose assets dropped by
about $1.1 trillion, nearly 19 percent.

It can hardly hurt as much. Warren Buffett lost $10 billion but still has $40
billion. Kirk Kerkorian has $3 billion left, after losing $8.2 billion.
Citigroup founder Sanford Weill dropped off the billionaires list, but still has
many millions.

Every year as I get worked up over Forbes's latest billionaire review, I try to
convince myself that accumulation of wealth at the top can serve a social
function. I tell myself that inequality of income is a standard feature of
capitalism, pushing the best and brightest into the most profitable jobs. It
encourages people to study hard and work hard, or at least to become a banker.
Big financial rewards push people to excel, and thus the economy to grow.

But $1.27 trillion? That's a decade of health care reform in one of the more
expensive versions. This isn't garden-variety inequality -- this is a
winner-take-all deal that can destroy incentives for everyone except those in
the upper crust.

Lawrence Katz, a labor economist at Harvard, sensibly points out that one could
generate incentives to excel for less: ''I don't think the added incentive of
earning $100 million over $50 million is very different than the incentive of
making $10 million over $5 million,'' he told me once.

Maybe the jolt of billion-plus losses can spur plutocrats to change. Ralph Nader
just wrote a novel called ''Only the Super-Rich Can Save Us!'' in which Mr.
Buffett (already a major philanthropist), Ross Perot and a few other
billionaires go to Maui to ''redirect'' society onto the right path. Warren
Beatty gets to run California. Wal-Mart workers unionize. Corporate greed is
brought to heel.

There is no sign of such enlightenment on Wall Street. Financial markets are
back up; bankers are scouring the horizon for new opportunity. The hottest new
incomprehensible financial object is the ''re-remic,''  bundles of distressed
mortgages repackaged in a way that banks and insurers  can minimize how much
cash they must set aside in case the investments go south, again. Amid all this
it's hard to see how our oligarchs could be persuaded to restrain their
appetites.

Perhaps I'm being too pessimistic. We could promise that Mr. Nader wouldn't have
a say in the outcome. That would seem like a reasonable incentive.

URL: http://www.nytimes.com

LOAD-DATE: October 5, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


