                   Copyright 2009 The New York Times Company


                             1005 of 2021 DOCUMENTS


                               The New York Times

                            November 15, 2009 Sunday
                              Late Edition - Final

Quotation of the Day

BYLINE: STANLEY V. WHITE,

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 3

LENGTH: 79 words


We were approached by the lobbyist, who asked if we would be willing to enter a
statement in the Congressional Record. I asked him for a draft. I tweaked a
couple of words. There's not much reason to reinvent the wheel on a
Congressional Record entry.

STANLEY V. WHITE,  chief of staff for Representative Robert A. Brady of
Pennsylvania, one of dozens of lawmakers who used speeches ghost-written by a
biotechnology company during the health-care  debate in the House. [26]

URL: http://www.nytimes.com

LOAD-DATE: November 15, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Quote

PUBLICATION-TYPE: Newspaper


