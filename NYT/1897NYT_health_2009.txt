                   Copyright 2010 The New York Times Company


                             1897 of 2021 DOCUMENTS


                               The New York Times

                             April 18, 2010 Sunday
                              Late Edition - Final

New York Offers Costly Lessons On Insurance

BYLINE: By ANEMONA HARTOCOLLIS

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 1

LENGTH: 1451 words


When her small executive search firm in New York City canceled its health
insurance policy last year because of the recession and rising premiums, April
Welles was able to buy her own plan and still be covered for her cancer and
multiple sclerosis.

She was lucky to live in New York, one of the first states to require insurance
companies to offer comprehensive coverage to all people regardless of
pre-existing conditions. But Ms. Welles, 58, also pays dearly: Her premium is
$17,876 a year.

''That's a lot of groceries,'' she said.

New York's insurance system has been a working laboratory for the core provision
of the new federal health care law -- insurance even for those who are already
sick and facing huge medical bills -- and an expensive lesson in unplanned
consequences. Premiums for individual and small group policies have risen so
high that state officials and patients' advocates say that New York's extensive
insurance safety net for people like Ms. Welles is falling apart.

The problem stems in part from the state's high medical costs and in part from
its stringent requirements for insurance companies in the individual and small
group market. In 1993, motivated by stories of suffering AIDS patients, the
state became one of the first to require insurers to extend individual or small
group coverage to anyone with pre-existing illnesses.

New York also became one of the few states that require insurers within each
region of the state to charge the same rates for the same benefits, regardless
of whether people are old or young, male or female, smokers or nonsmokers, high
risk or low risk.

Healthy people, in effect, began to subsidize people who needed more health
care. The healthier customers soon discovered that the high premiums were not
worth it and dropped out of the plans. The pool of insured people shrank to the
point where many of them had high health care needs. Without healthier people to
spread the risk, their premiums skyrocketed, a phenomenon known in the trade as
the ''adverse selection death spiral.''

''You have a mandate that's accessible in theory, but not in practice, because
it's too expensive,'' said Mark P. Scherzer, a consumer lawyer and counsel to
New Yorkers for Accessible Health Coverage, an advocacy group. ''What you get
left clinging to the life raft is the population that tends to have pretty high
health needs.''

Since 2001, the number of people who bought comprehensive individual policies
through HMOs in New York has plummeted to about 31,000 from about 128,000,
according to the State Insurance Department.

At the same time, New York has the highest average annual premiums for
individual policies: $6,630 for single people and $13,296 for families in
mid-2009, more than double the nationwide average, according to America's Health
Insurance Plans, an industry group.

Rates did not rise as high in small group plans, for businesses with up to 50
workers, because the companies had an incentive to provide insurance to keep
employees happy, and so were able to keep healthier people in the plans, said
Peter Newell, an analyst for the United Hospital Fund, a New York-based health
care research organization.

While premiums for large group plans have risen, their risk pools tend to be
large enough to avoid out-of-control rate hikes.

The new federal health care law tries to avoid the death spiral by requiring
everyone to have insurance and penalizing those who do not, as well as offering
subsidies to low-income customers. But analysts say that provision could prove
meaningless if the government does not vigorously enforce the penalties, as
insurance companies fear, or if too many people decide it is cheaper to pay the
penalty and opt out.

Under the federal law, those who refuse coverage will have to pay an annual
penalty of $695 per person, up to $2,085 per family, or 2.5 percent of their
household income, whichever is greater. The penalty will be phased in from 2014
to 2016.

''In this new marketplace that we envision, this requirement that everybody be
covered, that should draw better, healthier people into the insurance pool,
which should bring down rates,'' said Mark Hall, a professor of law and public
health at Wake Forest University. But he added, ''You have to sort of take a
leap of faith that that's going to happen.''

As part of the political bargain to get insurance companies to support insurance
for all regardless of risk, called community rating, New York State deregulated
the market, allowing insurers to charge as much as they wanted within certain
profit margins. The state can require companies to retroactively refund
overcharges to consumers, but it seldom does.

Now, Gov. David A. Paterson has proposed to reinstate prior approval by the
state of rate increases for the small group and individual plans, as a way to
reverse New York's death spiral of healthy people fleeing the market. The change
would affect about 3 million of the 10 million New Yorkers insured through
private plans, according to the Insurance Department. Most of those are in small
group plans, though the biggest beneficiaries might be those seeking individual
coverage, where premiums are highest.

New York's insurance companies are vigorously fighting prior approval. Mark L.
Wagar, the president of Empire BlueCross BlueShield, said New York's problem was
not deregulation of rates, but the lack of an effective mandate for everyone to
buy insurance. To illustrate, he offered a statistic on how many people in the
18-to-26 age group, who are largely healthy, have bought individual insurance
coverage through his company: 88 people out of 6 million insured by his company
statewide.

New York is ''the bellwether,'' Mr. Wagar said. ''We have the federal health
reform on steroids in terms of richness and strictness.''

The federal health care overhaul contains some protection for people who buy
into the new insurance exchanges -- organized marketplaces -- created by the
law. Beginning in 2014, states will be able to recommend that the Department of
Health and Human Services ban companies from the exchanges if they impose rate
increases the states consider unreasonable.

Mr. Wagar also said that New York's medical costs, universally acknowledged as
being among the highest in the country, were a factor in its high premiums. He
noted that the state already regulated insurance company profit margins,
allowing them to allocate no more than 25 cents of every dollar for profits and
administration in small group plans and 20 cents for individual plans. The
governor is proposing to lower both margins to 15 percent.

Troy Oechsner, deputy superintendent for health at the State Insurance
Department, blamed the insurance companies for raising rates beyond what was
necessary -- by being off on their projections -- thus accelerating the exodus
of healthy people.

''What we saw them do is they really jacked up rates because they could,'' Mr.
Oechsner said.

To a large extent, insurance companies police themselves, according to Mr.
Oechsner. From 2000 to 2007, insurance plans reported that they exceeded state
profit allowances just 3 percent of the time, resulting in about $48 million in
refunds to policyholders, Mr. Oechsner said. Yet subsequent Insurance Department
investigations found that insurers should have refunded three times as much.

The governor's budget projects that reinstating prior approval would help the
state close its $9 billion deficit, saving taxpayers $70 million in the first
year, and $150 million after that, by stemming the exodus of people from
high-priced plans into state-subsidized plans.

An analysis of the governor's plan released recently by the Business Council of
New York State, whose membership includes insurance companies, contested the
governor's savings estimate, saying that it was ''at best speculative,'' and
that the savings would probably be nominal.

Mr. Hall, the Wake Forest professor, said that with the risk spread over a
bigger pool of insured people under federal changes, insurers would be expected
to reduce their prices, especially in New York. But Mr. Hall said that insurers
might hesitate to do that until they were sure people were going to buy
coverage, which could lead to a sort of mutual paralysis.

''You can literally think of people standing around a swimming pool, saying
let's jump in at once,'' he said.

As for Ms. Welles, she is not sure how much longer she can keep paying rising
rates.

''This is not something that will be sustainable for the rest of my life,'' she
said. On the other hand, she added, ''frankly, with the kind of cancer I have, I
don't think I'll be paying this for too many years.''

URL: http://www.nytimes.com

LOAD-DATE: April 18, 2010

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: A Spiral: As health insurance membership declines, premiums
increase, and the cycle reinforces itself. (Source: New York State Insurance
Department) (A20)

PUBLICATION-TYPE: Newspaper


