                   Copyright 2009 The New York Times Company


                             352 of 2021 DOCUMENTS


                               The New York Times

                           August 19, 2009 Wednesday
                              Correction Appended
                              Late Edition - Final

Tackling the Mystery Of How Much It Costs

BYLINE: By GINA KOLATA

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 1171 words


You go to a restaurant, peruse the menu, take your waiter's suggestions, and
order a meal. But there is something odd:  the menu has no prices and you have
no idea what you will be required to pay until a few weeks later when the bill
arrives in the mail.

That, it turns out, is analogous to what goes on in health care, where fees are
hidden at the time of service. Making matters even worse, patients often are
seeking care when they are frightened and vulnerable, in no position to ask
about prices or haggle.

For the most part, doctor fees are a mystery. If people see a doctor who is part
of their insurance network, they are responsible only for deductibles and
co-payments, and the price the health insurer pays is often a secret. And if
people see a doctor outside their network, they usually have no idea what the
charge will be -- even though they are responsible for most of it -- until the
bill comes.

''This is a huge part of why expenditures are so high and why they are rising,''
said Dr. Alan Garber, a health economist at Stanford University.  ''How can you
get a market to work if no one has any idea of what the prices are?''

But the health care legislation under discussion does not directly address the
out-of-network fee issue. And that is intentional, says Dr. Mark McClellan of
the Brookings Institution. Dr. McClellan, a former head of Medicare who works
closely with policy makers, says the goal of the House and Senate bills is to
encourage people to stay in their networks. He added that the result should be
networks that provide better care ''so that people don't have so much need to go
outside of them.''

Linda Douglass, communications director for the White House Office of Health
Care Reform, explained the legislation in an e-mail message: ''Under health
insurance reform, insurance plans will be required to give consumers much more
information about what is covered and what is not. They will be expected to warn
consumers that if they go out of network, they can be hit with some very high
costs.''

Legislation proposed by the House Committee on Energy and Commerce also says
that health insurers must pay a full out-of-network bill in one circumstance --
if an insurance network is ''inadequate,'' as determined by a state insurance
commissioner, and a patient goes outside that network for care unavailable
within the network.

Some prominent health economists and policy experts, however, say there should
be limits on how much doctors can charge for out-of-network care.

Mark A. Hall, a professor of law and public health at Wake Forest University,
for example, advocates restricting out-of-network fees to a fixed amount,
perhaps 150 percent of the amount Medicare would pay.

That is how the system works in Germany, says Uwe Reinhardt, a health economist
at Princeton University. Professor Reinhardt advocates a national law that caps
the maximum doctors can charge when they are out of a patient's network.

America's Health Insurance Plans, which represents health insurers, is also
trying to draw attention to out-of-network doctors' fees. Last Tuesday, the
group released results of its own survey to show how high such fees can go.   It
included, for example, a patient in Colorado who was charged $26,000 for gall
bladder surgery, compared with Medicare's fee of $681, and a patient in
California who was billed $15,870 for cataract surgery for which Medicare pays
$638.

One company, Ventura, in San Francisco hopes to help people estimate ahead of
time what prices have been negotiated within insurance networks and how much
they would pay to go out of network.

''Providers have no interest in disclosing what their actual price is,'' said
Ventura's  founder, Dr. Giovanni Colella, a San Francisco doctor and
businessman.

Dr. Colella added that the issue of varying prices was not just a problem for
patients but also for insurers who negotiate with doctors and hospitals.

''There is the retail price and the negotiated price, and the negotiated price
stays secret,'' Dr. Colella said. And the negotiated price -- the amount doctors
agree to accept from insurers -- can vary enormously, for the same procedure in
the same geographical area. For example, the negotiated price for a colonoscopy
in San Francisco ranges from $700 to $7,000, Dr. Colella said.

Even well-insured and sophisticated health economists, like Dr. Garber, an
adviser to Dr. Colella's company, can be hit by unexpected fees. When his wife
had a baby, an anesthesiologist spent a few minutes administering an epidural
anesthetic. Dr. Garber assumed the doctor was part of his insurance plan -- the
hospital participated in the plan and the doctor had not said he did not. But
then the bill came for a few thousand dollars, and it was up to Dr. Garber and
his wife to pay it.

It would not have helped for the doctor to disclose his fee, though, Dr. Garber
said.

''With my wife in pain, I don't think I could have said, 'Hold off, I'm going to
call around to find another anesthesiologist,' '' he said. ''I would have had to
find an anesthesiologist who was available, in-network, and with privileges in
the same hospital.''

''Hospitals should warn that some of the hospital-based physicians do not
participate in the networks with them,'' Dr. Garber  said, and let patients line
up with a doctor who does participate.

Mr. Hall says his research shows that when people go to doctors and hospitals
that are not part of their insurance network, they can expect charges that are
double, triple, even quadruple the negotiated price within networks. And that,
he said, is ''price gouging,  exploiting market power to charge prices virtually
unrelated to actual cost or market value,'' and is a factor in what drives
people into medical bankruptcy.

The American Medical Association said the fundamental problem was that health
insurers often paid doctors too little and made it too burdensome to get
payments at all, driving doctors outside insurance networks.

But if patients want to pay more to see a doctor outside their network, that
should be their choice, said Dr. Robert M. Wah, a spokesman for the medical
association.

''We're in America, where people pay for things they value,'' Dr. Wah explained.

Jonathan Gruber, a health economist at M.I.T.,  says, however,  that it makes
more sense to encourage people to stay in networks.

If health care overhaul accomplishes its goals of encouraging medical care that
is not excessive and is based on the best evidence of what works, prices and
quality can be kept under control for patients in insurance networks, Dr. Gruber
said.

Some may want to pay anyway for an out-of-network doctor, and that is fine, he
said. ''If you want to go outside your network, God bless you,'' he said. ''It's
the American way.''

But the only way to fix the system, Dr. Gruber said, is to make the networks
better so that people will stay in them and then, most patients, knowing what it
will cost them to leave their networks, will decide not to.

URL: http://www.nytimes.com

LOAD-DATE: August 19, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: August 20, 2009



CORRECTION: An article on Wednesday about the hidden fees in health care
misstated the name of a company in San Francisco that hopes to help people
estimate ahead of time what prices have been negotiated and what they would pay
for services outside an insurance network. It is Ventana, not Ventura.

GRAPHIC: PHOTOS: Dr. Mark McClellan calls for better in-network health care.
(PHOTOGRAPH BY MANNIE GARCIA/REUTERS)
 Jonathan Gruber says price and quality can be controlled. (PHOTOGRAPH BY ERIK
JACOBS FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


