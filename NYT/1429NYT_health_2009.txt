                   Copyright 2010 The New York Times Company


                             1429 of 2021 DOCUMENTS


                               The New York Times

                            January 26, 2010 Tuesday
                              Late Edition - Final

Obama to Seek Spending Freeze To Trim Deficits

BYLINE: By JACKIE CALMES; David M. Herszenhorn contributed reporting from
Washington.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1194 words

DATELINE: WASHINGTON


President Obama will call for a three-year freeze in spending on many domestic
programs, and for increases no greater than inflation after that, an initiative
intended to signal his seriousness about cutting the budget deficit,
administration officials said Monday.

The officials said the proposal would be a major component both of Mr. Obama's
State of the Union address on Wednesday and of the budget he will send to
Congress on Monday for the fiscal year that begins in October.

The freeze would cover the agencies and programs for which Congress allocates
specific budgets each year, including air traffic control, farm subsidies,
education, nutrition and national parks.

But it would exempt security-related budgets for the Pentagon, foreign aid, the
Veterans Administration and homeland security, as well as the entitlement
programs that make up the biggest and fastest-growing part of the federal
budget: Medicare, Medicaid and Social Security.

The payoff in budget savings would be small relative to the deficit: The
estimated $250 billion in savings over 10 years would be less than 3 percent of
the roughly $9 trillion in additional deficits the government is expected to
accumulate over that time.

The initiative holds political risks as well as potential benefits. Because Mr.
Obama plans to exempt military spending while leaving many popular domestic
programs vulnerable, his move is certain to further anger liberals in his party
and senior Democrats in Congress, who are already upset by the possible collapse
of health care legislation and the troop buildup in Afghanistan, among other
things.

Fiscally conservative Democrats in the House and Senate have urged Mr. Obama to
support a freeze, and it would suggest to voters, Wall Street and other nations
that the president is willing to make tough decisions at a time when the deficit
and the national debt, in the view of many economists, have reached levels that
undermine the nation's long-term prosperity. Perceptions that government
spending is out of control have contributed to Mr. Obama's loss of support among
independent voters, and concern about the government's fiscal health could put
upward pressure on the interest rates the United States has to pay to borrow
money from investors and nations, especially China, that have been financing
Washington's budget deficit.

Republicans were quick to mock the freeze proposal. ''Given Washington
Democrats' unprecedented spending binge, this is like announcing you're going on
a diet after winning a pie-eating contest,'' said Michael Steel, a spokesman for
the House Republican leader, Representative John A. Boehner of Ohio.

The spending reductions that would be required would have to be agreed to by
Congress, and it is not clear how much support Mr. Obama will get in an election
year when the political appeal of greater fiscal responsibility will be vying
with the pressure to provide voters with more and better services. The
administration officials said the part of the budget they have singled out --
$447 billion in domestic programs -- amounts to a relatively small share, about
one-eighth, of the overall federal budget.

But given the raft of agencies and programs within that slice, the reductions
will mean painful reductions that will be fought by numerous lobbies and
constituent groups. And not all programs will be frozen, the administration
officials said; many will be cut well below a freeze or eliminated to provide
increases for programs that are higher priorities for the administration in
areas like education, energy, the environment and health.

The balancing act of picking winners and losers was evident on Monday at the
White House. Mr. Obama and Vice President Joseph R. Biden Jr. outlined a number
of new proposals that will be in the budget to help the middle class. They cover
issues including child care, student loans and retirement savings.

Administration officials also are working with Congress on roughly $150 billion
in additional stimulus spending and tax cuts to spur job creation. But much of
that spending would be authorized in the current fiscal year, the officials
said, so it would not be affected by the proposed freeze that would take effect
in the fiscal year beginning Oct. 1.

It is the growth in the so-called entitlement programs -- Medicare, Medicaid and
Social Security -- that is the major factor behind projections of unsustainably
high deficits, because of rapidly rising health costs and an aging population.

But one administration official said that limiting the much smaller
discretionary domestic budget would have symbolic value. That spending includes
lawmakers' earmarks for parochial projects, and only when the public believes
such perceived waste is being wrung out will they be willing to consider
reductions in popular entitlement programs, the official said.

''By helping to create a new atmosphere of fiscal discipline, it can actually
also feed into debates over other components of the budget,'' the official said,
briefing reporters on the condition of anonymity.

The administration officials did not identify which programs Mr. Obama would cut
or eliminate, but said that information would be in the budget he submits next
week. For the coming fiscal year, the reductions would be $10 billion to $15
billion, they said. Last year Mr. Obama proposed to cut a similar amount --
$11.5 billion -- and Congress approved about three-fifths of that, the officials
said.

The federal government's discretionary domestic spending has grown about 5
percent on average since 1993, according to the administration. It spiked to
about 27 percent from  2008 to  2009, however, because of the recession. The
sudden increase reflected both the first outlays from the $787 billion stimulus
package as well as automatic spending for unemployment compensation and food
stamps that is triggered during an economic downturn.

The freeze that Mr. Obama will propose for the fiscal years 2011 through 2013
actually means a cut in real terms, since the affected spending would not keep
pace with inflation.

According to the administration, by 2015 that share of the federal budget will
be at its lowest level in a half-century relative to the size of the economy.

''A lot of our caucus won't like it but I don't think we have any choice,'' said
an adviser to Congressional Democratic leaders, who would only speak on
condition of anonymity about internal party deliberations. ''After Massachusetts
and all the polls about independents' abandoning us for being fiscally
irresponsible, we can't afford to be spending more than Obama.''

While the Democrats' unexpected loss of a Massachusetts Senate seat in a special
election last week gave new impetus to administration efforts to tackle the
deficit, those efforts actually have been under way since last fall, when
officials began early work on the 2011 budget.

Mr. Obama's budget director, Peter R. Orszag, initially directed Cabinet
secretaries and agency heads to propose alternative budgets -- one with a freeze
and another that cut spending by 5 percent. Months of internal arguments and
appeals followed.

URL: http://www.nytimes.com

LOAD-DATE: January 26, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Days before his address to the nation, President Obama spoke to
the Middle Class Task Force on Monday at the White House.(PHOTOGRAPH BY DOUG
MILLS/THE NEW YORK TIMES)(A3)

PUBLICATION-TYPE: Newspaper


