                   Copyright 2010 The New York Times Company


                             1693 of 2021 DOCUMENTS


                               The New York Times

                             March 19, 2010 Friday
                              Late Edition - Final

On the Verge of Reform

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 24

LENGTH: 656 words


The best chance in decades of fixing this country's broken health care system
has now come down to whether the House's Democratic majority will approve the
already strong Senate version of the bill -- with a promise of some changes.

On Thursday, Speaker Nancy Pelosi unveiled a package of amendments that would,
on balance, make the Senate bill better. House Democrats should do what is best
for all Americans and approve it.

They can then challenge the Republicans -- who are refusing as a bloc to support
reform -- to explain in this fall's campaign why they are so devoted to a status
quo that denies affordable health care to tens of millions of uninsured
Americans and means that millions more Americans could be just one layoff away
from losing their coverage.

Even without amendments, the Senate bill is well worth passing. It would cover
some 31 million of the uninsured by 2019, force the insurance companies to stop
refusing coverage or charging high rates based on pre-existing conditions,
reduce deficits over the next two decades, and make a start at controlling the
costs of both insurance and medical care.

The amendments would first be voted on in the House and, if the package passes,
in the Senate under reconciliation rules that require only a majority. They
would make a strong bill stronger.

According to the official scoring by the Congressional Budget Office, it would
cover slightly more of the uninsured. And it would cut deficits more sharply --
by $138 billion over the first 10 years compared with $118 billion under the
Senate bill, and by even greater amounts in the second decade. It would
accomplish both of those with some additional, but still reasonable fees, taxes
and Medicare savings.

Struggling to comply with the very stringent rules governing reconciliation
bills, the Democrats made some reasonable trade-offs between enhancing benefits
and finding additional savings and revenues.

Their package would increase the Senate bill's subsidies to help low-  and
middle-income people pay for insurance in the initial years, scaling them back
in more distant years. It would do a bit more to help close a gap in Medicare
coverage for prescription drugs (the so-called doughnut hole), a welcome benefit
for chronically ill beneficiaries who run up big drug costs. It would also
remove some of the noxious special deals inserted in the Senate bill to win the
support of fence-sitting senators.

It is disappointing that the reconciliation package would postpone for several
years the strongest cost-cutting feature in the Senate bill: an excise tax on
insurers who issue high-cost policies. But the retreat is not disabling. The
main cost-cutting feature -- an annual index that would subject more and more
plans to the tax and encourage workers and employers to find cheaper coverage --
has actually been strengthened.

To make up for the loss of revenue from the excise tax and help cover the
enhanced benefits, the reconciliation package would extend the Medicare payroll
tax for high-income Americans to include investment income, putting the burden
on people who can clearly afford it. And it would squeeze even more savings out
of the private Medicare Advantage plans that receive heavy and clearly unfair
subsidies to participate in Medicare.

All told, these and other modest changes to the Senate bill look reasonable.
Liberals in Congress need to recognize that this is about the most generous
package of benefits that can possibly be passed in the midst of recession. And
fiscal hawks need to recognize that this bill would reduce deficits more than
any in recent years.

Like most historic legislation, the process leading up to these last votes has
been agonizingly long, relentlessly frustrating, and far too easy for opponents
to misrepresent and demagogue. But a willingness to compromise and good sense
should prevail. The country needs comprehensive health care reform.

URL: http://www.nytimes.com

LOAD-DATE: March 19, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


