                   Copyright 2010 The New York Times Company


                             1494 of 2021 DOCUMENTS


                               The New York Times

                          February 10, 2010 Wednesday
                              Late Edition - Final

G.O.P. Unity? Not So Fast

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 378 words


To many Americans who watched the Democrats' family feud over the last few
months, it might have seemed as if Republicans were more unified on health care.
One luxury of being the minority party in Congress is the ability to avoid
having internal disagreements in the spotlight.

In reality, Republicans generally do not speak with a single voice on health
care policy, any more than Democrats do. Consider the tax treatment of
employer-sponsored health benefits.

Americans who buy health insurance on their own typically pay full price, while
those who get coverage through an employer get a nice tax break. Many
Republicans favor ending the ''exclusion'' of employer-provided health benefits
from taxable income, which they say is unfair and creates a perverse incentive
for overspending on health care. Many economists agree.

Senator John McCain, Republican of Arizona, top  right, made ending the
exclusion a pillar of his health care plan in the 2008 presidential campaign. He
proposed replacing it with a uniform tax credit. Many Republicans in Congress
have put forward similar plans, and in many instances those credits would be
smaller than the value of benefits now shielded from income taxes, which is why
such plans help lower the deficit.

But Representative Dave Camp of Michigan, above  right, the senior Republican on
the tax-writing Ways and Means Committee, is opposed to ending the exclusion,
saying it would be too disruptive.

Mr. Camp ''believes we need to improve the current system, not upend the way 85
percent of the country receives their health care benefits, which is what taxing
employer-provided plans would do,'' said a spokesman, Sage Eastman.

Mr. Camp was a principal author of the alternate health care legislation that
House Republican leaders put forward in November, which was easily defeated.

While the nonpartisan Congressional Budget Office has projected that the
Republican alternative would slightly reduce the cost of premiums, the budget
office also says the proposal would do little to cover the uninsured.

Republican leaders nonetheless believe that a scaled-back approach is needed,
and they are expected to push for it at President Obama's bipartisan summit
meeting on health care on Feb. 25. DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: February 10, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS

PUBLICATION-TYPE: Newspaper


