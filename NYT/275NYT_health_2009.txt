                   Copyright 2009 The New York Times Company


                             275 of 2021 DOCUMENTS


                               The New York Times

                            August 8, 2009 Saturday
                              Late Edition - Final

August Is The Cruelest Month

BYLINE: By GAIL COLLINS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 19

LENGTH: 816 words


Protesters are following members of Congress around this summer, disrupting
their constituent meetings and shrieking about socialized medicine. They claim
to be following the great American tradition of dissent. This must refer to the
time back in colonial days when our founding fathers disguised themselves as
Indians and broke up a public hearing on a plan for national health care, which
involved regulating the price of leeches.

If nothing else, they're certainly scaring the heck out of the vacationing
legislators. In the state of Washington, Representative Brian Baird announced
that rather than risk an ''ambush,'' he was canceling his traditional
get-togethers with constituents and replacing them with ''telephone town
halls.'' Voters will get an automated call asking them to press various buttons
if they want to ask their congressman a question. His local newspaper, The
Columbian, reported that, meanwhile, Baird would be ''sitting at his own
telephone at an as-yet-undisclosed location'' listening and choosing which
queries he wanted to answer.

This does not actually seem like a good plan. True, some of the protesters have
been rowdy, but not to such an extent that the nation's elected officials need
to hide behind telephone trees. Here in New York, the guy who plays the sexy
vampire in the ''Twilight'' movies has been shooting a film, and he appears to
be facing far more daily danger of death-by-mob-hysteria than any member of the
House Committee on Transportation and Infrastructure.

Speaking of bad plans, the White House has been urging the Democrats to rally
their own forces of placard-waving, sweaty, yelling supporters to confront the
crazies. This makes no sense at all. It's not often that members of Congress
look as sympathetic as they've been lately on YouTube, surrounded by loud and
unlovable hecklers. In fact, the best chance for health care reform may be to
sell it as the thing that those people pounding on the doors of a town meeting
in Tampa and screaming at the fire marshals don't want.

Advocates need a better story line given what's happened to health care over the
last few weeks. The conservative Blue Dog Democrats are boasting that they
killed the public plan section of the House health care bill, which some liberal
Democrats think is the most important part. The White House has admitted that
the pharmaceutical industry's much-touted pledge of $80 billion in health care
savings over 10 years was made in return for a promise that there would not be
anything else in the legislation to threaten its profits.

''Eighty billion is the max --  no more or less,'' said Billy Tauzin, the chief
lobbyist for the drug makers. In a past incarnation, he was the congressman who
shepherded the Bush plan for Medicare coverage of prescription drugs through the
House, in a form that was very, very kind to his future employers. He was also
--  is this a world of coincidences or what?  --  a co-founder of the Blue Dog
Democrats.

The drug deal, although blessed by the White House, actually emerged from the
black hole which is otherwise known as the Senate Finance Committee. The
chairman is Max Baucus of Montana, whose powers and responsibilities on health
care are so great that they require the political genius and moral fiber of a
Rocky Mountain Lincoln. Unfortunately, nobody has ever once described Baucus in
anything remotely close to those terms.

Baucus has set up a special bipartisan negotiating committee on health care with
his pal Chuck Grassley, the Iowa Republican. He is hoping that if Grassley signs
off on a bill, other members of his party will follow. Grassley has said he will
not sign off on anything unless a whole lot of other Republicans accept it
first.

You will notice that these two plans are not really the same.

The senators on this special committee hail from Montana, North Dakota, New
Mexico, Iowa, Maine and Wyoming. This was quite a coup on Baucus's part, since
you have to work really hard to put together six states that represent only 2.77
percent of the population.

The Finance Six plan to keep doing whatever it was they were up to in July
throughout the August vacation. On Thursday, they met with President Obama for
an hour. Senator Olympia Snowe, a Maine Republican, told The Washington Post
that she urged him to get out and sell the health care program and assure the
people that the ''doctor-patient relationship is going to be preserved.''

Given the fact that Obama has spent the last few months saying this about every
15 seconds, he must have found the conversation somewhat frustrating. Also,
Snowe is a terrific senator, but have you ever heard her give a speech? If we
have come to the point where she is giving the president tips on how to
communicate, we are in trouble.

There's no time to lose. Schedule a lot of Congressional town halls fast, and
send Rush Limbaugh the list.

URL: http://www.nytimes.com

LOAD-DATE: August 8, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


