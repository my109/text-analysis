                   Copyright 2009 The New York Times Company


                             193 of 2021 DOCUMENTS


                               The New York Times

                            July 22, 2009 Wednesday
                              Late Edition - Final

Obama Wins Crucial Round In Senate Vote Against F-22

BYLINE: By CHRISTOPHER DREW

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 968 words

DATELINE: WASHINGTON


With some of his political capital on the line, President Obama won a crucial
victory on Tuesday when the Senate voted to strip out $1.75 billion in financing
for seven more F-22 jet fighters from a military authorization bill.

The president had repeatedly threatened to veto the $679.8 billion bill if it
included any money for the planes. The 58-to-40 vote clearly gives the Obama
administration more leeway to overhaul military spending.

The F-22, the world's most advanced fighter, has been a flashpoint in a battle
over the administration's push to shift more of the Pentagon's resources away
from conventional warfare projects, like the F-22, to provide more money for
fighting insurgencies.

Senate aides said that some Democrats who otherwise might have voted for more
planes sided with the president out of concern that a loss could have hurt him
in the fight for health care reform.

''The president really needed to win this vote,'' Senator Carl Levin, a Democrat
from Michigan who led the fight to cut financing for the plane, said after the
vote.

Lockheed Martin, the prime contractor for the F-22, has estimated that work on
the plane provides 25,000 jobs and indirectly supports about 70,000 others. But
Robert M. Gates, the defense secretary, has said that the Pentagon needs to
accelerate a new plane, the F-35, and that doing so would offset the job losses.

About 1,000 suppliers in 44 states provide the jobs, which will gradually be
phased out as some of the 187 F-22s that have been ordered are completed.

About two-thirds of the jobs are in California, Texas, Georgia, Washington and
Connecticut. Several large unions who supported Mr. Obama in his campaign for
the presidency, back building more planes.

All four senators from California and Washington are Democrats, and they all
voted in favor of preserving the money for more planes.

The senators from Connecticut -- Senator Christopher J. Dodd, a Democrat, and
Senator Joseph  Lieberman, a former Democrat who is now an independent -- also
voted to keep the money in the budget, as did the four Republican senators from
Georgia and Texas.

Senator John Kerry of Massachusetts, the Democratic presidential nominee in
2004, had long supported the plane, partly because of jobs in Massachusetts, but
he voted on Tuesday to strip out the money. Senator Edward M. Kennedy of
Massachusetts, another senior Democrat who also had supported the plane, is
battling brain cancer and did not vote Tuesday.

Immediately after the vote, Mr. Obama praised the Senate's decision, saying that
any money spent on the fighter was an ''inexcusable waste'' -- and that by
following his lead the Senate had demonstrated a commitment to changing
Washington's ingrained habits.

He also received crucial support from his Republican rival in last year's
presidential election, Senator John McCain of Arizona, who co-sponsored an
amendment with Mr. Levin to remove the $1.75 billion from the bill.

Mr. McCain told reporters after the vote that the result was ''definitely
attributable'' to the strong push by the president and Mr. Gates.

Mr. McCain added that the vote ''really means there's a chance of us changing
the way we do business in Washington,'' particularly in terms of Pentagon
contracting.

Mr. Levin said that Mr. Gates and the White House chief of staff, Rahm Emanuel,
made phone calls to influential senators to rally support.

''This was a very significant decision that the Senate made after a very, very
tough battle,'' Mr. Levin said.

Despite Mr. Obama's veto threat, the Armed Services Committee had set the stage
for Tuesday's decision by voting 13 to 11 in late June to shift the $1.75
billion from other parts of the Pentagon's budget for 2010 to add the seven
planes to the 187 that have been built or ordered.

The House has also voted to keep the plane alive by authorizing $369 million to
buy advanced parts for 12 more F-22s. Ultimately, a conference committee will
decide the next step.

And even though Tuesday's vote represented a test of strength for the White
House, the issue could potentially resurface in the Senate. Senator Daniel
Inouye, a Democrat from Hawaii and the chairman of the Senate Appropriations
Committee, was among the Democrats who voted on Tuesday to preserve the money
for the planes, and his panel will be putting together a separate military
appropriations bill soon.

Senator Saxby Chambliss, a Republican from Georgia who led the fight to save the
plane, said after the vote that he was disappointed. But he added that he hoped
a law banning exports of the plane might be changed to allow it to be sold to
allies like Japan. Lockheed assembles the planes in Marietta, Ga.

Still, military analysts said supporters had made much more progress in saving
the F-22 than most experts had expected when Mr. Gates announced plans in April
to cancel it and other major weapons systems.

Critics have long portrayed the F-22 as a cold war relic. The plane was designed
in the late 1980s and can perform tactical operations at higher altitudes than
other fighters. It can cruise at supersonic speeds without using telltale
afterburners, and it has a stealthy skin that scatters radar detection signals.
Proponents see it as a form of insurance against possible wars with countries
like China.

But the F-22 has never been used in war, and the Pentagon's focus has shifted to
simpler weapons needed in Iraq and Afghanistan.

Air Force leaders recently agreed that they could make do with the F-22s already
built or ordered, instead of the 381 that the service had sought.

Mr. Gates has said a new fighter, the F-35, is better designed to attack ground
targets. The plane will be used by the Navy, the Marine Corps and the Air Force,
and the Pentagon plans to buy more than 2,400 of them.

URL: http://www.nytimes.com

LOAD-DATE: July 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Senator Carl Levin, left, and Senator John McCain after the
Senate vote Tuesday. Mr. McCain said the vote meant ''there's a chance of us
changing the way we do business in Washington.''(PHOTOGRAPH BY STEPHEN
CROWLEY/THE NEW YORK TIMES)
 The F-22 can perform tactical operations at higher altitudes than other
fighters. It can cruise at supersonic speeds without using afterburners and has
a skin that scatters radar signals.(PHOTOGRAPH BY YURIKO NAKAO/REUTERS)(pg. B4)

PUBLICATION-TYPE: Newspaper


