                   Copyright 2010 The New York Times Company


                             1431 of 2021 DOCUMENTS


                               The New York Times

                           January 27, 2010 Wednesday
                              Late Edition - Final

A Big Point of Conflict

BYLINE: By MICHELLE ANDREWS

SECTION: Section A; Column 0; National Desk; CAPS ON COVERAGE; Pg. 17

LENGTH: 347 words


Though it may be the only way forward for President Obama's domestic priority,
it  seems increasingly unlikely that House Democrats will be persuaded to pass
the Senate health care legislation.

The House and Senate bills are alike in many ways, but Speaker Nancy Pelosi has
said there were Senate provisions that many House Democrats simply could not
swallow, provisions that might fuel voter outrage of the sort so vividly on
display in Massachusetts last week.

One little-discussed but instructive example is caps on insurance coverage.

Many health plans limit the total amount an insurer will pay for medical care
annually or over the policyholder's lifetime. It is an industry practice that
angers many consumers, and one of many things a health care overhaul must
address. But the two houses diverge wildly on it.

The House bill would eliminate lifetime caps in all individual and group
insurance plans starting in 2010 and eliminate annual caps in 2013.

The Senate bill, on the other hand, would eliminate lifetime limits for new
health plans six months after the law's enactment. Annual limits would be
prohibited for new plans starting in 2014 and allowed only on a restricted basis
before then.

Current plans would get a pass under the Senate bill. Health plans that people
already belong to at the time of enactment would be permanently exempt from the
requirement to eliminate lifetime and annual caps.

Advocates for patients with rare and chronic diseases are concerned that this
exemption would allow existing health plans to avoid new rules prohibiting
lifetime and annual limits on the dollar value of coverage. Indeed, since most
plans change some details of their coverage every year, it is not even clear
what would constitute a ''new'' plan.

But perhaps this is the real dilemma for House Democrats: In 2009, 20,000 to
25,000 people lost health insurance because they exceeded their lifetime limits,
according to a PricewaterhouseCoopers analysis. Without some change, the figures
are predicted to rise to roughly 300,000 people in 2019. MICHELLE ANDREWS

URL: http://www.nytimes.com

LOAD-DATE: January 27, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


