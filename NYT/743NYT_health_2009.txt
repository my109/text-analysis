                   Copyright 2009 The New York Times Company


                             743 of 2021 DOCUMENTS


                               The New York Times

                            October 3, 2009 Saturday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 848 words


International

RELIEF IS SLOW TO ARRIVE TO INDONESIA QUAKE VICTIMS

The situation in hundreds of villages is still unknown after an earthquake
struck off Indonesia's western coast on Wednesday. Nearly 3,000 people might
still be trapped under rubble, the authorities said.  PAGE A4

IRISH VOTE AGAIN ON TREATY

After rejecting it last year, Ireland voted again on the Lisbon Treaty, which
aims to smooth the European Union's operations. Exit polls appeared to reinforce
the expectation that the treaty would pass. PAGE A5

OBAMA MEETS COMMANDER

President Obama met in Copenhagen with Gen. Stanley A. McChrystal, his top
commander in Afghanistan, to discuss strategy and a proposed troop buildup. PAGE
A6

STUDENTS ARRESTED IN IRAN

Authorities arrested 18 student leaders in Tehran. The arrests were part of a
crackdown on anti-government demonstrations, which flared up at two universities
as classes resumed this week. PAGE A8

STILL NO RIGHTS IN HONDURAS

While the post-coup president of Honduras spent the week promising to lift his
order suspending civil liberties, his security forces were busy enforcing it.
PAGE A8

MUDSLIDES KILL 20 IN SICILY

Poor drainage caused by illegal construction was blamed for mudslides that
killed at least 20 people in eastern Sicily, and left hundreds of people
homeless. PAGE A10

Burmese Dissidents Ponder Vote A8

Obituaries

MAREK EDELMAN, 90

A cardiologist, he was the last surviving commander of the 1943 Warsaw ghetto
uprising against the Germans. PAGE A19

National

FINANCE PANEL WRAPS UP WORK ON HEALTH CARE BILL

After a marathon session that ran well past midnight, the Senate Finance
Committee completed work on the final amendments to health care legislation.
PAGE A16

A STUDY OF FIRE'S EFFECTS

The fire that burned nearly a quarter of the Angeles National Forest is finally
just about out. Now, firefighters are being replaced by scientists investigating
the increasing frequency and size of fires. PAGE A11

CLIMATE BILL CALLED UNLIKELY

President Obama's top aide on climate and energy said there was virtually no
chance a climate and energy bill would be passed before negotiations on an
international climate treaty begin in December. PAGE A11

RED CROSS TO HOLD AUCTION

The American Red Cross -- in an effort to close a $50 million budget shortfall
-- will auction off items packed away over many years in a warehouse in Lorton,
Va. PAGE A11

WALRUSES HURT AS ICE ERODES

Half a century after Pacific walruses began recovering from industrial-scale
hunting, marine biologists are worried they face a growing threat in global
warming. PAGE A15

WATER OFFICIAL QUITS

The head of the Los Angeles water department -- who has faced criticism over
several issues -- resigned to join the Clinton Climate Initiative. PAGE A15

New York

A NEW WELLNESS POLICY BANS BAKE SALES IN SCHOOL

In an effort to limit how much sugar and fat students put in their bellies at
school, New York City's Education Department has effectively outlawed most bake
sales. PAGE A17

Fashion

SOME LOOKS DON'T NEED FIXING

Bygone fashion labels have been reinvented often at Fashion Week in Paris. This
week was no exception: Bernhard Willhelm, Nina Ricci and Rick Owens already
followed the famous-labels-starting-over formula. Review by Eric Wilson. PAGE
A22

Business

KING SALMON FISHING BAN STRAINS ALASKAN VILLAGES

A total ban on commercial fishing for king salmon on the Yukon River in Alaska
has strained communities and stripped the fish off menus in the lower 48 states.
PAGE B1

SEEING FLAT HOLIDAY SALES

The retail industry expects this holiday shopping season will be as bad as last
-- which is to say, very bad. Still, many retailers say they would be relieved
by flat sales. PAGE B1

TOYOTA PRESIDENT APOLOGIZES

A little more than three months after becoming the president of Toyota, Akio
Toyoda delivered a litany of mea culpas. PAGE B1

BOTOX LAWSUIT CITES SPEECH

Allergan, maker of the antiwrinkle shot Botox, has filed a lawsuit against the
government, charging that restrictions on promoting unapproved drug uses
violates its free-speech rights. PAGE B3

Sports

LAWMAKER CALLS FOR HEARING ON HEAD INJURIES IN FOOTBALL

Representative John Conyers Jr., a Michigan Democrat and chairman of the House
Judiciary Committee, said the committee planned to hold hearings on the impact
of head injuries sustained by National Football League players. PAGE B12

Jaguars Become a Civic Concern B12

Arts

CARNEGIE'S SEASON OPENS AFTER LAST-MINUTE SCRAMBLE

With James Levine injured, the Boston Symphony Orchestra had to hustle to open
the 119th season at Carnegie Hall with a suitable substitute: the Italian
conductor Daniele Gatti. PAGE C1

NEW LEVELS OLD IN GERMANY

A plan to build a giant new railway station in Stuttgart -- which would require
trashing an architectural landmark -- joins a growing list of projects that
smooth over unpleasant historical truths, Nicolai Ouroussoff writes. PAGE C1

New York Art Book Fair Bustles C1

Op-ed

BOB HERBERT PAGE A21

GAIL COLLINS PAGE A21

CHARLES M. BLOW PAGE A21

URL: http://www.nytimes.com

LOAD-DATE: October 3, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


