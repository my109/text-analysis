                   Copyright 2009 The New York Times Company


                             612 of 2021 DOCUMENTS


                               The New York Times

                          September 17, 2009 Thursday
                              Late Edition - Final

Fighting for Industry

BYLINE: By BARRY MEIER

SECTION: Section A; Column 0; National Desk; POLITICS STILL LOCAL; Pg. 23

LENGTH: 229 words


During the long, tortured battle to win his seat as Minnesota's junior senator,
Al Franken received an unusually large number of campaign contributions from
fellow comedians and actors, compared with the  corporate donations candidates
traditionally rely upon.

But Mr. Franken, a Democrat, has apparently quickly learned one of the first
rules of incumbency: protect local industries.

In a letter on Tuesday that was also signed by three other senators from states
that are home to medical device makers, Mr. Franken told the Senate Finance
Committee chairman, Max Baucus, Democrat of Montana, that he opposed a provision
in his health care proposal that would impose $4 billion in annual fees on the
makers of products like heart pacemakers and artificial hips.

The others signing the letter were Senator Amy Klobuchar, also a Minnesota
Democrat, and Indiana's senators,  Richard G.  Lugar, a Republican, and Evan
Bayh, a Democrat. Indiana is  home to several producers of orthopedic implants,
while Minnesota is home to three big device makers: Medtronic, St. Jude Medical
and Boston Scientific.

''We believe that the medical device industry contributes to improved quality
and choice for patients, and we are concerned that this tax will stifle
technological innovations that can improve patient outcomes and lower health
care costs,'' the letter said. BARRY MEIER

URL: http://www.nytimes.com

LOAD-DATE: September 17, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


