                   Copyright 2009 The New York Times Company


                             1231 of 2021 DOCUMENTS


                               The New York Times

                            December 21, 2009 Monday
                              Late Edition - Final

In the Senate, Ill Effects From a Debate About Health

BYLINE: By CARL HULSE and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 1072 words

DATELINE: WASHINGTON


Nasty charges of bribery. Senators cut off midspeech. Accusations of politics
put over patriotism. Talk of double-crosses. A nonagenarian forced to the floor
after midnight for multiple procedural votes.

In the heart of the holiday season, Senate Republicans and Democrats are at one
another's throats as the health care overhaul reaches its climactic votes. A
year that began with hopes of new post-partisanship has indeed produced change:
Things have gotten worse.

Enmity and acrimony are coursing through a debate with tremendous consequences
for both sides as well as for the legislative agenda in the months ahead.

Should Democrats prevail, it will put an exclamation point on an eventful first
year of their control of Congress and the White House and leave Republicans on
the Napoleonic side of what one predicted could be President Obama's Waterloo. A
Republican victory would invigorate an opposition party that was back on its
heels at the beginning of 2009 and would strike a crushing blow to Democrats and
their claims to governing.

The toxic atmosphere is evident on the floor, on television talk shows and in
the hallways of the Capitol. Despite the fact that Democrats appear to have the
60 votes in hand to push through their legislation, Republicans say they intend
to force a series of six procedural showdowns that would keep the Senate in
session right through Christmas Eve.

The absence of a single member of the Democratic caucus could throw the process
off the rails.

Senator Tom Coburn, Republican of Oklahoma and a leading opponent of the
measure, said Sunday that a missing Democrat might be the best hope for foes of
the overhaul.

''What the American people should pray is that somebody can't make the vote,''
he said in a floor remark that Democrats found offensive and suggestive of
wishing misfortune on one of them.

''This statement goes too far,'' said Senator Richard J. Durbin of Illinois, the
No. 2 Democrat in the Senate. ''We are becoming more coarse and divided here.''

Members of both parties say the dispute over health care has created bad blood,
left both Democrats and Republicans suspicious of the opposition's motives, and
shattered some of the institution's traditional collegiality.

At the same time, Democrats say the apparently unbridgeable health care divide
has convinced them that Republicans are dedicated solely to blocking legislative
proposals for political purposes. Several said they now realized that they would
have to rely strictly on their own caucus to advance such defining issues as
climate change in 2010.

''We have crossed the mark of over 100 filibusters and acts of procedural
obstruction in less than one year,'' Senator Sheldon Whitehouse, Democrat of
Rhode Island, said on the floor Sunday. ''Never since the founding of the
Republic, not even in the bitter sentiments preceding Civil War, was such a
thing ever seen in this body.''

Republicans say that the pre-holiday legislative rush reflects an artificial
deadline set by Democrats who want to force through a highly complex measure
with minimal public scrutiny; Democrats say Republicans, under pressure from
conservative campaigners and commentators to stall the bill, are simply
unwilling to accept defeat.

Democratic tempers flared during consideration of a Pentagon spending bill, with
lawmakers suggesting that Republicans were playing politics at the expense of
American troops by extending debate over the $626 billion measure as way of
trying to deny Democrats time needed to pass the health care bill before
Christmas.

Democrats first thought they had Republican commitments to back the measure, but
any they had were later withdrawn, settling off complaints of a double cross.
Short of votes, Democrats had to prevail upon Senator Russ Feingold, an antiwar
Democrat from Wisconsin, to break his pattern of opposing military spending
bills and join them.

The thin margin also required that Senator Robert C. Byrd, who turned 92 last
month, be brought to the chamber in his wheelchair after midnight to cast his
vote. After Democrats produced the needed votes, some Republicans then cast
their votes for the Pentagon measure, drawing an audible murmur of disapproval
from Democrats who considered that bad form.

Tensions have run so high on the Senate floor, with Democrats so perturbed by
Republican stalling tactics, that party leaders told senators to object to any
senator who asked for additional speaking time -- even the routine extra minutes
that senators request to finish a sentence.

At one point during debate, Senator John Cornyn, Republican of Texas, made just
such a request for two minutes but was blocked by Senator Mark Begich, Democrat
of Alaska, who was presiding over the chamber at the time.

Mr. Cornyn was flabbergasted. ''I'm looking around -- I don't see any other
senator waiting to speak,'' he said. Mr. Begich relented, but similar incidents
followed.

On Sunday, Republicans did not mince words when characterizing provisions put in
the health care bill to attract the final votes for passage, particularly that
of Senator Ben Nelson, Democrat of Nebraska.

Some suggested that special Nebraska considerations in the bill amounted to
bribery and corruption. Senator Lindsey Graham, Republican of South Carolina,
said on CNN's ''State of the Union'' that it was reflective of ''seedy Chicago
politics.''

''In order to try to get the 60 votes, there has been basically a pay to play
approach to this, and it's just repulsive,'' Mr. Cornyn said.

The Republican leader, Senator Mitch McConnell of Kentucky, said the
increasingly hostile relationship between the parties reflected differences in
issues, not personalities.

''This is not about acrimony; this is about policy,'' Mr. McConnell said. ''So,
we're upset about it, but it's not a personal thing.''

But some of the issues do seem to have become personal. ''We've allowed
political disagreement to spill over into our relationships and friendships
here,'' Mr. Durbin said, ''and that really does hurt the institution.''

Whatever the cause, things have gotten bad enough that Senator Arlen Specter,
Democrat of Pennsylvania, said the Senate should be stripped of one of its
illustrious institutional claims.

''This body prides itself on being the world's greatest deliberative body,'' Mr.
Specter said. ''That designation has been destroyed with what has occurred here
the past few days.''

URL: http://www.nytimes.com

LOAD-DATE: December 21, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Senators Lindsey Graham, Tom Coburn, Kay Bailey Hutchison and
Saxby Chambliss, all Republicans, at the Capitol on Saturday.
 Senator Arlen Specter of Pennsylvania, a Republican-turned-Democrat, has
lamented the change of tone in the Senate. (PHOTOGRAPHS BY BRENDAN SMIALOWSKI
FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


