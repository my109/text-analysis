                   Copyright 2009 The New York Times Company


                             120 of 2021 DOCUMENTS


                               The New York Times

                             July 8, 2009 Wednesday
                              Late Edition - Final

Getting to Know Our Elderly Patients

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 24

LENGTH: 682 words


To the Editor:

Re ''The Patients Doctors Don't Know'' by Rosanne M. Leipzig (Op-Ed, July 2):

As someone who has been teaching geriatrics for more than 25 years, I found Dr.
Leipzig's article music to my ears! What also needed to be said, however, was
just how few board-certified geriatricians there are both to care for and to
teach about frail elders.

I often see octogenarians along with their daughters and aides, and each person
has questions that need to be answered. Granny has (at least) six problems and
10 medicines, all of which must be dealt with. For all of this Medicare will pay
a fee for service that is not much more than might be paid for treating a sore
throat.

Until our services are adequately valued, there will be way too few of us to
care for Granny, much less to teach students how to do it.

Robert L. Dickman Boston, July 3, 2009

The writer recently retired as chairman of family medicine at the Tufts
University School of Medicine.

To the Editor:

My wife and I have experienced firsthand the trials and tribulations of dealing
with the medical system.

Recently, both of our fathers died. Their treatment and that of my mother have
been cruel lessons about the inadequacy of the medical system to provide
holistic care for the elderly.

Hospitals deal well with acute care, but their impact on the aged causes  nearly
as many problems as are solved. Nursing homes do well in rehabilitating a broken
hip but are much less effective at assisting in recovery from a complicated
medical condition in the elderly.

Even the most diligent family cannot always be present, and the lack of
understanding and knowledge of caregivers can often be damaging when a patient
is not equipped to lobby for himself.

Thomas D. Carey Westport, Conn., July 2, 2009

To the Editor:

Medicare should require more geriatric training, but it should also encourage
geriatricians to provide home-based primary care to our most vulnerable elders,
to keep them out of the hospital.

A bill pending in Congress, the Independence at Home Act, would allow Medicare
to pay more to geriatricians and others who provide medical care at home to the
sickest 10 percent of patients with multiple chronic illnesses, who account for
more than 60 percent of program spending. This legislation should be part of
national health reform. It would save Medicare dollars; reduce unnecessary and
expensive hospital visits; and improve care to our highest-cost and sickest
patients.

Eric DeJonge Chevy Chase, Md., July 3, 2009

The writer is a geriatrician.

To the Editor:

My 96-year-old mother, who died two weeks ago, had difficulty finding physicians
trained in geriatric medicine, even though she lived in Florida.

She was overmedicated and misunderstood. Early symptoms -- and potential
treatments -- were missed. A robust woman who drove and played golf until she
was 90, she felt infuriated whenever presenting a symptom, only to hear many
doctors  respond, ''What do you expect ... at your age?''

Once a year I took her for a checkup with the head of geriatrics at a leading
New York teaching hospital, a gifted physician who reduced her medications. But
the geriatrics department was eliminated. My mother never found another
geriatric physician  with the same insight and sensitivity until she was in
hospice care. She received better medical care when she was dying than when she
was living.

We desperately need to fill this huge gap in medical care between middle-age
patients and the elderly at the final stages of their lives.

Candy Schulman New York, July 2, 2009

To the Editor:

As Depression kids, now 76 and 78, ours was the smallest generation in recent
history. We never learned to stand up for ourselves, and have been largely
ignored.

Luckily, for those of us who are still around, the baby boomers are coming. God
bless them. Our problems will become their problems, and will vanish in the wake
of boomer importance. Within five years, geriatrics will be the primary concern
of all graduating physicians.

Gwendolyn Moore   Patricia Britt Port Townsend, Wash., July 2, 2009

URL: http://www.nytimes.com

LOAD-DATE: July 8, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


