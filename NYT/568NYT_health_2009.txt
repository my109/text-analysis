                   Copyright 2009 The New York Times Company


                             568 of 2021 DOCUMENTS


                               The New York Times

                          September 12, 2009 Saturday
                              Late Edition - Final

Dole, Politics Aside, Pushes for Health Care Plan

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 10

LENGTH: 1034 words


When former Senator Bob Dole was the Republican minority leader, he helped
deep-six President Bill Clinton's health care plan. This year, Mr. Dole, 86, who
left the Senate in 1996 to run for president, is working behind the scenes to
help resurrect one.

His motivation comes partly from experience. After his body was shattered during
World War II, he underwent seven operations  in veterans hospitals and three
years of rehabilitation. ''I had good treatment and it's probably why I'm still
around,'' he said in an interview. He has been working on the issue since the
1970s, and admits now that ''we probably should have passed the Clinton bill,
but it got so politicized.''

With Democrats in control of both ends of Pennsylvania Avenue, Mr. Dole's
ability to influence legislation now is limited.

''I used to have a lot of cards,'' he said. ''I don't have them anymore. I don't
have a vote or anything.''

Where he may be useful is in helping to persuade fellow Republicans to work
toward a bill rather than block one.

Senator Max Baucus, Democrat of Montana and  chairman of the Finance Committee,
recently asked Mr. Dole, once chairman himself, to reach out to Republicans to
produce a bipartisan bill by next week, including three on the committee:
Charles E. Grassley of Iowa; Michael B. Enzi of Wyoming and Olympia J. Snowe of
Maine.

''I wasn't trying to sell them anything,'' Mr. Dole said. ''I'm just saying to
them, 'Don't throw in the towel.' ''

After President Obama addressed Congress on Wednesday night, Mr. Dole said he
shared his party's concern about the potential costs. But unlike many
Republicans, he said he detected a slim possibility that the two parties could
''negotiate a package that they and the president can live with.''

Mr. Dole knows about negotiating. His combined 11 years as majority and minority
leader in the 1980s and 90s make him the longest-serving Republican leader in
Senate history. During that time he thrashed out scores of deals with Democrats,
notably on Social Security (which included a payroll tax increase and prompted
Newt Gingrich later to call Mr. Dole ''the tax collector for the welfare
state'') and on creating the Americans with Disabilities Act.

''You give up some things and you get some things,'' Mr. Dole said. ''If you get
70 percent, that's O.K. You don't just put a big 'No' sign on your desk.''

The complexity of the health care bill, in his view, provides more options for
deal-making. ''I know some conservatives aren't too happy about what I'm doing
now,'' he said.

Mr. Dole remains active at the law and lobbying firm of Alston & Bird, which
represents more than 30 clients from the health care industry. He has been a
''rainmaker'' for the firm, but now describes himself as a ''sprinkler.'' He
also regularly greets hundreds of World War II veterans coming to Washington to
see the World War II memorial.

In 2007, he helped found the Bipartisan Policy Center, along with other former
Senate majority leaders: Howard H. Baker Jr., a Republican, and Tom Daschle and
George J. Mitchell, both Democrats. In June they presented a bipartisan template
for a health care overhaul that contains some of the basic elements of the plan
now being discussed by the Senate Finance Committee. The plan recommended that
all Americans have insurance but contained no public option.

Thomas Mann, a longtime Congress-watcher at the Brookings Institution, said the
plan ''could be a basis for broad agreement, but I see no interest in it among
Senate Republicans.''

Part of the reason, Mr. Mann said, might be the extreme partisanship that has
gridlocked the Capitol. ''My sense is that Dole is simply too reasonable,
pragmatic and non-ideological to be effective with today's breed of Republican
senators,'' Mr. Mann said.

As for the defeat of the Clinton plan in 1994, Mr. Clinton, in a new interview
in Esquire magazine, blamed presidential politics.

''And we now know, and I'm surer of this than anything: We just couldn't do it
as long as Bob Dole was running for president,'' Mr. Clinton told the magazine.
He said that Mr. Dole was ''really good on health care for a Republican'' and
that the two had mapped out a strategy for compromise.

''Then,'' Mr. Clinton said, ''he gets Bill Kristol's famous memo that says, you
know, 'If you let Bill Clinton pass any kind of health care bill, the Democrats
will be the majority party for a generation, and you can forget about your
presidential hopes. Your only option is to beat anything. Kill it off.' ''

Mr. Dole followed the advice, declaring back then that ''there is no health care
crisis.''

Now, in response to Mr. Clinton's comments in Esquire, Mr. Dole said politics
''on both sides'' played a role.

''President Clinton was covering his political bases and so was I,'' Mr. Dole
said. ''He lost Congress in 1994 and I lost the presidential race in '96. I
assume health care had some impact in '96.''

Chris Jennings, who was Mr. Clinton's senior health care adviser, has had a
close-up view of Mr. Dole's role. For more than a year, Mr. Jennings worked at
Mr. Dole's side  on the health care project for the Bipartisan Center.

''He has taken it upon himself to really push this cause and I've seen him do
it,'' Mr. Jennings said. ''He just picks up the phone, he knows people, he
corners people, whether it's the president or Democrats or Republicans on the
Finance Committee. He's trying to send a message to members not to blow this
opportunity.''

Mr. Jennings said Mr. Dole believed this was a rare moment when an overhaul was
possible. ''Frequently you regret those times when you didn't take advantage of
something,'' Mr. Jennings said. ''That's part of his lesson, from life and from
legislating.''

Mr. Dole's own health is increasingly fragile. He had a hip replacement a while
ago, and over the summer he underwent emergency surgery on a leg that was
infected with flesh-eating bacteria. His knees are arthritic, but his doctors
have advised against  more operations. Asked if he was thinking about his legacy
while pushing for health care, Mr. Dole demurred. ''A legacy is someone like Ted
Kennedy,'' he said. ''When I think of legacy, I don't think of Bob Dole.''

URL: http://www.nytimes.com

LOAD-DATE: September 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Bob Dole, as a presidential candidate in 1996, criticized
President Bill Clinton's health care plan.(PHOTOGRAPH BY RUTH FREMSON/ASSOCIATED
PRESS)

PUBLICATION-TYPE: Newspaper


