                   Copyright 2010 The New York Times Company


                             1545 of 2021 DOCUMENTS


                               The New York Times

                            February 22, 2010 Monday
                              Late Edition - Final

How the G.O.P. Can Fix Health Care

BYLINE: By MARK McCLELLAN.

MARK McCLELLAN, director of the Engelberg Center for Health Care Reform at the
Brookings Institution and former Medicare administrator in the George W. Bush
administration

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 19

LENGTH: 411 words


REPUBLICANS can't and won't accept a simple combination of the Democratic health
care bills that passed the House and Senate. But the country needs health care
reform, and Republicans must engage in the process.

They should insist on adding reforms that would do more to reduce costs, in
order to avoid  higher income, payroll or similar taxes. (These taxes are
different from the proposed excise tax on high-cost health insurance, which
would actually reduce health-care spending.) Further cost-saving reforms could
include meaningful changes in medical liability law; better opportunities for
people to save money when they take steps to lower their health care costs; and
Medicare savings from greater use of competitive bidding.

Some elements of the Democratic legislation could be part of a bipartisan
agreement -- for instance, help for people with limited means and/or high
medical needs to buy insurance in a competitive marketplace. Such a market would
let people easily compare a broad range of options and keep the savings if they
chose a less costly plan.

Republicans should also build on proposals to enable health care providers to
get higher Medicare payments when they deliver better care at less cost. For
example, the Democratic bills would let providers share in Medicare savings if
they can show that the way they prevent and manage illnesses reduces
complications and costs Medicare less. Republicans should further propose that
Medicare strengthen its capacity to provide data and measure patient outcomes --
to help providers and to evaluate changes in care quickly.

This is desperately needed to help make Medicare sustainable while improving the
quality of care. Currently, doctors lose money when they work with nurse
practitioners, pharmacists or wellness programs to help patients avoid costly
complications -- because Medicare doesn't pay for this, and it results in fewer
billings for the visits, tests and procedures Medicare does pay for.

All these steps can add up to a health care system that does much more to
support patients and health professionals in improving our health and saving
money. That's a bipartisan opportunity we can't afford to miss -- either because
Democrats are unwilling to change or because Republicans insist on starting
over.

-- MARK McCLELLAN, director of the Engelberg Center for Health Care Reform at
the Brookings Institution and former Medicare administrator in the George W.
Bush administration

URL: http://www.nytimes.com

LOAD-DATE: February 22, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID PLUNKERT)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


