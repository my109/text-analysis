                   Copyright 2009 The New York Times Company


                             507 of 2021 DOCUMENTS


                               The New York Times

                            September 6, 2009 Sunday
                              Late Edition - Final

On Day Off, a Day of Work

BYLINE: By STEVEN GREENHOUSE

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 236 words


The nation's labor unions seem to be approaching this Labor Day more as Health
Reform Lobbying Day than as a traditional holiday.

With President Obama scheduled to speak at a union-sponsored picnic in
Cincinnati on Monday, labor advocates are spending the weekend painting and
printing signs in support of a health care overhaul. That includes labor's pet
provision: the public-insurance option. Thirty thousand union members are
expected at the picnic.

Elsewhere Monday, the Greater Boston Labor Council plans to turn its annual
labor breakfast into not just a memorial for Senator Edward M. Kennedy, but a
mobilization for his lifelong cause: universal health coverage. Unions expect
thousands of demonstrators at the breakfast-cum-rally.

Meanwhile, the Service Employees International Union is sponsoring ''Let's Get
It Done'' Labor Day health care demonstrations in Philadelphia, Pittsburgh,
Texarkana, Ark., Rapid City, S.D., and several other cities. But the union also
wants its members to save some energy for Tuesday and Wednesday, when it hopes
they will deluge Capitol Hill with thousands of phone calls.

The unions want not only for wavering Democrats to stand by the overhaul. They
are also pushing President Obama to sell his health plan more vigorously. ''It's
important for him to get his message out across the country,'' said John
Sweeney, above, the A.F.L.-C.I.O. president.

STEVEN GREENHOUSE

URL: http://www.nytimes.com

LOAD-DATE: September 6, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


