                   Copyright 2009 The New York Times Company


                             616 of 2021 DOCUMENTS


                               The New York Times

                          September 17, 2009 Thursday
                              Correction Appended
                              Late Edition - Final

Opinions on the Baucus Plan

SECTION: Section A; Column 0; National Desk; Pg. 23

LENGTH: 327 words


Readers posted more than 1,250  comments on the Prescriptions blog Wednesday in
response to coverage of Senator Max Baucus's introduction of his proposal to
overhaul the  health care system.

Here is a sampling of the comments, in most cases condensed from longer
postings.

This is a good first step. It keeps the federal government in a regulatory role,
not a service provider, which is perfect. If Amtrak is your idea of how things
should run, by all means vote for public health insurance.

Dan Hinnah

We now have a bill that is criticized by both left and right. I think we finally
have a balanced plan! It's not perfect, but I think this is the best we can do
right now.

Paolo

Today, I learn that the Baucus bill would require that my healthy, non-smoking,
non-junk-food-eating family continue to pay Blue Cross $630 a month, with a
$1,500 per person annual deductible, no dental or vision. And that's out of the
''middle class'' paycheck of one public-school teacher supporting a family of
three, one of whom is in college.

Tony Huegel

As I understand the document, the penalty to an individual is somewhere between
$750 and $950 for not maintaining insurance and a maximum $1,500 for a family.
In Canada, that would typically buy a single individual his or her full coverage
insurance, excluding dental care.

Steve Hunter

The proposal reads like an insurance policy: lots of references to other
documents, lots of undefined terms, lots of loopholes and no fixed price tag.

Dave, Tucson

Expansion of the current ''system'' would be the way to go at this point. I am
satisfied with my current insurance and hope that it will keep on keeping on.

Sally Smooth

As one of millions of self-employed U.S. citizens (some by choice, others by
default after layoffs), this bill means that I still have no insurance (but I DO
get fined up to $3,400!), no tax credit for premiums I can't afford anyway, and
my taxes will likely be even higher.

BackToWork

URL: http://www.nytimes.com

LOAD-DATE: September 17, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: September 21, 2009



CORRECTION: A reader's comment in the Prescriptions column on Thursday  in
reaction to the health care proposal introduced by Senator Max Baucus misstated
the maximum penalty a family might face under the plan for not having insurance
coverage. As an accompanying article reported, the highest possible penalty
would be $3,800 a year, not $1,500.

GRAPHIC: PHOTO (PHOTOGRAPH BY DANNY MOLOSHOK/REUTERS)

PUBLICATION-TYPE: Newspaper


