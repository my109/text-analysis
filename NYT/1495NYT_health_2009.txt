                   Copyright 2010 The New York Times Company


                             1495 of 2021 DOCUMENTS


                               The New York Times

                           February 11, 2010 Thursday
                              Late Edition - Final

New Jersey Is Added to Trial Program to Streamline Health Insurance Paperwork

BYLINE: By REED ABELSON

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 4

LENGTH: 864 words


As part of the discussion last year over how best to overhaul the nation's
health care system, the insurance industry promised to do its part by tackling
the burdensome paperwork involved in paying medical claims.

Despite the health care legislation's  impasse in Congress, the insurers say
they still plan to make good on their promise.

On Thursday, the two main industry trade groups, America's Health Insurance
Plans and the Blue Cross  Blue Shield Association,  are to announce a pilot
program  in New Jersey. Five of the state's largest private insurers plan to
offer doctors and hospitals the ability to use a single Web portal to check a
patient's coverage and track claims, regardless of which of those five health
plans they are enrolled in.

The effort is aimed at one of the most vexing problems in the nation's insurance
system: hospitals and doctors spend enormous amounts of time and money trying to
determine whether a patient has coverage or why a claim was denied. Tens of
billions of dollars each year are said to be wasted because of such
administrative inefficiency.

The insurers started a similar project last fall in Ohio, where about 700
medical practices are using a single Web site to get access to insurance
information about their patients.

''The pilots are a great example of the industry's commitment to voluntarily
make progress eliminating the administrative hassles that physicians face,''
said Ronald Williams,  the chief executive of Aetna, the large insurer, who is
among those leading the industry's overall effort.

The promise to produce significant savings through streamlining paperwork was
made as part of the industry's discussions in June with the White House. But the
pilot project effort has taken place  parallel to the general discussions over
the health care legislation, said Karen Ignagni,  the president of America's
Health Insurance Plans.

The move  by the insurers to develop standards and systems to make it easier for
doctors to determine a patient's coverage and get paid will continue regardless
of the status of any federal legislation, she said.

''We wanted to make sure we were taking a leap,'' said Ms. Ignagni, who said the
two state projects were a way for the insurers to test different Web systems and
see what technology worked best for the hospitals and doctors before eventually
beginning other efforts.

Insurers in Florida and California are already talking about starting their own
initiatives, she said. ''We want to move this along very, very quickly so we can
offer this in all the states,'' she said.

Under the New Jersey pilot program, doctors and hospitals will be able to log
into a single Web portal, offered by NaviNet, which already provides similar Web
systems for individual health plans.

Aetna, AmeriHealth New Jersey, Cigna, Horizon Blue Cross Blue Shield of New
Jersey and United Healthcare, which represent about 95 percent of the people
covered by private insurers in the state, are participating. Doctors will able
to check instantly whether a patient has coverage and be able to track the
status of the claims.

But while doctors are able to go to a single Web site, they must still get
access to information about a particular patient from subsections of the site
fed by the individual insurance carriers. And not all of the health plans will
immediately provide the same level of information.

While some plans will allow a doctor to immediately determine how much a patient
will owe toward the cost of an expensive treatment, for example, taking into
account how much the co-payments involved and how much of the patient's
deductible has already been met, others will offer only the outlines of a
customer's coverage.

The doctors in Ohio using the system there say it is helping to make it easier
to find out if someone has coverage or to check a claim. The new system is ''a
good start,'' said Mark Jarvis,  a senior official for  the Ohio State Medical
Association, but he said more work was needed  to streamline the process.

Some insurers are clearer than others about why a claim is being denied, he
said, and a doctor's office must still enter billing information twice -- once
for its  own practice management system and again for the new site.

''What we really need to get is the information integrated into the workflow in
the office,'' Mr. Jarvis said.

The industry acknowledges that insurers are still in the process of deciding on
standards. ''We have a long way to go,'' said Mr. Williams of Aetna. But he
compared the current industry initiative with the early days of bank A.T.M.'s,
when a customer's banking card did not work in every machine.

''All of these things, over time, did get standardized,'' he said.

For doctors like Stephen A. Nurkiewicz, being able to easily find out if a
patient has coverage before an office visit is a tremendous step forward. As a
solo practitioner in Hammonton, N.J., Dr. Nurkiewicz already uses a NaviNet
system for some of his dealings with the insurers and looks forward to being
able to tell before he sees a patient whether that patient really has coverage.

''It's got to be better than it is now,'' he said.

URL: http://www.nytimes.com

LOAD-DATE: February 11, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


