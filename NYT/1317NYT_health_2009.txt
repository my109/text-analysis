                   Copyright 2010 The New York Times Company


                             1317 of 2021 DOCUMENTS


                               The New York Times

                            January 7, 2010 Thursday
                              Late Edition - Final

Oh, Byron, We Hardly Knew Ye

BYLINE: By GAIL COLLINS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 31

LENGTH: 777 words


Senator Byron Dorgan is retiring! I know this comes as a shock to you, people.
Also Senator Chris Dodd! We are only one week into the new year, and the
political world is in turmoil. It's a wonder we can continue on with our regular
duties.

Two Democratic senators quitting is seen as a terrible portent for 2010.
(''Democrats' Black Tuesday,'' said a headline on MSNBC.) That seems a tad
overblown given the fact that six Republican senators already have announced
their retirements.

Plus, Dodd has been in terrible trouble back home ever since he ran for
president and tried to get a jump on the competition by moving his family to
Iowa. Connecticut has feelings, too. When your senator registers his daughter in
kindergarten in Des Moines, the voters in Bridgeport don't feel the love.

It's really all good for the Nutmeg State Democrats. Dodd can leave with
dignity. He has an overall record to be proud of, including a major role in
health care reform. He also worked very hard on issues that have no political
payoff whatsoever, like early childhood education.

In his place, the Democrats can nominate the popular attorney general, Richard
Blumenthal, who has been waiting in the wings since Cyndi Lauper was at the top
of the charts. He was the Democrats' young man on the rise in the '70s, and he's
been attorney general for nearly 20 years. It's a good thing Dodd decided to get
out of the way now or when Blumenthal's turn came, Connecticut would have wound
up electing a new senator who looked like Robert Byrd without seniority.

Blumenthal's opponent might turn out to be Linda McMahon, who formerly ran World
Wrestling Entertainment with her husband, Vince. There are other, perhaps
better, Republican candidates in the race, but I am rooting for McMahon for
entertainment value. She used to be a central character in cable wrestling shows
whose scripts had family members shrieking, betraying and, occasionally,
slugging one another. One episode featured a villain who broke into the
''palatial McMahon headquarters'' while Linda was recovering from a neck injury
that she had received when an aggrieved wrestler flipped her upside down and
slammed her head onto the floor. ''You are a rather aggressive beauty, aren't
you?'' he breathed, before forcing a kiss upon her resistant lips and promising
to break both her son's legs.

Lately, there have not been all that many good times on the political front. In
Washington, the Democrats are sulky about Obama spending all his political
capital on health care while the sane people in both parties are completely
freaked out by the tea partiers. Meanwhile, interested civilians are being
required to spend an excessive amount of time worrying about cloture votes and
yearning for the good old days when the only senators you had to know anything
about were your own.

Until recently, all I knew about Byron Dorgan was that he was a populist from
North Dakota who had once been named Person of the Year by the durum wheat
growers. Now he is the center of the universe.

In North Dakota, Democrats are petitioning him to change his mind and run again.
The 60th vote could hang on it! Nobody seemed to have expected Dorgan to call it
quits even though he has been a professional politician since he was 26 and made
history as the most youthful person ever appointed to the North Dakota Tax
Commission.

Now the guy is 67 years old, and he says he wants to write books and teach. I
think we should give him a thumbs up on his new life plan, except for the part
about how he might ''also like to work on energy policy in the private sector.''
That sure does sound like a lobbyist, but perhaps it just means investing in a
gas station.

I'm beginning to suspect that he doesn't expect to go back to North Dakota at
all. I am basing this mainly on the fact that the ''Notable North Dakotans''
page on the Byron Dorgan Web site lists 19 people, none of whom seem to actually
live there. In fact, seven of them are dead and one of the others is the guy who
is married to the pop star Fergie.

No wonder that when Dorgan bolted, a top North Dakota Democrat instantly called
Ed Schultz, the former Fargo native turned MSNBC talk-show host, and asked him
if he had ever thought about running for the Senate. Even the people in North
Dakota don't seem to think there are any actual residents left in the state.

You're giving yourself too little credit, North Dakota Democrats. There's got to
be a potential junior senator somewhere in your 641,000 fine residents. Who, of
course, get exactly the same number of senate votes as the 36.8 million people
in California. But that's a complaint for another day.

URL: http://www.nytimes.com

LOAD-DATE: January 7, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


