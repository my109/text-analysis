                   Copyright 2009 The New York Times Company


                             939 of 2021 DOCUMENTS


                               The New York Times

                           November 5, 2009 Thursday
                              Late Edition - Final

Unhealthy America

BYLINE: By NICHOLAS D. KRISTOF

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 35

LENGTH: 853 words


The moment of truth for health care is at hand, and the distortion that perhaps
gets the most traction is this:

We have the greatest health care system in the world. Sure, it has flaws, but it
saves lives in ways that other countries can only dream of. Abroad, people sit
on waiting lists for months, so why should we squander billions of dollars to
mess with a system that is the envy of the world? As Senator Richard Shelby of
Alabama puts it, President Obama's plans amount to ''the first step in
destroying the best health care system the world has ever known.''

That self-aggrandizing delusion may be the single greatest myth in the health
care debate. In fact, America's health care system is worse than Slov--er, oops,
more on that later.

The United States ranks 31st in life expectancy (tied with Kuwait and Chile),
according to the latest World Health Organization figures. We rank 37th in
infant mortality (partly because of many premature births) and 34th in maternal
mortality. A child in the United States is two-and-a-half times as likely to die
by age 5 as in Singapore or Sweden, and an American woman is 11 times as likely
to die in childbirth as a woman in Ireland.

Canadians live longer than Americans do after kidney transplants and after
dialysis, and that may be typical of cross-border differences. One review
examined 10 studies of how the American and Canadian systems dealt with various
medical issues. The United States did better in two, Canada did better in five
and in three they were similar or it was difficult to determine.

Yet another study, cited in a recent report by the Robert Wood Johnson
Foundation and the Urban Institute, looked at how well 19 developed countries
succeeded in avoiding ''preventable deaths,'' such as those where a disease
could be cured or forestalled. What Senator Shelby called ''the best health care
system'' ranked in last place.

The figures are even worse for members of minority groups. An African-American
in New Orleans has a shorter life expectancy than the average person in Vietnam
or Honduras.

I regularly receive heartbreaking e-mails from readers simultaneously combating
the predations of disease and insurers. One correspondent, Linda, told me how
she had been diagnosed earlier this year with abdominal and bladder cancer --
leading to battles with her insurance company.

''I will never forget standing outside the chemo treatment room knowing that the
medication needed to save my life was only a few feet away, but that because I
had private insurance it wasn't available to me,'' Linda wrote. ''I read a
comment from someone saying that they didn't want a faceless government
bureaucrat deciding if they would or would not get treatment. Well, a faceless
bureaucrat from my private insurance made the decision that I wouldn't get
treatment and that I wasn't worth saving.''

It's true that Americans have shorter waits to see medical specialists than in
most countries, although waits in Germany are shorter than in the United States.
But citizens of other countries get longer hospital stays and more medication
than Americans do because our insurance companies evict people from hospitals as
soon as they can stagger out of bed.

For example, in the United States, 90 percent of hernia surgery is performed on
an outpatient basis. In Britain, only 40 percent is, according to a report by
the McKinsey Global Institute.

Likewise, Americans take 10 percent fewer drugs than citizens in other countries
-- but pay 118 percent more per pill that they do take, McKinsey said.

Opponents of reform assert that the wretched statistics in the United States are
simply a consequence of unhealthy lifestyles and a diverse population with
pockets of poverty. It's true that America suffers more from obesity than other
countries. But McKinsey found that over all, the disease burden in Europe is
higher than in the United States, probably because Americans smoke less and
because the American population is younger.

Moreover, there is one American health statistic that is strikingly above
average: life expectancy for Americans who have already reached the age of 65.
At that point, they can expect to live longer than the average in industrialized
countries. That's because Americans above age 65 actually have universal health
care coverage: Medicare. Suddenly, a diverse population with pockets of poverty
is no longer such a drawback.

That brings me to an apology.

In several columns, I've noted indignantly that we have worse health statistics
than Slovenia. For example, I noted that an American child is twice as likely to
die in its first year as a Slovenian child. The tone  --  worse than Slovenia!
--  gravely offended Slovenians. They resent having their fine universal health
coverage compared with the notoriously dysfunctional American system.

As far as I can tell, every Slovenian has written to me. Twice. So, to all you
Slovenians, I apologize profusely for the invidious comparison of our health
systems. Yet I still don't see anything wrong with us Americans aspiring for
health care every bit as good as yours.

URL: http://www.nytimes.com

LOAD-DATE: November 5, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


