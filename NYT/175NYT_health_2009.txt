                   Copyright 2009 The New York Times Company


                             175 of 2021 DOCUMENTS


                               The New York Times

                              July 19, 2009 Sunday
                              Late Edition - Final

First Lady Steps Into Policy Spotlight in Debate on Health Care

BYLINE: By RACHEL L. SWARNS

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 995 words

DATELINE: WASHINGTON


She has become one of the Obama administration's most visible surrogates on
health care, announcing the release of $851 million in federal financing for
health clinics, calling for tougher nutritional standards in the government's
school lunch program and urging Democrats to rally around the president's
efforts to revamp health care.

The high-profile emissary? Not Kathleen Sebelius, the health and human services
secretary, or Nancy-Ann DeParle, the White House health policy adviser. It is
the first lady, Michelle Obama.

''We're at a critical juncture in the debate about health care in this
country,'' Mrs. Obama said at a clinic here in June. ''The current system is
economically unsustainable, and I don't have to tell any of you that. And
despite having the most expensive health care system in the world, we're not
necessarily healthier for it.''

After several months of focusing on her family, her garden and inspiring young
people,  Mrs. Obama is stepping into more wonkish terrain. She is toughening her
message and talking more openly about influencing public policy as she works to
integrate her efforts more closely with those of policy makers in the West Wing.

In June, Mrs. Obama traveled to San Francisco with Melody C. Barnes, the
president's domestic policy adviser, for the start of the administration's
initiative to promote volunteerism.

A week later, she went to a Washington clinic with the director of the Health
Resources and Services Administration to announce the release of stimulus money
for clinics. This month, her policy director joined a new interagency working
group, including health, agriculture and housing officials, that will develop
policy, legislation and public outreach to combat obesity.

While her efforts are most visible in the health care arena, Mrs. Obama is also
focusing on other issues.

In May, she convened a meeting -- with James L. Jones, the president's national
security adviser; Gen. James E. Cartwright, the vice chairman of the Joint
Chiefs of Staff; and Peter R. Orszag, the White House budget chief, among others
-- to consider new policies and programs to help military families. Her staff is
also working with nonprofit, corporate and philanthropic groups to help rally
resources to support such families.

Mrs. Obama's aides say the substantive speeches and increased coordination with
the West Wing reflect the first lady's determination to have an impact on the
issues she cares about.

''The listening portion of this is over,'' said Camille Johnston, her
communications director. ''Let's make sure that we're having an impact and
delivering things as well.''

But Mrs. Obama, a Harvard-educated lawyer and former hospital executive, is
still treading cautiously as she fleshes out her priorities: promoting healthy
living and community service and supporting military and working families.

Susan Sher, the first lady's new chief of staff, emphasized that Mrs. Obama
would not testify before Congress or argue the merits of competing health care
plans. Indeed, the first lady did not stand alongside her husband in the White
House last week when he praised the Senate health committee for approving an
overhaul of the health care system.

Mrs. Obama has chosen instead to deliver her recent remarks in more traditional
settings for a first lady -- at a clinic, a playground and in the White House
garden. Her aides say she will promote policy, not make it, and will continue to
concentrate on children and families. They say they do not expect her to be
accused of overstepping her bounds as Hillary Rodham Clinton was when she tried
to remake health care as the first lady.

Jocelyn Frye, Mrs. Obama's policy director, said the first lady wanted to ''draw
the connection between this massive conversation that's happening about health
reform and what's really happening to people on the ground.''

Mrs. Obama's advisers say this is a natural progression. After settling her
family into the White House, the first lady could more easily turn to the garden
and then a discussion of obesity, the importance of preventive care and
corresponding government policies and legislation, they say.

The shift also coincides with Mrs. Obama's decision in June to choose Ms. Sher,
her longtime friend, to replace Jackie Norris as her chief of staff. At the time
of that announcement, Ms. Sher, who was Mrs. Obama's boss at the University of
Chicago Medical Center, was working on health care issues as an associate
counsel to the president.

Since then, as her husband has worked on health care, Mrs. Obama has peppered
her speeches with statistics and references to health-related legislation, like
the coming reauthorization of child nutrition programs.

She has repeatedly cited the costs of preventable illnesses like obesity,
diabetes and high blood pressure to the nation's health care system ($120
billion a year, she says) and the impact of poor diet on children. (''Nearly a
third of the children in this country are either overweight or obese, and a
third will suffer from diabetes at some point in their lifetime,'' she said.
''Those numbers are unacceptable.'')

She sent out a mass e-mail message to Democrats across the country, urging them
to volunteer to improve health care services and ''to make a difference as we
work to reform America's health care system.''

And while she has often focused on motherhood, gardening and other domestic
subjects in interviews, she took on a thorny policy issue last month: the
prospects for a health care overhaul in an Obama administration.

''There are going to be tough choices that have to be made,'' Mrs. Obama said on
the ABC program ''Good Morning America,'' emphasizing that ''no system is going
to be perfect.''

Still, Mrs. Obama said she believed that the president would succeed where the
Clintons did not. The country ''has moved to another point in time,'' she said.
''More and more people are ready for this kind of reform.''

URL: http://www.nytimes.com

LOAD-DATE: July 19, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Michelle Obama announced the release of $851 million in stimulus
money for health centers last month at a Washington clinic.(PHOTOGRAPH BY MANDEL
NGAN/AGENCE FRANCE-PRESSE -- GETTY IMAGES)

PUBLICATION-TYPE: Newspaper


