                   Copyright 2009 The New York Times Company


                             570 of 2021 DOCUMENTS


                               The New York Times

                          September 12, 2009 Saturday
                              Late Edition - Final

Heckler's Behavior May Bring Action in House

BYLINE: By CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 511 words

DATELINE: WASHINGTON


House Democrats intend to pursue a formal resolution admonishing Representative
Joe Wilson for his outburst against President Obama during the president's
speech to Congress on Wednesday night unless he apologizes on the floor.

Senior aides said Friday that the leadership had decided that the behavior of
Mr. Wilson, a South Carolina Republican, in shouting ''You lie!'' merited some
response by the House even though he had  issued an apology and expressed his
regrets to the president through Rahm Emanuel, the White House chief of staff.

''It is a clear violation of the rules of the House, and it needs to be resolved
on the floor of the House either by an apology or by a resolution,'' said
Brendan Daly, a spokesman for Speaker Nancy Pelosi.

House rules and precedents provide substantial guidance on how a House member
can and cannot refer to the president while speaking on the floor, and the
guidelines state that it has been found impermissible to call the president a
liar. The House was in formal session at the time of the speech.

Since Mr. Wilson interrupted the president to challenge him on health care for
illegal immigrants, he has become a focal point in the health care debate. He
has drawn condemnation from many, but emerged as a hero to conservative
opponents of proposed changes.

While Ms. Pelosi indicated on Thursday that she was ready to let the matter
drop, other Democratic leaders, including Representative James E. Clyburn of
South Carolina, the party whip, argued that the episode was too egregious to let
pass.

Aides said Mr. Wilson was aware of the Democrats' intentions and would decide
over the weekend what to do. He was  encouraged on Thursday by Mr. Clyburn,
among others, to apologize to his House colleagues, but he resisted doing so,
according to lawmakers and other aides.

In a video posted on his campaign Web site Thursday night, Mr. Wilson
acknowledged that heckling the president was wrong but said his retort had grown
out of months of emotional conversations with his constituents about health
care.

He added that he did not intend to be ''muzzled''  on the topic. ''I will speak
up and speak loudly against this risky plan,'' he said in the video, referring
to Democratic proposals.

He also asked for campaign contributions, saying he was now the political target
of proponents of the Democratic health care overhaul. ''They want to silence
anyone who speaks out against this,'' he said.

Both Mr. Wilson and his potential 2010 opponent, Rob Miller, have raised
substantial amounts of campaign money since the speech -- Mr. Miller has taken
in more than $1 million, and Mr. Wilson more than $700,000 -- as contributors on
both sides of the dispute register their sentiment.

Republican lawmakers also counseled Mr. Wilson to apologize on the floor
Thursday to bring the episode to a conclusion. One aide to the House Republican
leadership said the party had not been informed what any resolution might say,
but noted that Mr. Wilson had already offered an apology and that the president
had accepted it.

URL: http://www.nytimes.com

LOAD-DATE: September 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Joe Wilson, second from right, was tailed by reporters as he
made his way to the House  on Thursday, the day after his outburst.(PHOTOGRAPH
BY HARRY HAMBURG/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


