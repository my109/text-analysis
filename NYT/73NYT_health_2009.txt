                   Copyright 2009 The New York Times Company


                              73 of 2021 DOCUMENTS


                               The New York Times

                             June 23, 2009 Tuesday
                              Late Edition - Final

Federal Saving From Lowering of Drug Prices Is Unclear

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 895 words

DATELINE: WASHINGTON


The White House on Monday hailed what it described as a ''historic agreement to
lower drugs costs'' for older Americans, but it was not immediately clear how
much the government would reap in savings that could be used to pay for coverage
of the uninsured.

As part of the agreement, pharmaceutical companies promised to help narrow a gap
in Medicare coverage of prescription drugs that is known as the doughnut hole.

As Mr. Obama described the gap, ''Medicare covers up to $2,700 in yearly
prescription costs and then stops, and the coverage starts back up when the
costs exceed $6,100.''

Drug companies said they would give most beneficiaries a 50 percent discount on
brand-name medicines bought when they hit the gap in coverage.

This could be a boon to Medicare beneficiaries, and AARP praised the deal. But
drug company lobbyists and Senate aides said that none of these savings would
accrue to the government, which has no liability for a patient's drug costs in
the coverage gap. Indeed, that is the problem for beneficiaries: they are
responsible for the entire cost of drugs in the gap.

Charles A. Butler, a pharmaceutical analyst at Barclays Capital, the investment
bank, said the drug industry was offering an olive branch to Congress and the
White House, in contrast to its ''vociferous disagreement'' with President Bill
Clinton's proposals in 1993-94.

In an interview, Mr. Butler said he did not think the latest concessions would
have ''a material adverse impact'' on drug company earnings. ''Because of the
discounts,'' he said, ''Medicare beneficiaries are likely to continue filling
prescriptions in the doughnut hole, whereas in the past many stopped taking
their medications because the drugs were unaffordable to them.''

Congress and the White House are frantically seeking ways to help pay for
legislation securing coverage for all Americans. The cost of the bill could top
$1 trillion over 10 years.

The lobby for drug companies, the Pharmaceutical Research and Manufacturers of
America, or PhRMA, said it had pledged $80 billion over 10 years to help ''
reform our troubled health care system.'' The commitment came in a deal with the
White House and Senator Max Baucus, Democrat of Montana and chairman of the
Finance Committee.

Mr. Obama described the agreement as a ''significant breakthrough on the road to
health care reform.''

But Senate aides and drug company lobbyists said the $80 billion reflected total
projected savings to the health care system and included unspecified future
concessions, besides the drug discounts in the coverage gap.

Some of the $80 billion reflects savings to the government. Some reflects
savings to older Americans. But all the savings from closing the doughnut hole
would go to  beneficiaries, not the government, Senate aides said.

Reid H. Cherlin, a White House spokesman, said: ''Of the $80 billion, we
estimate that $30 billion could be used for the doughnut hole and passed on to
seniors. The other $50 billion could go to health care reform, but these savings
have not been identified at the moment.''

The House bill would go further. It would gradually eliminate the coverage gap
and require drug companies to pay rebates to the government on drugs for
low-income Medicare beneficiaries.

Ken Johnson, a spokesman for the drug manufacturers group, said the industry's
$80 billion commitment would include ''significant scorable savings to the
government,'' though details were not available.

Steven D. Findlay, a health policy analyst at Consumers Union, said: ''It's
great that PhRMA has stepped up to the plate with a proposal to help lower
seniors' drug costs. But this still leaves the doughnut hole in place and
hundreds of thousands, perhaps millions, of seniors on the hook for drug costs
they cannot afford. We hope Congress will eventually do away with the doughnut
hole.''

Congressional reaction suggested that the industry's voluntary price concessions
had whetted the appetite of lawmakers for broader, deeper discounts.

Senator Olympia J. Snowe, Republican of Maine, said the $80 billion commitment
amounted to ''a modest percentage'' of national spending on prescription drugs.
The Department of Health and Human Services estimates that drug spending will
total $3.3 trillion over 10 years.

Even as Mr. Obama welcomed the new agreement, drug companies opposed another
significant part of his agenda: a proposal that would prohibit brand-name drug
companies from paying generic drug makers to delay the marketing of generic
products, which often cost much less.

In his budget, Mr. Obama said such ''anticompetitive agreements'' kept generic
drugs off the market. Jon Leibowitz, chairman of the Federal Trade Commission,
said these deals would cost consumers tens of billions of dollars in the next
decade.

A House subcommittee has approved a bill to ban such ''pay-for-delay
settlements.'' A Senate committee is poised to approve a similar bill this week.

Mr. Leibowitz said, ''Drug companies are lobbying furiously against the
legislation because they want to preserve their monopoly profits at the expense
of consumers.''

But Diane E. Bieri,  executive vice president of the drug manufacturers
association, said the settlements sometimes benefited consumers because they
avoided litigation and allowed generic drugs to enter the market before drug
patents expired.

URL: http://www.nytimes.com

LOAD-DATE: June 23, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama on Monday with, from left, Barry Rand, chief
executive of AARP, and Senators Max Baucus and Christopher J.Dodd. Mr. Obama
discussed an agreement intended to narrow the gap for Medicare recipients in
prescription drug coverage. (PHOTOGRAPH BY BRENDAN SMIALOWSKI FOR THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


