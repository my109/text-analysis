                   Copyright 2009 The New York Times Company


                             581 of 2021 DOCUMENTS


                               The New York Times

                           September 13, 2009 Sunday
                              Late Edition - Final

Obama's Squandered Summer

BYLINE: By FRANK RICH

SECTION: Section WK; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 16

LENGTH: 1562 words


THE day before he gave his latest brilliant speech, Barack Obama repeated a
well-worn mantra to a television interviewer: ''My job is not to be distracted
by the 24-hour news cycle.'' The time has come for him to expand that job
description. His White House has a duty to push back against the 24-hour news
cycle, every 24 hours if necessary, when it threatens to derail his agenda, the
nation's business, or both. This was a silly summer, as wasteful in its way as
the summer of 2001, when Washington dithered over the now-forgotten Gary Condit
scandal while Al Qaeda plotted. The president deserves his share of the blame.

After a good couple of years of living with the guy, we know the drill that
defines his leadership, for better and worse. When trouble lurks, No Drama Obama
stays calm as everyone around him goes ballistic. Then he waits -- and waits --
for that superdramatic moment when he can ride to his own rescue with what the
press reliably hypes as The Do-or-Die Speech of His Career. Cable networks slap
a countdown clock on the corner of the screen and pump up the suspense. Finally,
Mighty Obama steps up to the plate and, lo and behold, confounds all the
doubting bloviators yet again by (as they are wont to say) hitting it out of the
park.

So it's a little disingenuous for Obama to claim that he is not distracted by
the 24-hour news cycle. What he's actually doing is gaming it for all it's
worth.

As a mode of campaigning, this tactic was worth a great deal. Obama not only
produced eloquent speeches  --  especially the classic disquisition on race that
silenced the Jeremiah Wright pogrom  --  but also executed a remarkably
disciplined tortoise-vs.-hare battle plan that outwitted and ultimately
vanquished the hypercaffeinated political strategies of Hillary Clinton and John
McCain. As a style of governing, however, this repeated cycle of extended
above-the-fray passivity followed by last-minute oratorical heroics has now been
stretched to the very limit.

Wednesday night's address on health care reform was inspired, lucid and, in the
literally and figuratively Kennedyesque finale, moving. It was also (mildly)
partisan, a trait much deplored by high-minded editorial writers but in real
life quite useful when your party is in the majority and you want to rally the
troops to get something done. But there was little in the speech that Obama
couldn't have said at the summer's outset. Its practical effect may prove nil.
Short of signing a mass suicide pact, the Democrats were always destined to pass
a bill. Will the one to come be substantially better than the one that would
have emerged if the same speech had been delivered weeks earlier? Not
necessarily  --  and marginally at most.

In the meantime, a certain damage has been done  --  to Obama and to the
country. The inmates took over the asylum, trivializing and poisoning the
national discourse while the president bided his time. The lies that Obama
called out so strongly in his speech  --  from ''death panels'' to ''government
takeover''  --  ran amok. So did all the other incendiary faux controversies,
culminating with the ludicrous outcry over the prospect that the president might
speak to the nation's schoolchildren on a higher plane than, say, ''The Pet
Goat.''

None of this served his cause of health care reform or his political standing.
The droop in Obama's job approval numbers isn't remotely as large or precipitous
as the Beltway's incessant doomsday drumbeat  suggests. But support for his
signature program declined, not least because he gave others carte blanche to
define it for him. Perhaps the most revealing of all the poll findings came in
an end-of-August Washington Post query asking voters what ''single word'' first
came to mind to describe their ''feelings'' about Obama and his health care
proposals. For Obama, the No. 1 feeling was ''good.'' For the policy package
he'd been ostensibly selling all summer, the No. 1 feeling was ''none.''

It's not, as those on the right would have us believe, that Obama's ideas are so
''liberal'' that the American public recoiled. It's that much of the public
didn't know what his ideas were. Even now I'm not convinced that most Americans
know what a ''public option'' really means or what Obama's precise position on
it is. But I'd bet that many more have a working definition of ''death panels.''
The 24-hour news cycle abhors a vacuum, and the liars and crazies filled it
while Obama waited for his deus ex machina descent onto center stage.

That he let the hard-core base of a leaderless minority party drive the debate
only diminished his stature. That's why his poll numbers on ''leadership''
declined. The right-wing fringe has become so deranged that it will yank its
kids out of school to protest the president and risk yanking more Americans off
assembly lines by boycotting General Motors to protest the administration's
Detroit bailout. Even Laura Bush and Newt Gingrich stepped in last week to
defend Obama's classroom  homily from the fusillades by some of their own
party's most prominent ideologues. The White House should have landed a punch
before they did.

Obama would have looked stronger if he'd stood up more proactively to the
screamers along the way, or at least to the ones not packing guns. As the
Roosevelt biographer Jean Edward Smith has reminded us, it didn't harm the New
Deal for F.D.R. to tell a national radio audience on election eve 1936 that he
welcomed the ''hatred'' of his enemies. Indeed Obama instantly gained a foot or
two in height Wednesday night once that South Carolina clown hollered ''You
lie!'' (One wonders what this congressman calls the Republican governor of his
own state, Mark Sanford.) As the political analyst Charlie Cook has pointed out,
Obama's leadership poll numbers have also suffered from his repeated deference
to Congress. Waiting for the pettifogging small-state potentates of both parties
in the Senate's Gang of Six is as farcical as waiting for Godot.

Now that he has taken charge, Obama will speed the process and, we must hope,
secure reform that may make a real difference for everyone, starting with the
46-million-plus Americans who have no health insurance. But when we gain some
perspective on the summer of 2009, the health care debate, like the crazed
town-hall sideshows surrounding it, may seem very small in the history of this
presidency  --  maybe even as small as the Condit follies and the breathlessly
reported shark attacks of summer 2001 now look in the history of the previous
administration.

The reason is that health care reform, while an overdue imperative, still is
overshadowed in existential  urgency by the legacies of the two devastating
cataclysms of the Bush years, 9/11 and 9/15, both of whose anniversaries we now
mark. The crucial matters left unresolved in the wake of New York's two
demolished capitalist icons, the World Trade Center and Lehman Brothers, are
most likely to determine both this president's and our country's fate in the
next few years. Both have been left to smolder in the silly summer of '09.

As we approach the eighth anniversary of the war that 9/11 bequeathed us in
Afghanistan, the endgame is still unknown and more troops are on their way.
Though the rate of American casualties reached an all-time high last month, the
war ranks at or near the bottom of polls tracking the issues important to the
American public. Most of those who do have an opinion about the war  oppose it
(57 percent in the latest CNN poll released on Sept. 1) and oppose sending more
combat troops (56 percent in the McClatchy-Ipsos survey, also released on Sept.
1). But the essential national debate about whether we really want to double
down in Afghanistan  --  and make the heavy sacrifices that would be required
--  or look for a Plan B was punted by the White House this summer even as the
situation drastically deteriorated.

No less unsettling is the first-anniversary snapshot of 9/15: a rebound for Wall
Street but not for the 26-million-plus Americans who are unemployed, no longer
looking for jobs, or forced to settle for part-time work. Some 40 million
Americans are living in poverty. While these economic body counts keep rising,
tough regulatory reform for reckless financial institutions, too-big-to-fail and
otherwise, seems more remote by the day. Last Sunday, Jenny Anderson of The
Times exposed an example of Wall Street's unashamed recidivism that takes
gallows humor to a new high  --  or would were it in The Onion, not The Times.
Some of the same banks that gambled their (and our) way to ruin by concocting
exotic mortgage-backed securities now hope to bundle individual Americans' life
insurance policies into a new high-risk financial product built on this
sure-fire algorithm: ''The earlier the policyholder dies, the bigger the
return.''

When we look back on these months, we may come to realize that there were in
fact ''death panels'' threatening Americans all along  --  but they were on the
Afghanistan-Pakistan border and on Wall Street, not in the fine print of a
health care bill on Capitol Hill. Obama's deliberative brand of
wait-and-then-pounce leadership let him squeak  --  barely  --  through the
summer. The real crises already gathering won't wait for him to stand back and
calculate the precise moment to spring the next Do-or-Die Speech.

URL: http://www.nytimes.com

LOAD-DATE: September 13, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY BARRY BLITT)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


