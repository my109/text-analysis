                   Copyright 2009 The New York Times Company


                             692 of 2021 DOCUMENTS


                               The New York Times

                          September 26, 2009 Saturday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 906 words


International

TOP U.S. MILITARY OFFICIALS MET SECRETLY IN GERMANY

Gen. Stanley A. McChrystal, the top commander of American and NATO forces in
Afghanistan, met with Adm. Mike Mullen, the chairman of the Joint Chiefs of
Staff, to discuss sending more troops to Afghanistan, Pentagon officials said.
PAGE A4

PHILIPPINE ISLAMISTS REGROUP

The Islamic separatist group Abu Sayyaf, driven from the bigger towns, continues
to operate in the dense forests of Basilan, an island in the southern
Philippines. PAGE A4

MISHAP KILLS 15 IRAQI SOLDIERS

At least 15 Iraqi soldiers were killed as they tried to detonate explosives
seized in northern Iraq, American and Iraqi officials said. PAGE A6

5 U.S. DEATHS IN AFGHANISTAN

A roadside bomb and assault-rifle fire killed four soldiers and a Marine in
three attacks in Zabul and Nimruz Provinces. PAGE A6

QAEDA TAPE WARNS EUROPE

A new recording from Osama bin Laden urged Europe to withdraw from Afghanistan.
The tape seemed particularly directed at Germany, which is on alert before
parliamentary elections on Sunday. PAGE A6

CHaVEZ SAID TO ENLIST IRAN

A senior aide to Venezuela's president, Hugo Chavez, said Iran was helping
Venezuela detect and test uranium deposits found in remote areas of Venezuela.
PAGE A8

Trial of Israel's Ex-Leader Begins A5

U.S. Roots Aid Israeli Envoy A5

Gas Explosion Rattles Beijing A7

Iran's President Mocks Charges A9

Obituaries

SUSAN ATKINS, 61

A member of Charles Manson's ''family,'' she spent the last four decades in
prison for her role in the killings of the actress Sharon Tate and seven others
in 1969. PAGE A18

National

CUTS DEVASTATE CALIFORNIA DOMESTIC ABUSE SHELTERS

State budget cutbacks have forced at least six domestic violence shelters in
California to close. Many others have had to lay off workers and curtail
services. PAGE A10

ATLANTA CLINIC WILL CLOSE

Uninsured patients who could be cut off from life-sustaining dialysis lost a
challenge when a state judge ruled that Grady Memorial Hospital could close its
outpatient dialysis clinic. But the hospital gave the patients a temporary
reprieve. PAGE A10

DETAILS ON FLU VACCINATION

The start of the swine flu vaccination campaign is ''going to be a little
bumpy,'' the director of the Centers for Disease Control and Prevention
predicted as he gave new details about how the vaccine would be distributed.
PAGE A11

SENATE HEALTH BILL ENDURES

Senate Democrats beat back Republican attacks on legislation meant to overhaul
the health care system. Still, the Senate Finance Committee deferred some of the
most contentious issues to next week. PAGE A12

SHIFT IN PICKING COLLEGE

The talk at an annual gathering of college admissions officers and high school
counselors in Baltimore seemed to indicate that the economy has some parents
focusing less on ''Can I get in?'' and more on ''Can I afford it?'' PAGE A13

20-Year Ruling in Hepatitis Case A13

Judge Orders Detainee Released A15

The Schwarzenegger Shower A15

New York

AMID A CITYWIDE SLUMP, AN EXCEPTION IN BROOKLYN

Rents that are more affordable than in surrounding, more affluent neighborhoods
and pent-up demand among residents have helped draw new shops and restaurants to
Bedford-Stuyvesant. PAGE A17

Business

NEW CHALLENGE IN GAMING: BEAT THE CELLPHONE APPS

The behemoths of the video game industry are scrambling to fend off an
unexpected onslaught from a newcomer who has altered the battlefield: Apple.
PAGE B1

RARE MINERAL HUNT IS INCITED

A Chinese threat to halt exports of rare minerals -- vital for high-performance
electric motors -- appears to have backfired, sparking a frenzied international
effort to develop alternative mines. PAGE B1

FIGHT OVER THE FLEX ACCOUNT

To help pay for health care reform, a cap on pretax accounts for health care
spending has been proposed. The question is whether such a cap is worth
fighting. Your Money, by Ron Lieber. PAGE B1

CHANNELING OBAMA AT G-20

An imagined pitch from President Obama in Pittsburgh, jawboning his peers on the
need for more bank capital. Talking Business, by Joe Nocera. PAGE B1

HIRING AN ELDER CARE EXPERT

When especially overwhelmed, many people caring for an elderly parent hire the
equivalent of a case worker. Patient Money.  PAGE B6

Cadbury Chief Says No Deal B2

Actors Guild Elects President B2

Europe's Stress Tests Show Risk B3

UBS Client Pleads to Tax Evasion B3

Signs of a Halting Recovery B3

HSBC Chief Goes to Hong Kong B4

Sports

TWO MISSED PUTTS BY WOODS THROW THE FIELD A LIFELINE

Tiger Woods missed putts inside five feet at the 15th and 16th holes at the Tour
Championship, forcing him to settle for merely taking over the lead at the
halfway point. PAGE B9

Phillies' Closers Cause Concern B9

A Wild Piston Mentors Shock B10

Arts

MORALS CLASS IS STARTING; PLEASE PASS THE POPCORN

The class that Michael J. Sandel, a moral philosopher, teaches at Harvard is
about to grow. Public television viewers will be able to sit in on his classroom
discussions for the next 12 weeks. PAGE C1

A SHOW OF CLEVELAND'S OWN

''The Cleveland Show,'' Seth MacFarlane's spin-off of his hit show ''Family
Guy,'' continues Mr. MacFarlane's indictment of the middle-aged American
suburban male. PAGE C1

Sinatra Sings as Dancers Swing C1

Ailing Orchestra Is Still Robust C1

Possible Fraud in Film Mementos C3

Op-ed

GAIL COLLINS PAGE A21

BOB HERBERT PAGE A21

CHARLES M. BLOW PAGE A21

URL: http://www.nytimes.com

LOAD-DATE: September 26, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


