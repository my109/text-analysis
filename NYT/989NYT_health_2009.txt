                   Copyright 2009 The New York Times Company


                             989 of 2021 DOCUMENTS


                               The New York Times

                           November 12, 2009 Thursday
                              Late Edition - Final

Trading Women's Rights for Political Power

BYLINE: By KATE MICHELMAN and FRANCES KISSLING.

Kate Michelman is the former president of Naral Pro-Choice America. Frances
Kissling is the former president of Catholics for Choice.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTORS; Pg. 35

LENGTH: 658 words

DATELINE: Washington


A  GRIM reality sits behind the joyful press statements from Washington
Democrats. To secure passage of health care legislation in the House, the party
chose a course that risks the well-being of millions of women for generations to
come.

House Democrats voted to expand the current ban on public financing for abortion
and to effectively prohibit women who participate in the proposed health system
from obtaining private insurance that covers the full range of reproductive
health options. Political calculation aside, the House Democrats reinforced the
principle that a minority view on the morality of abortion can determine
reproductive health policy for American women.

Many House members who support abortion rights decided reluctantly to accept
this ban, which is embodied in the Stupak-Pitts amendment. They say the tradeoff
was necessary to advance the right to guaranteed health care. They say they will
fight another day for a woman's right to choose.

Perhaps. But they can't ignore the underlying shift that has taken place in
recent years. The Democratic majority has abandoned its platform and
subordinated women's health to short-term political success. In doing so, these
so-called friends of women's rights have arguably done more to undermine
reproductive rights than some of abortion's staunchest foes. That Senate
Democrats are poised to allow similar anti-abortion language in their bill
simply underscores the degree of the damage that has been done.

Many women -- ourselves included -- warned the Democratic Party in 2004 that it
was a mistake to build a Congressional majority by recruiting and electing
candidates opposed to the party's commitment to legal abortion and to public
financing for the procedure. Instead, the lust for power yielded to misguided,
self-serving poll analysis by operatives with no experience in the fight for
these principles. They mistakenly believed that giving leadership roles to a
small minority of anti-abortion Democrats would solve the party's image problems
with ''values voters'' and answer critics who claimed Democrats were hostile to
religion.

Democrats were told to stop talking about abortion as a moral and legal right
and to focus instead on comforting language about reducing the number of
abortions. In this regard, President Obama was right on message when he declared
in his health care speech to Congress in September that ''under our plan, no
federal dollars will be used to fund abortions'' -- as if this happened to be a
good and moral thing. (The tone of his statement made the point even more
sharply than his words.)

The party has distanced itself from the abortion-rights movement in other ways.
It has taken to calling Democrats who oppose a woman's right to choose
''pro-life'' (and not ''anti-choice''). The group Democrats for Life of America,
whose Congressional members ultimately led the battle to exclude private
insurance companies that cover abortions from health insurance exchanges, was
invited to hold a press conference in Democratic Party offices. The party has
promoted ''pro-life progressives'' like Sojourners, Catholics United and
Catholics in Alliance for the Common Good, organizations whose leaders have
stated that abortions should be made ''more difficult to get.''

This, then, is where we stand as party leaders celebrate passage of the House
bill. When it comes to abortion, they seem to think all positions are of equal
value so long as the party maintains a majority. But the party will eventually
reap what it has sown. If Democrats do not commit themselves to defeating the
amendment, then they will face an uncompromising effort by Democratic women to
defeat them, regardless of the cost to the party's precious majority.

In the meantime, the victims of their folly will be the millions of women who
once could count on the Democratic Party to protect them from those who would
sacrifice their rights for political gains.

URL: http://www.nytimes.com

LOAD-DATE: November 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY ALEX NABAUM)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


