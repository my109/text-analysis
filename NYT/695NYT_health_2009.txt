                   Copyright 2009 The New York Times Company


                             695 of 2021 DOCUMENTS


                               The New York Times

                          September 26, 2009 Saturday
                              Late Edition - Final

The Cracks in a United Front

BYLINE: By JACKIE CALMES

SECTION: Section A; Column 0; National Desk; CONGRESSIONAL MEMO; Pg. 12

LENGTH: 960 words

DATELINE: WASHINGTON


Over four days and three late nights of meetings, Democrats on the Senate
Finance Committee have largely stood up to Republicans' attacks on a proposal to
overhaul the health care system.

But behind the scenes and away from the C-Span cameras, their united front has
given way to intraparty tensions, not just in the committee but in Congress
generally.

Those cracks will become more evident next week when the Finance Committee tries
to finish its work and liberals press to change a bill that is too conservative
for their liking. In the main event, they will propose a public option to
compete against private insurers in new exchanges where uninsured individuals
and small businesses would be able to shop for coverage.

The liberals do not expect to win in the moderate-to-conservative-leaning
committee. But Senator Charles E. Schumer, Democrat of New York, said, ''That's
just the first battle of a war, and the least friendly battlefield.''

The Senate floor, and certainly a conference with the more liberal House, will
be more receptive arenas, Mr. Schumer and others predict. Ultimately, the
liberals in Congress, as well as their allies in organized labor, expect to be
able to shape the final product more than they had hoped just weeks ago.

That unnerves the more conservative Democrats, many of them from
Republican-leaning districts and states.

Liberals have been emboldened by two factors. One is the failure of Senator Max
Baucus of Montana, a more conservative Democrat who heads the Finance Committee,
to get any Republicans to support his draft legislation, after months of trying.
That doomed President Obama's goal of bipartisan backing for a health care
overhaul, and now leaves party liberals arguing for a distinctly Democratic
health plan.

''One of the strongest arguments against a public option has been that the
Republicans will never go for it,'' Mr. Schumer said. ''Well, the Baucus bill
doesn't have a public option, and they're still not for it in any way, with the
possible exception of Olympia Snowe,'' a moderate Republican senator from Maine,
who has not ruled out supporting the overhaul that Mr. Obama is seeking.

The second development that has encouraged liberals is recent polling, including
some done for The New York Times and CBS News in the last week, that gives
Democrats a clear edge over Republicans as the party favored to deal with health
care issues. The same polls show significant support for a public option despite
months of criticism from Republicans, who describe it as a government takeover
of health insurance.

Congressional Democrats of all stripes have become more upbeat since returning
to work after the August recess, when they were thrown on the defensive by
conservative opponents' disruptions at public forums on health care. While the
mood could just as quickly shift again, liberal and moderate Democrats seem to
agree that they will pass a health care measure, if only because failure could
be politically devastating given Mr. Obama's stake, and theirs, in the outcome.

The sense that something will become law has only strengthened the resolve of
liberals, inside the Congress and out, to fight with intensity as Democrats
write the legislation this fall.

In the Finance Committee this week, some of the liberal members privately said
they were feeling squeezed between pressure from the White House to be good
party soldiers -- that is, to stifle their differences with Mr. Baucus's
legislation and quickly push a bill to the full Senate -- and pressure from
major unions to fight in the committee for the public option and other changes.

The Obama administration's argument to liberals has been that they should hold
their fire now, knowing that their leverage will be greater in the final
negotiations between the House and the Senate. Four other committees, three of
them in the House, have approved more liberal bills.

But to the chagrin of many Democrats, the Finance Committee's product has been
considered the most important, since only a relatively moderate package is
deemed capable of passing in the Senate.

Senate Democrats have a filibuster-proof majority of 60 lawmakers. But a number
of them are centrists, and the party cannot afford many defections, given
Republicans' nearly unanimous opposition. Further, seeking a semblance of
bipartisanship, the White House still wants Ms. Snowe's vote.

Yet unions, whose efforts were vital to the election of Mr. Obama and many
Congressional Democrats, counter by saying the Baucus bill is too objectionable
to let it slide. Not only does it lack a public option, it would not mandate
that employers provide insurance to their workers or else contribute toward
subsidies that would help the uninsured buy coverage.

But labor's main complaint is Mr. Baucus's proposal to tax insurance companies
for their most generous policies, as a way to raise revenue and to discourage
wasteful health care spending. Labor says insurers would pass on their tax costs
in higher premiums, not just for corporate executives but also for unionized
workers with rich health benefits.

Under Mr. Baucus's plan, the tax would apply to family policies worth more than
$21,000 a year. The typical employer-provided family plan costs roughly $13,000,
but packages for some unionized workers can run much higher. Democrats are
discussing raising the threshold, and the White House has privately assured
labor that union benefits will not be affected.

This week, with the president occupied by global issues at the United Nations
and the G-20 conference, Vice President Joseph R. Biden Jr. was enlisted to
intercede with some liberals and keep them on board. In coming weeks, Democrats
say, Mr. Obama will have to be much more involved.

URL: http://www.nytimes.com

LOAD-DATE: September 26, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Unlike centrist colleagues, Senator Charles E. Schumer, right,
is pushing a public option.(PHOTOGRAPH BY STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


