                   Copyright 2009 The New York Times Company


                             972 of 2021 DOCUMENTS


                               The New York Times

                           November 10, 2009 Tuesday
                              Late Edition - Final

A Word, Mr. President

BYLINE: By BOB HERBERT

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 35

LENGTH: 802 words


If I were a close adviser of President Obama's, I would say to him, ''Mr.
President, you have two urgent and overwhelming tasks in front of you: to put
Americans trapped in this terrible employment crisis back to work and to put the
brakes on your potentially disastrous plan to escalate the war in Afghanistan.''

Reforming the chaotic and unfair health care system in the U.S. is an important
issue. But in terms of pressing national priorities, the most important are the
need to find solutions to a catastrophic employment environment that is
devastating American families and to end the folly of an 8-year-old war that is
both extremely debilitating and ultimately unwinnable.

We have spent the better part of a year locked in a tedious and unenlightening
debate over health care while the jobless rate has steadily surged. It's now at
10.2 percent. Families struggling with job losses, home foreclosures and
personal bankruptcies are falling out of the middle class like fruit through the
bottom of a rotten basket. The jobless rate for men 16 years old and over is
11.4 percent. For blacks, it's a back-breaking 15.7 percent.

We need to readjust our focus. We're worried about Kabul when Detroit has gone
down for the count.

I would tell the president that more and more Americans are questioning his
priorities, including millions who went to the mat for him in last year's
election. The biggest issue by far for most Americans is employment. The lack of
jobs is fueling the nervousness, anxiety and full-blown anger that are becoming
increasingly evident in the public at large.

Last Friday, a huge crowd of fans marched in a ticker-tape parade in downtown
Manhattan to celebrate the Yankees' World Series championship. More than once,
as the fans passed through the financial district, the crowd erupted in
rhythmic, echoing chants of ''Wall Street sucks! Wall Street sucks!''

I would tell the president that the feeling is widespread that his
administration went too far with its bailouts of the financial industry, sending
not just a badly needed lifeline but also unwarranted windfalls to the
miscreants who nearly wrecked the entire economy. The government got very little
in return. The perception now is that Wall Street is doing just fine while
working people, whose taxes financed the bailouts, are walking the plank to
economic oblivion.

I would also tell him that rebuilding the economy in a way that allows working
Americans to flourish will require a sustained monumental effort, not just bits
and pieces of legislation here and there. But such an effort will never get off
the ground, will never have any chance of reaching critical mass and actually
succeeding, as long as we insist on feeding young, healthy American men and
women and endless American dollars into the relentless meat grinders of
Afghanistan and Iraq.

We learned in the 1960s, when Lyndon Johnson's Great Society was trumped by
Vietnam, that nation-building here at home is incompatible with the demands of
war. We've managed to keep the worst of the carnage  --  and the staggering
costs  --  of Iraq and Afghanistan well out of the sight of most Americans, so
the full extent of the terrible price we are paying is not widely understood.

The ultimate financial costs will be counted in the trillions. If you were to
take a walk around one of the many military medical centers, like Landstuhl in
Germany or Walter Reed in Washington, your heart would break at the sight of the
heroic young men and women who have lost limbs (frequently more than one) or who
are blind or paralyzed or horribly burned. Hundreds of thousands have suffered
psychological wounds. Many have contemplated or tried suicide, and far too many
have succeeded.

''Mr. President,'' I would say, ''we'll never be right as a nation as long as we
allow this to continue.''

The possibility of more troops for the war in Afghanistan was discussed Sunday
on ''Meet the Press.'' Gov. Ed Rendell of Pennsylvania noted candidly that ''our
troops are tired and worn out.'' More than 85 percent of the men and women in
the Pennsylvania National Guard have already served in Iraq or Afghanistan.
''Many of them have gone three or four times and they're wasted,'' said Mr.
Rendell.

More troops? ''Where are we going to find these troops?'' the governor asked.
''That's what I want somebody to tell me.''

While we're preparing to pour more resources into Afghanistan, the Economic
Policy Institute is telling us that one in five American children is living in
poverty, that nearly 35 percent of African-American children are living in
poverty, and that the unemployment crisis is pushing us toward a point in the
coming years where more than half of all black children in this country will be
poor.

''Mr. President,'' I would say, ''we need your help.''

URL: http://www.nytimes.com

LOAD-DATE: November 10, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


