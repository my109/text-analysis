                   Copyright 2009 The New York Times Company


                             634 of 2021 DOCUMENTS


                               The New York Times

                          September 19, 2009 Saturday
                              Late Edition - Final

SECTION: Section A; Column 0; National Desk; HOW THEY DO IT OVER THERE; Pg. 11

LENGTH: 286 words


Timothy Stoltzfus Jost is a law professor at Washington and Lee University and
frequently writes on comparative health care policy. His work includes an
examination of insurance coverage in Switzerland and a comparison of the Swiss
and Dutch systems. He spoke to Anne Underwood, a Prescriptions contributor.

Q. The Swiss health care system relies on public-private approaches that have
been recommended as models for the United States. What are the similarities?

A. In 1996, Switzerland instituted an individual mandate by which people are
legally required to purchase health insurance in a competitive market. People
buy coverage from private insurers, and the government provides subsidies for
those who can't afford coverage. About a third of the population receives
subsidies.

Q. What is a major difference between the Swiss system and most of the proposals
on Capitol Hill?

A. The most important difference is that health insurance in Switzerland is
provided by nonprofit insurers, though some are affiliated with for-profit
companies that offer supplemental policies along the lines of Medigap in the
United States. The basic benefit package is defined by law and is quite
generous. Maximum drug prices are regulated.

Q. How is the quality of care?

A. The quality of care is excellent. Waiting times are not reported to be a
serious problem in Switzerland, and most people can get the services they need
quite expeditiously. Modern, high-technology services are readily available.
Coverage of some new drugs and procedures, however, is reviewed for
effectiveness, and some drugs and procedures available in other countries may
not be available in Switzerland if they are not considered to be cost-effective.

URL: http://www.nytimes.com

LOAD-DATE: September 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHART: By the Numbers (Source: World Health Organization)

PUBLICATION-TYPE: Newspaper


