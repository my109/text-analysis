                   Copyright 2010 The New York Times Company


                             1380 of 2021 DOCUMENTS


                               The New York Times

                            January 19, 2010 Tuesday
                              Late Edition - Final

DEMOCRATS SEEK OPTIONS TO KEEP HEALTH BILL ALIVE

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR; Carl Hulse contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1250 words

DATELINE: WASHINGTON


The White House and Democratic Congressional leaders, scrambling for a backup
plan to rescue their health care legislation if Republicans win the special
election in Massachusetts on Tuesday, have begun laying the groundwork to ask
House Democrats to approve the Senate version of the bill and send it directly
to President Obama for his signature.

A victory by the Republican, Scott Brown, in Massachusetts would deny Democrats
the  60th vote they need in the Senate to surmount Republican filibusters and
advance the health legislation.

And with the race too close to call, Democrats are considering several options
to save the bill, which could be a major factor in how they fare in this year's
midterm elections.

Some Democrats suggested that even if their candidate, Martha Coakley, scraped
out a narrow victory on Tuesday, they might need to ask House Democrats to speed
the legislation to the president's desk, especially if lawmakers who had
supported the bill begin to waver as they consider the political implications of
a tough re-election cycle.

It is unclear if rank-and-file Democrats would go along, and House Democratic
leaders said no final decision would be made until they talked to their caucus.

But even as Democratic leaders pondered contingencies, the House speaker, Nancy
Pelosi, insisted that the legislation would move forward, though she
acknowledged that Tuesday's results could force a tactical shift.

''Certainly the dynamic will change depending on what happens in
Massachusetts,'' Ms. Pelosi told reporters in California on Monday. ''Just the
question of how we would proceed. But it doesn't mean we won't have a health
care bill.''

''Let's remove all doubt,'' she added. ''We will have health care one way or
another.''

Still, some lawmakers, aides and lobbyists described numerous obstacles to House
approval of the Senate-passed bill.

House Democrats have voiced a number of complaints with the Senate measure, and
top White House officials and Congressional leaders have struggled to bridge
differences between the two bills. Despite promises by Mr. Obama and
Congressional leaders to add those hard-fought deals and other changes later,
there would be no guarantees.

In an interview on Monday, Representative Bart Stupak, a Michigan Democrat  who
opposes the Senate bill in part because of provisions related to insurance
coverage of abortions, said: ''House members will not vote for the Senate bill.
There's no interest in that.''

When the idea was suggested at a Democratic caucus meeting last week, Mr. Stupak
said, ''It went over like a lead balloon.''

''Why would any House member vote for the Senate bill, which is loaded with
special-interest provisions for certain states?'' Mr. Stupak asked. ''That's not
health care.''

In addition to his concerns on the abortion provisions, Mr. Stupak said the
Senate bill did not do enough to improve the quality of health care, and it
preserved the federal antitrust exemption for health insurance, which the House
bill would repeal.

Officials in Congress and at the White House emphasized that the backup plan was
hypothetical, and that they hoped it would never be needed.

Many Democrats, eager to exude confidence, declined to comment publicly on the
possibility of a Plan B. A House Democratic leadership aide said: ''Obviously,
the outcome of Tuesday's election will be a factor in how we move forward, but
we are hopeful and continue to work towards a compromise that will garner 60
votes in the Senate. If we end up short of 60, we will need to see. But there
will be problems with just taking up the Senate bill.''

Republicans said they expected Democrats to do whatever it takes to pass the
bill.

''They are going to try every way, shape and form to shove this bill down the
throats of the American people,'' the House Republican leader, Representative
John A. Boehner of Ohio, said in a radio interview on Monday.

Persuading House Democrats to adopt the bill approved by the Senate on Christmas
Eve would obviate the need for an additional Senate vote. If Mr. Brown wins,
there might be enough time for Democrats to rush a revised bill through the
Senate before he is sworn in. But Democratic leaders have essentially ruled that
out as a politically perilous option.

Asking House Democrats to adopt the Senate bill is itself a high-risk call.
Agreements  to resolve differences would essentially be out the window.

The White House and labor unions, for instance, reached a tentative deal last
week on an excise tax on high-priced, employer-sponsored insurance plans. Labor
groups, an important segment of the Democratic Party's base, strongly opposed
the version of the tax included in the Senate bill because they said it would
hit too many union-sponsored health plans and hurt middle-class workers.

And then there is an array of outstanding issues on which no consensus has been
reached, like the emotionally charged issue of insurance coverage for abortions.

The House bill was passed by a vote of 220 to 215 in November. The restrictions
on abortion coverage were approved on a 240-to-194 vote, with support from 64
Democrats, including 41 who also voted to pass the bill.

The Republican whip, Representative Eric Cantor of Virginia, said he had
identified 11 ''pro-life House Democrats'' whose votes ''could be in play'' if
the abortion restrictions were weakened.

There are other disagreements. Members of the Congressional Hispanic Caucus, for
example, strongly oppose a Senate provision to bar illegal immigrants from
buying health insurance through new government-regulated exchanges even if they
paid the full cost with their own money. The House bill would permit illegal
immigrants to buy coverage on their own.

Despite these differences, the bills over all are similar, if not identical, on
a vast majority of issues. And supporters of the fallback strategy say it puts
Democrats just a single vote from sending historic legislation to Mr. Obama for
his signature.

Still, several House Democrats sounded less than enthusiastic Monday about the
possibility of adopting the Senate bill.

Representative Allyson Y. Schwartz, Democrat of Pennsylvania, said, ''It is my
hope that we will continue to work to take the best provisions from the House
and Senate bills to produce the right bill for America.''

Representative Robert E. Andrews, Democrat of New Jersey and chairman of the
Education and Labor subcommittee on health, said: ''Obviously, our first
preference would be to continue to negotiate and achieve 60 votes in the Senate.
Until it becomes obvious that we can't do that, I don't think we want to
speculate on any other possibility.''

Eric M. Ueland, a lobbyist who used to work for the Senate Republican
leadership, described the uncertainty surrounding a two-bill strategy this way:
''It's tantamount to the Senate saying to the House, 'We cannot tell you what,
when, why or how, but trust us, it will all work out.' ''

Ms. Pelosi, however, said that it was Republicans who could not be trusted on
health care, and that Democrats would not squander the opportunity to pass a
bill.

''I heard the candidate in Massachusetts, the Republican candidate, say 'Let's
go back to the drawing board,' '' Ms. Pelosi said. ''The drawing board for the
Republican Party on health care is to tear it up and throw it away and shred it
and never revisit it.''

She added: ''Back to the drawing board means a great big zero for the American
people.''

URL: http://www.nytimes.com

LOAD-DATE: January 20, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


