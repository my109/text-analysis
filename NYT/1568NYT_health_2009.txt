                   Copyright 2010 The New York Times Company


                             1568 of 2021 DOCUMENTS


                               The New York Times

                           February 25, 2010 Thursday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 767 words


International

BALANCING ACT AS INDIA

Reopens Talks With Pakistan

The resumption of diplomatic talks between India and Pakistan comes at a
critical moment, with the United States hoping that even a modest improvement in
relations between the nuclear-armed neighbors could help the broader American
military effort in Pakistan and Afghanistan. But analysts say achieving that is
likely to prove a challenge. PAGE A8

Darfur Peace Deal Stirs Hope A9

National

FIRST CHARGES FROM INQUIRY INTO KATRINA POLICE SHOOTING

Lt. Michael J. Lohman of the New Orleans Police Department pleaded guilty in
federal court to one count of conspiring to obstruct justice. It is the first
charge in a wide-ranging probe into police misconduct that led to civilian
deaths in the chaotic days after Katrina, and it is unlikely to be the last.
PAGE A14

RULING IN MIRANDA CASE

The Supreme Court ruled that the police can take a second run at questioning a
suspect who has invoked his Miranda rights, but they must wait until 14 days
after the suspect has been released from custody. PAGE A18

A DIVISIVE MEASURE

Few parliamentary rules are more divisive than reconciliation. And with the
Obama administration and Democrats in Congress leaning toward employing
reconciliation in hopes of passing their health care legislation, never has it
been put to use in a more bitterly partisan atmosphere. Congressional Memo. PAGE
A21

More Money to Charter Schools A16

New York

LEGAL TROUBLES MAY LOOM

For Acquitted Officer

Although Officer Richard Kern was acquitted of sexually abusing a suspect on a
subway platform, in the coming weeks and months, he and his co-defendants,
Officers Alex Cruz and Andrew Morales, could face stiff legal challenges over
the events, both within the Police Department and in federal court.   PAGE A27

Business

CITING SLOW RECOVERY,

Fed Does Not Change Rates

Ben S. Bernanke, the Federal Reserve chairman, told Congress that the central
bank did not intend to start raising short-term interest rates anytime soon,
saying the economic recovery would remain halting for months to come. His
message lifted the stock market, even as a new report showed a bleak drop in
sales of new homes. PAGE B1

G.M. TO SCUTTLE HUMMER

General Motors said it would shut down Hummer, the brand of big sport utility
vehicles that became synonymous with the term gas guzzler, after a deal to sell
it to a Chinese manufacturer fell apart.  PAGE B1

INDIA WRESTLES WITH DEBT

Overburdened with debt, India has raised $3.5 billion by selling off pieces of
state-run companies. The country desperately needs the cash to finance
development projects. But there are serious doubts about whether India can
maintain the trend, or accelerate it, as economists have urged. PAGE B3

The Pocket Projector Trend B1

S.E.C. Restricts Short-Selling B3

Sports

METS PLACE HOPES

On Venezuelan Players

Five players from Venezuela are likely to account for more than a third of the
Mets' contingent of pitchers and catchers, and how those players fare as a group
could go a long way in determining how the Mets do as a team. PAGE B19

BIDDING TO COVER OLYMPICS

High prime-time ratings have helped NBC Universal feel vindicated by a strategy
that features tape delay of some events. But fans who crave the chance to see
events live might find an ally in ESPN, which plans to bid for the 2014 Winter
and 2016 Summer Games. It would discontinue the tape-delay template.  PAGE B14

Arts

A DEVIL WHO TAKES

The Side of Angels

Janet Maslin writes that Joe Hill's novel ''Horns,'' whose protagonist sprouts
nubby horns from his head that compel people  to voice their most unspeakable
thoughts, is able to combine intrigue, editorializing, romance and fiery
theological debate in one well-told story.  PAGE C1

Home

WALT FRAZIER, EX-KNICK,

Settles In on St. Croix

Walt ''Clyde'' Frazier, a Hall of Fame player for the New York Knicks, bought a
single-level home on a one-acre property on St. Croix for $215,000 in 1979,
while looking for a tranquil hideaway after years as a paparazzi magnet -- a
pivotal moment in his transition from renowned hedonist to home-building
horticulturist. PAGE D1

Styles

BEFORE THE GLITTER,

Plenty of Bruises

Figure skaters at the Olympics make their sport look easy. Caydee Denney and
Jeremy Barrett, the top American pairs team heading into the Vancouver Winter
Games, say that people who want to try those moves  at home must develop good
fundamentals before trying them  on the ice --  and get used to the
inevitability of falling. A lot. PAGE E1

Op-ed

NICHOLAS D. KRISTOF PAGE A33

URL: http://www.nytimes.com

LOAD-DATE: February 25, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


