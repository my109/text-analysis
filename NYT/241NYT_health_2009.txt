                   Copyright 2009 The New York Times Company


                             241 of 2021 DOCUMENTS


                               The New York Times

                              July 31, 2009 Friday
                              Late Edition - Final

Health Care Realities

BYLINE: By PAUL KRUGMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 23

LENGTH: 795 words


At a recent town hall meeting, a man stood up and told Representative Bob Inglis
to ''keep your government hands off my Medicare.'' The congressman, a Republican
from South Carolina, tried to explain that Medicare is already a government
program -- but the voter, Mr. Inglis said, ''wasn't having any of it.''

It's a funny story -- but it illustrates the extent to which health reform must
climb a wall of misinformation. It's not just that many Americans don't
understand what President Obama is proposing; many people don't understand the
way American health care works right now. They don't understand, in particular,
that getting the government involved in health care wouldn't be a radical step:
the government is already deeply involved, even in private insurance.

And that government involvement is the only reason our system works at all.

The key thing you need to know about health care is that it depends crucially on
insurance. You don't know when or whether you'll need treatment -- but if you
do, treatment can be extremely expensive, well beyond what most people can pay
out of pocket. Triple coronary bypasses, not routine doctor's visits, are where
the real money is, so insurance is essential.

Yet private markets for health insurance, left to their own devices, work very
badly: insurers deny as many claims as possible, and they also try to avoid
covering people who are likely to need care. Horror stories are legion: the
insurance company that refused to pay for urgently needed cancer surgery because
of questions about the patient's acne treatment; the healthy young woman denied
coverage because she briefly saw a psychologist after breaking up with her
boyfriend.

And in their efforts to avoid ''medical losses,'' the industry term for paying
medical bills, insurers spend much of the money taken in through premiums not on
medical treatment, but on ''underwriting'' -- screening out people likely to
make insurance claims. In the individual insurance market, where people buy
insurance directly rather than getting it through their employers, so much money
goes into underwriting and other expenses that only around 70 cents of each
premium dollar actually goes to care.

Still, most Americans do have health insurance, and are reasonably satisfied
with it. How is that possible, when insurance markets work so badly? The answer
is government intervention.

Most obviously, the government directly provides insurance via Medicare and
other programs. Before Medicare was established, more than 40 percent of elderly
Americans lacked any kind of health insurance. Today, Medicare -- which is, by
the way, one of those ''single payer'' systems conservatives love to demonize --
covers everyone 65 and older. And surveys show that Medicare recipients are much
more satisfied with their coverage than Americans with private insurance.

Still, most Americans under 65 do have some form of private insurance. The vast
majority, however, don't buy it directly: they get it through their employers.
There's a big tax advantage to doing it that way, since employer contributions
to health care aren't considered taxable income. But to get that tax advantage
employers have to follow a number of rules; roughly speaking, they can't
discriminate based on pre-existing medical conditions or restrict benefits to
highly paid employees.

And it's thanks to these rules that employment-based insurance more or less
works, at least in the sense that horror stories are a lot less common than they
are in the individual insurance market.

So here's the bottom line: if you currently have decent health insurance, thank
the government. It's true that if you're young and healthy, with nothing in your
medical history that could possibly have raised red flags with corporate
accountants, you might have been able to get insurance without government
intervention. But time and chance happen to us all, and the only reason you have
a reasonable prospect of still having insurance coverage when you need it is the
large role the government already plays.

Which brings us to the current debate over reform.

Right-wing opponents of reform would have you believe that President Obama is a
wild-eyed socialist, attacking the free market. But unregulated markets don't
work for health care -- never have, never will. To the extent we have a working
health care system at all right now it's only because the government covers the
elderly, while a combination of regulation and tax subsidies makes it possible
for many, but not all, nonelderly Americans to get decent private coverage.

Now Mr. Obama basically proposes using additional regulation and subsidies to
make decent insurance available to all of us. That's not radical; it's as
American as, well, Medicare.

URL: http://www.nytimes.com

LOAD-DATE: July 31, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


