                   Copyright 2010 The New York Times Company


                             1971 of 2021 DOCUMENTS


                               The New York Times

                              May 11, 2010 Tuesday
                              Late Edition - Final

Rules Let Youths Stay on Parents' Insurance

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 585 words


WASHINGTON -- The White House issued rules on Monday allowing young adults to
remain covered by their parents' health insurance policies up to age 26.

The promise of such coverage has attracted great interest. Employers and
insurers say they have been flooded with inquiries.

Under the rules, an employer-sponsored health plan or a company selling
individual insurance policies must offer coverage to subscribers' children up to
the age of 26, regardless of whether a child lives with his or her parents,
attends college, is a dependent for income-tax purposes or receives financial
support from the parents.

Coverage is to be available to married and unmarried children alike.

Kathleen Sebelius, the secretary of health and human services, estimated that
1.2 million people would gain coverage because of the new requirement.

The health department estimated that the average cost to cover each new enrollee
would be $3,380 in 2011, $3,500 in 2012 and $3,690 in 2013.

The cost will be borne by all families with employer-sponsored insurance, with
family premiums expected to rise by about 1 percent, the government said.

The rules generally take effect for insurance plan-years that begin on or after
Sept. 23 this year.

However, the rules allow an exception for employer-sponsored health plans that
were in existence on March 23, when President Obama signed the health care bill.
In general, such health plans can exclude adult children of workers until 2014
if the children have access to insurance through another employer-sponsored
health plan. That might occur, for example, if a 24-year-old child is working
for a business that offers health benefits to employees.

Many insurance companies have voluntarily agreed to provide dependent coverage
immediately, without waiting for the requirement to take effect in September
2010 or in January 2011, when many companies renew their coverage.

On Monday, the White House urged employers to follow the example of insurance
companies and extend coverage to their employees' adult children up to age 26
immediately.

Under the rules, insurers and employers must provide young adults with a 30-day
opportunity to enroll in their parents' coverage. Terms of coverage cannot vary
based on the age of young adults under 26. Thus, the White House said, an
insurer violates the law if it imposes a surcharge on premiums for children 19
to 25.

The administration offered another example, involving a company that covers
employees' children up to age 19, or up to 23 if the children are full-time
students. If a worker's child lost coverage on turning 23, the company must
notify the worker that the child is again eligible for coverage starting Jan. 1,
2011.

Aaron B. Smith, executive director of Young Invincibles, an organization for
people 18 to 34, welcomed the new rules, saying they would help secure
affordable coverage for college graduates and other young adults looking for
jobs.

But James P. Gelfand, director of health policy at the United States Chamber of
Commerce, said: ''Regulatory agencies may have stretched their authority in
writing these rules. Adult children can live 2,000 miles away from their
parents, be married and not have spoken to Mom and Dad in a year, and they could
still be added to the parents' employer-sponsored health plan just like any
other child.''

Douglas H. Shulman, the commissioner of internal revenue, said that coverage
provided to an employee's adult children would generally be tax-free to the
employee.

URL: http://www.nytimes.com

LOAD-DATE: May 11, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


