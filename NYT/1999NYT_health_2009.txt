                   Copyright 2010 The New York Times Company


                             1999 of 2021 DOCUMENTS


                               The New York Times

                              May 23, 2010 Sunday
                              Late Edition - Final

High-Tech Alternatives to High-Cost Care

BYLINE: By STEVE LOHR

SECTION: Section BU; Column 0; Money and Business/Financial Desk; UNBOXED; Pg. 4

LENGTH: 1044 words


MENTION health care reform  and the image that instantly comes to mind is a big
government program. But there is another broad transformation in health care
under way, a powerful force for decentralized innovation. It is fueled in good
part by technology -- low-cost computing devices, digital sensors and the Web.

The trend promises to shift a lot of the diagnosis, monitoring and treatment of
disease from hospitals and specialized clinics, where treatment is expensive, to
primary care physicians and patients themselves -- at far less cost.

The new models emphasize early detection of health problems, prevention and
management of chronic disease. The approaches have adopted a range of labels
including ''wellness,'' ''consumer-directed  health care'' and the ''medical
home.''

The potential transformation faces formidable obstacles, to be sure. Some of
those hurdles include getting patients to embrace healthier lifestyles and
persuading  the government and insurers to reimburse at-home testing and
monitoring devices.

Yet the promise, according to Dr. David M. Lawrence, the former chief executive
of Kaiser Permanente, the nation's largest private health care provider, is ''an
array of technology-enabled, consumer-based services that constitute a new form
of primary health care.''

To glimpse the business opportunity -- and the challenge -- at the forefront of
this emerging, decentralized health care market, let's look at a start-up in the
field of sleep medicine.

The start-up, Watermark Medical, offers an at-home device and a Web-based
service for diagnosing sleep apnea. Characterized by snoring and pauses in
breathing, sleep apnea is a serious health problem that often goes undiagnosed.

Typically caused by tissue in the back of the throat obstructing the airway to
the lungs, it contributes to the severity of chronic conditions including
diabetes, heart disease, obesity, hypertension and depression, adding an
estimated $3.4 billion to the nation's health costs.

Sufferers battle chronic fatigue, and sleep experts suspect that apnea is the
cause of many workplace and car accidents. Treatments include a masklike
apparatus that pumps air to keep the patient's airway open; an oral appliance,
resembling an orthodontic retainer, that helps open the throat; and surgery to
shave tissue that blocks the air passage.

Today, sleep apnea diagnoses are mainly done in specialized sleep clinics, where
the patient sleeps under observation for a night or two, at a cost of up $4,000
-- with the expense usually shared by insurers and patients. Given the cost and
inconvenience, physicians say, patients often do not go to a clinic and seek
treatment until their sleep troubles are severe.

Watermark Medical traces its technical origins to the work of Dr. Philip
Westbrook, a Stanford-educated sleep expert who led the sleep disorder centers
at the Mayo Clinic in Rochester, Minn., and at Cedars-Sinai Medical Center in
Los Angeles. But during a brief period in private practice in California, seeing
the expense and trouble sleep-testing was for patients, Dr. Westbrook decided
there had to be a simpler, more efficient way.

He teamed up with a pair of medical-device technologists and won an innovation
grant from the National Institutes of Health to finance  a prototype. They came
up with  a headband  that holds a blue plastic device -- smaller than a deck of
cards and resting on the forehead -- equipped with a  microprocessor and
sensors, and a tube that fits into a patient's nose. If the tube falls out, the
patient hears a voice prompt.

In 2004, the device was approved for use by the Food and Drug Administration.
''We had a better mousetrap, but no business or marketing expertise,'' Dr.
Westbrook said.

But there also wasn't a real market, until the Centers for Medicare and Medicaid
Services approved reimbursement for home sleep-testing in 2008. That was when
two young entrepreneurs, Sean Heyniger and Charles Alvarez, who had recently
sold PDSHeart, a remote heart-monitoring company, to a larger corporation, were
looking for another opportunity. They researched the sleep market and found Dr.
Westbrook, who is now Watermark's chief medical officer.

They founded Watermark in March 2008, tweaked Dr. Westbrook's device, produced a
business model and conducted pilot projects. Watermark began introducing its
testing device  last September. Its sensor-equipped headband, powered by Intel's
Atom microprocessor, measures 10 things, including blood-oxygen saturation, air
flow, pulse rate and snoring levels.

The patient wears the device for a night or two, then returns the device to the
doctor's office. The data is downloaded to a personal computer, then sent on the
Web to a network of sleep professionals, one of whom delivers a report  to the
physician within 48 hours, with a diagnosis and suggested treatment. The
physicians typically charge from $250 to $450 a test, and a doctor collects $100
to $150 of that. ''It's a new revenue stream for the physician,'' said Mr.
Alvarez, Watermark's president.

Watermark also charges the physicians $4,000 for each digital headband.

So far, 35,000 patients have been tested using the Watermark device, with more
than 1,000 doctors prescribing about 4,000 tests a month.

DR. LEE SURKIN, a sole practitioner in Greenville, N.C., is one of them. He has
three Waterman devices and has done at-home tests on 50 patients so far.

Owning his own sleep lab, Dr. Surkin said, would be far more lucrative under
current insurer reimbursement rates, but he prefers the at-home tests as a
low-cost way to diagnose and treat far more patients. ''This is a tool that
moves health care toward where it has to go,'' he said.

If successful with sleep, Watermark plans to branch out to other kinds of
Web-based personal devices to monitor chronic conditions like heart disease and
diabetes. The company's executives talk of their technology as a platform that
can add many other services someday, a bit like Apple's applications store for
its consumer devices.

Indeed, Watermark's co-chairman and a major financial backer is John Sculley,
the former chief executive of Apple. ''We're starting with sleep, but the model
can extend to many other diagnostic services,'' he said.

URL: http://www.nytimes.com

LOAD-DATE: May 23, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Dr. Lee Surkin, with his wife, Elizabeth Webster, uses a
Watermark Medical device to record patients' sleep patterns, which specialists
interpret and summarize for him. (PHOTOGRAPH BY KAREN TAM FOR THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


