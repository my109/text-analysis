                   Copyright 2010 The New York Times Company


                             1683 of 2021 DOCUMENTS


                               The New York Times

                            March 18, 2010 Thursday
                              Late Edition - Final

Sex Scandals To Learn By

BYLINE: By GAIL COLLINS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 31

LENGTH: 824 words


Let's consider the story of Representative Eric Massa, a freshman Democrat from
upstate New York who made headlines recently when he resigned from office amid
talk about sexual harassment of male staffers.

Massa's defense was that the questionable conduct was boisterous horseplay by an
old ex-Navy officer and five of his single male aides, who roomed together in
one small Washington townhouse. ''Not only did I grope him,'' he said of one of
his roommates, ''I tickled him until he couldn't breathe and four guys jumped on
top of me. It was my 50th birthday, and it was: 'Kill the old guy.' ''

Already we have extracted our first important lesson from this scandal, which is
that voters are going to have to pay more attention to where their elected
officials bunk while they're in the nation's capital. Remember that last year we
had the Prayer House, a much larger and nicer townhouse full of conservative
Christian congressmen? They were supposed to be engaged in discussions of the
Bible, but, in fact, seemed to spend most of their time trying to bail each
other out of adultery crises.

The extremely overworked House ethics committee is looking into charges that the
Democratic leadership should have done something about Massa sooner. It might
have been a warning signal when he told a reporter that rather than pay
substantial salaries to a handful of aides like most members do, he preferred to
hire lots and lots of people who made so little that several of them were forced
to economize by living with their boss.

Massa has offered a raft of explanations for his sudden resignation, ranging
from a possible cancer recurrence to a plot by the Democrats to drive him out of
office because he opposed the health care reform bill. This last one instantly
excited Glenn Beck, who invited Massa on his program in hopes of hearing more
about the claim that a naked Rahm Emanuel poked an angry finger at him in the
Congressional gym.

The Beck interview was mesmerizing. Whatever Massa did or didn't do with his
aides, it was obvious that as a legislator, he is utterly loopy. His examples of
political corruption consisted of Democratic leaders begging him to support the
president and donors telling him that they wouldn't give him more money if he
voted against the programs they care about.

To clean up the mess in Washington, Massa told Beck, voters should call their
representatives and urge them to forget about the party or the president or even
said voters' own personal convictions and just ''do what you think is right.''

Now, people, here is the problem exactly. There are quite a few members of
Congress who are as out to lunch as Eric Massa, although we hope the rest of
them are not career ticklers. Do we really want to see them all follow their own
private drummers and go careening around from one position to another like a
bunch of Frisbees?

Here we are, looking at the upcoming health care vote and mulling the virtues of
bipartisan political independence. You think George Washington; I think Eric
Massa.

Another star of this week's political sex scandal headlines was Rielle Hunter,
the John Edwards mistress. This particular saga is less interesting than it
would have been if Edwards was actually an elected official rather than a man
who has spent the past six years being consistently rejected by the American
voters, state by state, primary by primary.

Plus we have already learned the most important tip the story had to teach us,
which is to avoid any candidate who makes his or her marriage the centerpiece of
a campaign.

Nevertheless, Hunter was eager to tell her version of the affair. As soon as
Edwards told a national TV audience that the only woman he had ever loved was
his wife, she reported, he called to assure her that he didn't mean it.

Which, she says, she believes, since she knows he will love her ''till death do
us part.''

If Hunter was a more credulous soul, I would have taken all this as a terrible
failure on the part of the national news media. Really, the whole point of
writing about one straying, lying political alpha dog after another is to alert
the next generation that when a powerful pol invites you to come to his hotel
room for a date, and suggests you arrive disguised as the turn-down maid, he is
not going to stick with you through thick and thin.

But Hunter is 45 and has been around the block a time or three. So I think it's
safe to say that she is not deluded so much as a woman intent on rearranging
reality to suit her convenience.

''Before I met Johnny, I had a lot of judgment about infidelity. Now I have a
deeper and greater understanding and acceptance of people's processes,'' she
told an interviewer for GQ.

This was in happier times, before Hunter professed herself to be shocked by the
magazine's pictures of her lying on a bed wearing pearls and no pants, since she
was sure the photographer would be interested only in face shots.

URL: http://www.nytimes.com

LOAD-DATE: March 18, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


