                   Copyright 2009 The New York Times Company


                             640 of 2021 DOCUMENTS


                               The New York Times

                          September 19, 2009 Saturday
                              Late Edition - Final

Obama Rejects Race as the Lead Cause of Criticism

BYLINE: By JEFF ZELENY

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 655 words

DATELINE: WASHINGTON


President Obama said Friday that he did not believe his race was the cause of
fierce criticism aimed at his administration in the contentious national debate
over health care, but rather that the cause was a sense of suspicion and
distrust many Americans have in their government.

''Are there people out there who don't like me because of race? I'm sure there
are,'' Mr. Obama told CNN. ''That's not the overriding issue here.''

In five separate television interviews at the White House, Mr. Obama said he did
not agree with former President Jimmy Carter's assertion that racism was fueling
the opposition to his administration. He described himself as just the latest in
a line of presidents whose motives had been questioned because they were trying
to enact major change.

Mr. Obama will appear on five Sunday talk shows -- an unprecedented step for a
president -- to promote his health care plan. The television networks broadcast
brief parts of their interviews on Friday evening, all of which focused on a
question the White House has sought to avoid all week: Has race played a role in
the debate?

Mr. Obama, the nation's first black president, said ''race is such a volatile
issue in this society'' that he conceded it had become difficult for people to
tell whether it was simply a backdrop of the current political discussion or ''a
predominant factor.''

''Now there are some who are, setting aside the issue of race, actually I think
are more passionate about the idea of whether government can do anything
right,'' he told ABC News. ''And I think that that's probably the biggest driver
of some of the vitriol.''

The president spoke to anchors from three broadcast networks, ABC, CBS and NBC
as well as the cable networks CNN and Univision.

He conceded that many people were skeptical of the health care legislation
making its way through Congress.

''The overwhelming part of the American population, I think, is right now
following this debate, and they are trying to figure out, is this going to help
me?'' Mr. Obama said in one of the interviews. ''Is health care going to make me
better off?''

But even as the White House sought to push it aside, the issue of race persisted
through the week, with some critics saying it was the reason a Republican
lawmaker was disrespectful to the president last week, calling him a liar as Mr.
Obama addressed a joint session of Congress. The television interviews on Friday
were the first time Mr. Obama had weighed in.

''Look, I said during the campaign there's some people who still think through a
prism of race when it comes to evaluating me and my candidacy. Absolutely,'' Mr.
Obama told NBC News. ''Sometimes they vote for me for that reason; sometimes
they vote against me for that reason.''

But he said that the matter was really ''an argument that's gone on for the
history of this republic. And that is, what's the right role of government?''

The president said the contentious health care debate, which came on the heels
of extraordinary government involvement in bailing out banks and automobile
companies, had led to a broader discussion about the role of government in
society.

''I think that what's driving passions right now is that health care has become
a proxy for a broader set of issues about how much government should be involved
in our economy,'' Mr. Obama told CBS News. ''Even though we're having a
passionate disagreement here, we can be civil to each other, and we can try to
express ourselves acknowledging that we're all patriots, we're all Americans and
not assume the absolute worst in people's motives.''

The president used the media blitz to add his own commentary about the news
media.

He said he blamed cable television and blogs, which he said ''focus on the most
extreme element on both sides,'' for much of the inflamed rhetoric.

''The easiest way to get 15 minutes of fame,'' Mr. Obama said, ''is to be rude
to someone.''

URL: http://www.nytimes.com

LOAD-DATE: September 19, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama, at a health care talk recently in Maryland,
will be on five Sunday talk shows.(PHOTOGRAPH BY LUKE SHARRETT/THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


