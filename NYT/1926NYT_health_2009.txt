                   Copyright 2010 The New York Times Company


                             1926 of 2021 DOCUMENTS


                               The New York Times

                             April 23, 2010 Friday
                              Late Edition - Final

U.S. May Add to Global Food Fund

BYLINE: By HELENE COOPER

SECTION: Section A; Column 0; Foreign Desk; WORLD BRIEFING THE AMERICAS; Pg. 6

LENGTH: 713 words


WASHINGTON -- The Obama administration is proposing to add $408 million to a
global fund to boost food production and encourage good farming practices in the
developing world, the Treasury Department announced on Thursday.

The fund, created after the Group of 20 meeting in Pittsburgh last year, will
begin with contributions from the governments of Canada ($230 million), Spain
($95 million) and South Korea ($50 million) and from the Bill and Melinda Gates
Foundation ($30 million). It is meant to provide money to poorer countries,
particularly in Africa, that invest in local farming programs and agricultural
development to increase crop yields, administration officials said.

In an interview, Mr. Gates said he believed that a ''renaissance'' period of
growth and development was under way in many African countries and could be
spurred by the infusion of money. ''I wouldn't work on something that was some
bleak 'you should feel guilty so give some money' type of thing,'' he said.

But he acknowledged that the structure of the agriculture aid program --
funneling money to governments that demonstrate they are investing wisely in
agriculture -- could leave farmers in countries with struggling governments out
in the cold.

''If I had one wish, it would be for good governance in all of these
countries,'' Mr. Gates said. He specifically cited the Democratic Republic of
Congo, where since 1998 fighting has raged, particularly in the east, and where
the prevalence of rape and sexual violence is believed to be among the worst in
the world.

The United States has already contributed $67 million to the fund, and has
requested another $408 million in President Obama's fiscal 2011 budget proposal.
If approved, the new money would be available when the federal fiscal year
starts on Oct. 1, administration officials said.

''A global economy where more than one billion people suffer from hunger is not
a sustainable one,'' Treasury Secretary Timothy Geithner said in a statement.
''At a time of limited resources and large global challenges, this fund will
leverage support from around the world to achieve lasting progress against
hunger and bolster agricultural productivity and growth.'' Mr. Geithner and Mr.
Gates wrote about the new initiative in an opinion article in The Wall Street
Journal on Thursday.

But Mr. Gates warned that getting Congress to approve the budget request will
require a fight by Mr. Obama, whom he noted had ''been to Africa -- the speeches
he gave were fantastic.'' He was referring to Mr. Obama's visit to Ghana last
summer, where the president told a rapt audience that American aid must be
matched by African acceptance of responsibility for the continent's own
problems.

After the battles in Washington over the economic stimulus, health insurance
reform and, now, financial regulation, Mr. Obama will face some opposition from
both Democrats and Republicans over the scale of federal spending. The fund was
created as part of $22 billion in pledges made by world leaders at the G-8
meeting in L'Aquila, Italy, last summer, and finalized at the G-20 meeting in
Pittsburgh a few months later.

The funds are meant to be invested to improve land use planning, irrigation and
farm machinery, to provide technical help to farmers and to build better roads
linking farmers with their markets.

The World Bank will administer the fund and help choose projects to finance, in
conjunction with the African Development Bank and the International Fund for
Agricultural Development. Jeffrey Sachs, the Columbia University economist, said
that supporting farm programs in poor countries has been shown to work. He
pointed to the example of Malawi, where the government recently doubled the
country's food production by putting in the same sort of agricultural
development programs that the global fund is pushing. Mr. Sachs noted that the
Group of Eight wealthy nations last year announced that they would like the
program funded at $22 billion.

''It's of course completely stark that there's a $22 billion announcement and
less than a $1 billion capitalization,'' he said. But, he added, ''I think this
is the first step of something very, very important.''

This is a more complete version of the story than the one that appeared in
print.

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


