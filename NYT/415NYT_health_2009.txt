                   Copyright 2009 The New York Times Company


                             415 of 2021 DOCUMENTS


                               The New York Times

                           August 26, 2009 Wednesday
                              Late Edition - Final

Distortions On Health Bill, Homegrown

BYLINE: By JIM DWYER.

E-mail: dwyer@nytimes.com

SECTION: Section A; Column 0; Metropolitan Desk; ABOUT NEW YORK; Pg. 16

LENGTH: 742 words


You might think that New York has made almost no contribution to the national
health care debate, given that the state tends toward robotic support of
Democrats, and that the most bloodcurdling argument came from Alaska, with Sarah
Palin's claim that ''Obama's death panels'' would decide who would get care and
who wouldn't.

So you might think that this debate is happening elsewhere, but you would be
wrong.

Before Sarah Palin, there was Betsy McCaughey of the Upper East Side -- a former
lieutenant governor of New York and now the quartermaster of much of the
high-powered intellectual ammunition against the proposals. Ms. McCaughey has
been the hammer to Ms. Palin's nail.

They have driven some of the most disturbing, and distorted, claims about the
proposals. Ms. McCaughey did not coin ''death panel,'' but she has repeatedly
argued that parts of the legislation would deliberately drive the elderly to
early graves -- a claim, though dismissed by Republicans and Democrats in
Congress, and by the White House, that has remained a source of alarm.

For Ms. McCaughey, 60, dismantling huge health care proposals by Democrats has
been a career maker. In 1994, an article she wrote for The New Republic, titled
''No Exit,'' was widely credited with stopping a plan assembled by Hillary
Rodham Clinton. Ms. McCaughey became a star, particularly among the small but
influential circles of wealthy New York conservatives who often felt locked out
of debates by the state's political orthodoxies.

Based on her success in marshaling the anti-Clinton forces, she was drafted by
George E. Pataki, a Republican candidate for governor, to run with him in 1994
for lieutenant governor.

Once they were elected, though, Ms. McCaughey said the Pataki  administration
was not spending enough on education and health care. When Mr. Pataki delivered
his State of the State speech in 1996, Ms. McCaughey stood behind him through
the talk -- a mistake, she later said, though others saw it as a way to draw
attention to herself. (Mr. Pataki dumped her from the ticket in 1998, and she
tried to get the Democratic nomination. She also accused her
soon-to-be-ex-husband, the financier Wilbur L. Ross Jr., of not keeping a
promise to finance her campaign.)

In recent years, Ms. McCaughey, who has a doctorate from Columbia University in
constitutional history, has written widely on reducing hospital infections.

On her first reading of the 2009 health care proposal, Ms. McCaughey has said,
she wrote the word ''disgusting'' in the margin. She wrote criticisms of it for
Bloomberg News, The New York Post and The Wall Street Journal.

Last month, in a radio interview with former Senator Fred Thompson, Ms.
McCaughey said, ''One of the most shocking things I found in this bill, and
there were many, is on Page 425, where the Congress would make it mandatory,
absolutely require, that every five years, people in Medicare have a required
counseling session that will tell them how to end their life sooner.'' She went
on to say that it encouraged people to cut their lives short ''in society's best
interest.''

In fact, the bill offers to pay for Medicare patients to speak with their
doctors about their wishes for the end of their life, on issues like who will be
allowed to make decisions if they are incapacitated, and what kinds of care are
available for terminally ill people.

Ms. McCaughey did not reply to a request for comment on the discrepancy between
her statements and the contents of the bill. Last week, she did go on ''The
Daily Show'' to discuss her objections. She told the host, Jon Stewart, that she
didn't mind an insurance benefit that paid doctors to discuss end-of-life issues
with their patients, but maintained that the bill's evaluation system would
penalize doctors if patients changed their minds about what kinds of care they
wanted.

''Putting pressure on doctors to require patients to go through a consultation
that's prescribed by the government,'' Ms. McCaughey said, ''and then to
penalize them if the patient or their family changes their mind about their
living will in a moment of crisis, that's really wrong.''

Mr. Stewart replied, ''It would be really wrong if that was in any way what this
said.''

The day that the interview with Mr. Stewart was broadcast, Ms. McCaughey
resigned from the board of the Cantel Medical Corporation, a medical device
company, to avoid the appearance of a conflict of interest.

URL: http://www.nytimes.com

LOAD-DATE: August 26, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


