                   Copyright 2009 The New York Times Company


                              74 of 2021 DOCUMENTS


                               The New York Times

                             June 23, 2009 Tuesday
                              Late Edition - Final

Something For Nothing

BYLINE: By DAVID BROOKS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 25

LENGTH: 837 words


On May 12, the Senate Finance Committee held a hearing on health care reform.
There was a long table of 13 experts, and a vast majority agreed that ending the
tax exemption on employer-provided health benefits should be part of a reform
package.

They gave the reasons that experts -- on right or left -- always give for
supporting this idea. The exemption is a giant subsidy to the affluent. It
drives up health care costs by encouraging luxurious plans and by separating
people from the consequences of their decisions. Furthermore, repealing the
exemption could raise hundreds of billions of dollars, which could be used to
expand coverage to the uninsured.

Democratic Senator Ron Wyden piped up and noted that he and Republican Senator
Robert Bennett have a plan that repeals the exemption and provides universal
coverage. The Wyden-Bennett bill has 14 bipartisan co-sponsors and the
Congressional Budget Office has found that it would be revenue-neutral.

The Finance Committee's chairman, Senator Max Baucus, looked exasperated. With
that haughty and peremptory manner that they teach in Committee Chairman School,
he told Wyden and the world that this idea was not going to happen.

In the World's Greatest Deliberative Body, senators don't run things. Chairmen
and their staffs run things. During the spring, as the Obama administration
faded to invisibility, the finance and health committees separately put together
plans. These plans did not alter the employer exemption. They did build on the
current system. They did include approaches that have been around since Richard
Nixon.

The problem with the committee plans is that they don't do much to change the
underlying incentives, and consequently don't do much to control costs. ''The
single most expensive option is to build on the existing system,'' says the
health care costs guru John Sheils of the Lewin Group.

The C.B.O. measured the plans, and the results were devastating. A successful
plan has to be revenue-neutral for the government over the next 10 years, and it
has to reduce the total health care burden over the long term so the country
doesn't go bankrupt. The Senate committee plans failed both criteria. They would
cost the government more than $1 trillion this decade and send total health care
costs zooming at least twice as fast as the economy as a whole.

The C.B.O. reports sent shock waves through Washington. Senators and staffs
began casting about for a way to get a good C.B.O. score. President Obama
redoubled his rhetoric about fundamentally reducing health care costs. Everybody
continued looking around for a compromise that could get a bipartisan majority.

Now you might think that in these circumstances someone might take a second look
at the ideas incorporated in the Wyden-Bennett plan, which already has a good
C.B.O. score, bipartisan support and a recipe for fundamental reform.

If you did think that, you are mistaking the Senate for a rational organism. For
while there are brewing efforts to incorporate a few Wyden-Bennett ideas, there
is stiff resistance to the aspects that fundamentally change incentives.

The committee staffs don't like the approach because it's not what they've been
thinking about all these years. The left is uncomfortable with the language of
choice and competition. Unions want to protect the benefits packages in their
contracts. Campaign consultants are horrified at the thought of fiddling with a
popular special privilege.

So the process is moving along as it has been. There is a great deal of talk
about the need to restrain costs. There's discussion about interesting though
speculative ideas to bend the cost curve. There are a series of frantic efforts
designed to reduce the immediate federal price tag. Some senators and advisers
suggest cutting back on universal coverage. Others have come up with a bunch of
little cuts in hopes of getting closer to the trillion-dollar tab. The
administration has ambitious plans to slash Medicare spending.

But there is almost nothing that gets to the core of the problem. Under the
leading approaches, health care providers would still have powerful incentives
to provide more and more services and use more expensive technology.

We've built an entire health care system (maybe an entire government) on the
illusion of something for nothing. Instead of tackling that basic logic, we've
got a reform process that is trying to evade it.

This would be bad enough in normal times. But the country is already careening
toward fiscal ruin. We've already passed a nearly $800 billion stimulus package.
The public debt is already projected to double over the next 10 years.

Health care reform is important, but it is not worth bankrupting the country
over. If this process goes as it has been going -- with grand rhetoric and
superficial cost containment -- then we will be far better off killing this
effort and starting over in a few years. Maybe then there will be leaders
willing to look at the options staring them in the face.

URL: http://www.nytimes.com

LOAD-DATE: June 23, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


