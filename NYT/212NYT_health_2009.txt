                   Copyright 2009 The New York Times Company


                             212 of 2021 DOCUMENTS


                               The New York Times

                             July 25, 2009 Saturday
                              Late Edition - Final

Buying a Hearing Aid? You've Got a Lot to Learn

BYLINE: By WALECIA KONRAD

SECTION: Section B; Column 0; Business/Financial Desk; PATIENT MONEY; Pg. 5

LENGTH: 1356 words


BOB BUCKWALTER, a retired pastor in Williamstown, Mass., bought his first pair
of hearing aids in January.

Like most people suffering from gradual hearing loss, he had resisted the idea
for years. But, after talking with people who have benefited from aids and doing
research to find a nearby audiologist, Mr. Buckwalter was ready to take the
plunge.

But there was one thing he was not ready for: the $4,600 price tag.

''It's a monumental amount,'' Mr. Buckwalter said. ''The technology is
impressive, and they've certainly made a difference in my life. But the fact is,
they're extremely expensive. And what I'm really surprised by is the fact that
insurance doesn't pay for them.''

Indeed, Medicare and most private insurance plans will pay only for the doctor
visit to determine the need for a hearing aid,  but not for the device itself.
(There are exceptions, including coverage for cochlear implants to treat severe
hearing loss or deafness, some Veterans Affairs programs and some federal
employee insurance.) The private plans that do pay for hearing aids usually
contribute only $500 to $1,000 toward the cost.

What's more, in the current health reform effort in Congress, while the House
legislation would provide more coverage for children's hearing aids, no one is
proposing to pay for adults' devices. Separately, there is legislation in both
the House and Senate proposing a tax break for hearing aid purchases.

''Unfortunately, hearing aids are seen as one of those medical costs, like
vision and dental, that doesn't deserve full coverage,'' said Gyl A. Kasewurm,
an audiologist in St. Joseph, Mich.

About 37 million people suffer from some form of hearing loss -- from minor
impairment to total deafness -- in the United States. But less than a quarter of
the people who could benefit from hearing aids actually use them. One reason is
people's reluctance to admitting a disability.

But cost is a big factor, says Lise Hamlin, director of public policy for the
Hearing Loss Association, an advocacy and lobbying group. Hearing aids average
about $2,000 each,  Ms. Hamlin said, ''but I've seen prices range from $1,400 to
$5,000 apiece.''

Hearing aid technology has improved significantly in the last decade. Almost all
hearing aids used today are digital; sound goes into a microphone and is
digitally processed by a chip, amplified, then sent to the ear. Digital
technology allows the aids to be programmed for an individual's exact hearing
loss needs -- a big improvement from the old-fashioned analog varieties that
were little more than an amplifier with a volume control.

But the technical leap is not the only reason for high prices. Traditionally,
hearing aids have been sold through professionals who also fit and adjust the
devices as part of the overall cost.  The system, however, leaves room for
plenty of inept or even greedy providers to take the reins.

That's why choosing an audiologist or hearing aid technician can be just as
important as choosing the aid itself. What follows is advice on how to do just
that -- as well  for finding financial help when purchasing hearing aids.

SHOP FOR THE PROVIDER. No matter how state of the art your hearing aid may be,
Ms. Hamlin says,  if it is not properly programmed and adjusted it will not do
you any good.

Ms. Kasewurm, the audiologist, says two people with the same degree of hearing
loss will process sound differently depending on the cause of the hearing
diminishment and how long the problem has gone untreated.  That makes it
difficult to buy the right hearing aid over the counter or through the Internet.

Most hearing aids are sold by an audiologist or a hearing instrument specialist.
Audiologists have a doctorate, but not a medical degree, while instrument
specialists go through a training program  and an apprenticeship. Sometimes a
hearing aid center  will employ both types of professionals. Your ear, nose and
throat medical specialist may have an audiologist on staff who will dispense
hearing aids.

The Hearing Loss Association of America provides a good checklist for anyone
purchasing a hearing aid and looking for a good dispenser. In addition, the
American Academy of Audiologists lists providers by locale.

WHAT TO ASK BEFORE YOU BUY.

Do you sell a wide range of products?

Your dispenser should sell every style of hearing aid, including behind-the-ear
open fit, behind-the-ear with ear mold, inside the ear  and inside the ear
canal. That way you will have a better chance of finding the type that works
best and is most comfortable for you.

Because it would be difficult for a dispenser to  become an expert in fitting
all the various brands of hearing aids available,  a limited choice of
manufacturers isn't necessarily a red flag. But do watch out if a dispenser is
pushing a particular brand without discussing other options.

What kind of testing do you do?

Make sure your dispenser will do an extensive hearing assessment in a soundproof
booth to determine the type, degree and dimension of your hearing loss.

The Food and Drug Administration, which regulates the hearing loss industry,
also requires you to get a doctor's examination. If you do not have that exam
and do not want one, the dispenser must have you sign a waiver before selling
you a hearing aid.

In addition, the audiologist or hearing aid technician should also do a
follow-up test while you are wearing the new aid to determine how well it is
working in several simulated situations.

Is there a separate fitting fee?

In most cases, the initial fitting -- when you first learn how to insert and use
your new aid -- should be included in the purchase price. With the
behind-the-ear style using an ear mold,  however, some dispensers charge an
extra fee for the mold.

How often can I come in for adjustments?

The best answer is, as much as you need to. Barring that, make sure the purchase
price covers at least two or three visits after the initial fitting. The need
for adjustments and fine-tuning is almost inevitable.

''Most people wait eight to 10 years before they decide to get a hearing aid,''
said Carole Rogin, executive director of the Hearing Industries Association, a
hearing aid manufacturer trade group. ''It takes a long time to retrain the
brain how to hear.''

Can I get a copy of my test results?

As with eyeglass prescriptions, you should be able to get a copy of your hearing
tests. That way, if you do not like working with that particular dispenser, you
will not have to repeat that rigmarole when you choose another one.

READ THE CONTRACT CAREFULLY. Once you choose a provider, make sure you get a
written contract and understand what exactly is included in the price. Any fee
for follow-up visits should be spelled out.

Once you buy, manufacturers offer a trial period during which you can return
your hearing aid at no cost or for a minimal fee. Many states mandate a minimum
trial period, usually 30 days. Some providers and manufacturers offer 45 days.
And make sure your contract specifies that you will get all or most of your
money back if you return the hearing aid or aids within the trial period.

This document should also cover the length of the warranty  and the cost to
replace lost or damaged aids, and specify the policy on batteries. Some
providers include a continuous supply of batteries to encourage clients to
continue to use their aids. Although the batteries are widely available in
retail stores for a dollar or so, the cost can add up.

''If I wear my hearing aids continuously, the battery runs out in five days,''
Mr. Buckwalter said. ''That's something I hadn't bargained for.''

FOR FINANCIAL HELP... There are resources for people who cannot afford the high
cost of hearing aids. The Lions Club International has an expansive program.

Also, most states have vocational rehabilitation programs that may cover the
cost of hearing aids if you cannot perform your job without them. In addition,
check the Web sites of the Better Hearing Institute and the Hearing Loss
Association  for comprehensive lists of federal and state financial assistance.

URL: http://www.nytimes.com

LOAD-DATE: July 25, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Bob Buckwalter of Williamstown, Mass., was surprised by the
price of his hearing aids -- $4,600 -- and that insurance did not cover
them.(PHOTOGRAPH BY ROBERT SPENCER FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


