                   Copyright 2009 The New York Times Company


                             323 of 2021 DOCUMENTS


                               The New York Times

                            August 15, 2009 Saturday
                              Late Edition - Final

Obama Says Some Insurers Are Trying to Block Change

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 803 words

DATELINE: BELGRADE, Mont.


President Obama on Friday accused some insurance companies of trying to
undermine his plans for overhauling health care by ''funding in opposition,'' a
comment that could inflame tensions at a time when Mr. Obama is hoping to keep
insurers at the negotiating table.

The president, who did not mention any company by name, made the comment in a
question and answer session at a town-hall-style meeting here. He was responding
to an insurance salesman who challenged him on why the White House had decided
to ''vilify the insurance companies'' by shifting its strategy from talking
about reshaping health care to emphasizing changes in health insurance.

''O.K., that's a fair question,'' Mr. Obama told the salesman, Marc Montgomery
of Helena. He went on to say that some companies have ''been constructive,''
citing Aetna, whose chief executive, Ronald A. Williams, is a major Obama
supporter. Mr. Obama then criticized other companies, though not by name.

''Now, I want to just be honest with you, and I think Max will testify,'' Mr.
Obama said, referring to Senator Max Baucus, the Montana Democrat and chairman
of the Senate Finance Committee, who is spearheading the Senate legislation.
''In some cases what we've seen is also funding in opposition by some other
insurance companies to any kind of reform proposals.''

The exchange underscored the delicate line Mr. Obama is walking in taking on the
insurers. As public forums held by members of Congress have grown raucous this
week, many Democrats have accused insurers -- who oppose Mr. Obama's call for a
government-sponsored plan -- of sending protesters to the events.

The industry has denied engaging in such tactics. A spokesman for America's
Health Insurance Plans, an industry trade group, said Friday that his
organization was ''putting our resources behind advancing comprehensive health
reform.''

The exchange between Mr. Obama and the salesman was not nearly as fiery as those
that have erupted during lawmakers' events. But it was one of two pointed
clashes -- the other came when a man in a National Rifle Association jacket
accused the president of planning to raise taxes to pay for a health care
overhaul -- at the session, where participants otherwise seemed overwhelmingly
supportive of the president.

The trip here, the first stop in a three-state Western swing, mixes business
with pleasure. Mr. Obama's wife, Michelle, and his daughters, Malia and Sasha,
are along for planned excursions to Yellowstone National Park and the Grand
Canyon. On Saturday, Mr. Obama is scheduled to conduct another town-hall-style
meeting, in Grand Junction, Colo., to talk about health care again.

In a sense, the president came to Montana looking for pointed questions. At a
time when he said critics were making unsubstantiated claims that his plan would
create government ''death panels'' to ''pull the plug on grandma,'' Mr. Obama is
trying to push back by convincing Americans that overhauling the system will
improve their care by ending such industry practices as denying coverage for
pre-existing conditions.

The White House said 1,300 people attended the Belgrade event, held in an
airplane hangar that had been draped with a giant American flag. Roughly 70
percent of the tickets were distributed first come first served, officials said,
with the remaining 30 percent going to elected officials and community leaders.

The salesman, Mr. Montgomery, said he stood in line all night waiting for
tickets; he complained afterward that the president had delivered a ''classic
political answer'' to his question.

Another who waited all night for a ticket was the rifle association member,
Randy Rathie, a welder from the tiny town of Ekalaka, who said he had driven
several hundred miles to the event and had slept in his truck.

''Max Baucus, our senator, has been locked up in a dark room there for months
now trying to come up with some money to pay for these programs, and we keep
getting the bull,'' Mr. Rathie told the president. He went on: ''You have no
money. The only way you're going to get that money is to raise our taxes.''

Mr. Obama replied that he would keep his pledge not to raise taxes on families
earning $250,000 or less, but Mr. Rathie said afterward that he was dissatisfied
with the answer, adding, ''He won't tell Americans where the real money comes
from.''  While the crowd inside the hangar was polite -- not surprising, given
the customary respect for the president and the fact that Mr. Obama, like all
presidents, travels with gun-toting Secret Service agents -- there were
protesters chanting across the street from the airport, both favoring and
opposing an overhaul of health care.

The Rush Limbaugh show blared from a nearby sound truck.

Amid the din, a woman stood with a sign that said, ''Please be civil.''

URL: http://www.nytimes.com

LOAD-DATE: August 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama speaking about his plans for health care on
Friday in Belgrade, Mont. (PHOTOGRAPH BY DOUG MILLS/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


