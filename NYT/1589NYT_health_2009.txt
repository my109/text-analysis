                   Copyright 2010 The New York Times Company


                             1589 of 2021 DOCUMENTS


                               The New York Times

                             March 2, 2010 Tuesday
                              Late Edition - Final

Obama to Focus on Costs in New Push for Health Care Bill

BYLINE: By JACKIE CALMES and DAVID HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 1032 words

DATELINE: WASHINGTON


President Obama this week will begin a climactic push to rally restive
Congressional Democrats to pass major health care legislation by hammering the
argument that the costs of failure will be higher insurance premiums and lost
coverage for individuals and businesses.

While Mr. Obama prepared for a speech on Wednesday to outline ''the way
forward'' and to flesh out the substance of his proposed compromise based on the
bills passed by the House and Senate, the two parties on Monday stepped up their
battle to define the Democrats' legislative strategy. It was framed as ''a
divisive trick'' in Republicans' telling and, in Democrats' view, as a procedure
regularly used and honed by Republicans.

Their fight for public perception could itself determine how some politically
vulnerable Democrats vote.

Democrats are planning to attach any compromise revisions to a budget
reconciliation measure, which would circumvent a Republican filibuster and allow
the plan to be adopted by a simple majority in the Senate.

With Democratic Congressional leaders, particularly Speaker Nancy Pelosi, facing
a steep challenge in mustering votes for the health care legislation, Mr.
Obama's remarks on its financial and fiscal implications could be crucial. In
effect, officials indicate, he will contrast Democrats' proposals for expanding
coverage and for regulating insurance company practices with what he sees as the
shortcomings of the Republicans' incremental plans.

Mr. Obama on Wednesday ''will talk about the merits of the legislation, mainly
about the costs of doing nothing versus the cost of doing something and what
this will accomplish,'' said his chief of staff, Rahm Emanuel.

The White House is also pressing business supporters to take what could be a
final stand in favor of the Democrats' initiative. Mr. Obama's emerging message
against higher insurance costs was seconded on Monday by the Business
Roundtable, a Washington group representing major corporations. Mr. Obama spoke
to the group last week, and had its executive board to the White House for
dinner.

In a statement, the group's president, John J. Castellani, repeated its position
that overhauling the health insurance system should be ''a national priority''
given the burden of rising expenses on individuals, the government and
especially employers. Without action, Mr. Castellani said, ''if present trends
continue, the cost to provide an employee with health care will rise from
$10,000 to $28,000 over 10 years.''

The president's spokesman, Robert Gibbs, took up the argument in his daily news
briefing. Mr. Gibbs cited insurance premium increases proposed by Anthem Blue
Cross in California that have become Democrats' favored ammunition against the
insurance industry -- and against their Republican opponents, including
Representative Eric Cantor of Virginia, the second-ranking House Republican
leader.

''If we don't act, this is what our future is,'' Mr. Gibbs said. ''If we don't
act, insurance companies like Anthem that are sending letters to individuals in
the individual insurance market saying their rates will rise 39 percent --
that's what will happen. Congressman Cantor says let's start over, but the
insurance companies aren't starting over.''

Of the 38 House Democrats who voted against the health care bill in November, 25
are members of the fiscally conservative Blue Dog Coalition. They have expressed
deep reservations about whether the country can afford such an expansive and
expensive bill, though it includes spending reductions and tax increases to
avoid adding to annual deficits.

The nonpartisan Congressional Budget Office has projected that the bills passed
in the House and the Senate would reduce future federal deficits over the next
10 years by more than $100 billion, and the White House has insisted that the
plan Mr. Obama unveiled last week would do the same.

But the budget office has yet to analyze the president's plan -- indeed, the
White House has yet to provide legislative language that would allow it to do
so. In any case, critics have expressed doubt that the projected savings would
ever materialize, given Congress's failure to follow through on some past
efforts to reduce the growth of Medicare.

''On policy, I need to hear more cost containment,'' said Representative Jason
Altmire, Democrat of Pennsylvania and a Blue Dog who opposed the House bill in
November. ''It's still weak.''

Republicans have capitalized on such skepticism and have had some success in
defining the Democratic proposals by their immediate costs in new spending to
expand coverage to the uninsured. By contrast, the administration and
Congressional Democrats continue to struggle, despite support from nonpartisan
analysts, to make the case that long-term costs would be lower under the
overhaul they propose.

''Obviously we expect costs to come down because of a lot of the things we've
done in the bill,'' said Representative Steny H. Hoyer of Maryland, the House
Democratic leader. ''But,'' Mr. Hoyer acknowledged, ''this is not the financing
reform vehicle I think that it might have been.''

As for the legislative strategy, the Democrats are not trying to pass an
entirely new bill under the reconciliation process.

Instead, a reconciliation measure would include only provisions to change the
Senate-passed bill in ways reflecting compromises with the House and the White
House. Then, in a legislative two-step, the House would adopt the original
Senate bill, the House and the Senate would approve the package of changes, and
Mr. Obama would sign both into law.

The compromises would adjust subsidies to help people with moderate incomes buy
insurance, raise Medicare payroll taxes on the wealthy, further reduce the
impact of an excise tax on high-cost employer-sponsored insurance policies, and
increase federal aid to states for their Medicaid costs.

Some House Democrats who oppose abortion rights have said they might vote
against the Senate bill because its restrictions on insurance coverage for
abortions are not as great as those in the House version. But reconciliation
rules prevent changing provisions that do not have any budgetary impact.

URL: http://www.nytimes.com

LOAD-DATE: March 2, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Steny H. Hoyer and Speaker Nancy Pelosi face a
tough task in lining up House votes to overhaul health care. (PHOTOGRAPH BY LUKE
SHARRETT/THE NEW YORK TIMES  )

PUBLICATION-TYPE: Newspaper


