                   Copyright 2009 The New York Times Company


                             828 of 2021 DOCUMENTS


                               The New York Times

                            October 16, 2009 Friday
                              Late Edition - Final

Where Things Stand

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 383 words


Two Senate versions of health care legislation. Three in the House. And
President Obama is still hoping to sign a final health bill into law by
Christmas.

As Congress heads toward floor debates on the effort to overhaul the nation's
health care system, the Prescriptions blog introduces a new feature: Checkpoint,
a frequently updated status report on the health care debate in Washington and
beyond.

If you are only now getting engaged with the debate, or need to make sure you
haven't missed the latest big developments, you can check Checkpoint.

In the Senate, the majority leader, Harry Reid, Democrat of Nevada, is  working
to combine two bills into a single measure that will be brought to the floor.
Mr. Reid wants to get the debate started as soon as possible but faces a number
of hurdles.

Among the crucial decisions he faces are whether to include a government-run
health plan, or public option, in the combined bill, or to force senators who
support a public option to propose amendments during floor debate that would add
it to the bill.

The Senate Republican leader, Mitch McConnell of Kentucky, called on Thursday
for at least two months of debate on the  legislation. Under Senate rules, to
end debate and push the legislation to a final vote, Mr. Reid will need the
votes of 60 senators.

Senate Democrats hold a 60 to 40 majority, so they must remain unified, or win
over a Republican for every Democrat who decides to vote no. The Democrats' best
hopes beyond the party include Senators Olympia J. Snowe and Susan Collins, both
of Maine.

Senator Joseph I. Lieberman, the Connecticut independent who caucuses with the
Democrats, has said  he opposes the Finance Committee bill, but is open to
supporting the combined measure.

In the House, where the Democrats hold a wider majority, three versions of
health care legislation were approved before the summer recess. Speaker Nancy
Pelosi of California, above, is working to combine them into a single measure.
The House Democratic leaders must still bridge  big gaps between the
liberal-progressive wing of the party and the conservative wing. The liberals,
for instance, are strong supporters of a public option, while conservatives are
less enthusiastic.

Checkpoint is at:  prescriptions.blogs.nytimes

.com/checkpoint

URL: http://www.nytimes.com

LOAD-DATE: October 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


