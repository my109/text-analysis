                   Copyright 2009 The New York Times Company


                              78 of 2021 DOCUMENTS


                               The New York Times

                             June 25, 2009 Thursday
                              Late Edition - Final

The Drug Industry's Offer

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 22

LENGTH: 546 words


President Obama hailed a pledge by the pharmaceutical industry to contribute $80
billion in drug discounts and other savings over the next 10 years as ''a
significant breakthrough on the road to health care reform.'' The pledge should
help large numbers of older Americans struggling to pay high drug bills. But
before anyone gets too ecstatic, we will need a lot more details about what
industry is giving up and what it is getting.

The deal was negotiated in private among the industry, Senator Max Baucus,
chairman of the Finance Committee and a crucial figure in shaping health reform,
and the White House. From the broad outlines described by the White House, it
looks as if $30 billion would be used to cut in half the price of brand-name
drugs sold to Medicare patients who have reached a gap in coverage known as the
doughnut hole.

That could be a great help for elderly Americans of modest means who, after
reaching the gap, stop taking their medicines rather than pay the full cost
themselves. And it could reduce the need for the federal government to close the
hole entirely on its own. The move has been justifiably praised by  AARP and the
Medicare Rights Center, both advocates for beneficiaries.

The other $50 billion pledged is meant to save the government money that could
be applied to covering the uninsured. There appear to be three main elements: a
big increase in the discounts drug companies are required to give to Medicaid
programs for the poor; a new fee imposed upon the companies; and a new
regulatory pathway to foster the development of generics to compete with
brand-name biological drugs.

Eager to have a strong voice in the health care debate -- and head off more
draconian demands -- six major health care industries have promised to help
reduce the growth in spending. So far the pharmaceutical industry is the only
one to  come up with specific requirements that it is willing to meet.

The Congress and the public should see these proposals as an opening bid and not
the final word.

They will still provide less savings to the federal budget than the
administration had been seeking. And viewed against the backdrop of the $1
trillion or more needed to finance health care reform over the next decade,
these cost reductions seem small. The $80 billion is also a fraction of the
nation's overall drug spending, which federal estimates suggest will total $3.3
trillion over the next decade.

Congress should also keep in mind that these companies have already profited
handsomely from the Medicare prescription drug act, which opened a huge new
market for their products. And they profited from the ensuing switch of poor
older patients from Medicaid drug coverage, where the manufacturers had to
provide large discounts, to the Medicare program, where drug payments were much
higher.

If health care reform brings coverage to tens of millions of uninsured
Americans, the pharmaceutical industry will profit from additional sales.

With such gains on the horizon, Congress and the administration need to be sure
that the drug companies are contributing their fair share of cost savings to the
reform effort. And they should insist that other health care industries put
their own offers on the table for the same rigorous review.

URL: http://www.nytimes.com

LOAD-DATE: June 25, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


