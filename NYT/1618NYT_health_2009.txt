                   Copyright 2010 The New York Times Company


                             1618 of 2021 DOCUMENTS


                               The New York Times

                              March 8, 2010 Monday
                              Late Edition - Final

Taking Guns To Cafes To Show They Can

BYLINE: By IAN URBINA

SECTION: Section A; Column 0; National Desk; Pg. 11

LENGTH: 1108 words


For years, being able to carry a concealed handgun has been a sacred right for
many gun enthusiasts. In defending it, Charlton Heston, the actor and former
president of the National Rifle Association, used to say that the flock is safer
when the wolves cannot tell the difference between the lions and the lambs.

But a grass-roots effort among some gun rights advocates is shifting attention
to a different goal: exercising the right to carry unconcealed weapons in the 38
or more states that have so-called open-carry laws allowing  guns to be carried
in public view with little or no restrictions. The movement is not only raising
alarm among gun control proponents but also exposing rifts among gun rights
advocates.

The call for gun owners to carry their guns openly in the normal course of
business first drew broad attention last summer, when opponents of the Obama
administration's health care overhaul began appearing at town-hall-style
meetings wearing sidearms. But in recent weeks, the practice has expanded as gun
owners in California and other states that allow guns to be openly carried have
tested the law by showing up at so-called meet-ups, in which gun owners appear
at Starbucks, pizza parlors and other businesses openly bearing their weapons.

''Our point is to do the same thing that concealed carriers do,'' said Mike
Stollenwerk, a co-founder of OpenCarry.org, which serves as a national forum.
''We're just taking off our jackets.''

The goal, at least in part, is to make the case for liberalized concealed weapon
laws by demonstrating how uncomfortable many people are with publicly displayed
guns. The tactic has startled many business owners like Peet's Coffee and Tea
and California Pizza Kitchen, which forbid guns at their establishments. So far,
Starbucks has resisted doing the same.

The open-carry movement is a wild card in gun rights advocacy and in some ways
is to the N.R.A. and other mainstream gun rights advocacy groups what the Tea
Party movement is to the Republican Party.

Newer, more driven by grass-roots and the Internet than the N.R.A., open-carry
groups are also less centralized, less predictable and often more
confrontational in their push for gun rights. In the last year, there have been
at least 140 formal and informal meet-ups at coffee shops and restaurants in
California alone, organizers say.

Some gun rights advocates see risks in the approach.

''I'm all for open-carry laws,'' said Alan Gottlieb, founder of the Second
Amendment Foundation, a gun rights advocacy organization in Washington State.
''But I don't think flaunting it is very productive for our cause. It just
scares people.''

Robert Weisberg, a gun law expert and a criminal justice professor at Stanford
University, described the open-carry activists as ''a liability'' for the
N.R.A., in particular.

While the N.R.A. is almost always going to support the increased deregulation of
guns, Professor Weisberg said, the organization keeps its distance from
open-carry advocacy because it does not want to distract attention from its
higher priority of promoting the right to carry concealed weapons.

''Add to this that the N.R.A. is a very disciplined, on-message organization,''
he said, contrasting the N.R.A.'s approach with the free-wheeling nature of some
open-carry advocates.

Asked to comment on the open-carry movement, Andrew Arulanandam, a spokesman for
the N.R.A., said the organization ''supports the right of law abiding people to
exercise their self-defense rights in accordance with state local and federal
law.'' He declined to comment further.

Gun control advocates have raised particular concerns about open-carry laws
because under these laws in many states, gun owners are not required to have a
permit or any sort of training or testing.

The first meet-ups by open-carry advocates started nearly a decade ago in
Virginia, but they became popular more recently in California because the law
there makes it difficult for people to get a permit to carry a concealed weapon.

''It is a discriminatory issue in California,'' said Paul Higgins, 43, a
software engineer who runs a Web forum called CaliforniaOpenCarry.org. ''If you
are politically connected, if you're rich, if you're a politician, if you're a
celebrity, you get a permit. Otherwise, you don't.''

Mr. Higgins said the meet-ups were not meant to be confrontational. The hope, he
said, is that if other restaurant or cafe patrons are uncomfortable with guns
being displayed so conspicuously, pressure will increase on lawmakers to
consider changing the law so that weapons can be carried more discreetly.

Mr. Stollenwerk, the co-founder of OpenCarry.org., who is a retired Army officer
from Fairfax County, Va., said the meet-ups were also meant as chances for gun
owners to exercise and advertise their rights in states that allow people to
openly carry firearms. More than 27,000 members are registered for his group's
online discussion forum, he said.

Gun control advocates say the open-carry movement's real aim is to push the
envelope and to force companies to take a public stand on the issue.

''You have to wonder where their next frontier will be,'' said Paul Helmke,
president of the Brady Campaign to Prevent Gun Violence. ''Will gun owners start
trying to carry firearms openly into banks, on subways and buses, in schools?''

For Starbucks, the debate has become a headache.

After California gun owners began holding meet-ups in January at Starbucks, the
Brady Campaign began sending out petitions to pressure the company to forbid
weapons. Starbucks released a statement saying it would not turn gun carriers
away from its cafes, and would instead continue to comply with local laws and
statutes.

''The political, policy and legal debates around these issues belong in the
legislatures and courts, not in our stores,'' Starbuck officials said. They said
the company did not want to be in the middle of the controversy.

Other businesses have taken a different tack -- and are embracing the movement.

The East Coast Pizza Bar and Grill in Walnut Creek, Calif., about 25 miles east
of San Francisco, invited gun owners to host open carry meet-ups. At least 70
people attended one last Sunday, many carrying firearms, said the owner, Jessie
Grunner, 30. And over a dozen returned on Thursday night for more.

''Frankly, I wasn't sure how I would feel in that type of situation, and it
really turned out to be a total nonissue,'' Ms. Grunner said.

''The families were great,'' she said. ''These were very gracious people.'' The
fact that customers wore sidearms, she said, ''just faded into the background.''

URL: http://www.nytimes.com

LOAD-DATE: March 8, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Greg Dement was served a Starbucks drink in Seattle last week as
those with opposing views on customers carrying arms made their position known.
(PHOTOGRAPH BY ELAINE THOMPSON/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


