                   Copyright 2009 The New York Times Company


                             432 of 2021 DOCUMENTS


                               The New York Times

                             August 28, 2009 Friday
                              Late Edition - Final

Cuts? No and Yes

BYLINE: By BERNIE BECKER

SECTION: Section A; Column 0; National Desk; MEDICARE VS. MEDICARE, PART I; Pg.
12

LENGTH: 140 words


Michael S. Steele, the chairman of the Republican National Committee, seems to
be ambivalent about Medicare.

In an op-ed column in The Washington Post on Monday, announcing a Seniors'
Health Care Bill of Rights, Mr. Steele wrote that his party wanted ''to protect
Medicare and not cut it in the name of 'health insurance reform.' ''

In an interview broadcast Thursday on the NPR radio program ''Morning Edition,''
Mr. Steele said he ''absolutely'' supported making certain cuts to Medicare.
''You want to maximize the efficiencies of the program,'' he told the program's
host, Steve Inskeep.

''I'm not saying I like or dislike Medicare,'' Mr. Steele said. ''It is what it
is. It is a program that has been around for over 40 years, and in those 40
years, it has not been run efficiently and well enough to sustain itself.''
BERNIE BECKER

URL: http://www.nytimes.com

LOAD-DATE: August 28, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


