                   Copyright 2009 The New York Times Company


                             1292 of 2021 DOCUMENTS


                               The New York Times

                          December 30, 2009 Wednesday
                              Late Edition - Final

The Case for Reform

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 26

LENGTH: 917 words


Reforming this country's broken health care system is an urgent and essential
task. Given all of the fabrications and distortions from Republican critics, and
the squabbling among Democratic supporters, it is no surprise that many
Americans still have doubts.

President Obama and Democratic leaders have a strong case. They need to make it
now. Here are compelling reasons  for all Americans to root for the reform
effort to succeed and urge Congress to complete the job:

THE HEALTH OF MILLIONS OF AMERICANS The fact that 46 million people in this
country have no health insurance should be intolerable. Every other major
industrial country guarantees health coverage to its citizens, yet the United
States, the richest of them all, does not.

Claims that the uninsured can always go to an emergency room for charity care
ignore the fact that American taxpayers pay a high price for that care. And it
ignores the abundant evidence that people who lack insurance don't get necessary
preventive care or screening tests, and suffer gravely when they finally do seek
treatment because their diseases have become critical.

The American Cancer Society now says the greatest obstacle to reducing cancer
deaths is lack of health insurance. It is so persuaded of that fact that two
years ago, instead of promoting its antismoking campaign or publicizing the need
for cancer screening, it devoted its entire advertising budget to the problem of
inadequate health insurance coverage.

We consider it a moral obligation and sound policy to provide health insurance
to as many people as possible. While the pending bills would fall short of
complete coverage, by 2019, the Senate bill would cover 31 million people and
the House bill 36 million who would otherwise be uninsured under current trends.

MORE SECURITY FOR ALL  Horror stories abound of people -- mainly those who buy
individual policies -- who were charged exorbitant premiums or rejected because
of pre-existing conditions or paid out for years and then had their policies
rescinded when they got sick.

Such practices would be prohibited completely in three or four years under the
reform bills. Before that, insurers would be barred from rescinding policies
retroactively and the bills would establish temporary high-risk pools to cover
people with pre-existing conditions.

The legislation would also allow unmarried dependent children to remain on their
parents' policies until age 26 (the Senate version) or age 27 (the House
version).

If reform legislation is approved, employees enrolled in group coverage at work
would also be more secure. If workers are laid off -- an all too common
occurrence these days -- and need to buy policies on their own, insurers would
be barred from denying them coverage or charging exorbitant premiums for health
reasons.

CUTTING COSTS  Americans are justifiably concerned about the rising cost of
health insurance and of the medical care it covers. The reform bills won't solve
these problems quickly, but they would make a good start.

Despite overheated Republican claims that the reforms would drive up premiums,
the Congressional Budget Office projected that under the Senate bill the vast
majority of Americans (those covered by employer policies) would see little or
no change in their average premiums or even a slight decline. Those who buy
their own policies would pay somewhat more -- but for greatly improved coverage.

Most people who would be buying their own policies would qualify for tax
subsidies to help pay their premiums, which could reduce their costs by
thousands of dollars a year. And small businesses would qualify for tax credits
to defray the cost of covering their workers.

The inexorably rising cost of hospital and medical care is the underlying factor
that drives up premiums, deductibles and co-payments. No one yet has an answer
to the problem. But the bills would launch an array of pilot projects to test
new payment and health care delivery systems within Medicare. These include, for
example, incentives to coordinate hospital and post-hospital care to head off
needless readmissions, better coordination of care for the chronically ill, and
incentives for doctors to provide a patient's total care for a flat fee instead
of charging for each test or service provided.

The Senate bill would set up an independent board to spur the use of programs
that save money or improve care -- subject to Congressional veto. Optimists
believe the savings might come quickly but this could still take many years.
Without passing a reform bill, there is little chance of success.

THE TIME HAS COME  For decades, presidents from both parties have tried in vain
to reform the health care system and cover the uninsured. Still many Americans
wonder, given the deep recession, whether it makes sense to do it now. The first
thing to keep in mind is that the C.B.O. says that the reform bills are paid for
over the next 10 years and would actually reduce future deficits.

The need is clear and the political timing is right with the Democrats
controlling the White House, the Senate and the House. If this chance is
squandered and Republicans gain seats, as expected, in the midterm elections, it
could be a decade or more before reformers have another opportunity. Americans
shouldn't have to wait any longer.

This editorial is a part of a comprehensive examination of the debate over
health care reform. You can read all of the articles at:
nytimes.com/healthcare2009.

URL: http://www.nytimes.com

LOAD-DATE: December 30, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


