                   Copyright 2010 The New York Times Company


                             1574 of 2021 DOCUMENTS


                               The New York Times

                           February 27, 2010 Saturday
                              Late Edition - Final

What Happened at the Health Forum?

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 20

LENGTH: 968 words


To the Editor:

Re ''Health Meeting Fails to Bridge Partisan Rift'' (front page, Feb. 26):

I was riveted to my seat for seven hours during President Obama's health care
forum on Thursday. What I saw was a masterful leader trying, with some success,
to herd cats toward the goal of solving the huge health care problem this
country faces.

Unfortunately, the president faced an opposition party stuck on the mantra
''scrap the plan and start over with incremental changes.'' He repeatedly
demonstrated how this approach would lead to worsening health care and even
larger budget problems in the future.

It became clear to me that, sadly, the Republican Party's strategy is not to
offer any viable alternative but to defeat this president at any cost. One
wonders why.

Harry Lockwood Newton, Mass., Feb. 26, 2010

To the Editor:

I watched the health care summit meeting in utter fascination and came to the
conclusion that this critical issue is no longer resolvable through bipartisan
efforts.

What Congress believes is the right way to resolve this complex debate is
immaterial; that's politics, and this is about leadership.

President Obama must now do what he believes is the right thing to do for the
American people. Right doesn't necessarily mean perfect. It doesn't necessarily
mean popular. It doesn't even necessarily even mean right as judged by history.
It simply means right in his estimation at this time, under these circumstances.

That's what we Americans have come to expect from our presidents; we elect them
to do what's right (in their estimation) for us.

If in the end people are dissatisfied with the decision he makes, with the
results of this initiative, then go ahead and vote him out at the next election
and give someone else a chance to do it better; that's the way the system works.

But great leaders do something that puts them above everyone else -- they lead.

Bob Falcey Skillman, N.J., Feb. 26, 2010

To the Editor:

Disappointed, yes; surprised, no. The Republican Party opposed the Social
Security and Medicare programs from their birth.

For decades, the Republican Party has pursued a strategy of building up deficits
by cutting taxes, howling at attempts to increase revenues in order to
undermine, if not kill, both programs.

The Republican arguments (and philosophy behind the arguments) against the
proposed Democratic health initiative can easily be used against the existing
Social Security and Medicare programs. It is not surprising that, in fact, these
same arguments were actually used in 1935 against Social Security and again in
1965 against Medicare.

The president is committed to a collegial, bipartisan approach. Democrats at
large have no such problem. They should focus the electorate on the Republican
permanent opposition to the very principles behind these two successful programs
that are at the heart of a healthy and functioning democracy.

Charles J. Zwick   Peter A. Lewis  New York, Feb. 26, 2010

The writers are, respectively, the former director and assistant director of the
Bureau of the Budget (now O.M.B.) in the Johnson administration.

To the Editor:

Distinguished leaders went round and round on Thursday debating the problems of
delivering and paying for health care for the American people without finding
actionable agreement. Not one mentioned a simple known model to cut costs
dramatically and deliver care efficiently to everyone -- Medicare for all.

No one dared to recommend this common-sense way to extract the poisons of
useless costs and unnecessary complexity from the system.

Connell J. Maguire Riviera Beach, Fla., Feb.  26, 2010

To the Editor:

Here's an argument not expressed on Thursday that should give free-market
Republicans pause: A lack of affordable individual plans that cover all
pre-existing conditions is preventing potential entrepreneurs from striking out
on their own to bring a creative new idea or business to market. Instead, they
are chained to their present jobs for fear of losing insurance for themselves
and their families.

Current health insurance policies are actually stifling what has made this
country a financial powerhouse in the past -- the possibility that anyone can
make it to the top if he or she is smart and works hard enough.

Pamela D. Crooks La Mesa, Calif., Feb.  26, 2010

To the Editor:

Isn't the real meaning of the health care summit meeting that we have a
president who can debate extemporaneously for seven hours on policy substance?

I think that President Obama won in the short and long run because the
difference between him and the previous president is so obvious.

Charlie Spiegel San Francisco, Feb.  26, 2010

To the Editor:

Re ''Not as Dull as Expected!,'' by David Brooks, and ''Afflicting the
Afflicted,'' by Paul Krugman (columns, Feb. 26):

Here is a brief summary of Mr. Brooks's and Mr. Krugman's points of view on
Senator Lamar Alexander's presentation of the Republicans' arguments against
President Obama's health reform plan:

Mr. Brooks praised Senator Alexander's ability to lead the way ''genially and
intelligently.'' Mr. Krugman pointed out Senator Alexander's shocking ''fibs''
on both the cost of premiums and the process of reconciliation, which Senator
Alexander claimed had ''never been used for something like this.'' (Mr. Krugman
noted that President George W. Bush had used it twice for tax cuts.)

Methinks that verity is in the ear of the listener.

Carol Schlesinger  Livingston, N.J., Feb. 26, 2010

To the Editor:

As I read Paul Krugman's and David Brooks's dueling Feb. 26 columns about the
health care summit meeting, my faith in the ability of two people looking at the
same set of facts and reaching diametrically opposed conclusions has been
reinforced.

Orin Hollander Jamison, Pa., Feb.  26, 2010

URL: http://www.nytimes.com

LOAD-DATE: February 27, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY PETE RYAN)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


