                   Copyright 2009 The New York Times Company


                             533 of 2021 DOCUMENTS


                               The New York Times

                          September 9, 2009 Wednesday
                              Late Edition - Final

After Recess, Health Talk Steps Lively

BYLINE: By SHERYL GAY STOLBERG

SECTION: Section A; Column 0; National Desk; NEWS ANALYSIS; Pg. 1

LENGTH: 1111 words

DATELINE: WASHINGTON


The conventional wisdom, here and around the country, is that the centerpiece of
President Obama's domestic agenda -- remaking the health care system to cut
costs and cover the uninsured -- is on life support and that only a political
miracle could revive it.

Here's why the conventional wisdom might be wrong:

While the month of August clearly knocked the White House back on its heels, as
Congressional town hall-style meetings exposed Americans' unease with an
overhaul, the uproar does not seem to have greatly altered public opinion or
substantially weakened Democrats' resolve.

Critical players in the health care industry remain at the negotiating table,
meaning they are not out whipping up public or legislative opposition.

Despite tensions between moderate and liberal Democrats, there is broad
agreement within the party over most of what a package would look like. Four of
the five Congressional committees considering health care legislation have
already passed bills. Each would require all Americans to have insurance and
provide government subsidies for those who cannot afford it. Each would bar
insurance companies from refusing coverage for pre-existing conditions; imposing
lifetime caps on coverage; or dropping people when they get sick.

Getting a bill through the Senate remains a big challenge, but even there, the
Obama administration has a reasonable chance of corralling the 60 votes it would
need to pass legislation more or less on its terms. One wavering Democratic
moderate, Senator Ben Nelson of Nebraska, signaled over the weekend that he
might be able to go along with one of the compromise proposals under discussion.
Senator Olympia J. Snowe, the Maine Republican whose vote would be vital to Mr.
Obama, remains deeply engaged in negotiations, and there are indications that
one or two other Republicans, like Senator George V. Voinovich of Ohio, might be
in play.

Politically, there is an imperative for Democrats to act; they remember well the
disastrous political fate that befell them in 1994, when they lost control of
the House and Senate  after failing to pass a health bill under President Bill
Clinton. Rahm Emanuel, the bare-knuckled political operative and former Clinton
aide who is now the White House chief of staff, has wasted little time in
reminding his fellow Democrats that, as he said in an interview Tuesday, ''the
inability to act here will have political consequences.''

None of this is to understate the magnitude of the task facing Mr. Obama as he
begins a final drive for the legislation with a nationally televised address to
Congress on Wednesday night. The size and complexity of the legislation, the
deep partisan divide, the undercurrent of concern among voters about whether
government is getting too big and intrusive, opposition from special interests
-- all create land mines that could still blow up the effort.

But even after weeks filled with seemingly ominous portents for Mr. Obama's
ambitions, there is evidence that public opinion remains basically supportive of
him. Despite intense controversy over the ''public option,'' a government-backed
insurance plan that would compete with the private sector, a CBS poll at the end
of August found that 60 percent of Americans still support the idea, down from
66 percent in July. And half the respondents to the poll said Mr. Obama had
better ideas on health care than Republicans, down from 55 percent.

Mr. Obama likes to say that in the 100 years since President Theodore Roosevelt
began advocating universal health care, ''we've never had such broad agreement
on what needs to be done.'' On Capitol Hill, it is possible to see how a
compromise could come together; Mr. Nelson indicated over the weekend that he
could back a provision known as a ''trigger'' to create a public option if
private efforts to cover the uninsured failed.

And despite the fracas of August, the major stakeholders in the health care
debate -- hospitals, doctors, insurers and the pharmaceutical industry -- have
not abandoned the negotiations. Ralph G. Neas, chief executive of the National
Coalition on Health Care and a veteran of Washington legislative fights, said
this was especially significant.

''They're saying to themselves: 'We're going to get 30 to 40 or 50 million new
customers. This is in our economic self interest,' '' Mr. Neas said. ''That, as
much as anything else, could propel this forward to a law that does provide
quality health care for all.''

Mr. Obama still clearly has not closed the deal, which is a major reason he will
be making his case directly to the American people and their elected
representatives on Wednesday night. The CBS poll found that 6 in 10 Americans
say Mr. Obama has not clearly explained what his plans for health reform would
mean.

That is a problem for the White House, though it also presents the president
with an opportunity to reframe the debate on his own terms. In his address on
Wednesday, Mr. Obama has promised to outline what he wants to see in a bill;
Republican leaders say the message from August is that Democrats and the
president need to start over.

''At this point, there really should be no doubt where the American people
stand: the status quo is not acceptable, but neither are any of the proposals
we've seen from the White House or Democrats in Congress,'' Senator Mitch
McConnell of Kentucky, the Republican leader, said in a statement, adding: ''It
should be clear by now that the problem isn't the sales pitch. The problem is
what they're selling.''

Yet Mark McClellan, who ran the Food and Drug Administration and later Medicare
under President George W. Bush, said he saw the churning in August as a part of
the public's education, a ''necessary step in the process'' and not a fatal
blow.

Whether or not Mr. Obama gets the kind of comprehensive bill he is hoping for,
Dr. McClellan said, Congress is all but certain to take up health legislation by
early next year to fix a measure that would impose a draconian 21 percent cut in
Medicare reimbursements to doctors. And once it is tinkering with health care,
he said, it is not that big a leap to imagine lawmakers using that bill to take
smaller steps toward expanding coverage and passing insurance market reforms.

''Everybody is talking about how the public is very concerned about some of the
specifics that they've heard,'' Dr. McClellan said. ''But the public is also
very concerned about some aspects of the health care system, including the cost,
including the security of their coverage. So depending on how this plays
politically, I think there is the foundation for building support for broader
legislation.''

URL: http://www.nytimes.com

LOAD-DATE: September 9, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: News Analysis

PUBLICATION-TYPE: Newspaper


