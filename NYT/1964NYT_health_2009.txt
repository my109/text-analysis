                   Copyright 2010 The New York Times Company


                             1964 of 2021 DOCUMENTS


                               The New York Times

                               May 9, 2010 Sunday
                              Late Edition - Final

Democrats See Hopes for West Dim in Colorado

BYLINE: By JEFF ZELENY

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1108 words


DENVER -- When Barack Obama stood before an admiring audience at Mile High
Stadium here and accepted his presidential nomination 21 months ago, Democratic
leaders crowed about turning Colorado into a reliable stronghold, another step
toward building the party's strength in the West.

Those dreams of expansion have given way to hopes for survival.

Republicans are now well positioned for a statewide resurgence, threatening
several Democratic seats in the midterm elections and raising questions about
whether the opening chapter of the Obama administration has eroded gains that
Democrats had been making here for the previous six years.

A persistently sluggish economy, the ninth-highest foreclosure rate in the
country, the rising federal budget deficit and opposition to the new health care
law have all contributed to a volatile environment for Democrats. The number of
registered Democrats has dropped slightly since Mr. Obama's 9-point victory
here, becoming only the third presidential candidate of his party to carry
Colorado since Harry S. Truman.

''We were set up for a 15- or 20-year Democratic dominance,'' said Gary Hart, a
former Democratic senator from Colorado, adding that the party would have
prospered if the economy had not collapsed. ''This state, more than most, is a
pendulum. It seems to be to be swinging faster than it did before.''

The president's approval ratings in Colorado have hovered just below 50 percent,
according to a variety of public and private polls, but it is the sentiment of
those who strongly disapprove of Mr. Obama that causes particular worry for
Democrats. Party strategists believe that independent male voters will be
difficult, if not impossible, to win over, so Democrats are trying to improve
their standing with independent female voters here and in other states, who will
hold considerable sway in the fall elections.

At stake this year in Colorado are Democratic efforts to hold on to the
governor's office, a United States Senate seat and at least three competitive
House districts, along with both chambers of the state legislature.

The White House has angered some Democrats here by trying to quash a primary
challenge against Senator Michael Bennet, who was appointed to the seat when Ken
Salazar joined the cabinet as interior secretary. Party leaders feared that a
costly primary battle could weaken Mr. Bennet, a former superintendent of
schools in Denver who had never held elective office.

''A fair reading of the facts are that I was appointed, the president endorsed
me and 10 months later someone decided to run against me,'' Mr. Bennet said in
an interview. He added, ''But like anybody else, I have to win this on my own.''

Andrew Romanoff, a former Colorado House speaker, rebuffed repeated appeals and
is challenging Mr. Bennet from the left. He won the Democratic caucuses this
spring and is expected to deliver a strong showing at the state convention on
May 22.

His popularity among people active in party politics provoked enough worry on
Mr. Bennet's behalf that he is hedging his bets and collecting signatures to get
his name on the ballot in case he fails to win enough support at the convention.

''It is not only the stubbornness of Republicans that's at fault here; it is too
often the spinelessness of Democrats,'' Mr. Romanoff said recently at a house
party in Aurora, where voters vented their frustrations about Congress. ''I
don't take any joy in that conclusion.''

While the White House endorses all Democratic incumbents, some voters say the
Colorado case should be different because Mr. Bennet has never been elected. The
Democratic National Committee has tried to fire up the Obama army of supporters,
known as Organizing for America, to help Mr. Bennet. But that decision did not
sit well with some Democrats, several of whom aired their disapproval in
interviews last week.

''Keep your nose out of local politics, you know?'' said Jane Johnson, a
retiree, who first became involved in politics as a volunteer for Mr. Obama's
presidential campaign and supports Mr. Romanoff. ''It's ironic coming from
Obama.''

The unstable situation facing Mr. Obama and his party is a stark contrast to the
mood less than two years ago at the national convention. Democratic strategists
believe their party's popularity will rise as the health care law begins taking
effect and the economy improves.

But registered Democrats have fallen by 30,000 since November 2008, a drop of
about 4 percent, according to the Colorado secretary of state. And since April,
when the state began allowing new voters to register online, more Republicans
than Democrats have done so. As of May 1, the state had 849,572 Republicans,
813,126 Democrats and 752,503 voters not affiliated with either party.

''Never in my wildest dreams did I feel we'd be in this position,'' said Dick
Wadhams, chairman of the Colorado Republican Party. ''Voters got carried away
with the charisma of Obama, but the bailouts, health care, cap-and-trade was not
what they bargained for.''

The political atmosphere has produced the biggest burst of optimism for
Republicans since 2004, Mr. Wadhams said, with the party setting its sights on
Democratic Representatives Betsy Markey, Ed Perlmutter and John Salazar (the
brother of the interior secretary). Ms. Markey, who represents Fort Collins and
the eastern swatch of the state, is seen as one of the most vulnerable members
of Congress.

Yet for all the enthusiasm among party stalwarts, Republicans in Colorado must
also navigate several primary contests on Aug. 10, the outcomes of which will
help determine whether their candidates hold wide appeal to independent voters.
The discontent with government in Washington has produced an active Tea Party
movement and challenges to the Republican establishment.

Democratic strategists concede that in Colorado, as in many other states, their
best chance to avoid big losses in November would be for the Republican Party to
fracture.

At the Cool River Cafe in Greenwood Village, a suburb of Denver that is home to
many independent voters, Jane Norton, a former lieutenant governor, praised Tea
Party activists. ''I say, God bless people for getting off their chairs!'' But
in her bid for the Republican Senate nomination, for which she is one of three
candidates, she said the party needed to produce nominees who could win and be
unified.

''I know we have some disenfranchised independents and Democrats here,'' Ms.
Norton said Friday, speaking over applause that filled the crowded room. ''If
you can't get elected, you can't govern. If you can't govern, you can't change
this country.''

URL: http://www.nytimes.com

LOAD-DATE: May 9, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Andrew Romanoff is challenging Michael Bennet, a fellow
Democrat appointed to the Senate. Jane Norton is seeking the Republican Senate
nomination. (PHOTOGRAPHS BY MATTHEW STAVER FOR THE NEW YORK TIMES) (A23)

PUBLICATION-TYPE: Newspaper


