                   Copyright 2009 The New York Times Company


                             1033 of 2021 DOCUMENTS


                               The New York Times

                            November 20, 2009 Friday
                              Late Edition - Final

Guidelines Push Back Age For Cervical Cancer Tests

BYLINE: By DENISE GRADY; Gardiner Harris contributed reporting from Washington,
and Pam Belluck from Connecticut.

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1267 words


New guidelines for cervical cancer screening say women should delay their first
Pap test until age 21, and be screened less often than recommended in the past.

The advice, from the American College of Obstetricians and Gynecologists, is
meant to decrease unnecessary testing and potentially harmful treatment,
particularly in teenagers and young women. The group's previous guidelines had
recommended yearly testing for young women, starting within three years of their
first sexual intercourse, but no later than age 21.

Arriving on the heels of hotly disputed guidelines calling for less use of
mammography, the new recommendations might seem like part of a larger plan to
slash cancer screening for women. But the timing was coincidental, said Dr.
Cheryl B. Iglesia, the chairwoman of a panel in the obstetricians' group that
developed the Pap smear guidelines. The group updates its advice regularly based
on new medical information, and Dr. Iglesia said the latest recommendations had
been in the works for several years, ''long before the Obama health plan came
into existence.''

She called the timing crazy, uncanny and ''an unfortunate perfect storm,''
adding, ''There's no political agenda with regard to these recommendations.''

Dr. Iglesia said the argument for changing Pap screening was more compelling
than that for cutting back on mammography -- which the obstetricians' group has
staunchly opposed -- because there is more potential for harm from the overuse
of Pap tests. The reason is that young women are especially prone to develop
abnormalities in the cervix that appear to be precancerous, but that will go
away if left alone. But when Pap tests find the growths, doctors often remove
them, with procedures that can injure the cervix and lead to problems later when
a woman becomes pregnant, including premature birth and an increased risk of
needing a Caesarean.

Still, the new recommendations for Pap tests are likely to feed a political
debate in Washington over health care overhaul proposals. The mammogram advice
led some Republicans to predict that such recommendations would lead to
rationing.

Senator Tom Coburn, a Republican from Oklahoma who is also a physician, said in
an interview that he would continue to offer Pap smears to sexually active young
women. Democratic proposals to involve the government more deeply in the
nation's health care system, he said, would lead the new mammography, Pap smear
and other guidelines to be adopted without regard to patient differences,
hurting many people.

''These are going to be set in stone,'' Mr. Coburn said.

Senator Arlen Specter, a Pennsylvania Democrat and longtime advocate for cancer
screening, said in an interview: ''And this Pap smear guideline is yet another
cut back in screening? That is curious.'' Mr. Specter, who was treated for
Hodgkin's lymphoma in 2005 and 2008, said Congress was committed to increasing
cancer screenings, not limiting them.

Representative Rosa DeLauro, Democrat of Connecticut, said that the new
guidelines would have no effect on federal policy and that ''Republicans are
using these new recommendations as a distraction.''

''Making such arguments, especially at this critical point in the debate, merely
clouds the very simple issue that our health reform bill would increase access
to care for millions of women across the country,'' she said.

There are 11,270 new cases of cervical cancer and 4,070 deaths per year in the
United States. One to 2 cases occur per 1,000,000 girls ages 15 to 19 -- a low
incidence that convinces many doctors that it is safe to wait until 21 to
screen.

The doctors' group also felt it was safe to test women less often because
cervical cancer grows slowly, so there is time to catch precancerous growths.
Cervical cancer is caused by a sexually transmitted virus, human papillomavirus,
or HPV, that is practically ubiquitous. Only some people who are exposed to it
develop cancer; in most, the immune system fights off the virus. If cancer does
develop, it can take 10 to 20 years after exposure to the virus.

The new guidelines say women 30 and older who have three consecutive Pap tests
that were normal, and who have no history of seriously abnormal findings, can
stretch the interval between screenings to three years.

In addition, women who have a total hysterectomy (which removes the uterus and
cervix) for a noncancerous condition, and who had no severe abnormalities on
previous Pap tests, can quit having the tests entirely.

The guidelines also say that women can stop having Pap tests between 65 and 70
if they have three or more negative tests in a row and no abnormal test results
in the last 10 years.

The changes do not apply to women with certain health problems that could make
them more prone to aggressive cervical cancer, including H.I.V. infection or
having an organ transplant or other condition that would lead to a suppressed
immune system.

It is by no means clear that doctors or patients will follow the new guidelines.
Medical groups, including the American Cancer Society, have been suggesting for
years that women with repeated normal Pap tests could begin to have the test
less frequently, but many have gone on to have them year after year anyway.

Debbie Saslow, director of breast and gynecologic cancer for the American Cancer
Society, said professional groups were particularly concerned because many
teenagers and young women were being tested and then needlessly subjected to
invasive procedures.

In addition, Dr. Saslow said, doctors in this country have been performing 15
million Pap tests a year to look for cervical cancer in women who have no
cervix, because they have had hysterectomies.

Dr. Carol L. Brown, a gynecologic oncologist and surgeon at Memorial
Sloan-Kettering Cancer Center, said the new guidelines should probably not be
applied to all women, because there are some girls who begin having sex at 12 or
13 and may be prone to develop cervical cancer at an early age.

''I'm concerned that whenever you send a message out to the public to do less,
the most vulnerable people at highest risk might take the message and not get
screened at all,'' Dr. Brown said.

Dr. Kevin M. Holcomb, an associate professor of clinical obstetrics and
gynecology at NewYork-Presbyterian/Weill Cornell hospital, said that when he
heard the advice to delay Pap testing until 21, ''My emotional response is 'Wow,
that seems dangerous,' and yet I know the chances of an adolescent getting
cervical cancer are really low.''

As with the new mammogram recommendations, women may not readily give up a
yearly cancer test.

''For people who've been having the testing regularly every year, it's a big
emotional change to test less frequently and there's this fear of  'Oh my gosh,
I might be missing something,' '' said Ivy Guetta, 49, of Westport, Conn., who
plans to continue with annual Pap tests. Ms. Guetta has three daughters, ages
17, 14 and 8, and at the moment, she would not encourage them to wait until they
turn 21.

Jen Jemison, 24, a legal assistant from Babylon, N.Y., said she thought she
began getting Pap smears when she was about 18, but said that if she had been
aware that the procedure for treating precancerous lesions could lead to
premature births, she would have waited until she turned 21.

On the other hand, Ms. Jemison said that now that she is over 21, ''I would
still go every year'' for the Pap test.

''One of my cousins had cervical cancer, so that's in my head too,'' she said.
''I'd rather get it checked out regularly than have to worry about that.''

URL: http://www.nytimes.com

LOAD-DATE: November 20, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


