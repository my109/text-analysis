                   Copyright 2009 The New York Times Company


                             1129 of 2021 DOCUMENTS


                               The New York Times

                           December 5, 2009 Saturday
                              Late Edition - Final

Home-Care Patients Worried Over Cuts in Health Overhaul

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1183 words

DATELINE: CARIBOU, Me.


Dozing in a big lift chair, propped up by pillows in the living room of her
modest home here, Bertha G. Milliard greeted the nurse who had come to check her
condition and review the medications she takes for chronic pain, heart failure,
stroke and dementia.

Ms. Milliard, 94, said those visits had been highly effective in keeping her out
of the hospital. But the home care she receives could be altered under
legislation passed by the House and pending on the Senate floor as Congress
returned to work this week.

As they are across the nation, Medicare patients and nurses in this town in
northern Maine are anxiously following the Congressional debate because its
outcome could affect Medicare's popular home health benefit in a big way. The
legislation would reduce Medicare spending on home health services, a lifeline
for homebound Medicare beneficiaries, which keeps them out of hospitals and
nursing homes.

Under the bills, more than 30 million Americans would gain health coverage. The
cost would be offset by new taxes and fees and by cutbacks in Medicare payments
to health care providers.

The impact of the legislation on Medicare beneficiaries has been a pervasive
theme in the first week of Senate debate, which is scheduled to continue through
the weekend.

Home care shows, in microcosm, a conundrum at the heart of the health care
debate. Lawmakers have decided that most of the money to cover the uninsured
should come from the health care system itself. This raises the question: Can
health care providers reduce costs without slashing services?

Under the legislation, home care would absorb a disproportionate share of the
cuts. It currently accounts for 3.7 percent of the Medicare budget, but would
absorb 10.2 percent of the savings squeezed from Medicare by the House bill and
9.4 percent of savings in the Senate bill, the Congressional Budget Office says.

The House bill would slice $55 billion over 10 years from projected Medicare
spending on home health services, while the Senate bill would take $43 billion.

Democratic leaders in the House and Senate justify the proposed cuts in nearly
identical terms. ''These payment reductions will not adversely affect access to
care,'' but will bring payments in line with costs, the House Ways and Means
Committee said. The Senate Finance Committee said the changes would encourage
home care workers to be more productive.

The proposed cuts appear to be at odds with other provisions of the giant health
care bills. A major goal of those bills is to reduce the readmission of Medicare
patients to hospitals. Medicare patients say that is exactly what home care
does.

''It helps me be independent,'' said Mildred A. Carkin, 77, of Patten, Me., as a
visiting nurse changed the dressing on a gaping wound in her right leg, a
complication of knee replacement surgery. ''It's cheaper to care for us at home
than to stick us in a nursing home or even a hospital.''

Delmer A. Wilcox, 89, of Caribou, lives alone, is losing his vision, uses a
walker and has chronic diseases of the lungs, heart and kidneys. He said his
condition would deteriorate quickly without the regular visits he received from
Visiting Nurses of Aroostook, a unit of Eastern Maine Home Care.

The Aroostook County home care agency, which lost $190,000 on total revenues of
$1.9 million in the year that ended Sept. 30, estimates that it would lose an
additional $313,000 in the first year of the House bill and $237,000 under the
Senate bill.

The prospect of such cuts has alarmed patients and home care workers. ''We would
have to consider shrinking the area we serve or discontinuing some services,''
said Lisa Harvey-McPherson, who supervises the Aroostook agency as president of
Eastern Maine Home Care.

''Our staff are scared,'' Ms. Harvey-McPherson said, ''but it's our patients who
will pay the price if Congress makes the cuts in home care.''

The four agencies under the umbrella of Eastern Maine Home Care cover a huge
geographic area. Its nurses aim to see five patients a day, and they drive an
average of 25 miles between patients, traversing potato fields and forests of
spruce, birch and maple trees -- and a few bear, moose and lynx. In winter, they
may need a snowmobile, or even cross-country skis, to reach patients in remote
areas.

President Obama has said that the savings in Medicare would be achieved by
eliminating ''waste and inefficiency'' and that ''nobody is talking about
reducing Medicare benefits.'' Moreover, he said, health care providers stand to
benefit because they would gain tens of millions of new paying customers.

Home care executives question the arithmetic.

''No family or individual should ever go without health care coverage,'' Ms.
Harvey-McPherson said, as she drove up to a patient's home here. ''But an
increase in the number of people with insurance would not necessarily help our
agency because we depend so heavily on caring for seniors, with 80 to 90 percent
of our home care revenue coming from Medicare.''

The impact on Medicare is a major concern for Maine's senators, Susan Collins
and Olympia J. Snowe, both Republicans being courted by the White House. Ms.
Collins, a longtime champion of home care, has indicated she will resist the
proposed cuts.

''Deep cuts to home health care would be completely counterproductive to our
efforts to control overall health care costs,'' Ms. Collins said. ''Home care
and hospice have consistently proven to be cost-effective and compassionate
alternatives to institutional care.''

Private insurance companies often follow Medicare's lead. So cuts in home-care
payments could also jeopardize home care for privately insured patients like
Christopher M. Hayes, a 35-year-old police officer in Presque Isle, Me. His left
leg was crushed when he was struck by a car while jogging. He is learning to
walk again with the help of a physical therapist.

In trying to slow the growth of Medicare, Democrats in Congress assume that
health care providers can increase their productivity at the same pace as the
overall economy.

But Saundra Scott-Adams, executive vice president of Eastern Maine Home Care,
said: ''That's a joke for home health care. We provide one-on-one care.''

Her doubts are shared by Richard S. Foster, chief actuary of the federal Centers
for Medicare and Medicaid Services. Mr. Foster said the health care industry was
''very labor-intensive'' and could probably not match the productivity gains of
the overall economy.

While nurses can monitor some patients with electronic telecommunications
devices, they said they still needed to provide hands-on care to many.

Phillip H. Moran, a 65-year-old diabetic in Houlton, Me., lost his right leg
several years ago. His kidneys are failing. Without regular visits from a home
health nurse, Mr. Moran said, he would be in danger of losing his other leg
because of complications from diabetes. As a double amputee, he would be more
likely to go into a nursing home.

''The nurses' visits are really important,'' Mr. Moran said. ''If they are cut,
it could cost people their lives.''

URL: http://www.nytimes.com

LOAD-DATE: December 5, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Bertha G. Milliard, 94, with Ruth Collins, a nurse, during a
home-care visit in Fort Fairfield, Me.
Christopher M. Hayes receiving physical therapy from Betty Jean Gallagher at his
home in Presque Isle, Me.
 Ron Notembaum, of the Visiting Nurses of Aroostook, coaching Mary Abbott of
Caribou, Me., through arm exercises. (PHOTOGRAPHS BY CRAIG DILGER FOR THE NEW
YORK TIMES) (A3)

PUBLICATION-TYPE: Newspaper


