                   Copyright 2010 The New York Times Company


                             1660 of 2021 DOCUMENTS


                               The New York Times

                             March 15, 2010 Monday
                              Late Edition - Final

A National Measure, Inextricably Enmeshed With Local Interests

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; PRESCRIPTIONS MAKING SENSE OF THE
HEALTH CARE DEBATE; Pg. 13

LENGTH: 1026 words


WASHINGTON -- Take a glance at the political map, and it makes perfect sense
that President Obama will open the most critical week in his push for major
health care legislation with a visit on Monday to the Walter F. Ehrnfelt
Recreation and Senior Center, in Strongsville, Ohio.

Just a quick 15-minute drive to the northeast, up Pearl Road, lies the Parma
district office of Representative Dennis J. Kucinich, one of the 39 House
Democrats who voted against the health care legislation in November.

Follow Pearl Road in the opposite direction, south out of Strongsville, and it
also takes about 15 minutes to cross into the 16th Congressional District, home
to Representative John Boccieri, a freshman, who also voted no in November.

As House Democratic leaders seek to corral wavering lawmakers ahead of a
climactic vote in the coming days, there is no doubt that Mr. Obama will be the
cajoler in chief and that the White House will unleash all of its powers of
persuasion in a last-ditch pitch to push the legislation through.

The effort will include calling  lawmakers into the Oval Office and highlighting
what the White House says are specific benefits in the legislation for
individual districts and states.

The final high-stakes lobbying campaign underscores one of the main challenges
that Mr. Obama and Democratic Congressional leaders have faced on health care:
how to fashion legislation for a national overhaul when lawmakers cast votes
based on the particular interests of their particular districts. On issue after
issue in the health care bill, what's good for one is not necessarily good for
all.

As a result, the fate of the legislation now rests in the hands of a few dozen
House Democrats, who represent starkly different places, including
Representatives Brian Baird of Washington,  Michael E. McMahon of Staten Island,
Suzanne M. Kosmas of Florida, Bart Gordon of Tennessee  and  Harry Teague of New
Mexico.

There are currently 431 House members, as a result of four vacancies, and if
everyone votes, the Democrats need 216 votes to pass the bill. While Speaker
Nancy Pelosi has expressed confidence, she does not have enough firm
commitments. White House aides said the vote count in Congress was only part of
their thinking. An even bigger factor in traveling to the area, they said, was a
letter  Mr. Obama received from Natoma A. Canfield, a cleaning woman from
Medina, Ohio, about how she could no longer afford health insurance.

Mr. Obama read the letter aloud to insurance executives at a meeting at the
White House this month.

In the letter, Ms. Canfield, a cancer survivor, described how she had paid
$6,705.24 in premiums in 2009, plus out-of-pocket costs, while her insurance
company paid out only $935.32 in benefits. And yet, her premiums were increased
by more than 40 percent for 2010, prompting her to drop her policy.

''I simply can no longer afford to pay for my health care costs,'' she wrote.

The White House had asked her to introduce Mr. Obama at Monday's event but she
was recently hospitalized with a new diagnosis of leukemia, and the president is
now planning to visit her.

Still, White House aides acknowledge that there are needed votes to be found in
Ohio as well.

In November, as the clock ticked down on the House floor, and the tally remained
extremely tight, Mr. Kucinich hung back in the cloakroom, and aides to Ms.
Pelosi asked if she wanted to speak to him. But the speaker had the votes and
she knew it. ''I don't need him,'' she said, according to an aide.

Mr. Boccieri could be even harder to persuade. He was elected in 2008 to replace
Ralph Regula, a Republican who retired after  holding the seat for 36 years. In
a statement announcing that he would vote against the Democrats' bill in the
fall, Mr. Boccieri said he was concerned that it would not do enough to contain
spiraling health costs.

''It is clear to me that too many of my constituents share my concern about the
cost of this bill and how we pay for it,'' he said.

The senior center in Strongsville would seem to be in a perfect spot.

It lies at the southern edge of Mr. Kucinich's district, right on the border of
the 13th Congressional District, where Representative Betty Sutton is a strong
supporter of the Democrats' legislation. It is in the heart of the greater
Cleveland media market and just a short drive away is Medina, where Ms. Canfield
lives in Mr. Boccieri's district.

The president's trip to Glenside, Penn., outside Philadelphia, last Monday was
similarly aimed at two Democrats who voted against the legislation in the fall.
The event at Arcadia University landed him between the New Jersey district of
Representative John Adler, a freshman, and the Harrisburg-area district of
Representative Tim Holden, who is in his ninth term.

Mr. Holden, who is among the most socially conservative Democrats in the House,
told a local newspaper last week that he would not vote for the Senate-passed
health care bill.

In their intricate maneuvering to try to finish the legislation, Democrats have
said the House will first approve the Senate-passed bill, and then make changes
to it using an expedited budget measure.

Mr. Obama's other trip last week, to St. Charles, Mo., brought him to the home
state of Representative Ike Skelton, a 33-year veteran of the House and the most
senior Democrat to vote against the health care bill in November.

Faced with a tough re-election campaign, Mr. Skelton has indicated in recent
interviews that he will not budge. Unlike the 14 Democratic freshmen who opposed
the health care bill in the fall, Mr. Skelton will not be wowed by a visit to
the Oval Office.

Some lawmakers say it is impossible for Ms. Pelosi or the White House to lock in
votes without first providing final legislative language, which could happen on
Monday.

Another veteran Democrat who opposed the bill in November, Representative Rick
Boucher of Virginia, declined in an interview on Friday to say how he was
leaning. ''At the moment I have no comment to make about health care,'' he said.
''I'm going to make a judgment based on what I think is best for my region.''

URL: http://www.nytimes.com

LOAD-DATE: March 15, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: The no vote of Representative Dennis J. Kucinich, left, when
the House last voted on a health bill did not much matter. Now it might. Two
other Democrats, Representatives Michael E. McMahon of Staten Island, top, and
Harry Teague of New Mexico may also play decisive roles. (PHOTOGRAPHS BY BRENDAN
HOFFMAN/BLOOMBERG NEWS
HARRY HAMBURG/ASSOCIATED PRESS
 MARY DIBIASE BLAICH FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


