                   Copyright 2010 The New York Times Company


                             1819 of 2021 DOCUMENTS


                               The New York Times

                             March 30, 2010 Tuesday
                              Late Edition - Final

Do You Have Health Insurance Now?

BYLINE: By FARHANA HOSSAIN

SECTION: Section D; Column 0; Science Desk; Pg. 3

LENGTH: 1561 words


The new health care law is expected to extend coverage to as many as 32 million
uninsured Americans over the next decade. Under the law, most Americans will be
required to have health insurance by 2014 and the most affluent households will
face higher taxes. Here, how the overhaul could affect those who have health
insurance now and those who do not.

If you're sick, you could get coverage three months from now.

If you have a pre-existing medical condition and have been uninsured for more
than six months, you can get subsidized coverage through a new high-risk
insurance pool until 2014, when insurers can no longer refuse applicants with
pre-existing conditions. The premiums for the program will be ''established for
a standard population,'' according to the Kaiser Family Foundation. Annual
out-ofpocket medical costs will be capped at $5,950 for individuals and $11,900
for families.

You could qualify for Medicaid, the federal-state program for the poor.

Medicaid will cover everyone under the age of 65 earning below 133 percent of
the federal poverty level -- about $14,400 for individuals and $29,327 for a
family of four. Childless adults are now typically excluded from Medicaid and
many parents often face tighter restrictions. In 2009, only about a dozen states
covered parents earning more than 133 percent of poverty level.

You could buy insurance in the new state-run insurance exchanges.

If your employer does not cover you, and you earn too much to qualify for
Medicaid, you can buy coverage through your state's insurance exchange -- a
marketplace where you can compare benefits and prices and choose the policy that
best suits your needs. People who earn up to four times the poverty threshold --
$88,200 for a family of four -- will get subsidies. That means you will pay
somewhere between 2 percent and 9.5 percent of your income for premiums. Your
out-of-pocket costs will also be capped.

You may pay fines if you remain uninsured.

The penalty starts at $95 or 1 percent of income in 2014, whichever is higher,
and rises to $695 or 2.5 percent of income in 2016. But families will not pay
more than $2,085.

You will not be penalized if: You are an American Indian. You have religious
objections. You can show financial hardship. You would pay more than 8 percent
of your income for the cheapest available plan. Your income is below the
tax-filing threshold -- $9,350 for individuals and $18,700 for couples in 2009.

On Your Own

You can keep your current plan

The plan will have to stop some practices within six months, like setting
lifetime limits on coverage, but it doesn't have to meet the new coverage
standards required of all policies in 2014. The plan may not be viable for long
because insurers cannot add benefits or enroll more people in noncompliant
policies.

or, starting in 2014, you can buy coverage through the exchanges.

If your income is below four times the poverty level ($43,320 a year in 2010),
you can get tax credits to help with your premiums. Out-of-pocket costs, like
co-payments, will also be capped.

Premiums for individual plans will be 10 to 13 percent higher by 2016 than the
average premium that year under prior law, according to the Congressional Budget
Office. But most people would qualify for subsidies, meaning they might pay less
than they do now.

Starting in 2013, you may pay higher taxes.

If you are an individual making more than $200,000, or a family making more than
$250,000, you will contribute more to the Medicare program from your payroll
taxes -- 2.35 percent instead of the current 1.45 percent. Your investment
income, now exempt from the payroll tax, will also be subject to a 3.8 percent
levy.

You must spend more than 10 percent of your income on medical expenses before
they can be deducted, up from the existing 7.5 percent threshold.

Through Your Employer

You can keep your current plan

The plan will have to stop some practices within six months, like setting
lifetime limits on coverage, but it doesn't have to meet the new coverage
standards, unless the insurer makes significant changes in benefits or
cost-sharing.

or, starting in 2014, you can buy coverage through the exchanges.

If your employer's plan covers less than 60 percent of the costs, or you are
paying more than 9.5 percent of your income to get it, you can buy in the
exchange.

If your income is below four times the poverty level, and your premiums cost
more than 8 percent of your income, but less than 9.8 percent, you can get a
voucher from your employer to buy in the exchange. Starting in 2013, you may pay
higher taxes.

Top earners will pay increased Medicare payroll tax on wages and investments.

The income threshold for claiming tax deductions on medical costs will be
higher.

Flexible spending accounts -- which typically allow employees to shelter as much
as $4,000 or $5,000 from taxes -- will be reduced to $2,500, and they will not
pay for over-the-counter drugs.

If you have a disability

Starting in 2011, workers can choose to take part in a long-term-care insurance
program, which will provide cash benefits if they become disabled after paying
premiums for some years.

If you have high-value plan

In 2018, the most expensive insurance policies will be subject to a new tax,
which means beneficiaries could face higher out-of-pocket costs or reduced
benefits.

Through Medicare

You will pay less for preventive care.

Medicare will pay for an annual checkup. And deductibles and co-payments for
many preventive services and screenings will be eliminated.

If you receive prescription drug benefits

If you hit the gap in coverage of prescription drugs, known as the ''doughnut
hole,'' you will receive a $250 rebate this year and a 50 percent discount on
brand-name drugs in 2011. The government would completely close the doughnut
hole by 2020 by phasing in additional discounts on brand-name and generic drugs.

If you are an individual who makes more than $85,000, or part of a household
that earns more than $170,000, you will pay higher premiums starting in 2011.

If you are enrolled in a private Medicare Advantage plan

You may face higher premiums or reduced benefits. In 2012, the government would
begin to cut subsidies to these plans run by private insurance companies. These
plans cost the government more, on average, than traditional Medicare, but often
provide more comprehensive coverage.

If you live in an area where primary care doctors are in short supply

From 2011 through 2015, Medicare will pay a 10 percent bonus to doctors who
participate in underserved areas, like inner cities and rural communities.

Through Medicaid

You can continue your coverage.

States have to maintain current income eligibility for the program until 2014
for most adults and 2019 for children, but beneficiaries may lose some optional
benefits, like dental and vision care. Because of the economic downturn, many
states had made or were considering substantial cuts in the public program for
the poor and the disabled. (States also cannot reduce eligibility until 2019 for
the Children's Health Insurance Program, which covers children in families that
earn too much to qualify for Medicaid, but too little to afford private health
insurance.)

You may have more doctors to choose from.

In 2013 and 2014, Medicaid's reimbursements to doctors will be increased to the
same level as Medicare, so more doctors may be willing to participate in the
program. In 2008, Medicaid reimbursements averaged only 72 percent of the rates
paid by Medicare, according to a recent Urban Institute study.

You may pay less for preventive care.

Next year, states will receive additional money to offer certain preventive
services recommended by the government without additional cost to Medicaid
beneficiaries. Also, starting in October, Medicaid programs will be required to
cover services that help pregnant women quit smoking.

If you require institutional care because of a disability or a chronic illness

States will receive incentives to offer home- and community-based services to
chronically ill and disabled individuals through Medicaid.

Effect on Insurers

Within six months

Plans that include coverage of children cannot deny coverage for a pre-existing
condition. Plans cannot drop people when they become sick.

Young adults can stay on their parents' plan until they turn 26. The rule does
not apply to employer-sponsored plans if the child's job offers health
insurance. 2014 and after

Insurers cannot deny coverage because of a person's medical condition or charge
higher premiums because of a person's sex or health status.

All new plans have to offer a minimum package of benefits defined by the federal
government, including certain preventive services without any costs.

Effect on Employers

2010 to 2013

Small businesses will receive tax credits to offer coverage to their employees.
If an employer pays at least half of its employee's premiums, it can receive a
tax credit up to 35 percent of the contribution.2014 and after

Small businesses can buy insurance through the exchanges and get a tax credit up
to 50 percent of employees' premiums for two years.

An employer with more than 50 workers will pay a penalty if it does not offer
health insurance and any of its full-time workers get subsidized coverage in the
exchanges.

URL: http://www.nytimes.com

LOAD-DATE: May 1, 2010

LANGUAGE: ENGLISH

GRAPHIC: CHART: On Your Own
Through Your Employer
Through Medicare
Through Medicaid
Effect on Insurers
Effect on Employers
 DRAWINGS

PUBLICATION-TYPE: Newspaper


