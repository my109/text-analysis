                   Copyright 2009 The New York Times Company


                             1025 of 2021 DOCUMENTS


                               The New York Times

                           November 19, 2009 Thursday
                              Late Edition - Final

A Counter Ad Campaign

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 228 words


The onslaught of television commercials opposing  health care legislation has
prompted a liberal advocacy group and two labor unions to begin a $1.7 million
advertising campaign in favor of the  overhaul.

The commercials, being shown on both cable and broadcast stations in 13
Congressional districts, try to buck up 12 Democrats and the lone Republican who
voted in favor of health care legislation when it passed the House on Nov. 7.

They will be faced with a second vote after the House bill is merged with the
Senate's version.

These 13 members have been under assault in television advertisements by the
United States Chamber of Commerce and affiliated business organizations that
contend a health care overhaul would raise taxes, cost jobs and hurt the already
weakened economy.

The new commercials are being paid for by Americans United for Change, the
advocacy group; the American Federation of State, County and Municipal
Employees; and the Service Employees International Union. They say businesses
are attacking these members because an overhaul would keep the health insurance
industry from raising premiums.

They also say an overhaul would ''strengthen Medicare,'' and has been endorsed
by AARP, which represents older people. The commercials urge viewers to call
their Congressional representative and ''tell him not to back down.''

KATHARINE Q. SEELYE

URL: http://www.nytimes.com

LOAD-DATE: November 19, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


