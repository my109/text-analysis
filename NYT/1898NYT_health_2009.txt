                   Copyright 2010 The New York Times Company


                             1898 of 2021 DOCUMENTS


                               The New York Times

                             April 18, 2010 Sunday
                              Late Edition - Final

Welcome to Confederate History Month

BYLINE: By FRANK RICH

SECTION: Section WK; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 10

LENGTH: 1599 words


It's kind of like that legendary stunt on the prime-time soap ''Dallas,'' where
we learned that nothing bad had really happened because the previous season's
episodes were all a dream. We now know that the wave of anger that crashed on
the Capitol as the health care bill passed last month -- the death threats and
epithets hurled at members of Congress -- was also a mirage.

Take it from the louder voices on the right. Because no tape has surfaced of
anyone yelling racial slurs at the civil rights icon and Georgia Congressman
John Lewis, it's now a blogosphere ''fact'' that Lewis is a liar and the
''lamestream media'' concocted the entire incident. The same camp maintains as
well that the spit landing on the Missouri Congressman Emanuel Cleaver was
inadvertent spillover saliva from an over-frothing screamer -- spittle, not
spit, as it were. True, there is video evidence of the homophobic venom directed
at Barney Frank -- but, hey, Frank is white, so no racism there!

''It's Not About Race'' declared a headline on a typical column defending
over-the-top ''Obamacare'' opponents from critics like me, who had the nerve to
suggest a possible racial motive in the rage aimed at the likes of Lewis and
Cleaver -- neither of whom were major players in the Democrats' health care
campaign. It's also mistaken, it seems, for anyone to posit that race might be
animating anti-Obama hotheads like those who packed assault weapons at
presidential town hall meetings on health care last summer. And surely it is
outrageous for anyone to argue that conservative leaders are enabling such
extremism by remaining silent or egging it on with cries of ''Reload!'' to
pander to the Tea Party-Glenn Beck base. As Beck has said, it's Obama who is the
real racist.

I would be more than happy to stand corrected. But the story of race and the
right did not, alas, end with the health care bill. Hardly had we been told that
all that ugliness was a fantasy than we learned back in the material world that
the new Republican governor of Virginia, Robert McDonnell, had issued a state
proclamation celebrating April as Confederate History Month.

In doing so, he was resuscitating a dormant practice that had been initiated in
1997 by George Allen, the Virginia governor whose political career would implode
in 2006 when he was caught on camera calling an Indian-American constituent
''macaca.'' McDonnell had been widely hailed by his party as a refreshing new
''big tent'' conservative star when he took office in Richmond, the former
capital of the Confederacy, in January. So perhaps his Dixiecrat proclamation,
if not a dream, might have been a staff-driven gaffe rather than a deliberate
act of racial provocation.

That hope evaporated once McDonnell was asked to explain why there was no
mention of slavery in his declaration honoring ''the sacrifices of the
Confederate leaders, soldiers and citizens.'' After acknowledging that slavery
was among ''any number of aspects to that conflict between the states,'' the
governor went on to say that he had focused on the issues ''I thought were most
significant for Virginia.'' Only when some of his own black supportersjoined
editorialists in observing that slavery was significant to some Virginians too
-- a fifth of the state's population is black -- did he beat a retreat and
apologize.

But his original point had been successfully volleyed, and it was not an
innocent mistake. McDonnell's words have a well-worn provenance. In ''Race and
Reunion,'' the definitive study of Civil War revisionism, the historian David W.
Blight documents the long trajectory of the insidious campaign to erase slavery
from the war's history and reconfigure the lost Southern cause as a noble battle
for states' rights against an oppressive federal government. In its very first
editorial upon resuming publication in postwar 1865, The Richmond Dispatch
characterized the Civil War as a struggle for the South's ''sense of rights
under the Constitution.'' The editorial contained not ''a single mention of
slavery or black freedom,'' Blight writes. That evasion would be a critical
fixture of the myth-making to follow ever since.

McDonnell isn't a native Virginian but he received his master's and law degrees
at Pat Robertson's university in Virginia Beach during the 1980s, when Robertson
was still a rare public defender of South Africa's apartheid regime. As a major
donor to McDonnell's campaign and an invited guest to his Inaugural breakfast,
Robertson is closer politically to his protege than the Rev. Jeremiah Wright
ever was to Barack Obama. McDonnell chose his language knowingly when initially
trying to justify his vision of Confederate History Month. His sanitized spin on
the Civil War could not have been better framed to appeal to an unreconstructed
white cohort that, while much diminished in the 21st century, popped back out of
the closet during the Obama ascendancy.

But once again you'd have to look hard to find any conservative leader who
criticized McDonnell for playing with racial fire. Instead, another Southern
governor -- who, as it happened, had issued a Confederate Heritage Month
proclamation of his own -- took up his defense. The whole incident didn't
''amount to diddly,'' said Haley Barbour, of Mississippi, when asked about it by
Candy Crowley of CNN last weekend.

Barbour, a potential presidential aspirant, was speaking from New Orleans, where
the Southern Republican Leadership Conference was in full cry. Howard Fineman of
Newsweek reported that he couldn't find any African-American, Hispanic or
Asian-American attendees except for the usual G.O.P. tokens trotted out as
speakers -- J. C. Watts, Bobby Jindal and Michael Steele, only one of them
(Jindal) holding public office.

New Orleans had last attracted G.O.P. attention in 2008, when John McCain
visited there as part of a ''forgotten places'' campaign tour to deliver the
message that his party cared about black Americans and that ''never again''
would the city's tragedy be ignored. ''Never'' proved to have a shelf life of
less than two years. None of the opening-night speakers at last weekend's
conference (Newt Gingrich, Liz Cheney, Mary Matalin et al.) so much as mentioned
Hurricane Katrina, according to Ben Smith of Politico. When Barbour did refer to
it later on, it was to praise the Bush administration's recovery efforts and
chastise the Democrats' ''man-made disaster'' in Washington.

Most Americans who don't like Obama or the health care bill are not racists. It
may be a closer call among Tea Partiers, of whom only 1 percent are black,
according to last week's much dissected Times/CBS News poll. That same survey
found that 52 percent of Tea Party followers feel ''too much'' has been made of
the problems facing black people -- nearly twice the national average. And
that's just those who admit to it. Whatever their number, those who are
threatened and enraged by the new Obama order are volatile. Conservative
politicians are taking a walk on the wild side by coddling and encouraging them,
whatever the short-term political gain.

The temperature is higher now than it was a month ago. It's not happenstance
that officials from the Sons of Confederate Veterans in Virginia and Mississippi
have argued, as one said this month, that the Confederate Army had been
''fighting for the same things that people in the Tea Party are fighting for.''
Obama opposition increasingly comes wrapped in the racial code that McDonnell
revived in endorsing Confederate History Month. The state attorneys general who
are invoking states' rights in their lawsuits to nullify the federal health care
law are transparently pushing the same old hot buttons.

''They tried it here in Arkansas in '57, and it didn't work,'' said the
Democratic governor of that state, Mike Beebe, likening the states' health care
suits to the failed effort of his predecessor Orval Faubus to block nine black
students from attending the all-white Little Rock Central High School. That
battle for states'  rights ended when President Eisenhower, a Republican who
would be considered a traitor to his party in 2010, enforced federal law by
sending in troops.

How our current spike in neo-Confederate rebellion will end is unknown. It's
unnerving that Tea Party leaders and conservatives in the Oklahoma Legislature
now aim to create a new volunteer militia that, as The Associated Press
described it, would use as yet mysterious means to ''help defend against what
they believe are improper federal infringements on state sovereignty.'' This is
the same ideology that animated Timothy McVeigh, whose strike against the
tyrannical federal government will reach its 15th anniversary on Monday in the
same city where the Oklahoma Legislature meets.

What is known is that the nearly all-white G.O.P. is so traumatized by race it
has now morphed into a bizarre paragon of both liberal and conservative racial
political correctness. For irrefutable proof, look no further than the peculiar
case of its chairman, Steele, whose reckless spending and incompetence would
cost him his job at any other professional organization, let alone a political
operation during an election year. Steele has job security only because he is
the sole black man in a white party hierarchy. That hierarchy is as fearful of
crossing him as it is of calling out the extreme Obama haters in its ranks.

At least we can take solace in the news that there's no documentary evidence
proving that Tea Party demonstrators hurled racist epithets at John Lewis. They
were, it seems, only whistling ''Dixie.''

URL: http://www.nytimes.com

LOAD-DATE: April 18, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY BARRY BLITT)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


