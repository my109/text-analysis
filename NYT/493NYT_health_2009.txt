                   Copyright 2009 The New York Times Company


                             493 of 2021 DOCUMENTS


                               The New York Times

                            September 4, 2009 Friday
                              Late Edition - Final

Once and Future Taxes

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 20

LENGTH: 793 words


So far, the Obama administration's plan for dealing with the budget deficit --
an estimated $9 trillion over a decade -- is to not dig the hole any deeper.
That's an important first step. President Obama deserves credit for proposing
ways to pay for his two big initiatives to date: health care reform and energy
legislation. Reducing the growth in health care costs, in particular, is vital
to curbing future deficits.

As for the hundreds of billions of dollars in economic stimulus, their impact on
long-term deficits is marginal because the spending is temporary. More
important, deficit spending is warranted in a recession because it eases the
downturn and in so doing, averts even worse damage to the economy and the
budget.

But, sooner than he may prefer, Mr. Obama will have to face up to what he has so
far avoided: the need to raise taxes broadly to rein in deficits.

The deficits are not of his making. Some two-thirds of the $9 trillion shortfall
resulted from policies that predate his administration; most of the rest is the
cost of policies that both parties consider necessary, like continued relief
from the alternative minimum tax.

But when he inherited the burden of the budget mess, Mr. Obama also inherited
the responsibility to clean it up. Neither economic growth nor spending cuts
will be enough to fix the projected shortfalls. Nor is there enough to be gained
by confining tax increases only to families making more than $250,000 a year, a
campaign promise that Mr. Obama still says he will keep.

Assuming the economy has begun to recover by 2010, next year would be the
natural time to start raising taxes. That's because the Bush-era tax cuts are
set to expire at the end of 2010. If Congress does nothing, taxes will revert to
higher levels for everyone; if it extends all of the cuts, taxes will stay low
for everyone; if it extends some and lets others expire, taxes will stay low for
some taxpayers and go up for others.

Since 2010 is also a Congressional election year, lawmakers will be reluctant to
raise taxes at all, and certainly not without considerable support from the
White House, which is already worried about the 2010 elections.

Under these political pressures, Congress might be tempted to extend all of the
Bush cuts at least through 2011 -- and that would be a dangerous move because
time is not necessarily on Mr. Obama's side.

No one is angling to raise taxes during the recession, but the longer it takes
to show real progress on deficit reduction, the greater the possibility that the
nation's creditors will demand higher interest rates on loans to the Treasury.
That would worsen the deficit by raising the nation's borrowing costs. And with
the recovery of both the financial system and the housing market dependent on
low interest rates, an unanticipated or uncontrolled rate increase would be a
crisis in its own right.

The question then is not whether taxes must go up, but when, how and how much.
The White House budget director, Peter Orszag, has said the administration is
working to bring the deficit down in the 2011 budget, due early next year. But
when asked recently by The Wall Street Journal for details, including the
possibility of higher taxes on families making less than $250,000, Mr. Orszag
said that the administration was not yet giving any specifics on the next
budget.

In the meantime, the tax code remains inadequate to the task of raising
sufficient revenue -- and high-income taxpayers are about to benefit once again.
Next year, a misguided law enacted in 2006 will take effect, giving high-income
taxpayers the chance to shelter much of their money from future tax increases.

The law will let high-income taxpayers transfer traditional individual
retirement accounts into so-called Roth I.R.A.'s. Unlike regular I.R.A.'s., no
tax is due when money is withdrawn from a Roth. That often makes Roths a better
deal, especially if you believe that tax rates will be higher in the years to
come -- and they are bound to be higher. Taxpayers who switch to Roths will have
to pay tax upfront on the amounts they transfer, so the government will get a
jolt of revenue. But later, the transfers will be a money loser for the
government as high-income Americans and their heirs make tax-free withdrawals
that would have been taxable at tomorrow's higher rates.

The Obama administration may not want to talk about the need for broad tax
increases while other issues dominate the agenda. But if the administration and
Congress do not act rationally and in a timely way, they risk being forced to
act by circumstances beyond their control. In that event, the economic harm to
Americans would be far greater than simply acknowledging the obvious and acting
accordingly.

URL: http://www.nytimes.com

LOAD-DATE: September 4, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


