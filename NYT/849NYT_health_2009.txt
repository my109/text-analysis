                   Copyright 2009 The New York Times Company


                             849 of 2021 DOCUMENTS


                               The New York Times

                            October 19, 2009 Monday
                              Late Edition - Final

An Overhaul And a Separate Bill To Fix Medicare

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; PRESCRIPTIONS MAKING SENSE OF THE
HEALTH CARE DEBATE; Pg. 14

LENGTH: 885 words

DATELINE: WASHINGTON


''A.M.A. Opens Bid to Kill Medicare.''

That was the blunt headline that appeared in The New York Times on Jan. 27,
1965, as Congress prepared to take up legislation to provide government health
insurance to Americans 65 and over.

''The American Medical Association has opened its 'national educational'
campaign against medical care for the aged under social security,'' the article
began.

And in a detail that seems almost quaint, the article noted that two of the
major television networks, CBS and NBC, had refused to sell one-minute
commercial slots to the doctors' group, citing the controversial nature of the
issue.

The A.M.A. is now broadcasting a new television commercial about Medicare but
with a different take: the doctors want to protect the program, and themselves,
from steep cuts in payments.

But an effort to avert those cuts -- at a cost of nearly $250 billion over 10
years, in legislation separate from the big health care overhaul -- is raising
uncomfortable questions for President Obama and for Democratic leaders in
Congress who promised that revamping the health care system would not add to the
nation's deficits.

In the new A.M.A. ad, a female voice-over says, ''For seniors, a doctor can mean
everything -- independence, hope, security -- and Medicare makes it possible,''
as the screen shows gray-haired Americans in various happy settings with their
doctors.

''But every year Congress must make a temporary fix to the Medicare payment plan
so seniors can keep their doctor and the care they depend on,'' the voice-over
says. ''We need a permanent solution to protect Medicare and ensure seniors get
the security and stability they have earned. Call your senators today. Ask them
to pass S. 1776 to protect seniors' access to quality care.''

S. 1776 is a Senate bill that would permanently adjust a Medicare payment
formula that for years has threatened to impose steep annual cuts in the rates
that doctors are paid. The formula, tracing to laws passed in 1989 and 1997, was
devised to keep Medicare spending in check.

But in recent years Congress has stepped in with a patch, known on Capitol Hill
as the annual ''doc fix,'' to prevent the cuts. Congressional Democrats have no
plans to offset the cost of S. 1776, which is why they are eager to keep it
separate from the broader health care legislation and avoid breaking the
president's promise.

''I will not sign a plan that adds one dime to our deficits -- either now or in
the future. Period,'' Mr. Obama said in his televised speech to Congress last
month. Congressional Democrats insist that fixing the doctor payment formula
should not count toward the cost of the big health care legislation, because it
is a problem they inherited. What they have trouble explaining, though, is how
the flawed formula is different from any of the zillion other entrenched
problems in the health care system that the proposed overhaul aims to fix.

House Democrats included the formula adjustment in their health care bill. They
said its cost should be counted separately.

Political Calculus

Fixing the formula permanently is a top priority for the A.M.A., a powerful
lobbying force. And with the health insurance industry attacking the overhaul
legislation, Congressional Democrats and the White House are anxious to keep
doctors from turning against them as well.

Last week, Senator Debbie Stabenow, Democrat of Michigan, introduced S. 1776 as
a stand-alone bill. The Senate majority leader, Harry Reid of Nevada, said it
was a necessary step. ''The slate should be wiped clean,'' Mr. Reid said.
''Everyone understands we address these physician payment cuts every year.''

Not all Democrats agree.

Senator Kent Conrad, Democrat of North Dakota, and chairman of the Budget
Committee, has derided the stand-alone bill and said he would not vote for it.
Mr. Conrad engaged in months of intense negotiations to make sure that the
Senate Finance Committee version of the broad health care bill -- the one the
committee approved last Tuesday -- could make a credible claim of fiscal
responsibility.

The Finance Committee bill includes only a one-year patch of the doctor payment
formula, with the cost offset by taxes and reduced spending elsewhere. Mr.
Conrad has called for a commission to develop a longer-term solution to the
doctor payment formula as well as other recurring budgetary problems.

But critics of the broader legislation, including many Republicans, say the
dispute over how to fix the Medicare formula highlights a larger flaw in the
health care overhaul. To pay for expanding health benefits to the uninsured, the
broader legislation calls for other, potentially painful cuts to slow the steep
growth in Medicare spending.

Skeptics note that the doctor payment formula was originally meant to be a
cost-saving measure. Under the formula, Medicare is supposed to reduce payments
to doctors whenever Medicare spending for their services exceeds a goal, the
''sustainable growth rate,'' linked to the nation's economic growth.

Because Congress has repeatedly stepped in to prevent these cuts, the projected
Medicare savings have not materialized. And critics predict that Congress will
similarly balk when it comes to making the Medicare cuts promised in the current
health care legislation.

URL: http://www.nytimes.com

LOAD-DATE: October 19, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: The American Medical Association is running ads in support of a
bill that would avert cuts in Medicare payments.

PUBLICATION-TYPE: Newspaper


