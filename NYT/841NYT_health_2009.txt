                   Copyright 2009 The New York Times Company


                             841 of 2021 DOCUMENTS


                               The New York Times

                            October 18, 2009 Sunday
                              Late Edition - Final

From Five Proposals, A Mix of Possibilities

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; Pg. 24

LENGTH: 808 words


Polls show that a majority of people in this country are utterly bewildered by
the health care bills Congress is considering, and that they have no idea how
each would affect their family budgets or their medical care.

Those questions should become somewhat easier to answer as Congress moves closer
to producing a single piece of legislation. (Don't hold your breath; that may
not be until Christmas.)

But even now, while lawmakers hash things out behind closed doors, consumers can
make some preliminary assessments of the bills that have been passed by
Congressional committees. Just keep in mind that the final product could be
something else entirely.

You could start by visiting The New York Times Web site (nytimes.com), where an
interactive chart allows users to compare various elements of each of the bills.
(The online version of this article has a link to that chart.)

Provisions in the bills could affect everyone, but there is intense debate over
whether costs will go up or down. Unfortunately, the nonpartisan Congressional
Budget Office says it cannot forecast what will happen to premiums because there
are so many uncertain variables.

What is clear is that if Congress requires everyone to have coverage, some low-
to middle-income families will be spending hefty chunks of their income, despite
tax credits from the government.

''Policies will be much more affordable than they are now for the uninsured,''
said Drew Altman, chief executive of the Kaiser Family Foundation. ''But for
some middle-class families it could still be a stretch, especially if they face
other big bills at the same time.''

A Closer Look

An interactive calculator created by the Kaiser foundation shows how expensive
health insurance could be for those who would have to buy insurance on their
own.

The calculator allows you to see estimates of the premiums and the government
subsidies for different groups under the different bills. The estimates apply
only to those who would be buying insurance on their own, not to those who are
already covered through their employers, Medicare or Medicaid.

The Kaiser calculator lets a user plug in age, income and family status, as well
as indicate whether the person lives in a high- or low-cost region -- insurance
premiums vary substantially by area -- and then determine what that person would
pay.

For example, if you are 40 years old with a family of four and an annual income
of $70,350 (the national median) and you live in a medium-cost area, your annual
premium would be $9,435 under the Senate Finance Committee bill, according to
the calculator. And you would get a subsidy of $993. That would be 12 percent of
your net income, in return for protection against potentially catastrophic
medical expenses.

Under the Senate health committee bill, that same person's premium would be
$10,513 -- higher than under the Finance Committee bill because it provides more
generous insurance. And the subsidy, also more generous, would be $4,339. So the
family would end up paying 8.8 percent of income.

Under proposals from the House Ways and Means Committee and the House labor
committee, which are similar on costs, the premium would be $9,435, and the
subsidy $2,835 -- accounting for 9.4 percent of income.

And under the House Energy and Commerce Committee bill, the premium would be
$9,435 with a subsidy of $2,132. That is 10.4 percent of income.

The Public Option

The Kaiser calculator does not figure what the cost of premiums or the amount of
subsidies might be under a so-called public option, a government-run insurance
plan that would compete with -- and presumably charge less than -- private
insurers.

All but one of the bills in Congress now contain a public option, but none of
them is the same and there is a fight raging over whether any public option
should be in the final legislation. And even if one is, it would probably not be
available to people covered by their employers -- most people, in other words.

The Kaiser calculator also does not take into account co-payments or other
potential costs Congress may impose, like taxes or fees on high-income
taxpayers.

Larry Levitt, the Kaiser foundation's vice president, who helped design the
calculator, said it was more useful for comparing people in different
circumstances than in determining exactly how much each would pay.

''No one can predict with certainty what health insurance will cost 10 years
from now, even under the status quo,'' Mr. Levitt said.

And no one is predicting that Congress will pass any of the current bills
intact. So these estimates serve only as a guidepost.

''Congress is struggling right now with how to pay for health care reform,'' Mr.
Levitt said. ''And the calculator illustrates one of the biggest consequences of
the decisions they'll make -- what real people might pay.''

URL: http://www.nytimes.com

LOAD-DATE: October 18, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


