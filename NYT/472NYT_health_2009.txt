                   Copyright 2009 The New York Times Company


                             472 of 2021 DOCUMENTS


                               The New York Times

                          September 2, 2009 Wednesday
                              Late Edition - Final

Looking to Save Health Care Reform

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 22

LENGTH: 1001 words


To the Editor:

Re ''Majority Rule on Health Care Reform'' (editorial, Aug. 30):

You are certainly correct that ''it has become inescapably clear that
Republicans are unlikely to vote for substantial reform this year,'' and you are
also correct to warn against further delays and compromises with a party
determined to use the health care debate as a partisan tool to destroy the Obama
presidency, as Senator Jim DeMint was foolish enough to admit publicly.

But the reconciliation process may not be the best solution because of the
procedural hurdles that the opponents of reform can be counted on to raise. A
better solution would be for President Obama to try to get the necessary votes
for cloture.

The president must exert strong leadership and make it clear to the Democratic
caucus in general and the Blue Dog Democrats in particular that for the sake of
their party, the country and the future of the Obama administration, they must
vote for cloture so that a bill that will accomplish substantive reform can have
an up-or-down vote on the floor.

If the Blue Dog Democrats then feel that they must vote against the final bill
so that their campaign contributors in the health care industry will not be
angry at them, then that is their choice. This plan would enable President Obama
to present the country with a real and coherent reform that could be enacted
with 51 votes and to avoid a reconciliation process fraught with difficulties
and imponderables.

But it depends on the Democrats' realizing what a catastrophe it would be for
the country and for their party if there were no reform or a reform in name only
that did not accomplish its purpose.

Michael Ossar  Portland, Ore., Aug. 30, 2009

To the Editor:

Your editorial favors using reconciliation bills to pass comprehensive health
care reform through the Senate, arguing that ''the ideological split between
parties is too wide'' to achieve a compromise. But in your December 2005
editorial ''Protecting Public Lands,'' budget reconciliation bills are called
''a handy hiding place for ideas that could never stand up to public scrutiny on
their own.''

If it is wrong to legislate through this backdoor channel on a relatively minor
issue like whether the holders of mining claims should be allowed to buy public
land rather than leasing it, isn't it even worse to enact legislation affecting
the health of millions of people through this same method?

Furthermore, how can one square this view with your March 2005 editorial
''Walking in the Opposition's Shoes,'' which calls the filibuster a ''peculiar
but effective form of government''?

Galen Simmons  New York, Aug.   30, 2009

To the Editor:

In your clarion call for imposition of the reconciliation process when the
Senate considers a Democratic bill for transformative health care reform, a
process that would supersede the usual voting procedure, you do not mention
polls indicating great concern within the body politic with the legislation you
so heartily advocate.

There is no small quotient of hubris attached to altering Senate procedure to
force transformative change on 16 percent of the gross domestic product, all in
the face of substantial opposition. Unseemly tactics will hardly persuade the
doubtful majority of Americans that they have been well served.

Paul Bloustein  Cincinnati, Aug.  30, 2009

The writer is a physician.

To the Editor:

America should hope that all Democratic senators read and heed your editorial
suggesting how their party can enact President Obama's signature health care
reform. Clearly, most aspects of it fit within the rubric of ''budget
reconciliation.''

Nearly every domestic issue facing the nation is affected by health care costs
-- whether it is the fiscal well-being of struggling icons like General Motors;
the competitive position of producers of goods and services that have to vie for
orders with producers in countries whose governments relieve them of employee
health care costs; the individual health insurance costs that are sucked from
other consumer spending; or the uncontrollable cost of health care driven by
transfer of unpaid medical care of the uninsured to the bills of the insured.

The editorial sagaciously warns that this is the moment. America must hope --
yes, demand -- that pursuit of campaign contributions does not steal this
historic opportunity.

Sharon Morrison  Whitefish, Mont., Aug.  30, 2009

To the Editor:

Re ''Until Medical Bills Do Us Part,'' by Nicholas D. Kristof (column, Aug. 30):

Thirty years ago, as my father wasted away from the ravages of Parkinson's
disease, my mother faced the same dilemma -- divorce or bankruptcy.

The fact that nothing has changed in three decades does not augur well for
health care reform and speaks volumes about our nation's misguided values and
priorities.

Carole S. Tunick  Scarsdale, N.Y., Aug. 30, 2009

To the Editor:

The column  cites a very real problem, but unwittingly makes the case for less
government intrusion, not more.

It is government-run Medicaid that forces people to deplete their savings in
order to qualify for aid. Why would we want government in control of health care
when bureaucrats routinely establish and carry out arbitrary rules that deny
help to anyone who has accumulated any assets during a lifetime?

Joseph Melody  Los Angeles, Sept.  1, 2009

To the Editor:

Nicholas D. Kristof's column on the use of divorce by the elderly to avoid
financial impoverishment because of long-term custodial illness unfortunately
perpetuates a harmful myth.

In New York State, the healthy spouse may retain $109,560 in savings while the
ill spouse qualifies for Medicaid. Under ''spousal refusal,'' assets of an even
greater amount can be legally protected.

The best advice is for the couple to seek the services of a lawyer who devotes
his or her career to these issues: an elder law attorney.

Daniel G. Fish  New York, Aug.  31, 2009

The writer is a past president of the National Academy of Elder Law Attorneys.

URL: http://www.nytimes.com

LOAD-DATE: September 2, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY LINDSAY BALLANT)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


