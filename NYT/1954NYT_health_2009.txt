                   Copyright 2010 The New York Times Company


                             1954 of 2021 DOCUMENTS


                               The New York Times

                              May 4, 2010 Tuesday
                              Late Edition - Final

Teaching Physicians The Price Of Care

BYLINE: By SUSAN OKIE; This article was produced in collaboration with Kaiser
Health News -- an editorially independent program of the Kaiser Family
Foundation, a nonpartisan health policy research organization not affiliated
with Kaiser Permanente.

SECTION: Section D; Column 0; Science Desk; Pg. 5

LENGTH: 1421 words


When Dr. Ryan Thompson, an internist, was a medical resident at  Massachusetts
General Hospital in Boston a few years ago, he worried that he and his fellow
trainees weren't learning about the cost of medical treatments, the financing of
health care and the impact of high medical bills on their patients.

So, with the help of faculty members at Harvard Medical School and another
resident, Dr. Thompson organized an elective course for residents on the topic.
In one session, ''I made up a kind of 'Price Is Right' game,'' he recalled. ''I
got the bill of a patient and actually went through it,'' asking colleagues to
guess the cost of commonly ordered tests, like  CT scans. Their answers ''were
all over the map,'' he said. ''They had no idea.''

Doctors in training have traditionally been insulated from information about the
cost of the tests and treatments they order for patients -- in fact, for
decades, the subject was virtually taboo when professors and trainees discussed
treatment decisions during hospital rounds. During four years of medical school,
students learn to order tests and treatments based on their knowledge of
diseases and of scientific evidence.

Until recently, most schools included little information on financial factors,
like how the insurance system works and how treatment costs affect patients'
behavior. As a result, most physicians enter practice with little sense of how
to make the most cost-effective choices for patients, or how their own decisions
affect the patient's -- and the nation's -- medical bills.

''Medical schools have done a really terrible job over the years in educating
students about the system that they're going to encounter,'' said Dr. Michael
Whitcomb, former senior vice president for medical education at the American
Association of Medical Colleges, or A.A.M.C., and former editor of the journal
Academic Medicine.

But escalating costs and the national debate over the health care overhaul are
forcing medical schools and residency programs to grapple with teaching about
the financial side of their profession. Accrediting organizations now require
such teaching, and students and residents recognize that they need to understand
finances as well as blood tests.

''It's a very odd system where we make purchasing decisions on behalf of
patients but we don't know what anything costs,'' said Dr. Neel Shah, a
first-year resident in obstetrics-gynecology at Brigham and Women's Hospital in
Boston. ''There's no disincentive to ordering tests -- all we have to do is
click a button and we've ordered it.''

To be accredited, medical schools and hospital residency programs, in which
doctors spend three to five years learning a specialty, are supposed to be
teaching future doctors about health care costs and cost-effective practices.
The A.A.M.C., which does the accreditation of medical schools, made this clear
in a 1998 report. And since 2007, residency programs in the United States have
been required to teach doctors to ''incorporate considerations of cost awareness
and risk-benefit analysis'' in caring for patients, according to the
Accreditation Council for Graduate Medical Education.

A commentary in the April 8 issue of The New England Journal of Medicine by Dr.
Molly Cooke, a professor at the University of California, San Francisco, and
director of the school's Academy of Medical Educators, said it was ''a critical
responsibility of medical schools and residency programs'' to educate physicians
about cost issues.

''Medical schools and residencies, in general, are taking this very seriously,''
said Dr. John Prescott, a physician and the medical college association's chief
academic officer.

Nonetheless, the effort has not been universal. According to a recent A.A.M.C.
survey, about 60 percent of 102 American and Canadian medical schools include
some material on health care costs, although the time they  devote to it varies
widely.

Dr. Prescott said a separate survey of 155 large teaching hospitals that
together sponsor more than two-thirds of accredited residency programs in the
United States found that only 41 percent  had made sure that all their
residencies included material on health care  costs.

Dr. Whitcomb, the former A.A.M.C. vice president, said one reason medical
schools and residencies have been slow to tackle the subject is that most of
their faculty members are academic doctors or researchers who know little about
health care economics and don't feel comfortable teaching it. ''Trying to figure
out how to do it and who's going to do it is a real challenge,'' he said.

Some medical schools have met the challenge by creating courses for first- or
second-year students on health policy or professional responsibility that
include information about treatment costs and insurance.

A second-year student at the Yale School of Medicine, Alexandra Ristow, said,
''We discussed the fact that we have all this technology now that we can't
necessarily afford to provide to every single patient -- and it's not necessary,
either.'' She added that, in other classes, cost considerations are part of the
discussion of how to treat hypothetical patients.

Professors ask: ''What is this test going to tell you? Is it going to change the
diagnosis, or change how you manage the patient's care?'' Ms. Ristow said.
Concerns about treatment costs, she said,  are ''just very pervasive.''

But once students begin working on medical teams in hospitals, during their
third year of medical school, they must focus on developing clinical skills and
learning to care for patients -- and, according to students at several schools,
the financial aspects of care are seldom discussed.

''We all know that the cost of health care is high, but it's sort of fuzzy how
doctors play into this,'' said Chitra Akileswaran, who has completed three years
at Harvard Medical School and is enrolled in a joint degree program in medicine
and business administration.

At the Mount Sinai School of Medicine in New York City, students can get
hands-on lessons about the impact of treatment costs on patients by volunteering
Saturdays at the East Harlem Health Outreach Program, a student-run free clinic
for uninsured residents of a low-income neighborhood nearby.

Medical students evaluate patients, choose which drugs are prescribed, arrange
care for patients who need to see a surgeon or other specialist and collaborate
with a social worker to help those who need social services or assistance paying
for medicine. They also sit on the steering committee and are in charge of
researching and updating the clinic's formulary, a stock of inexpensive,
cost-effective drugs that are purchased from the hospital pharmacy.

Dr. Yasmin S. Meah, an assistant professor of medicine at Mount Sinai who
directs the program, noted that medical students initially campaigned to
establish the clinic, similar to others at several New York medical schools and
an estimated 110 nationwide, as a service to the community. Since opening in
2004, it has become an invaluable setting for teaching future doctors to
practice cost-effectively. Students come to ''understand what is necessary and
what is not,'' Dr. Meah said. ''They're using the cheapest drugs to get the
biggest bang for the buck.''

During residency, trainees' heavy workloads and the need to care efficiently for
very sick people make it hard for them to consider the cost of tests and
treatments. ''You definitely try to think about it,'' said Dr. Ian Warrington, a
first-year resident in emergency medicine at Beth Israel Deaconess Medical
Center in Boston. ''All of us are aware that a CT scan is significantly more
expensive than a chest X-ray, and an M.R.I. is significantly more than a CT
scan.''

Emergency-medicine residents memorize guidelines that help them determine when
imaging studies are needed after certain kinds of injuries, and professors
routinely ask trainees to defend their rationale for ordering certain tests. But
sometimes, Dr. Warrington said, residents and the doctors supervising them will
end up ordering an expensive test even when the probability of finding a
fracture or a hemorrhage is low because they are worried about missing something
serious, and about being sued if they do.

''You're sort of working on not enough time and not always enough information,
and you do the best you can,'' he said. Cost ''should be factored in, but
realistically, with the environment that we practice in, you have to be very
cautious how you do that.''

URL: http://www.nytimes.com

LOAD-DATE: May 4, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


