                   Copyright 2009 The New York Times Company


                             509 of 2021 DOCUMENTS


                               The New York Times

                            September 6, 2009 Sunday
                              Late Edition - Final

President Obama's Health Choices

SECTION: Section WK; Column 0; Editorial Desk; EDITORIAL; Pg. 7

LENGTH: 715 words


President Obama's address to Congress about health care reform on Wednesday is
the moment for him to stand tough for a large and comprehensive plan. This is no
time to yield on core elements of reform or on the scale of the effort in search
of enough Republican support to provide the veneer of bipartisanship, or even
the one or two Republican votes needed to overcome a filibuster.

As a political tactic, Mr. Obama has thus far simply issued broad principles for
reform and left it to Congress to flesh out the details, leaving great
uncertainty as to what reforms he considers essential. Given the raucous, often
ill-informed attacks on Democratic proposals over the past month, and the clear
aim of most Republicans to oppose any bill, no matter how much he compromises,
Mr. Obama now needs to spell out in some detail what he wants and how it would
benefit both the uninsured and most other Americans as well.

Mr. Obama needs to highlight the concerns that got many people agitating for
government help in the first place -- the rising premiums and co-payments
required for their health insurance policies, and the likelihood that, if forced
to buy insurance on their own, perhaps after losing a job, they would be unable
to afford it or even be denied coverage because of pre-existing conditions.

At a minimum, we believe Mr. Obama should declare himself in favor of covering
as many of the uninsured as possible in the near future; should insist that
insurers grant and renew coverage without regard to health status; and should
insist on new insurance exchanges in which people without group coverage and
those working for the smallest employers could buy insurance at large-group
rates.

Despite calls from Republicans that he jettison support for a new public plan to
compete with private plans on those exchanges, he should not do so now. If he
decides to bargain it away later, he should insist, minimally, that a strong
public plan be introduced if private insurers fail to hold costs down in the
future.

We are alarmed at reports that the price for winning over Republicans and
conservative Democrats might be a drastically scaled-down plan that would cost
not $1 trillion over 10 years but perhaps only $700 billion or much less. That
big a reduction would be a mistake. The insurance reforms that people most want
-- and the insurance industry is willing to accept -- depend on achieving near
universal coverage to spread the risks over a large group of healthy and
unhealthy people.

The big costs of reform involve expanding Medicaid to cover more of the poor and
providing subsidies to help those with low or modest incomes buy insurance on
the new exchanges. Scaling down too far would most likely result in subsidies
too limited to really help people. Imagine the backlash if millions of Americans
were required to carry insurance and found they could not  afford to buy it.

The other major element of reform is slowing the  rise in medical costs while
improving quality. Mr. Obama should say whether he believes pending bills need
an extra dose of cost-cutting reforms, or pilot projects, in Medicare. Nobody in
either party has a sure-fire solution to rein in medical inflation. Mr. Obama
should insist on an independent commission to monitor reforms, expand those that
work and drop those that do not.

If Mr. Obama is reaching out for broader support, he may be too diplomatic to
point out the cynicism of Republican opponents who are late-blooming advocates
of deficit reduction. The Bush administration and a Republican-controlled
Congress enacted a Medicare prescription drug benefit that will cost the
government almost $1 trillion over the next decade without raising or saving a
penny to pay for it.

They also passed tax cuts for wealthy Americans that will cost more than $1.7
trillion over 10 years, again without making provisions to offset the costs. Now
they are complaining that $1 trillion for health care reform -- fully paid for
over the next 10 years -- is too much to spend on a problem that has been
festering for decades.

Rather than yield to Republican intransigence, the Democrats ought to resort to
a parliamentary maneuver known as ''budget reconciliation,'' which would allow
them to push through most reforms by majority vote.

URL: http://www.nytimes.com

LOAD-DATE: September 6, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


