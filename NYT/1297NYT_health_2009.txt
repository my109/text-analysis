                   Copyright 2009 The New York Times Company


                             1297 of 2021 DOCUMENTS


                               The New York Times

                             January 1, 2010 Friday
                              Late Edition - Final

The Time Is Now to Enact Health Care Reform

SECTION: Section A; Column 0; Editorial Desk; LETTER; Pg. 28

LENGTH: 193 words


To the Editor:

You make a good case for reforming health care now under the leadership of
President Obama and the Democratic majorities in the House and Senate (''The
Case for Reform,'' editorial, Dec. 30).

Republicans and the health insurance companies are in cahoots with each other
and have done, and continue to do, everything they can to keep reform from
happening.

Fox News and right-wing talk radio hosts have also played a major role in trying
to defeat reform and President Obama, who deserves a lot of credit for making
health care reform a priority in his first year in office.

The ''survival of the fittest'' Republican legislators do not deserve to gain
any seats in the 2010 midterm elections, based on their being the party of
negativity on the health care issue.

The money in politics and capitalistic greed must be reformed before meaningful
legislation on any issue can become law. Health care is a part of American life
that should not be commercialized.

Our country should be ashamed: we are the only industrialized nation in the
world that does not have universal health care.

Paul L. Whiteley Sr. Louisville, Ky., Dec. 30, 2009

URL: http://www.nytimes.com

LOAD-DATE: January 1, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


