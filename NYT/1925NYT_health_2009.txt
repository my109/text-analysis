                   Copyright 2010 The New York Times Company


                             1925 of 2021 DOCUMENTS


                               The New York Times

                             April 23, 2010 Friday
                              Late Edition - Final

PepsiCo Raises Profit After Buying Two Bottlers

BYLINE: By BLOOMBERG NEWS

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 2

LENGTH: 265 words


PepsiCosaid Thursday its first-quarter profit rose 26 percent after it acquired
its two largest bottlers.

Net income increased to $1.43 billion, or 89 cents a share, from $1.14 billion,
or 72 cents, a year earlier, PepsiCo said. Earnings excluding some one-time
items were 76 cents a share,  just beating the 75 cents expected by industry
analysts.

PepsiCo completed the $7.8 billion purchase of its top two bottlers in March in
an attempt to save $400 million a year and get control of its distribution
system for drinks in North America. The company estimates it will spend $650
million for programs to cut annual expenses as it integrates the bottlers, up
from the $400 million it previously predicted.

Sales in the quarter rose 13 percent, to $9.37 billion from $8.26 billion,
falling short of analysts' estimates.

Sales volume at PepsiCo Americas Foods grew 1 percent, helped by its Frito-Lay
subsidiary.

PepsiCo Americas Beverages' volume declined 4 percent while the unit handling
Africa, the Middle East and Asia raised snack volume 13 percent and beverage
volume 10 percent.

PepsiCo said it would take a $40 million charge in the second quarter related to
the federal health care overhaul. The charge is a result mostly of a tax on
federal subsidies that the company receives for providing a Medicare Part D
prescription drug benefit, the chief financial officer, Hugh F. Johnston, said
on a call with reporters.

The company said it planned to buy back about $4.4 billion in shares this year.

Stock in PepsiCo, which is based in Purchase, N.Y., fell $1.22, to $64.76 a
share.

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


