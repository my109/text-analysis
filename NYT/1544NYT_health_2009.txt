                   Copyright 2010 The New York Times Company


                             1544 of 2021 DOCUMENTS


                               The New York Times

                            February 22, 2010 Monday
                              Late Edition - Final

How the G.O.P. Can Fix Health Care

BYLINE: By CHARLES KOLB.

CHARLES KOLB, former domestic policy adviser to President George H. W. Bush and
president of the nonpartisan Committee for Economic Development

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 19

LENGTH: 384 words


HEALTH care reform should have two critical goals: reducing costs and covering
more people. To meet them, Democrats and Republicans must abandon simplistic,
ideologically driven proposals that animate each party's base. Liberals cannot
insist on Medicare for all, and conservatives cannot insist on markets for all,
plus tort reform. Neither approach will work.

Medicare is an inherently inflationary fee-for-service system that rewards
volume, not value. Market-based strategies cannot cover the uninsured or prevent
some insurance companies from covering only healthy Americans.

Our existing employer-sponsored system -- a pre-World War II dinosaur awaiting
extinction -- offers most Americans little, if any, real choice among competing
insurance plans. Both parties should jettison it in favor of giving Americans
better health care choices.

To do this, Congress should not micromanage people's health care decisions by
imposing price controls or setting up more bureaucracy. Rather, it should
introduce competition to the insurance market by creating a system of regional
exchanges, similar to the one now operated by the federal government for its
employees, to allow everyone the opportunity to choose an insurance plan. Then
Americans would be able to shop for health insurance and pocket what they save
by choosing lower-priced plans. Appropriate risk adjustment -- a mechanism by
which insurers who cover more sick people are compensated by insurers who cover
fewer of them -- could reduce the incentive of some insurance companies to sign
up only the healthy.

Congress should also end the current tax exemption for employer-sponsored
insurance coverage. This change would encourage people to pay more attention to
the price of their health insurance. And it would provide the money that will be
needed to help underwrite coverage for the uninsured.

Finally, no backroom deals -- for pharmaceutical companies, individual members
of Congress or anyone else.

We have a rare opportunity to lower America's health care costs, extend coverage
and provide better care. What's uncertain is whether our bipartisan leadership
is up to the task.

-- CHARLES KOLB, former domestic policy adviser to President George H. W. Bush
and  president of the nonpartisan Committee for Economic Development

URL: http://www.nytimes.com

LOAD-DATE: February 22, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID PLUNKERT)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


