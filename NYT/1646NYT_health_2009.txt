                   Copyright 2010 The New York Times Company


                             1646 of 2021 DOCUMENTS


                               The New York Times

                             March 12, 2010 Friday
                              Late Edition - Final

Getting Obama Right

BYLINE: By DAVID BROOKS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 27

LENGTH: 810 words


Who is Barack Obama?

If you ask a conservative Republican, you are likely to hear that Obama is a
skilled politician who campaigned as a centrist but is governing as a
big-government liberal. He plays by ruthless, Chicago politics rules. He is
arrogant toward foes, condescending toward allies and runs a partisan political
machine.

If you ask a liberal Democrat, you are likely to hear that Obama is an inspiring
but overly intellectual leader who has trouble making up his mind and fighting
for his positions. He has not defined a clear mission. He has allowed the
Republicans to dominate debate. He is too quick to compromise and too cerebral
to push things through.

You'll notice first that these two viewpoints are diametrically opposed. You'll,
observe, second, that they are entirely predictable. Political partisans always
imagine the other side is ruthlessly effective and that the public would be with
them if only their side had better messaging. And finally, you'll notice that
both views distort reality. They tell you more about the information cocoons
that partisans live in these days than about Obama himself.

The fact is, Obama is as he always has been, a center-left pragmatic reformer.
Every time he tries to articulate a grand philosophy -- from his book ''The
Audacity of Hope'' to his joint-session health care speech last September -- he
always describes a moderately activist government restrained by a sense of
trade-offs. He always uses the same on-the-one-hand-on-the-other sentence
structure. Government should address problems without interfering with the
dynamism of the market.

He has tried to find this balance in a town without an organized center -- in a
town in which liberals chair the main committees and small-government
conservatives lead the opposition. He has tried to do it in a context maximally
inhospitable to his aims.

But he has done it with tremendous tenacity. Readers of this column know that
I've been critical on health care and other matters. Obama is four clicks to my
left on most issues. He is inadequate on the greatest moral challenge of our
day: the $9.7 trillion in new debt being created this decade. He has misread the
country, imagining a hunger for federal activism that doesn't exist. But he is
still the most realistic and reasonable major player in Washington.

Liberals are wrong to call him weak and indecisive. He's just not always
pursuing their aims. Conservatives are wrong to call him a big-government
liberal. That's just not a fair reading of his agenda.

Take health care. He has pushed a program that expands coverage, creates
exchanges and moderately tinkers with the status quo -- too moderately to
restrain costs. To call this an orthodox liberal plan is an absurdity. It more
closely resembles the center-left deals cut by Tom Daschle and Bob Dole, or Ted
Kennedy and Mitt Romney. Obama has pushed this program with a tenacity unmatched
in modern political history; with more tenacity than Bill Clinton pushed his
health care plan or George W. Bush pushed Social Security reform.

Take education. Obama has taken on a Democratic constituency, the teachers'
unions, with a courage not seen since George W. Bush took on the
anti-immigration forces in his own party. In a remarkable speech on March 1, he
went straight at the guardians of the status quo by calling for the removal of
failing teachers in failing schools. Obama has been the most determined
education reformer in the modern presidency.

Take foreign policy. To the consternation of many on the left, Obama has
continued about 80 percent of the policies of the second Bush term. Obama
conducted a long review of the Afghan policy and was genuinely moved by the
evidence. He has emerged as a liberal hawk, pursuing victory in Iraq and
adopting an Afghan surge that has already utterly transformed the momentum in
that war. The Taliban is now in retreat and its leaders are being assassinated
or captured at a steady rate.

Take finance. Obama and Tim Geithner are vilified on the left as craven to Wall
Street and on the right as clueless bureaucrats who know nothing about how
markets function. But they have tried with halting success to find a center-left
set of restraints to provide some stability to market operations.

In a sensible country, people would see Obama as a president trying to define a
modern brand of moderate progressivism. In a sensible country, Obama would be
able to clearly define this project without fear of offending the people he
needs to get legislation passed. But we don't live in that country. We live in a
country in which many people live in information cocoons in which they only talk
to members of their own party and read blogs of their own sect. They come away
with perceptions fundamentally at odds with reality, fundamentally
misunderstanding the man in the Oval Office.

URL: http://www.nytimes.com

LOAD-DATE: March 12, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


