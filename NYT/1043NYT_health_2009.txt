                   Copyright 2009 The New York Times Company


                             1043 of 2021 DOCUMENTS


                               The New York Times

                           November 21, 2009 Saturday
                              Late Edition - Final

The Senate Health Reform Bill

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 18

LENGTH: 473 words


The long struggle by Senate Democratic leaders to merge two versions of health
care reform into a single bill was worth the wait. The bill that the majority
leader, Harry Reid, hopes to bring to the floor soon for debate is an
improvement in many respects from the Senate Finance Committee's bill, which had
previously been considered a possible template for what the Senate was likely to
approve.

The merged bill would cost $848 billion over the next decade and would cover
some 31 million people who would otherwise be uninsured in 2019, bringing
coverage to 94 percent of all citizens and legal residents below Medicare age.
And it would reduce the deficit by $130 billion over the first decade and by
more than half-a-trillion dollars over the next decade, putting the lie to
Republican charges that the reforms would drive up deficits.

The bill also includes two provisions that could slow the rise in health care
costs. One is a tax on the most expensive employer-provided insurance, the
so-called Cadillac plans. It would push employers and workers toward lower-cost
plans and shift compensation toward higher wages. The other is a new commission
with power to propose changes in Medicare that would be hard for Congress to
overrule.

The merged bill would make insurance more affordable to the middle class on new
insurance exchanges. It would limit the amount that subsidized individuals and
families would have to pay toward premiums in the exchanges to just under 10
percent of their incomes, well below the 12 percent to 12.5 percent in other
bills.

But that reduction would come at the expense of slightly higher contributions
demanded of low-income enrollees, a shortcoming that ought to be remedied as
this legislation progresses through Congress.

The Senate bill is weaker in many respects than the trillion-dollar bill passed
by the House, which would cover more of the uninsured and provide greater
subsidies. It would postpone many reforms until 2014, a year later than the
House bill, delaying benefits for millions of Americans. It also lacks an
explicit mandate on employers to offer coverage. The House bill does a better
job of closing the gap in Medicare that leaves many elderly beneficiaries
struggling to pay for medicines.

Conservative Democratic senators whose votes will be needed to break a
Republican filibuster are restive over the costs of the overall plan and over
including a public option, even with an opportunity for states to opt out. Some
may also object to provisions that would allow enrollees to buy plans that cover
abortions on the exchanges using their own money, a more reasonable standard
than the virtual ban on abortion coverage under the House bill. Despite these
concerns, conservative Democrats owe it to the nation to help break a Republican
filibuster and allow debate to proceed.

URL: http://www.nytimes.com

LOAD-DATE: November 21, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


