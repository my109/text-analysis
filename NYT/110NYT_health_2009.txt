                   Copyright 2009 The New York Times Company


                             110 of 2021 DOCUMENTS


                               The New York Times

                              July 6, 2009 Monday
                              Late Edition - Final

Health Care's Infectious Losses

BYLINE: By PAUL O'NEILL.

Paul O'Neill was the secretary of the Treasury from 2001 to 2002.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 19

LENGTH: 643 words

DATELINE: Pittsburgh


HEALTH care reform seems to be on the way, whether we want it or not. So I have
been asking questions about the various proposals. Here is a sampling.

Which of the reform proposals will eliminate the millions of infections acquired
at hospitals every year?

Which of the proposals will eliminate the annual toll of 300 million  medication
errors?

Which of the proposals will eliminate pneumonia caused by ventilators?

Which of the proposals will eliminate falls that injure hospital patients?

Which of the proposals will capture even a fraction of the roughly $1 trillion
of annual  ''waste'' that is associated with the kinds of process failures that
these questions imply?

So far, the answer to each question is ''none.''

Let's consider that $1 trillion of waste. If we could capture all of it, the
savings over 10 years would be five times what President Obama has said he will
extract from insurance companies over the same period. The president's vision of
bringing down health care inflation by 1.5 percent a year over the next decade
would not be a victory, but a capitulation to the enormous waste in the delivery
of medical care.

The president says he likes audacious goals. Here is one: ask medical providers
to eliminate all hospital-acquired infections within two years. This is hardly
pie in the sky: doctors and administrators already know how to do it. It
requires scrupulous adherence to simple but profoundly important practices like
hand-washing, proper preparation of surgical sites and assiduous care and
maintenance of central lines and urinary catheters. With these small steps, we
would no longer have the suffering and death associated with infections acquired
in hospitals and we would save tens of billions of dollars every year -- money
we should have in hand before new health-care entitlements are enacted.

What policymakers tend to forget is that only the people who do the work can
make this happen. Legislation can't do it, regulation can't do it,
infection-control committees can't do it, financial incentives and disincentives
can't do it. But excellence is possible, and it has been demonstrated.

Where it works, the common denominators are strong leadership and a committed
work force. Among those doctors showing the way are Brent James at Intermountain
Health in Utah, Gary Kaplan at Virginia Mason Clinic in Seattle and Richard
Shannon at the University of Pennsylvania, who have helped bring infection rates
down drastically at their own hospitals and at others.

Hospitals and medical schools have great impetus to increase the ranks of such
doctors: these improvements in patient care don't cost money, they save money.
And they represent only the tip of the iceberg in opportunities for improving
outcomes and reducing costs at the same time.

A next step would be for the government to finance a prompt, detailed and
hard-headed study of every example of error, infection and other waste in five
major medical centers. Such data would give policymakers and caregivers a
clearer picture of the possibilities for cost-saving improvements.

It would also help if reporters and pundits became more informed about the
opportunities for improvement, so they could help educate the public and improve
the level of the reform debates. As for members of Congress, perhaps it would
help them to understand the problem if we assembled the data, by House district,
on hospital-acquired infections, medication errors and other waste indicators.
They are more likely to push for the right sort of change when they realize that
people they know and represent are being hurt or killed by practices we know how
to stop.

In the end, any health care reform that does not address the pervasive waste and
the associated burden of needless suffering for patients and staff  alike will
give us little to celebrate.

URL: http://www.nytimes.com

LOAD-DATE: July 6, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


