                   Copyright 2009 The New York Times Company


                             382 of 2021 DOCUMENTS


                               The New York Times

                            August 22, 2009 Saturday
                              Late Edition - Final

Masters and Slaves of Deception

BYLINE: By CHARLES M. BLOW

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 17

LENGTH: 947 words


President Obama made a huge mistake in pursuing health care reform. He tried to
be hands-off when he needed to be knee-deep. The White House overcompensated for
the Clinton-era defeat of health care reform by offering too little guidance and
support to its own party's lawmakers, who are now struggling to craft a coherent
message out of a confusing bill. Adding to the challenge: this must be done in a
bipartisan way.

But true bipartisanship, not withstanding candidate Obama's grandiose visions of
a post-partisan world, is becoming an increasingly quaint notion.

In this Washington, no outreached hand goes unmangled. Seeing an opening,
conservatives have rushed in.

They have started Operation Master and Slaves of Deception -- cooking up scary,
outlandish claims about the plan and feeding them to a desperate base, eager to
believe the worst about the man they loathe the most, President Obama.

Conservatives are now on fertile ground. Still enraged by the ignominy of having
been trounced, they are most likely feeling marginalized, ignored and afraid.
For them, it has been less about clarifying health care reform and more about a
clarion call to resistance.

And the deceptions have worked. According to an NBC News poll released this
week, 76 percent of Republicans believe that the health care plan will lead to a
government takeover of the health care system; 70 percent believe it will allow
the government to make decisions about when to stop providing medical care to
the elderly; and 61 percent believe it will allow the use of taxpayer dollars
for abortions.

Conservative lawmakers should be delirious. Not only is this stoking the
passions of their dwindling base, it is opening the spigot of money from the
health care industry and conservative groups.

According to a report on Wednesday by OpenSecrets.org's Center for Responsive
Politics, health services and H.M.O.'s increased their campaign donations by 37
percent from the first to the second quarter this year. Republican and
conservative advocacy groups increased their contributions by a whopping 237
percent over the same time period. For comparison, donations by Democratic and
liberal advocacy groups declined.

Realizing that the plan was running into trouble with a wrangling right, the
president began kowtowing to the crazies himself. When the president of the
United States has to reassure us that he is not going to kill our grandmothers,
you know there's a problem.

The president should have treated health care reform the same way he treated the
stimulus package -- by personally helping to shape it and push it through from
the start.

On Feb. 11, a Gallup poll found that 59 percent of Americans over all supported
the economic stimulus package, but only 28 percent of Republicans supported it.
This is not so different from the way Americans feel about health care.
According to a CNN/Opinion Research poll released this month, 50 percent of
Americans said that they favored ''Barack Obama's plan to reform health care,''
compared with just 19 percent of Republicans.

The Democrats forged ahead with the stimulus package. The House passed it
without a single Republican vote, and the Senate passed it with the help of
three moderate Republicans.

Six months later, what is the result? According to a Washington Post/ABC News
poll released yesterday, half of Americans now expect the recession to be over
within 12 months and ''almost twice as many say the program has made things
better as say it has made things worse (43 percent to 23 percent).''

Nothing succeeds like success. Now that the stimulus plan appears to be working,
no one even remembers the acrimony about its passage.

Finally the Democrats are making rumblings that they may try to go it alone on
health care reform as well. It's about time.

The conservatives are not negotiating in good faith anyway. Chuck ''snake in
the'' Grassley, ranking Republican member of the Senate Finance Committee and
one of three Republicans supposedly working on the bipartisan health bill, is
even propagating the nonsense, telling supporters that they have ''every right
to fear ... a government program that determines if you're going to pull the
plug on grandma.''

There is nothing that the Democrats can do to placate this cabal. This is how
the Republicans see it: for them to win, the plan must fail.

Concessions simply weaken the bill, with little reward. For instance, a
Rasmussen Reports poll last week found that 78 percent of conservatives opposed
the reform plan. This week, they were asked if they would support the plan if
the controversial public option was removed, and 73 percent said that they would
still oppose it. By contrast, liberal support for the plan sans public option
dropped by a third.

The conservatives are never going to play ball. As the Senate Republican whip,
Jon Kyl of Arizona, put it, ''there is no way that Republicans are going to
support a trillion-dollar-plus bill,'' even if it doesn't increase the deficit.

Most important, this two-year window may be the only time Democrats can push
through reform without Republican support. As Nate Silver pointed out on his
blog last week, ''Without major intervening events like 9/11, the party that
wins the White House almost always loses seats at the midterm elections -- since
World War II, an average of 17 seats in the House after the White House changes
parties.'' There is no reason to believe that this won't be the case next year.

A poll released by Rasmussen Reports this week found that on a generic
Congressional ballot, Republican candidates hold a 5 percent advantage over
Democratic ones.

The time is now. Just do it.

URL: http://www.nytimes.com

LOAD-DATE: August 22, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHART: Feelings about the health care reform legislation currently
being considered in Congress ... : Including public option
 Without public option(Source: Rasmussen Reports)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


