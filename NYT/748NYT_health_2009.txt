                   Copyright 2009 The New York Times Company


                             748 of 2021 DOCUMENTS


                               The New York Times

                             October 4, 2009 Sunday
                              Late Edition - Final

A Call to Change the N.C.A.A.'s Direction

BYLINE: By WILLIAM C. RHODEN.

E-mail: wcr@nytimes.com

SECTION: Section SP; Column 0; Sports Desk; SPORTS OF THE TIMES; Pg. 5

LENGTH: 884 words


The death of Myles Brand last month silenced a strong voice of academic reform
in intercollegiate athletics. It also created a void and myriad questions about
the direction of the N.C.A.A., which he served as its president for the past six
years.

The N.C.A.A. faces ethical challenges like raging commercialism and escalating
salaries for star coaches, and the subsequent tension between athletics and
academia. And it must decide whether to recommit to Title IX, and roll back the
length of playing seasons, especially in so-called minor sports.

''We have to do what Obama's trying to do in health care reform,'' Donna
Shalala, the University of Miami president, said during a recent interview.

In fact, the task of reforming the N.C.A.A. bureaucracy, with all of its vested
interests, may be more daunting than reforming health care because the emotions
of alumni and boosters are involved.

The first step is determining whether Brand's successor will be another
university president. Brand, the president of Indiana University when he was
selected in 2003, was the first to lead the N.C.A.A.

Before then, it was run by the athletic wing, from Walter Byers starting in
1951, to Dick Schultz in 1988, to Cedric Dempsey in 1993. Schultz and Dempsey
were former athletic directors. They talked about academic reform, but they were
not as passionate as Brand and did not understand that they had to force the
issue by imposing harsh penalties.

''I think that having a president brings a level of respect from the board,''
said Mary Sue Coleman, the University of Michigan president, referring to the
N.C.A.A. executive board, which is made up of college presidents. ''It's knowing
how to run a university, how to run an athletic program in the context of a
university.''

Where will the next president come from? Although N.C.A.A. Division III has the
most members, Coleman said the new president should come from a Division I
institution, as Brand did.

But Scott Cowen, the Tulane University president, said Brand was a relative
N.C.A.A. outsider.

''He wasn't part of a lot of the old battles and fights that went on,'' Cowen
said. ''That was an advantage he had that some university presidents will not
have because they've been too involved for too long with the N.C.A.A. Some of
them may be part of the problems the N.C.A.A. has. A fresh face and a fresh set
of thoughts are always useful.''

Cowen has been mentioned as a possible successor to Brand, as have Coleman,
Shalala, Cowen and Michael Adams, the University of Georgia president. During
separate phone interviews, each denied any interest in the position, now held on
an interim basis by Jim Isch, an N.C.A.A. senior vice president.

''I am deeply engaged at the University of Michigan,'' Coleman said.

Shalala said: ''At the end of the day, the reason I'm in higher education is
because I love the kids. Helping the kids from a distance is not what I want to
do. I want to walk across campus, I want to teach a class. It's just not me.''

Adams said: ''I love the University of Georgia. It's where I fit best. I feel
pretty strongly this is where I'll be.''

Cowen said, ''I don't have the temperament for that job.''

Yet Cowen or Adams could be just the person to shake the N.C.A.A. to its core.

For all the talk about priorities, the great conundrum facing the organization
is what to do with the  teams that  compose the Football Bowl Subdivision, which
competes for the Bowl Championship Series title. The N.C.A.A. administers 88
championships in 23 sports. But the B.C.S. operates its bowl games independently
of the N.C.A.A.

''I've always found that unconscionable,'' Cowen said.

Unlike a large number of their colleagues, Adams and Cowen want the N.C.A.A. to
take back Division I football. It effectively lost control in 1984, when the
Supreme Court ruled that the N.C.A.A.'s existing television contract violated
antitrust laws.

The issue is much larger than a debate about a national football playoff. It
gets to the essential question of whether the tail is wagging the dog in
football and basketball, the two biggest, most commercialized college sports.
The commissioners of the B.C.S. conferences, as well as bowl organizers and some
university presidents and boosters, refuse to cede control of a billion-dollar
football enterprise to the N.C.A.A. This is the N.C.A.A.'s equivalent of health
care reform, just as daunting and emotional and, in some ways, more of a mission
impossible.

''I don't think they can take it back,'' Shalala said, referring to the N.C.A.A.
''I do not think the conferences are going to give it back. I don't see that in
my lifetime.''

Will the new N.C.A.A. president have the courage and political clout to begin
the process of taking it back?

More than that, will members of the N.C.A.A. executive board even consider
someone who hints at wanting to begin a serious discussion about reclaiming
football?

''It would be interesting if we got a head of the N.C.A.A. who said, 'Listen, we
need to put this on the table and we need to have a conversation about it,' ''
Cowen said. ''I would welcome that conversation. I think it's unlikely that will
happen, but I would welcome it.''

In an ever-expanding college sports industry, there continue to be more
questions than answers.

URL: http://www.nytimes.com

LOAD-DATE: October 4, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: The University of Georgia president, Michael Adams, left, and
Tulane's president, Scott Cowen, have been mentioned as possible candidates to
be the N.C.A.A. president.(PHOTOGRAPHS, LEFT, MARK HUMPHREY/ASSOCIATED PRESS
 CHERYL GERBER FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


