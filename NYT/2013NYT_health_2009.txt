                   Copyright 2010 The New York Times Company


                             2013 of 2021 DOCUMENTS


                               The New York Times

                              May 30, 2010 Sunday
                              Late Edition - Final

The Trans-Atlantic Crisis

SECTION: Section WK; Column 0; Editorial Desk; EDITORIAL; Pg. 7

LENGTH: 983 words


America's Uncertain Recovery

The stock market gyrated wildly last week, soaring when China denied reports it
was about to forsake the euro, then retreating on bad news about Spain's
creditworthiness. Contagion from Europe is obviously still a threat to the
United States. But Europe's woes are only one of several risks still facing the
American economy. And those risks are made all the worse by an increasingly
incoherent response from Congress.

The biggest danger is high and intractable unemployment, currently 9.9 percent,
or 15.3 million people, nearly half of whom have been jobless for more than six
months. Even if last month's strong job growth is sustained -- a big if -- it
would take five years to get back to a more tolerable unemployment rate of 5
percent.

At the same time, state budgets are collapsing, with governments facing a
collective budget hole of $112 billion in the fiscal year that starts for most
of them on July 1. Closing the gaps will mean tax increases and spending cuts,
which will provoke more layoffs, both among public employees and businesses that
contract with state and local governments.

Adding to the economic pressure on families, more house price declines are
likely, the result of millions of foreclosures and other distressed homes sales.
Adding to the pressure on small businesses, credit will continue to be tight, as
potentially hundreds of small banks fail, most of them weakened by souring loans
on commercial real estate. The Federal Deposit Insurance Corporation recently
reported that 775 banks were on its watch list, the most since mid-1993.

Any one of those issues would be worrying. Collectively, they constitute an
emergency. Yet, the Senate left town on Friday for a week's break without
passing a bill to extend expiring unemployment benefits; senators said they
would get to it later. The House voted for the bill, but stripped out provisions
for a health-insurance subsidy for jobless workers and for more aid to states.

Congress's reluctance to pass the package of measures, which costs about $70
billion, is grounded in familiar arguments that the nation must act now to cut
the budget deficit. In fact, withholding support now -- when the recovery is
still fragile and needs are still great -- runs the risk of condemning the
economy to a long, hard slog of subpar growth, barely indistinguishable, in
effect, from recession itself. That would be worse for the nation's finances
than upfront spending.

It is up to the Obama administration and Democratic Congressional leaders to
make that case, ideally in tandem with a credible plan for reducing the deficit
in the longer term. Health care reform was a down payment on controlling costs.
Further serious discussion of the deficit has been put off until after the
midterm election, when the president's budget panel is due to report.

Even then, how many politicians will find the courage to deal frankly with the
tax increases that deficit reduction will require? Economic recovery is not a
given. Neither is effective political leadership. We will not have one without
the other.

Europe's Endangered Banks

So far, Europe's troubles have not hurt American banks, which own little debt
from beleaguered Greece, Portugal or Spain. But the American and European
financial sectors are entwined, and it is unclear whether American banks are
prepared for what could happen.

Doubts remain about the solvency of the weaker European countries, despite the
plan by the European Union and the International Monetary Fund to make up to $1
trillion available to indebted economies.

The plan has brought needed respite to financial markets. Crucially, the
European Central Bank threw a lifeline to banks in the euro area, promising to
buy tens of billions worth of weak-country debt from them, a small portion. The
German Bundestag overcame public resistance and approved the rescue package.

But it has a fundamental shortcoming: it relies on deep budget cuts from
countries that are in a recession or teetering on the edge. Several have weak
governments that may not be able to carry through the prescribed fixes. Even if
they do, the budget cuts are likely to make them even weaker.

Consider Ireland, which was quick to address the financial crisis. Last year,
the Irish government slashed spending, cutting the pay of public-sector workers.
It raised taxes. The Irish economy contracted 7 percent,  and the budget deficit
widened to 14 percent of its gross domestic product. This year, the deficit is
expected to reach 12 percent of G.D.P.

Greece has now pledged to slash its budget deficit by more than 10 percentage
points of G.D.P. by 2014. Spain says it will cut its deficit to 6 percent of
G.D.P. next year from 11.2 percent in 2009. Italy announced cuts worth $30
billion. Portugal and even Germany have also announced budget cuts.

This is a recipe for economic stagnation. It also may not avert a debt
rescheduling by some of the weaker European countries, which would force
European banks to take a cut on their holdings. Sitting on slim cushions of
capital reserves, European banks are in no shape for a sharp drop in the value
of their assets.

It would be best to recognize that debt restructuring is inevitable. That would
allow weaker countries to start growing again more quickly, but to withstand it
European banks must be made to raise more capital. German banks' capital amounts
to less than 5 percent of their total assets, compared with 12.4 percent in the
United States. And they are sitting on $650 billion in debt from the four most
stricken countries.

American banks are more stable and better capitalized but must remain alert.
American banks ended 2009 with $1.2 trillion worth of total European debt. That
is about par with the amount of subprime residential mortgage debt outstanding
in 2008. It would be foolhardy to assume this problem is far away.

URL: http://www.nytimes.com

LOAD-DATE: May 30, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


