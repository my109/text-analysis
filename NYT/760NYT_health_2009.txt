                   Copyright 2009 The New York Times Company


                             760 of 2021 DOCUMENTS


                               The New York Times

                            October 6, 2009 Tuesday
                              Late Edition - Final

Study Finds Calorie Postings in Restaurants Don't Change Eating Habits

BYLINE: By ANEMONA HARTOCOLLIS; Jonathan Allen contributed reporting.

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 26

LENGTH: 844 words


A study of New York City's pioneering law on posting calories in restaurant
chains suggests that when it comes to deciding what to order, people's stomachs
are more powerful than their brains.

The study, by several professors at New York University and Yale, tracked
customers at four fast-food chains -- McDonald's, Wendy's, Burger King and
Kentucky Fried Chicken -- in  poor  neighborhoods of New York City where there
are high rates of obesity.

It found that about half the customers noticed the calorie counts, which were
prominently posted on menu boards. About 28 percent of those who noticed them
said the information had influenced their ordering, and 9 out of 10 of those
said they had made healthier choices as a result.

But when the researchers checked receipts afterward, they found that people had,
in fact, ordered slightly more calories than the typical customer had before the
labeling law went into effect, in July 2008.

The findings, to be published Tuesday in the online version of the journal
Health Affairs come amid the spreading popularity of calorie-counting proposals
as a way to improve public health across the country.

''I think it does show us that labels are not enough,'' Brian Elbel, an
assistant professor at the New York University School of Medicine and the lead
author of the study, said in an interview.

New York City was the first place in the country to require calorie posting,
making it a test case for other jurisdictions. Since then, California, Seattle
and other places have instituted similar rules.

Calorie posting has even entered the national health care reform debate, with a
proposal in the Senate to require calorie counts on menus and menu boards in
chain restaurants.

This study focused primarily on poor black and Hispanic fast-food customers in
the South Bronx, central Brooklyn, Harlem, Washington Heights and the Rockaways
in Queens, and used a similar population in Newark,  which does not have a
calorie posting law, as a control group. The locations were chosen because of a
high proportion of obesity and diabetes among poor minority populations.

The researchers collected about 1,100 receipts,  two weeks before the calorie
posting law took effect and four weeks after. Customers were paid $2 each to
hand over their receipts.

For customers in New York City, orders had a mean of 846 calories after the
labeling law took effect. Before the law took effect, it was 825 calories.  In
Newark, customers ordered about 825 calories before and after.

On Monday, customers at the McDonald's on 125th Street near St. Nicholas Avenue
provided anecdotal support for the  findings.

William Mitchell, from Rosedale, Queens, who was in Harlem for a job interview,
ordered two cheeseburgers, about 600 calories total, for $2.

When asked if he had checked the calories, he said: ''It's just cheap, so I buy
it. I'm looking for the cheapest meal I can.''

Tameika Coates, 28, who works in the gift shop at St. Patrick's Cathedral,
ordered a Big Mac, 540 calories, with a large fries, 500 calories, and a large
Sprite, 310 calories.

''I don't really care too much,''  Ms. Coates said. ''I know I shouldn't, 'cause
I'm too big already,'' she added with a laugh.

April Matos, a 24-year-old family specialist, bought her 3-year-old son, Amari,
a Happy Meal with chicken McNuggets, along with a Snack Wrap for herself. She
said with a shrug that she had no interest in counting calories.  ''Life is
short,'' she said, adding that she used to be a light eater. ''I started eating
everything now I'm pregnant.''

Nutrition and public health experts said the findings showed how hard it was to
change behavior, but they said it was not a reason to abandon calorie posting.

One advocate of calorie posting suggested that low-income people were more
interested in price than calories.

''Nutrition is not the top concern of low-income people, who are probably the
least amenable to calorie labeling,'' said Michael F. Jacobson, executive
director of the Center for Science in the Public Interest, a nonprofit health
advocacy group in Washington.

New York City health officials said that because the study was conducted
immediately after the law took effect, it might not have captured changes in
people's behavior that have taken hold more gradually.

A year ago, officials pointed out, the city began an advertising campaign
telling subway riders that most adults should eat about 2,000 calories a day,
which might put the calorie counts in context.

While the N.Y.U. study examined 1,100 restaurant receipts, the city is doing its
own analysis of 12,000 restaurant receipts, which it plans to release in a few
months, said Cathy Nonas, director of nutrition programs for the City Department
of Health and Mental Hygiene.

People sometimes confuse intentions with actions, said Marie Roth, a registered
dietitian with Blythedale Children's Hospital in Valhalla, N.Y.

''Just by contemplating healthier choices, they feel like they could have done
it and maybe they will the next time,'' Ms. Roth said.

URL: http://www.nytimes.com

LOAD-DATE: October 6, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: April Matos, 24, bought a Happy Meal at a McDonald's for her
3-year-old son, Amari, and a Snack Wrap for herself. ''Life is short,'' she
said. ''I started eating everything now I'm pregnant.''(PHOTOGRAPH BY HIROKO
MASUIKE FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


