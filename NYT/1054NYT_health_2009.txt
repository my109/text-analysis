                   Copyright 2009 The New York Times Company


                             1054 of 2021 DOCUMENTS


                               The New York Times

                            November 22, 2009 Sunday
                              Late Edition - Final

Leveraging the Obama Brand

BYLINE: By MATT BAI.

Matt Bai writes on national politics for the magazine.

SECTION: Section MM; Column 0; Magazine Desk; THE WAY WE LIVE NOW; Pg. 9

LENGTH: 1126 words


Earlier this month, almost a year from the day when Barack Obama rode the wave
of history into Grant Park, he had one of those weeks that makes his presidency
seem, at times, so confounding. First Obama endured an electoral embarrassment,
watching his party lose off-year gubernatorial elections in New Jersey and
Virginia, in part because many of the voters he had so successfully engaged in
his presidential campaign, particularly younger voters, stayed home and made
popcorn for ''Dancing With the Stars'' instead. The president had been exposed,
the pundits solemnly declared, as having no ''coattails,'' which are something
you really need, they said, if you want congressmen and senators to go along
with you on hard votes. Then, just five days later, as Obama's manifest
political weakness was still being dissected on cable TV, the president stood in
the Rose Garden and hailed the passage in the House of Representatives of the
most comprehensive health care bill in the nation's history -- a bill he had
helped pass, narrowly but solidly, by making a rare weekend visit to the Capitol
to lobby for it personally. It was, as Obama acknowledged, just a first step,
but it was a step no other president had managed to scale, and it seemed to
confuse this issue of how powerful a president Obama really is.

If Obama has no coattails, then how was he able to pressure nervous lawmakers
into making such a perilous vote? And if he had enough influence to get health
care reform done, then why were his coattails missing?

This whole conversation about coattails must baffle most voters under 40, who
have no visual sense of what a coattail actually is, unless they've played a lot
of Monopoly or overdressed for a prom. The term dates back to Abraham Lincoln,
who accused his adversaries of hiding under the dangling coattails of a
general's uniform. The modern idea is that candidates can grab onto the coat of
a successful president and glide to victory. This makes some sense when it is
used to refer to the years when a presidential candidate is actually on the
ballot, the oft-cited demonstration being 1980, when Ronald Reagan's power at
the polls helped unseat nine incumbent Democratic senators. But the coattails
metaphor has now apparently been extended to refer to the basic transferable
appeal of a president, whether he himself is up for election or not. In other
words, if Obama is wearing sturdy coattails, then the same New Jersey voters who
waited in line last year to elect an exciting young African-American president
should have been willing to make the same sacrifice for an unpopular white
governor, just because Obama himself told them they should.

In fact, though, some of the most successful navigators of the modern culture --
in advertising, sports, entertainment -- have come to realize that Americans are
increasingly inured to the suggestive power of all celebrity, political or
otherwise. A few years ago, for instance, when fans of the New York Yankees were
relentlessly booing Alex Rodriguez, Derek Jeter, the team's beloved captain,
refused to intervene. ''The only thing I'm not going to do is tell the fans what
to do,'' Jeter explained. ''I don't think it's my job to tell fans to boo or not
to boo.'' Columnists accused Jeter of shirking his responsibility to a teammate,
but in fact he had displayed a remarkably shrewd appreciation for the limits of
modern idolatry. Jeter understood that his relationship with the fans wasn't a
transferable commodity that he could dole out to others as he wished.

Like Jeter, the president probably should have known -- and maybe even did,
judging from the almost dutiful way he campaigned -- that his own exalted
stature wouldn't easily be conferred on others. Obama seems to understand that
Americans in the satellite age tend to see the president more as a
self-contained personality than as the boss of a top-down party organization,
the kind of machine-era figurehead who once presided over conventions full of
straw hats and placards. We care about his plans for health care and economic
growth, and maybe even about the kind of movies he watches or whether his
marriage is holding up under the strain, but we don't necessarily see much
connection between his agenda and the fight over local toll rates.

In fact, there's no evidence that Obama's failure to influence the 2009
elections will mean that he can't get his legislation passed -- or that he won't
have an impact on the more-important national races in 2010. That's because,
while Obama's personal brand may not be directly transferable to other
candidates, it can still be leveraged into public support for his agenda.
Increasingly, presidents of the television era have resolved to take their cases
directly to the American people in order to pressure Congress into acting,
rather than cutting parochial deals, in the style of a Lyndon Johnson, to round
up votes. Obama is better positioned to do this than any president since Reagan.
He has outsider credibility, a rare gift for explanation and a comfort level
with all manner of pop culture. (Did the leader of the free world actually tape
a promo for George Lopez's new late-night show?) Unlike his predecessors, Obama
can use the Web to reach a new generation of voters and, at least theoretically,
to mobilize an expansive network of campaign supporters. Those vulnerable
Democrats who risked voting for Obama's health care bill in the House probably
did so not because they hope he will stand next to them on a stage for a few
minutes next fall but because they are betting that Obama -- whose approval
rating runs more than 30 points above the Democratic Congress -- will ultimately
persuade their constituents that it was the right vote to make.

The real problem for Obama is that he hasn't really done that yet. The White
House has, to this point, mostly directed its power to persuade toward committee
chairmen and other lawmakers, turning its attention to a broader public campaign
only sporadically, when pressed to defend its agenda by tea-party protesters and
fading poll numbers. As a result, perhaps, public support for each of Obama's
signature initiatives, including health care, remains lower than support for the
president himself. In other words, without a sustained sales pitch to the
voters, Obama has yet to convert his personal good will with the electorate into
corresponding enthusiasm for his agenda. And as anxious congressmen study poll
numbers and look toward their own re-election campaigns next year, that's bound
to become a more pressing problem for the White House. Lawmakers may yet be
persuaded to enact the president's ambitious agenda, coattails or not, but only
if he gives them something else to which they can cling.

URL: http://www.nytimes.com

LOAD-DATE: December 11, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY DAMON WINTER) DRAWING (DRAWING BY JEZ BURROWS)
 GRAPHIC: 27% of New Jersey voters who say they approve of Obama's job
performance didn't vote to re-elect the Democratic governor. (DATA SOURCE (TOP
LEFT) : EDISON RESEARCH EXIT POLLS)

PUBLICATION-TYPE: Newspaper


