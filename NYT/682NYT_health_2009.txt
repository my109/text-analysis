                   Copyright 2009 The New York Times Company


                             682 of 2021 DOCUMENTS


                               The New York Times

                           September 25, 2009 Friday
                              Late Edition - Final

Public Plan to Be Pushed

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 18

LENGTH: 246 words


Liberal Senate Democrats will push to add a government-run insurance program to
the health care bill in the Finance Committee on Friday, setting off a
potentially explosive debate with Republicans who view the idea as a step toward
''socialized medicine.''

Senators John D. Rockefeller IV, right, Democrat of West Virginia,  and Charles
E. Schumer, Democrat of New York, said Thursday  that they would propose the
so-called public option because efforts to revamp the health system and cover
the uninsured would fail without it.

''The best way to control costs, the best way to make availability easier is to
have competition to the insurance industry which does not have to make a
profit,'' Mr. Rockefeller said in a conference call with reporters.

Centrist Senate Democrats long ago rejected  the idea, knowing that it would be
a deal-breaker with Republicans, and are likely to defeat the proposal on
Friday.

Even the one Republican still willing to vote for the bill, Senator Olympia J.
Snowe of Maine, has said she would support a public plan only as a fallback in
areas where the legislation fails to provide affordable insurance.

Mr. Schumer said he would push the idea even if the finance panel rejected it.

Even if Congress created a public plan, it would not be available to the
majority of Americans who now have health insurance sponsored by their
employers, because the legislation would force them to keep their existing
coverage. DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: September 25, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


