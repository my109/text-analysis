                   Copyright 2009 The New York Times Company


                              24 of 2021 DOCUMENTS


                               The New York Times

                              June 9, 2009 Tuesday
                              Late Edition - Final

Spending Disparities Set Off a Fight in the Effort to Overhaul Health Care

BYLINE: By ROBERT PEAR; Sheryl Gay Stolberg contributed reporting.

SECTION: Section A; Column 0; National Desk; Pg. 17

LENGTH: 1109 words

DATELINE: WASHINGTON


President Obama recently summoned aides to the Oval Office to discuss a magazine
article investigating why the border town of McAllen, Tex., was the country's
most expensive place for health care. The article became required reading in the
White House, with Mr. Obama even citing it at a meeting last week with two dozen
Democratic senators.

''He came into the meeting with that article having affected his thinking
dramatically,'' said Senator Ron Wyden, Democrat of Oregon. ''He, in effect,
took that article and put it in front of a big group of senators and said, 'This
is what we've got to fix.' ''

As part of the larger effort to overhaul health care, lawmakers are trying to
address the problem that intrigues Mr. Obama so much -- the huge geographic
variations in Medicare spending per beneficiary. Two decades of research
suggests that the higher spending does not produce better results for patients
but may be evidence of inefficiency.

Members of Congress are seriously considering proposals to rein in the growth of
health spending by taking tens of billions of dollars of Medicare money away
from doctors and hospitals in high-cost areas and using it to help cover the
uninsured or treat patients in lower-cost regions.

Those proposals have alarmed lawmakers from higher-cost states like Florida,
Massachusetts, New Jersey and New York. But they have won tentative support
among some lawmakers from Iowa, Minnesota, Montana, North Dakota, Oregon and
Washington, who say their states have long been shortchanged by Medicare.

Nationally, according to the Dartmouth Atlas of Health Care, Medicare spent an
average of $8,304 per beneficiary in 2006. Among states, New York was tops, at
$9,564, and Hawaii was lowest, at $5,311.

Researchers at Dartmouth Medical School have also found wide variations within
states and among cities. Medicare spent $16,351 per beneficiary in Miami in
2006, almost twice the average of $8,331 in San Francisco, they said.

The Senate Finance Committee recently suggested that one way to pay for health
care overhaul would be to reduce geographic variations by cutting or capping
Medicare payments in ''areas where per-beneficiary spending is above a certain
threshold, compared with the national average.''

Another proposal would spare health care providers in low-spending, efficient
areas from across-the-board cuts in Medicare payments.

The committee chairman, Senator Max Baucus, Democrat of Montana, and the panel's
senior Republican, Senator Charles E. Grassley of Iowa, are from lower-spending
states.

But the proposals are not just pork-barrel politics. They are based on the
research by Dartmouth experts who have documented wide geographic variations in
health spending. The research has become phenomenally influential on Capitol
Hill since it was popularized by Peter R. Orszag, as director of the
Congressional Budget Office and then as President Obama's budget director.

Aides said Mr. Obama had been intrigued by regional variations in health
spending since before his inauguration. The topic came up at a meeting with Mr.
Orszag in Chicago late last year.

The magazine article, by Dr. Atul Gawande in the June 1 issue of The New Yorker,
said a major cause of the high costs in McAllen was ''overuse of medical care.''

Dr. Elliott S. Fisher, one of the Dartmouth researchers, diagnosed the problem
this way: ''Medicare beneficiaries in higher spending regions are hospitalized
more frequently, are referred to specialists more often and have a much smaller
proportion of their visits to primary care physicians.''

In his blog last month, Mr. Orszag wrote, ''The higher-cost areas and hospitals
don't generate better outcomes than the lower-cost ones.''

But other researchers and politicians are not so sure. They say it would be a
mistake to cut or cap Medicare payments without knowing why spending in some
places far exceeds the national average.

''There is too much uncertainty about the Dartmouth study to use it as a basis
for public policy,'' said Senator John Kerry, Democrat of Massachusetts.
''Researchers can't explain why some areas of the country spend more on health
care than others. There are many reasons spending could vary: higher costs of
living, sicker people or more teaching hospitals.''

''States like Massachusetts are concentrated centers of medical innovation where
cutting-edge treatments are tested and some of the nation's finest doctors are
trained,'' Mr. Kerry added. ''This work might cost a little more, but it
benefits the entire country.''

Madeline H. Otto, an aide to Senator Bill Nelson, Democrat of Florida, said he
was ''adamantly opposed'' to the proposed cuts in higher-spending areas because
the cuts did not distinguish between necessary and unnecessary care.

Mr. Orszag says health spending could be reduced by as much as 30 percent, or
$700 billion a year, without compromising the quality of care, if more doctors
and hospitals practiced like those in low-cost areas. The supply of hospitals,
medical specialists and high-tech equipment ''appears to generate its own
demand,'' Mr. Orszag has said.

A Democrat from a low-spending state said critics were trying to ''blow holes in
the Dartmouth analysis.''

Dr. Michael L. Langberg, senior vice president of Cedars-Sinai Medical Center in
Los Angeles, is among the critics.

''The statement that Medicare costs can be cut by 30 percent has been repeated
so many times that it has come to be viewed as a proven fact by some,'' Dr.
Langberg said in a recent letter to the Senate Finance Committee. ''It is not a
fact. It is a gross oversimplification of an untested theory.''

Dr. Langberg endorsed the goal of covering the uninsured, but said, ''We do not
believe that rushing to make large cuts in Medicare payments to hospitals is the
right way to fund that coverage.'' The Dartmouth team has cited Cedars-Sinai as
having very high Medicare spending per beneficiary.

Research by Dr. Robert A. Berenson and Jack Hadley of the Urban Institute
suggests that much of the geographic variation in health spending can be
explained by differences in ''individual characteristics, especially patients'
underlying health status and a range of socio-economic factors, including
income.''

''Some patients may benefit from higher spending,'' said Mr. Hadley, who is also
a professor at George Mason University in Virginia. ''They could be adversely
affected if they live in geographic areas where payments are cut.''

Dr. Berenson, who was a Medicare official in the Clinton administration, said,
''There remains too much uncertainty about the Dartmouth findings to ground
public policy on them.''

URL: http://www.nytimes.com

LOAD-DATE: June 9, 2009

LANGUAGE: ENGLISH

GRAPHIC: MAP/CHART: Regional Variations in Medicare Costs: Spending per
beneficiary in each state, 2006. New York ranks highest in Medicare spending per
beneficiary, at $9,564, but there are wide variations within the state. Such
disparities are also seen in large, populous states like Texas and
Florida.(Source: The Dartmouth Atlas of Health Care)

PUBLICATION-TYPE: Newspaper


