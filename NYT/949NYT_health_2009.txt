                   Copyright 2009 The New York Times Company


                             949 of 2021 DOCUMENTS


                               The New York Times

                           November 7, 2009 Saturday
                              Late Edition - Final

Top Democrats Push for Votes Before Health Debate

BYLINE: By DAVID M. HERSZENHORN and ROBERT PEAR; Carl Hulse contributed
reporting.

SECTION: Section A; Column 0; National Desk; Pg. 13

LENGTH: 1002 words

DATELINE: WASHINGTON


House Democrats put the finishing touches on their health care legislation on
Friday, but failed to bridge an internal divide over the politically treacherous
issue of insurance coverage for abortions, as party leaders and the White House
pushed to lock in votes on the eve of a historic floor debate.

Democratic leaders said they were confident that they would have the 218 votes
needed to pass the bill, but  they continued to scramble behind the scenes. A
planned visit to the Capitol by President Obama was postponed until  Saturday so
that a face-to-face appeal would have greater impact on wavering lawmakers just
before the vote.

Vice President Joseph R. Biden Jr., cabinet secretaries and other senior
officials, including the White House chief of staff, Rahm Emanuel, worked the
phones, imploring undecided Democrats to vote yes on a bill that they said would
be remembered as a landmark achievement.

''What I explained to the president is my district is a difficult district on
this,'' said Representative Jason Altmire, Democrat of Pennsylvania, who
received  calls from Mr. Obama, Mr. Emanuel and two cabinet secretaries. Mr.
Altmire said he remained undecided.  He quoted Mr. Obama as saying, ''The only
two options are failure or move the ball down the field.''

As rank-and-file members of both parties girded for hours of boisterous floor
debate on Saturday, lines began to harden and attention was focused on Democrats
coming out in opposition to the bill, including several centrists from rural
districts.

With Democrats controlling 258 of 435 House seats as of Friday, they can afford
only 40 of their members breaking ranks and still win approval for the health
measure.

As of Friday evening, Democratic officials counted just more than  205 certain
votes and were hopeful that a successful resolution of the abortion and
immigration issues would produce the final commitments to ensure passage of the
bill.

Speaker Nancy Pelosi was devoting significant individual attention to holdouts.
Securing the final supporters was proving difficult, officials said.

''This is a much heavier lift than we thought it would be,'' said a top
Democratic official who did not want to be named when talking about the internal
lobbying efforts.

Representative Stephanie Herseth Sandlin of South Dakota, an influential leader
of the fiscally conservative Blue Dog Democrats, announced her opposition to the
bill.

''It is not the right answer for South Dakota because it could unintentionally
threaten existing access to health care in our state, and it does not include
sufficient cost-containment and deficit reduction measures,'' Ms. Herseth
Sandlin said in a statement.

But others said their concerns had been addressed by party leaders eager to
build support for the bill that Mr. Obama has declared his top domestic
priority.

Representative Jared Polis, a freshman Democrat from Colorado who raised
concerns this summer about the potential impact on small businesses, said he
would back the bill.

''It has become a much more favorable bill for small businesses,'' Mr. Polis
said, ''and small businesses and self-employed people really have the most to
benefit from increased choice and competition.''

The House majority leader, Representative Steny H. Hoyer, Democrat of Maryland,
said the vote, tentatively scheduled for Saturday evening, could end up taking
place on Sunday or even early next week depending on Republican tactics.

''We will be successful in the next two or three days,'' Mr. Hoyer said.

Not one of the 177 House Republicans is expected to support the bill. And as
they prepared for a showdown, Republican leaders said they were less inclined to
mount a full-fledged guerrilla war on the floor to slow the bill than simply to
make their case against it and let Democrats live with the potentially sour
political consequences.

But Republican leaders also acknowledged that they could not necessarily control
all of their colleagues and that combative parliamentary tactics were still
possible.

After weeks of intraparty wrangling, Democratic leaders offered additional
concessions to abortion opponents on Friday. Failing to reach a compromise
during late-night negotiations between foes and supporters of abortion rights,
Democrats said party leaders would allow a vote on an amendment that would bar
the use of government subsidies to buy a health plan that covers abortion.

Democrats who support abortion rights said they were furious, but it seemed
unlikely that they could prevent the amendment from being approved, especially
if many Republicans joined Democrats who oppose abortion to support it.  And
party leaders seemed to be calculating that supporters of abortion rights,
despite their anger, would vote for the health care bill.

Ms.  Pelosi, who is a strong supporter of abortion rights, had no choice but to
make the concessions because Democrats who oppose abortion seemed to control
enough votes to endanger the bill.

Representative Bart Stupak, Democrat of Michigan and the chief sponsor of the
anti-abortion amendment, said its purpose was to strengthen existing federal
law.

''It does one very simple thing:  it applies current law,  the Hyde Amendment,
in barring federal funding for abortion,'' Mr. Stupak said at a late-night
meeting of the House Rules Committee. ''We are not writing a new federal
abortion policy.''

Republicans cited a  report on Friday that unemployment had risen above 10
percent in October as evidence that this was the wrong time to pass the health
care legislation.

''The speaker's bill includes job-killing taxes and mandates that will hurt
small businesses,'' the Republican leader, Representative John A. Boehner of
Ohio, said at a news conference. ''And for the sake of our families and small
businesses, this job-killing bill needs to be defeated.''

The House Rules Committee, which is controlled by the Democrats, worked
throughout Friday to set the parameters of Saturday's floor proceedings.

URL: http://www.nytimes.com

LOAD-DATE: November 7, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Joe Wilson and other Republicans expressed
concerns about voter verification in the House's health bill on
Friday.(PHOTOGRAPH BY LUKE SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


