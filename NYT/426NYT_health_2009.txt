                   Copyright 2009 The New York Times Company


                             426 of 2021 DOCUMENTS


                               The New York Times

                            August 27, 2009 Thursday
                              Correction Appended
                              Late Edition - Final

Many the World Over, Whether Exalted or Everyday, Reminisce

BYLINE: By JAMES BARRON; Reporting was contributed by Ethan Bronner from
Jerusalem; Ariana Green from Hyannis, Mass.; Brian Stelter from New York; and
Jeff Zeleny from Oak Bluffs, Mass.

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 1311 words


Faythe Collins remembered meeting Senator Edward M. Kennedy as a child and
later, as a worried mother whose children had been detained in Brazil, calling
his office and leaving a message.

''I expected a secretary to maybe call me back,'' she said.

But the voice on the phone was that of the senator himself.

Ms. Collins, who went to the John F. Kennedy Museum in Hyannis, Mass., on
Wednesday to sign one of three condolence books put out hours after the
senator's death, recalled his telling her that ''he couldn't pull any strings''
to hasten her children's return, but suggesting options that she could try on
her own and calling back several times to check on how things were going.

''The information he gave me helped me get my kids back to this country,'' said
Ms. Collins, 45, who grew up in Hyannis and still lives not far from the Kennedy
compound in Hyannis Port. ''It's so rare that politicians take the time to help
ordinary people.''

As word of Mr. Kennedy's death spread Wednesday, memories of him were shared by
people from across the globe and all walks of life, from the truck driver from
Waltham, Mass., who declared that the senator had ''fought for the average
people instead of special interests,'' to the president of the United States,
who called him a ''singular figure in American history.''

But while his allies spoke of Mr. Kennedy's idealism and his steadfast
commitment to democratic ideals and the ideals of the Democratic Party, his
conservative adversaries on cable television and talk radio complained that
Democrats would try to capitalize on his death by rallying support for a health
care overhaul that the senator had hoped would be his final legislative triumph.

Rush Limbaugh countered the Senate majority leader, Harry Reid of Nevada, who
called Mr. Kennedy a ''lion of the Senate,'' by saying, ''We were his prey.''

Some remembered him as the Kennedy who was elected to the Senate when one of his
brothers was president and another attorney general, and who lived through their
assassinations, his own failings at Chappaquiddick and a presidential bid that
went nowhere in 1980, but who remained a senator in the course of 10
presidencies and had a hand in shaping laws that touched on education, civil
rights, Medicare, Social Security and much more.

''I'm a Republican, but I respected him for what he tried to do,'' said Dudley
Thomas, a retired computer manager who lives near the Kennedy compound. ''He was
always fighting for what he thought was right.''

Others stood on a corner near the compound where well-wishers had placed flowers
and keepsakes and explained that their reminiscences had nothing to do with
politics.

After spending two hours on the road getting there, Danielle Nunes of Billerica,
Mass., said: ''We came because I remember when I was a girl and my parents had a
boat, we passed Ted and I said, 'Hi, Ted!' My mother said, 'It's Mr. Kennedy,'
and he said: 'Don't worry about it. Call me Ted.' ''

Some of the most passionate tributes came from his colleagues in the Senate,
where he served for 46 years.

First among them was Mr. Obama, who was the junior senator from Illinois when he
decided to run for president, and whose candidacy Mr. Kennedy championed with an
endorsement that provided a boost against Hillary Rodham Clinton as the height
of the Democratic primary season last year.

Mr. Obama said Wednesday that he was ''heartbroken'' at Mr. Kennedy's death and
had ''profited as president'' from his counsel.

''His ideas and ideals,'' the president said, ''are stamped on scores of laws
and reflected in millions of lives -- in seniors who know new dignity, in
families that know new opportunity, in children who know education's promise and
in all those who can pursue their dream in an America that is more equal and
more just, including myself.''

The White House press secretary, Robert Gibbs, said Mr. Obama had expressed his
condolences in a telephone call to Mr. Kennedy's wife, Victoria. There had been
speculation that Mr. Obama would visit Mr. Kennedy during the president's
vacation on Martha's Vineyard this week, but aides said that the senator's
condition had been too serious and that a presidential visit would have been too
disruptive.

Some other Senate colleagues echoed Senator John Kerry of Massachusetts, who
called Mr. Kennedy an ''irrepressible, larger-than-life presence.'' Senator John
McCain of Arizona, who as the Republican nominee lost to Mr. Obama in the
presidential election last year, said that Mr. Kennedy ''had become
irreplaceable in the institution he loved'' and that there would be an
''emptiness'' in the Senate without him.

There were tributes from foreign leaders and from the United Nations secretary
general, Ban Ki-moon, who said Mr. Kennedy's had been ''a voice for those who
would otherwise go unheard.''

Natan Sharansky, the Israeli politician and former Soviet political prisoner,
called him ''one of the towering figures in human rights.'' When Mr. Sharansky
was released from a Soviet prison in 1986, he said, it was Mr. Kennedy who
called his wife, in Israel, to let her know.

Mr. Sharansky recalled a time when American officials visiting Moscow had
worried about upsetting the Soviet government and endangering democracy
activists there by going to see them. But in the spring of 1974, Mr. Kennedy
ended official talks there and headed to the home of Alexander Lerner, a
well-known dissident.

''He was the first to dare to do it, and that was the precedent that allowed
other American officials to visit refuseniks,'' Mr. Sharansky said. ''Before
that, it was like an iron curtain. Nobody did it.''

Former President Jimmy Carter, whom Mr. Kennedy challenged in the Democratic
primaries in 1980 and denounced as a ''clone of Ronald Reagan,'' said Mr.
Kennedy had been ''pre-eminent'' on Capitol Hill.

''He has been staunch and honest and open, and very able to express his view to
the American people,'' Mr. Carter told the BBC from Ramallah, in the West Bank.
''I think Ted Kennedy, although he came from a very affluent family, a very
prominent family, successful in politics, I think his first commitment was
always to the people who were most in need.''

Nancy Reagan, whose husband defeated Mr. Carter in 1980 and once called Mr.
Kennedy ''the champion of big-government spending and an all-pervasive federal
presence in our lives and wallet,'' said Ronald Reagan and Mr. Kennedy had had
''great respect for one another.''

''Given our political differences,'' Mrs. Reagan said, ''people are sometimes
surprised by how close Ronnie and I have been to the Kennedy family. But Ronnie
and Ted could always find common ground.'' Noting that she considered him ''an
ally and a dear friend,'' she added that she and Mr. Kennedy had found ''our
common ground in stem cell research.''

Democratic leaders said they would commemorate Mr. Kennedy by pushing health
care legislation through Congress. Senator Robert C. Byrd, for one, suggested in
a statement that the bill be named for him.

Mr. Limbaugh said he predicted months ago that Democrats would do just that, and
Sean Hannity, a radio talk show host who also has a program on the Fox News
Channel, called it ''unseemly.'' Mr. Hannity said Mr. Kennedy's death ''is not
added reason to push for passage.''

But in an age of often bitter political divisions, some maintained that Mr.
Kennedy had been a singular force for bipartisanship.

''Don't you find it remarkable that one of the most partisan, liberal men in the
last century serving in the Senate had so many of his -- so many of his foes
embrace him because they know he made them bigger?'' said Vice President Joseph
R. Biden Jr., fighting back tears as he talked in a previously scheduled
appearance at the Energy Department. ''He made them more graceful by the way in
which he conducted himself.''

URL: http://www.nytimes.com

LOAD-DATE: August 27, 2009

LANGUAGE: ENGLISH

CORRECTION-DATE: January 3, 2010



CORRECTION: An article last Sunday about the death of Edward M. Kennedy in
August misstated the length of his tenure in the Senate. He served 47 years, not
46 years. (The error appeared in Mr. Kennedy's obituary and another article,
about memories of the Senator, on Aug. 27, and also in an editorial that day.
The error was repeated in an article on Aug. 28 about how the Senate had changed
during the time he served.) The article also referred incorrectly to the
assassination of his brother President John F. Kennedy. The president was
assassinated in 1963, the year after Edward Kennedy was elected to the Senate --
not the same year.

GRAPHIC: PHOTOS: Henry Sanford and Meagan Manning, staff assistants to Senator
Edward M. Kennedy, were at their posts on Wednesday in his office on Capitol
Hill.(PHOTOGRAPH BY ALEX WONG/GETTY IMAGES)
 On Martha's Vineyard, a vacationing President Obama said Mr. Kennedy's ''ideas
and ideals are stamped on scores of laws and reflected in millions of lives.''
In Washington, Vice President Joseph R. Biden Jr. fought back tears in
discussing the senator. And at the Washington Monument, flags were at
half-staff.(PHOTOGRAPH BY LUKE SHARRETT FOR THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


