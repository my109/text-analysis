                   Copyright 2009 The New York Times Company


                             905 of 2021 DOCUMENTS


                               The New York Times

                            October 30, 2009 Friday
                              Late Edition - Final

Strong Opinions

BYLINE: By THE NEW YORK TIMES

SECTION: Section A; Column 0; National Desk; READER'S REACTIONS; Pg. 20

LENGTH: 243 words


Readers' reactions to the health care legislation proposed by House Democrats on
Thursday ranged from cautious optimism to acid condemnation -- the latter from
both ends of the political spectrum. A sampling of comments submitted to the
Prescriptions blog:

''So when will someone tell the health insurance industry that making a profit
by denying payments for health care benefits is no longer a viable business
model for the United States?'' BacktoBasicsRob

''Our government has had so many effective programs: Medicare, Medicaid, the
E.P.A., the F.D.A., the Armed Forces, to name but a few. Yet people are afraid
that [government] will take over for an industry that has clearly failed us. It
boggles my mind.''  Paul

''I am surprised some of you so-called progressives are willing to stake your
lives on a government that cannot pay its own bills, has bankrupted multiple
entitlement programs and could be paying Social Security benefits with i.o.u.'s
in a few years.''  Alma

''My health care shouldn't be up to a board of directors at a company. Bravo to
the new health care bill  --  welcome to the 21st century, America.'' Cmxsmitty

''Our country deserved a national debate on how health care will be delivered.
Instead, all of the bickering was about how or who was going to pay for a
spending bill. ... So now we have an expansion of Medicaid and federal subsidies
for private insurance companies. This is the change we voted for?'' Diego in
Texas

URL: http://www.nytimes.com

LOAD-DATE: October 30, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING

PUBLICATION-TYPE: Newspaper


