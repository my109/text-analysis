                   Copyright 2010 The New York Times Company


                             1933 of 2021 DOCUMENTS


                               The New York Times

                             April 27, 2010 Tuesday
                              Late Edition - Final

With Expanded Coverage for the Poor, Fears of a Big Headache

BYLINE: By RONI CARYN RABIN

SECTION: Section D; Column 0; Science Desk; Pg. 6

LENGTH: 1052 words


Of all the changes wrought by the new health care law, none is more sweeping
than the transformation of Medicaid -- from the government's health insurance
plan for poor families into a much wider program for millions of the poorest
Americans who cannot afford insurance on their own.

''Medicaid is finally living up to its role of serving as the health care safety
net for poor and lower-income individuals and families,'' said Jennifer Tolbert,
principal policy analyst at the Kaiser Family Foundation's Commission on
Medicaid and the Uninsured.

Able-bodied adults under 65 -- now mostly ineligible unless they have dependent
children -- will qualify for coverage in 2014 as long as they earn no more than
$14,404 (in current dollars) for a single adult and $29,326 for a family of
four, or 133 percent of the federal poverty level.

Of the 32 million uninsured Americans expected to gain health coverage under the
new law, as many as 20 million will be insured by Medicaid, experts estimate.
Asset tests will be largely eliminated, so workers who lose their jobs can get
health coverage even if they own their homes or have money saved for retirement.
(Illegal immigrants will not be eligible.)

Absorbing that many people into the system will not be easy. The program is
administered and partly financed by the states, which are now racing to figure
out how to carry out the necessary changes and simplify enrollment even as they
struggle to cope with severe budget cuts and staff shortages.

Many residents don't realize they will be eligible, and it will be up to the
states to let them know. And the program has long been haunted by questions
about quality of care.

''We're pretty darn busy, I can tell you that,'' said Ann Kohler, director of
health services at the American Public Human Services Association, which
oversees the National Association of State Medicaid Directors. ''Many of my
members opposed the bill and still do, frankly. They don't like it. But it is
the law, and we're working hard to get it implemented.''

One of the most pressing  problems, she said, is old computers that need to be
upgraded so they can  share information with the new insurance exchanges being
established to offer coverage to those who won't qualify for Medicaid.

But that may prove to be the least of the worries. Even now, many physicians
don't accept Medicaid patients; the plan pays less than private insurance and
Medicare, the government program for the elderly. Filing claims involves a lot
of paperwork and long payment delays.

To encourage more doctors to participate, the federal government will raise
Medicaid payments for primary care to Medicare levels in 2013 and 2014, and will
not require the states to share the extra costs. But the increases are
temporary, and do nothing to encourage specialists to participate, experts say.

Beyond the payment problem, many physicians shun Medicaid patients, saying they
tend to have complicated medical problems, skip appointments and ignore their
treatment plans.

Only 40 percent of those questioned in 2008 by the Center for Studying Health
System Change, a research organization based in Washington, said they accepted
all new Medicaid patients.

''One thing you can do with Medicaid is go to the emergency room and get into a
hospital, but hospital care is not adequate for the most common conditions that
kill people, like high blood pressure, cholesterol and diabetes,'' said Dr.
Steffie Woolhandler, a primary care physician and a founder of the group
Physicians for a National Health Program, which has been critical of the health
act. ''When you look nationally at who got preventive care like Pap smears and
mammograms and who got their blood pressure controlled, Medicaid patients don't
look much better than the uninsured.''

Several studies have found that Medicaid beneficiaries fare less well than
patients with private insurance -- for example, that they tend to get cancer
diagnoses at a later stage, and die earlier, than do privately insured patients,
and that migraine sufferers insured by Medicaid get substandard treatment
compared with the privately insured.

But Medicaid beneficiaries are more likely to have a usual source of care than
people who have no insurance, according to a 2008 analysis by the Kaiser
Commission on Medicaid and the Uninsured, which found that only 10 percent of
Medicaid beneficiaries did not have a usual source of care, compared with more
than half of the uninsured.

Vanessa Ghigliotty, a 36-year-old mother from the Bronx, used to go to a
hospital clinic for treatment. But about 10 years ago, the clinic doctor
dismissed her complaints of severe abdominal pain, fatigue and nausea, telling
her she had gas and should lose weight and eat more leafy green vegetables. A
year later, so ill she had to go to the hospital, she learned she had advanced
colon cancer.

Although she had been upset by the clinic doctor's response, Ms. Ghigliotty
said, it was difficult to find another doctor. ''The minute they hear you have
Medicaid they say, 'Sorry, we don't accept that.' ''

Ms. Ghigliotty is one of several patients involved in a civil rights complaint
filed with the New York State attorney general's office charging that several
New York City hospitals discriminate against Medicaid patients; the complaint
says that Medicaid patients are referred to clinics while privately insured
patients are referred to faculty practice offices, and that they do not receive
the same quality of care. The complaint was filed by a health coalition called
Bronx Health Reach in partnership with New York Lawyers for the Public Interest.

The influx of new patients is likely to put even more pressure on the strained
network of Medicaid providers. Many of the new patients are likely to be in
their 50s and early 60s, with complex medical problems, including mental health
issues, said Peter J. Cunningham, senior fellow at the Center for Studying
Health System Change.

Andy Hyman, senior program officer with the Robert Wood Johnson Foundation,
agreed. ''A major issue that people need to think about with this expansion,''
he said, ''is that just because you're going to get 16 million new people
enrolled, that does not mean those 16 million will have easy access to all the
care that they need.''

URL: http://www.nytimes.com

LOAD-DATE: April 27, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID PLUNKERT)

PUBLICATION-TYPE: Newspaper


