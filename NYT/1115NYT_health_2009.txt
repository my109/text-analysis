                   Copyright 2009 The New York Times Company


                             1115 of 2021 DOCUMENTS


                               The New York Times

                           December 3, 2009 Thursday
                              Late Edition - Final

The Long Hot Winter

BYLINE: By GAIL COLLINS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 43

LENGTH: 795 words


Now is the winter of our discontent. And, officially, it's still fall.

I have developed a whole new appreciation for the hysteria over Tiger Woods.
Many critics of the news media believe we are spending way too much time
worrying about why a golfer had a car accident at the end of his driveway. But
given the incredibly depressing nature of all the big news stories of the week,
if you want to focus on Tiger's marital problems, I am in total sympathy.

The president ordered 30,000 more troops into Afghanistan. The New York State
Senate defeated a same-sex marriage bill. Even the murder and racketeering trial
of John Gotti Jr. ended badly.

I have been a fan of this particular judicial proceeding ever since the day that
Gotti got into a courtroom brawl with a prosecution witness that ended with
Junior shouting: ''You're a punk! You're a dog! You're a dog! You always were a
dog your whole life, you punk dog.''

However, on Tuesday, it vanished from the news cycle when the judge declared the
fourth mistrial in five years because of a hung jury.

Meanwhile, in Washington, the U.S. Senate began its groundbreaking debate over a
national health care plan. In honor of this historic event, the Republican Judd
Gregg of New Hampshire -- who you will remember was so bipartisan a while back
that President Obama wanted to make him the secretary of commerce -- passed out
a list of tips on how to best stall any conceivable progress with meaningless
points of order.

The way things are going, we should get to a vote at about the same time that
the president is planning to get the troops out of Afghanistan.

Much of the debate has revolved around mammograms. You will remember that
recently a government task force suggested that women who don't have any special
predisposition to breast cancer consider beginning mammograms at 50 rather than
40.

''This was based mainly on cost,'' said  John Ensign, a Republican who was
actually completely wrong despite the extensive expertise he brings to the
debate in his capacity as the only veterinarian in the Senate.

The practical effects of the task force recommendation, under the health care
reform bill, might be to increase the number of insurance policies that require
a co-payment for those early tests unless a woman's doctor intervened to say
that they were needed. Given the fact that some experts never did think the
early mammograms were a good idea, and that others now believe they actually do
more harm than good, this did not seem like the worst possible thing in the
world.

Especially since, um, right now a lot of women have no health insurance and no
mammograms at all.

The Democrats, terrified by cries of ''rationing!'' are now trying to amend the
bill to expand insurance coverage of health care screenings for women. Not to be
outdone, the Republicans seem bent on making sure that every single 40-year-old
woman in America gets a free mammogram even if she never sees a doctor for
anything else for the rest of her life.

Another big debate topic has been cost. The price tag on the bill needs to be
kept under control -- except, of course, when it involves mammograms. That's a
concern for us all since spending on health care is spiraling out of control and
threatening to wreck the economy.

So you really did need to pay attention when the Republicans offered their first
big motion of the debate, under the leadership of that famous fiscal hawk and
former G.O.P. standard-bearer, John McCain. ... Who got up and demanded that the
bill be stripped of $450 billion in proposed Medicare savings.

''Come back with another bill. Only this time, don't put the cost of it on the
backs of senior citizens of this country,'' he said.

It was a riveting moment. Perhaps never before had a member of the Senate dared
to suggest that a piece of pending legislation should be changed so that senior
citizens would be exempt from suffering.

Medicare eats up more than 3 percent of the gross domestic product, and it is
the one entitlement that the government has no current prospects of ever getting
under control. You would think that shaving some of its costs might interest a
guy who practically based an entire presidential campaign on his opposition to a
$3 million DNA test for endangered grizzly bears.

But no, there was McCain, waving the bloody shirt and predicting that cutting
the cost of Medicare would  --  Yes!  --  ''eventually lead to rationing of
health care in this country.''

Whatever happened to the John McCain who wanted to balance the budget and work
with the Democrats to fight global warming? Gone the way of the wild grizzly, I
guess.

Let's try not to think about what January will bring. Maybe another sports hero
will do us a favor and run into a fire hydrant.

URL: http://www.nytimes.com

LOAD-DATE: December 3, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


