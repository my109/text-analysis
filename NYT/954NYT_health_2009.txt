                   Copyright 2009 The New York Times Company


                             954 of 2021 DOCUMENTS


                               The New York Times

                            November 8, 2009 Sunday
                              Late Edition - Final

Overweight Americans Push Back With Vigor in the Health Care Debate

BYLINE: By SUSAN SAULNY

SECTION: Section A; Column 0; National Desk; Pg. 23

LENGTH: 1114 words


Marilyn Wann is an author and weight diversity speaker in Northern California
who has a message for anyone making judgments about her health based on her
large physique. ''The only thing anyone can accurately diagnose by looking at a
fat person is their own level of stereotype and prejudice about fat,'' said Ms.
Wann, a 43-year-old San Franciscan whose motto in life is also the title of her
book: ''Fat? So!''

Hers has been an oft-repeated message this summer and fall by members of the
''fat pride'' community, given that the nation is in the midst of a debate about
health care. That debate has, sometimes awkwardly, focused its attention on the
growing population of overweight and obese Americans with unambiguous overtones:
fat people should lose weight, for the good of us all.

Heavier Americans are pushing back now with newfound vigor in the policy debate,
lobbying legislators and trying to move public opinion to recognize their point
of view: that thin does not necessarily equal fit, and that people can be
healthy at any size.

Extra weight brings with it an increased risk of chronic disease, medical
experts say, and heavier people tend to have medical costs that are
substantially higher than their leaner counterparts. As a result, Congress is
considering proposals in the effort to overhaul health care that would make it
easier for employers to use financial rewards or penalties to promote healthy
behavior by employees, like weight loss.

Other less-scientific arguments have also gained traction on blogs, chat shows
and editorial pages since talk of the overhaul began in earnest, with the
overweight cast as lazy or gluttonous liabilities and therefore not entitled to
universal health coverage because of poor personal decision-making. As that
thinking goes, a healthful eater should not have to pay for the consequences of
someone else's greasy burger binges.

Either way, heavy people -- characterized as over-consumers of health care or as
those who should miss out on discounts because of their size -- say they have
been maligned throughout the debate.

''I thought, 'Health reform? Yay!' '' said Lynn McAfee, the director of medical
advocacy for the Council on Size and Weight Discrimination, an advocacy group
for heavy people. But Ms. McAfee said it was not long before her sentiment
changed to the more sober, ''Oh no, we're being scapegoated again.''

It is an uphill battle. But the health care debate has, unexpectedly, also
provided an opportunity for new expressions of what Ms. Wann calls ''fat
pride,'' the notion that weight diversity is a good thing and that size
discrimination is as offensive as any other kind.

''The stigma is so heavy a burden that it took our community 40 years before it
could go to Capitol Hill and lobby for ourselves,'' said Ms. Wann, a member of
the National Association to Advance Fat Acceptance, an advocacy group that
organized a lobbying trip to Washington for its members this spring. ''We're
kind of a popular punching bag. You can do incredibly discriminating, hurtful,
hateful things to fat people in public and not only get away with it but be seen
as some kind of superhero.''

On Capitol Hill, the association asked legislators for a public option from
which fat people could not be excluded because of weight and for coverage that
did not consider excess weight a pre-existing condition.

''Basically,'' Ms. Wann continued, ''we want to be treated with respect the same
as everyone else.''

Americans are more overweight and obese than they were 10 years ago, or even one
year ago, according to the Robert Wood Johnson Foundation and the Trust for
America's Health, which published a state-by-state study in June.

It showed that the trend is up sharply.

Two-thirds of all Americans are overweight or obese. In four states -- Alabama,
Mississippi, Tennessee and West Virginia -- more than 30 percent of adults are
obese. In 1991, in contrast, no state had an obesity rate over 20 percent.

And, according to the American Obesity Association, a research organization,
poor minority women have the greatest likelihood of being overweight.

Weight is an incendiary issue, experts said, and that may be why it had such
staying power as a hot topic of conversation through the health care debate.

''All national health insurance systems are built on the idea that we're all
part of a community, we all get sick and die, so we're going to take care of one
another,'' said James Morone, a professor of political science and urban studies
at Brown University. ''The best philosophical way to stop national health
insurance is to say we're not a community, it's 'us vs. them.' ''

But what has been different about this particular issue, this year, is that
''people are pushing back,'' Professor Morone said.

Peggy Howell, the public relations director for the National Association to
Advance Fat Acceptance, said she had been on the phone delivering her group's
message and answering more news media calls this year than ever before.

The message is simple, she said: ''We believe that fat people can eat healthy
food and add movement to their lives and be healthy. And healthy should be the
goal, not thin.''

That idea is gaining strength and popularity among a segment of the overweight
population that feels as though traditional dieting to lose weight does more
harm than good, ultimately benefiting the $30 billion weight loss industry, not
the public.

''I get so angry when I feel people pushing a weight-loss agenda,'' said Linda
Bacon, a nutrition professor at City College of San Francisco and author of
''Health at Every Size,'' a book published last year whose title has become the
rallying cry of the fat pride community. ''What we're doing in public health
care policy is harmful. We give a direct and clear message that there's
something wrong with being fat.''

A federally financed study by Ms. Bacon, published in the book, found that there
were many people who could be healthy in fat bodies.

Ms. Wann used some of Ms. Bacon's findings as her talking points when she
visited legislators with other lobbyists for ''fat acceptance'' in May.

She said she felt encouraged that the health care bill the House Democratic
leaders unveiled on Thursday does not allow changes in insurance pricing based
on obesity. But there is still a long way to go before any bill becomes law.

''For me, the takeaway point that was heartening and historic and exhilarating
is that it was the first time we started lobbying for a humane health-enhancing
system,'' said Ms. Wann, who is self-employed and, in her own words, fat and
uninsurable.

''We're all in this life raft together,'' she said.

URL: http://www.nytimes.com

LOAD-DATE: November 8, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: ''We're kind of a popular punching bag,'' said Marilyn Wann,
author of ''Fat? So!''(PHOTOGRAPH BY HEIDI SCHUMANN FOR THE NEW YORK TIMES)(A31)

PUBLICATION-TYPE: Newspaper


