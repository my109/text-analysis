                   Copyright 2010 The New York Times Company


                             1869 of 2021 DOCUMENTS


                               The New York Times

                             April 11, 2010 Sunday
                              Late Edition - Final

Southern Discomfort

BYLINE: By JON MEACHAM.

Jon Meacham, the editor of Newsweek, won the 2009 Pulitzer Prize for biography
for ''American Lion: Andrew Jackson in the White House.''

SECTION: Section WK; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 12

LENGTH: 661 words


IN 1956, nearly a century after Fort Sumter, Robert Penn Warren went on
assignment for Life magazine, traveling throughout the South after the Supreme
Court's school desegregation decisions. Racism was thick, hope thin. Progress,
Warren reported, was going to take a while -- a long while. ''History, like
nature, knows no jumps,'' he wrote, ''except the jump backward, maybe.''

Last week, Virginia's governor, Robert McDonnell, jumped backward when he issued
a proclamation recognizing April as Confederate History Month. In it he
celebrated those ''who fought for their homes and communities and Commonwealth''
and wrote of the importance of understanding ''the sacrifices of the Confederate
leaders, soldiers and citizens during the period of the Civil War.''

The governor originally chose not to mention slavery in the proclamation, saying
he ''focused on the ones I thought were most significant for Virginia.'' It
seems to follow that, at least for Mr. McDonnell, the plight of Virginia's
slaves does not rank among the most significant aspects of the war.

Advertently or not, Mr. McDonnell is working in a long and dispiriting
tradition. Efforts to rehabilitate the Southern rebellion frequently come at
moments of racial and social stress, and it is revealing that Virginia's
neo-Confederates are refighting the Civil War in 2010. Whitewashing the war is
one way for the right -- alienated, anxious and angry about the president,
health care reform and all manner of threats, mostly imaginary -- to express its
unease with the Age of Obama, disguising hate as heritage.

If neo-Confederates are interested in history, let's talk history. Since Lee
surrendered at Appomattox, Confederate symbols have tended to be more about
white resistance to black advances than about commemoration. In the 1880s and
1890s, after fighting Reconstruction with terrorism and after the Supreme Court
struck down the 1875 Civil Rights Act, states began to legalize segregation. For
white supremacists, iconography of the ''Lost Cause'' was central to their
fight; Mississippi even grafted the Confederate battle emblem onto its state
flag.

But after the Supreme Court allowed segregation in Plessy v. Ferguson in 1896,
Jim Crow was basically secure. There was less need to rally the troops, and
Confederate imagery became associated with the most extreme of the extreme: the
Ku Klux Klan.

In the aftermath of World War II, however, the rebel flag and other Confederate
symbolism resurfaced as the civil rights movement spread. In 1948, supporters of
Strom Thurmond's pro-segregation Dixiecrat ticket waved the battle flag at
campaign stops.

Then came the school-integration rulings of the 1950s. Georgia changed its flag
to include the battle emblem in 1956, and South Carolina hoisted the colors over
its Capitol in 1962 as part of its centennial celebrations of the war.

As the sesquicentennial of Fort Sumter approaches in 2011, the enduring problem
for neo-Confederates endures: anyone who seeks an Edenic Southern past in which
the war was principally about states' rights and not slavery is searching in
vain, for the Confederacy and slavery are inextricably and forever linked.

That has not, however, stopped Lost Causers who supported Mr. McDonnell's
proclamation from trying to recast the war in more respectable terms. They would
like what Lincoln called our ''fiery trial'' to be seen in a political, not a
moral, light. If the slaves are erased from the picture, then what took place
between Sumter and Appomattox is not about the fate of human chattel, or a
battle between good and evil. It is, instead, more of an ancestral skirmish in
the Reagan revolution, a contest between big and small government.

We cannot allow the story of the emancipation of a people and the expiation of
America's original sin to become fodder for conservative politicians playing to
their right-wing base. That, to say the very least, is a jump backward we do not
need.

URL: http://www.nytimes.com

LOAD-DATE: April 11, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY OLIVER MUNDAY)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


