                   Copyright 2009 The New York Times Company


                             1182 of 2021 DOCUMENTS


                               The New York Times

                           December 15, 2009 Tuesday
                              Late Edition - Final

The Pros and Cons of 60

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; CURSE OF THE LUCKY NUMBER; Pg. 24

LENGTH: 249 words


Sixty is the magic number for the Senate majority leader, Harry Reid. It is the
number of votes he needs to overcome any Republican filibusters that might block
the health care bill. It is also the number of senators in the Democratic
caucus.

But in many ways, 60 is also a mirage -- falsely raising Democrats' hopes,
particularly those of more liberal senators, that they have the muscle to push
the health care bill without making concessions to centrists in both parties.

And so the number 60 now seems to hang from a chain around Mr. Reid's neck like
some outsized piece of political bling that weighs him down at virtually every
legislative twist and turn.

Mr. Reid's burden was never more apparent than on Sunday when Senator Joseph I.
Lieberman, independent of Connecticut, who as part of the Democratic caucus is
one of those 60, went on the CBS program ''Face the Nation.''

''There are not 60 votes for health care reform in the Senate now,'' Mr.
Lieberman said.

Mr. Lieberman opposes two parts of the bill that many centrists criticize as
being potentially too expensive or as giving the government too large of a role
in health care.

With Mr. Reid eager to finish work on the health care legislation before
Christmas, how far Democratic leaders are willing to go to appease Mr. Lieberman
is likely to become clear within the next few days.

What is already clear is that controlling 60 votes in the Senate is a lot nicer
in theory than in practice.

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: December 15, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


