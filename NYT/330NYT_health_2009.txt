                   Copyright 2009 The New York Times Company


                             330 of 2021 DOCUMENTS


                               The New York Times

                             August 16, 2009 Sunday
                              Late Edition - Final

Fat Tax

BYLINE: By DAVID LEONHARDT.

David Leonhardt is an economics columnist for The Times and a staff writer for
the magazine.

SECTION: Section MM; Column 0; Magazine Desk; THE WAY WE LIVE NOW; Pg. 9

LENGTH: 1060 words


Two years ago, the Cleveland Clinic stopped hiring smokers. It was one part of a
''wellness initiative'' that has won the renowned hospital -- which President
Obama recently visited -- some very nice publicity. The clinic has a farmers'
market on its main campus and has offered smoking-cessation classes for the
surrounding community. Refusing to hire smokers may be more hard-nosed than the
other parts of the program. But given the social marginalization of smoking, the
policy is hardly shocking. All in all, the wellness initiative seems to be a
feel-good story.

Which is why it is so striking to talk to Delos M. Cosgrove, the heart surgeon
who is the clinic's chief executive, about the initiative. Cosgrove says that if
it were up to him, if there weren't legal issues, he would not only stop hiring
smokers. He would also stop hiring obese people. When he mentioned this to me
during a recent phone conversation, I told him that I thought many people might
consider it unfair. He was unapologetic.

''Why is it unfair?'' he asked. ''Has anyone ever shown the law of conservation
of matter doesn't apply?'' People's weight is a reflection of how much they eat
and how active they are. The country has grown fat because it's consuming more
calories and burning fewer. Our national weight problem brings huge costs, both
medical and economic. Yet our anti-obesity efforts have none of the urgency of
our antismoking efforts. ''We should declare obesity a disease and say we're
going to help you get over it,'' Cosgrove said.

You can disagree with the doctor -- you can even be offended -- and still come
to see that there is a larger point behind his tough-love approach. The debate
over health care reform has so far revolved around how insurers, drug companies,
doctors, nurses and government technocrats might be persuaded to change their
behavior. And for the sake of the economy and the federal budget, they do need
to change their behavior. But there has been far less discussion about how the
rest of us might also change our behavior. It's as if we have little
responsibility for our own health. We instead outsource it to something called
the health care system.

The promise of that system is undeniably alluring: whatever your ailment, a pill
or a procedure will fix it. Yet the promise hasn't been kept. For all the
miracles that modern medicine really does perform, it is not the primary
determinant of most people's health. J. Michael McGinnis, a senior scholar at
the Institute of Medicine, has estimated that only 10 percent of early deaths
are the result of substandard medical care. About 20 percent stem from social
and physical environments, and 30 percent from genetics. The biggest
contributor, at 40 percent, is behavior.

Today, the great American public-health problem is indeed obesity. The
statistics have become rote, but consider that people in their 50s are about 20
pounds heavier on average than 50-somethings were in the late 1970s. As a
convenient point of reference, a typical car tire weighs 20 pounds.

This extra weight has caused a sharp increase in chronic diseases, like
diabetes, that are unusually costly. Other public-health scourges, like lung
cancer, have tended to kill their victims quickly, which (in the most tragic
possible way) holds down their long-term cost. Obesity is different. A recent
article in Health Affairs estimated its annual cost to be $147 billion and
growing. That translates into $1,250 per household, mostly in taxes and
insurance premiums.

A natural response to this cost would be to say that the people imposing it on
society should be required to pay it. Cosgrove mentioned to me an idea that some
economists favor: charging higher health-insurance premiums to anyone with a
certain body-mass index. Harsh? Yes. Fair? You can see the argument. And yet it
turns out that the obese already do pay something resembling their fair share of
medical costs, albeit in an indirect way. Overweight workers are paid less than
similarly qualified, thinner colleagues, according to research by Jay
Bhattacharya and M. Kate Bundorf of Stanford. The cause isn't entirely clear.
But the size of the wage difference is roughly similar to the size of the
difference in their medical costs.

It's also worth noting that the obese, as well as any of the rest of us
suffering from a medical condition affected by behavior, already have plenty of
incentive to get healthy. But we struggle to do so. Daily life gets in the way.
Inertia triumphs.

The question of personal responsibility, then, ends up being more complicated
than it may seem. It's hard to argue that Americans have collectively become
more irresponsible over the last 30 years; the murder rate has plummeted, and
divorce and abortion rates have fallen. And our genes certainly haven't changed
in 30 years.

What has changed is our environment. Parents are working longer, and takeout
meals have become a default dinner. Gym classes have been cut. The real price of
soda has fallen 33 percent over the last three decades. The real price of fruit
and vegetables has risen more than 40 percent.

The solutions to these problems are beyond the control of any individual. They
involve a different sort of responsibility: civic -- even political --
responsibility. They depend on the kind of collective action that helped cut
smoking rates nearly in half. Anyone who smoked in an elementary-school hallway
today would be thrown out of the building. But if you served an
obesity-inducing, federally financed meal to a kindergartner, you would fit
right in. Taxes on tobacco, meanwhile, have skyrocketed. A modest tax on sodas
-- one of the few proposals in the various health-reform bills aimed at health,
rather than health care -- has struggled to get through Congress.

Cosgrove's would-be approach may have its problems. The obvious one is its
severity. The more important one is probably its narrowness: not even one of the
nation's most prestigious hospitals can do much to reduce obesity. The
government, however, can. And that is the great virtue of Cosgrove's idea. He is
acknowledging that any effort to attack obesity will inevitably involve making
value judgments and even limiting people's choices. Most of the time, the
government has no business doing such things. But there is really no other way
to cure an epidemic.

URL: http://www.nytimes.com

LOAD-DATE: August 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY JEN DAVIS)
(PHOTOGRAPH: LEE MARKS FINE ART)  CHART: 9.1: is the percentage of annual U.S.
medical costs that are obesity-related.(SOURCE (TOP LEFT) : RTI INTERNATIONAL,
THE AGENCY FOR HEALTH CARE RESEARCH AND QUALITY AND THE U.S. CENTERS FOR DISEASE
CONTROL AND PREVENTION.)

PUBLICATION-TYPE: Newspaper


