                   Copyright 2009 The New York Times Company


                             475 of 2021 DOCUMENTS


                               The New York Times

                          September 2, 2009 Wednesday
                              Late Edition - Final

Getting on the Front Lines

BYLINE: By JACKIE CALMES

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 185 words


President Obama is planning for ''a new season'' of more hands-on advocacy for
an overhaul of the health care system, according to his advisers. Among the
likely steps are a nationally televised speech that close allies have
recommended and a 10-year price tag below the $1 trillion mark.

Mr. Obama met on Tuesday with advisers including Rahm Emanuel, the White House
chief of staff, and David Axelrod, a senior strategist, to prepare for
Congress's return to work next week, after a month in which many lawmakers have
been spooked by contentious town-hall-style meetings and polls showing slipping
support for Mr. Obama and his health plans.

''We're obviously entering a new season here, and this issue has been debated
and discussed and chewed over at great length now,'' Mr. Axelrod said. ''There
are a lot of ideas on the table, and now it's time to pull those strands
together and finish the work.''

That suggests that the president could, for the first time, put in writing the
elements of a health care plan, drawing from the common pieces of measures in
four House and Senate committees. JACKIE CALMES

URL: http://www.nytimes.com

LOAD-DATE: September 2, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

PUBLICATION-TYPE: Newspaper


