                   Copyright 2009 The New York Times Company


                             1219 of 2021 DOCUMENTS


                               The New York Times

                            December 20, 2009 Sunday
                              Late Edition - Final

End to the Abstinence-Only Fantasy

SECTION: Section WK; Column 0; Editorial Desk; EDITORIAL; Pg. 6

LENGTH: 413 words


The omnibus government spending bill signed into law last week  contains an
important victory for public health. Gone is all spending for highly restrictive
abstinence-only sex education programs that deny young people accurate
information about contraceptives, sexually transmitted diseases and pregnancy.
The measure redirects sex-education resources to medically sound programs aimed
at reducing teenage pregnancy.

Federal support for the wishful abstinence-only approach, which began in the
1980s, ballooned during George W. Bush's presidency. As the funding grew, so did
evidence of the policy's failure. A Congressionally mandated study released in
2007 found that elementary and middle school students who received abstinence
instruction were just as likely to have sex in the following year as students
who did not get such instruction.

Many states rightly declined to participate in the abstinence program, forgoing
federal money.  Most of the nation's recent progress in reducing the abortion
rate has occurred in states that have shown a commitment to real sex education.

The last Bush budget included $99 million for abstinence-only education programs
run by public and private groups. The new $114 million initiative, championed by
the White House, will be administered by a newly created Office of Adolescent
Health within the Department of Health and Human Services with a mandate to
support ''medically accurate and age appropriate programs'' shown to reduce
teenage pregnancy.

Unfortunately, some of this progress could be short-lived. The health care
reform bill approved by the Senate Finance Committee includes an amendment,
introduced by the Republican Senator Orrin Hatch, that would revive a separate
$50 million grant-making program for abstinence-only programs run by states.
Democratic leaders must see that this is stricken, and warring language that
would provide $75 million for state comprehensive sex education programs should
remain.

In another positive step, the spending bill increases financing for
family-planning services for low-income women. It also lifts a long-standing,
and utterly unjustified, ban on the District of Columbia's use of its own tax
dollars to pay for abortion services for poor women except in cases when a
woman's life is at risk, or the pregnancy is the result of rape or incest.

Ideology, censorship and bad science have no place in public health policy. It
is a relief to see some sense returning to Capitol Hill.

URL: http://www.nytimes.com

LOAD-DATE: December 20, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


