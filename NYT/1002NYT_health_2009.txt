                   Copyright 2009 The New York Times Company


                             1002 of 2021 DOCUMENTS


                               The New York Times

                            November 15, 2009 Sunday
                              Late Edition - Final

Job Security? Consider a Career in Health Care

BYLINE: By PHYLLIS KORKKI

SECTION: Section BU; Column 0; Money and Business/Financial Desk; THE COUNT; Pg.
2

LENGTH: 163 words


For all the hand-wringing about the weaknesses of health care, one aspect of it
has remained strong: its ability to provide jobs. In fact, health care
employment has increased during the recession, while employment as a whole has
declined, according to data from the Bureau of Labor Statistics.

Within the private sector, more than 11 percent of the American work force is
engaged in health care work, compared with just 3 percent before 1960, the
bureau says.

Regardless of how health care reform shakes out, the industry jobs picture is
likely to remain robust, given the aging population and technological advances
in medicine.

High school and college students take note: the positions expected to post some
of the largest increases include registered nurses; personal and home care
aides; home health aides; nursing aides, orderlies and attendants; medical
assistants; licensed practical and licensed vocational nurses; pharmacy
technicians; and physicians and surgeons.

URL: http://www.nytimes.com

LOAD-DATE: November 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: TOTAL WORKERS
 HEALTH CARE WORKERS (Source: Bureau of Labor Statistics)

PUBLICATION-TYPE: Newspaper


