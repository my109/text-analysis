                   Copyright 2009 The New York Times Company


                             210 of 2021 DOCUMENTS


                               The New York Times

                              July 24, 2009 Friday
                              Late Edition - Final

As Health Bill Is Delayed, White House Negotiates

BYLINE: By DAVID M. HERSZENHORN and JEFF ZELENY; David M. Herszenhorn reported
from Washington, and Jeff Zeleny from Shaker Heights, Ohio. Robert Pear
contributed reporting from Washington.

SECTION: Section A; Column 0; National Desk; Pg. 14

LENGTH: 1021 words

DATELINE: WASHINGTON


White House officials negotiated furiously on Thursday to keep major health care
legislation on track after the Senate majority leader, Harry Reid of Nevada,
said his chamber would not vote on a health measure until after Congress
returned from its summer recess.

Mr. Reid's acknowledgment that the Senate would miss the deadline that President
Obama had set for each chamber to pass a bill is a serious, if widely expected,
setback for the president. From Shaker Heights, Ohio, Mr. Obama argued against
''delay for the sake of delay,'' so that lawmakers could avoid taking a
controversial stand on legislation to expand health insurance coverage and
control medical costs.

The White House chief of staff, Rahm Emanuel, led a hastily called three-hour
negotiating session at the Capitol with conservative Blue Dog Democrats, the
group of fiscal hawks who have stalled action on the health care bill in the
House.

While Congress can resume its efforts in the fall, other major items on the
president's agenda, like climate change and rewriting financial regulation, have
also been postponed, and are likely to be further delayed until the health care
debate is resolved.

The latest Congressional churning came as Mr. Obama traveled to Ohio to press
his case directly with the American public. He sought to play down the Senate's
missed deadline, saying, ''As long as I see folks working diligently and
consistently, I am comfortable with moving a process forward that builds as much
consensus as possible.''

Democratic leaders fended off suggestions that health care legislation could
lose momentum if there is no action until the fall. But the delay will give
Republican opponents ample time to highlight what they say are the bill's flaws,
and will subject moderate lawmakers, many of whom are on the fence, to a barrage
of questions, including whether the nation can afford the $1 trillion, 10-year
price tag.

Industry groups and consumer advocates on both sides of the issue are planning
to follow House and Senate members home to their states and districts in August
with intensive advertising and public relations campaigns in an effort to sway
votes.

The Obama administration is drawing up plans to join the fray, with the
president planning trips across the country to keep the momentum alive, while
his Organizing for America group opens a new lobbying push to build grass-roots
support.

Mr. Reid had some good news for the White House on Thursday. He said that he
expected a bipartisan agreement to emerge from the Finance Committee before the
Senate recess began on Aug. 8, and that he would spend the break merging that
bill with legislation approved by the Committee on Health, Education, Labor and
Pensions.

And some White House aides were optimistic that the negotiations between Mr.
Emanuel and the Blue Dogs in the House would result in Speaker Nancy Pelosi's
scheduling a floor vote for next week. Emerging Thursday afternoon from the
session with Mr. Emanuel, Representative Mike Ross, Democrat of Arkansas and a
leader of the Blue Dogs, said many issues remained unresolved and that talks
would continue. The Blue Dogs have expressed concerns about the cost and scope
of the health legislation. But even if those differences are settled, the lack
of a Senate vote before the recess hinders Ms. Pelosi's ability to bring a
health plan to the House floor.

Many House Democrats are wary of casting a potentially career-defining vote to
raise taxes to pay for expanded health coverage unless Democratic senators are
also on board for a tax increase. Ms. Pelosi said Thursday that if she got some
clear signals from the Senate, she would consider keeping the House in session
beyond July 31.

Ms. Pelosi said she was not worried about losing steam if votes were postponed
to the fall.

''I am not afraid of August,'' she said. ''It is a month. What I am interested
in is the sooner, the better to pass health care for the American people.''

As Mr. Obama took questions from his audience in Shaker Heights, he was asked
whether he intended to call on Democratic leaders in Congress to cancel their
August recess to try to reach a compromise on health care. For now, he said, he
had no plans to do so.

''I don't want a delay just because of politics,'' he said. ''I have to tell you
that sometimes delays in Washington occur when people just don't want to do
anything that they think might be controversial.''

Mr. Reid tried to blame Republicans for the delay, saying they had asked for
more time. But he also acknowledged the cooperation of three Republican
negotiators, Senators Michael B. Enzi of Wyoming, Charles E. Grassley of Iowa
and Olympia J. Snowe of Maine.

''Working with the Republicans, one of the things that they asked for was to
have more time,'' Mr. Reid said. He added: ''I don't think it's unreasonable.
This is a complex, difficult issue.''

Democrats themselves remain deeply divided over the health care legislation. Mr.
Reid's remarks came after a contentious meeting earlier in the day among
Democratic members of the Finance Committee. As the committee chairman, Senator
Max Baucus, Democrat of Montana, nears a deal with Republicans on the panel,
several Democrats have begun raising concerns that Mr. Baucus is conceding on
too many points.

Mr. Baucus met Thursday with Democrats on the panel to report on the work of a
bipartisan group of six senators trying to forge a compromise. Several Democrats
said they found the meeting useful because they had not known the details of Mr.
Baucus's marathon talks with the bipartisan group. Senator John D. Rockefeller
IV, Democrat of West Virginia, said he had ''basically been shut out of the
process,'' even though he was chairman of the panel's health subcommittee.

Mr. Baucus said he hoped the full committee could debate and vote on the
legislation in the week of Aug. 3.

In closing in Ohio, Mr. Obama urged his cheering supporters to do lobbying of
their own over the August break.

''All right, everybody, stay on your members of Congress,'' Mr. Obama said.
''Keep up the heat. We've got to get this done.''

URL: http://www.nytimes.com

LOAD-DATE: July 24, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


