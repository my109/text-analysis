                   Copyright 2009 The New York Times Company


                             551 of 2021 DOCUMENTS


                               The New York Times

                          September 10, 2009 Thursday
                              Late Edition - Final

In Lawmaker's Outburst, a Rare Breach of Protocol

BYLINE: By CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 26

LENGTH: 521 words

DATELINE: WASHINGTON


It was a rare breach of the protocol that governs ritualistic events in the
Capitol.

In an angry and very audible outburst, Representative Joe Wilson, Republican of
South Carolina, interrupted President Obama's speech Wednesday night with a
shout of ''You lie!''

His eruption -- in response to Mr. Obama's statement that Democratic health
proposals would not cover illegal immigrants -- stunned members of both parties
in the House chamber.

Democrats said it showed lack of respect for the office of the presidency and
was reminiscent of Republican disruptions at recent public forums on health
care.

''It is outrageous,'' said Representative Joseph Crowley, Democrat of New York,
who said it reminded him of the ''antics that are being used to disrupt and fog
what is going on.''

After the speech, Rahm Emanuel, the White House chief of staff who sat a few
rows in front of Mr. Wilson, said he immediately approached senior Republican
lawmakers to encourage them to identify the heckler and urge him to issue an
apology quickly.

''No president has ever been treated like that. Ever,'' Mr. Emanuel said.

Other Democrats said they did not want to dwell on the outburst or allow it to
overshadow what they saw as an effective address by the president. But they also
said it bolstered their contention that some Republicans were not interested in
constructive dialogue, and they noted that Democratic plans specifically barred
coverage for illegal immigrants.

Republicans also said the heckling was out of line. ''I think we ought to treat
the president with respect,'' said Senator Mitch McConnell of Kentucky, the
Republican leader, ''and anything other than that is not appropriate.''

Mr. Wilson seemed rattled in the wake of his comment, and quickly left the
chamber at the end of the speech.

His office later issued an apology, saying: ''This evening I let my emotions get
the best of me when listening to the president's remarks regarding the coverage
of illegal immigrants in the health care bill. While I disagree with the
president's statement, my comments were inappropriate and regrettable. I extend
sincere apologies to the president for this lack of civility.''

Mr. Wilson also phoned the White House and reached Mr. Emanuel, who accepted an
apology on behalf of the president.

Critical body language and murmurs of disapproval are typical at presidential
addresses and part of the political theater. But members of both parties were
trying to recollect such a pointed attack from an individual lawmaker at a
presidential address and noted that a similar remark could draw a formal
reprimand if delivered at a routine session of the House.

When President Clinton addressed Congress in 1993 to push his health care plan,
''both sides of the aisle received the President warmly,'' according to a report
in The New York Times at the time.

''But when he began talking about raising taxes on tobacco to pay for the plan,
or the need to cut Medicare and Medicaid, many on the Republican side of the
aisle began snickering, shaking their heads skeptically and making faces at each
other,'' the article said.

URL: http://www.nytimes.com

LOAD-DATE: September 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Representative Joe Wilson of South Carolina, pointing, who
yelled, ''You lie!'' at the president.(PHOTOGRAPH BY DOUG MILLS/THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


