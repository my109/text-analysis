                   Copyright 2009 The New York Times Company


                             1161 of 2021 DOCUMENTS


                               The New York Times

                            December 11, 2009 Friday
                              Late Edition - Final

Protests Over Medicare Plan

BYLINE: By ROBERT PEAR and DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 31

LENGTH: 290 words


Hospital groups are mobilizing against a proposal under consideration by Senate
Democrats to let some people ages 55 to 64 buy insurance coverage through
Medicare beginning in 2011.

Hospitals, doctors and other health care providers often complain that Medicare
payment rates are insufficient to cover their expenses. They would rather have
patients be covered by private insurance.

The Medicare buy-in proposal is part of a tentative deal that Democrats have
sent for a cost analysis by the Congressional Budget Office. Liberals are
seeking the Medicare expansion in exchange for agreeing not to push for a new
government-run insurance plan, or public option, to compete with private
insurers.

Initially, the Medicare buy-in proposal would allow some people ages 55 to 64 to
buy coverage at full cost. Once the broader health care legislation took effect,
people could buy Medicare coverage through new government-supervised exchanges
and receive federal subsidies to help defray the cost.

The American Hospital Association has issued an alert urging its members to
oppose the plan and to call their senators' offices. ''Urge your senators to
reject expansion of Medicare,'' the group said in its alert.

The Federation of American Hospitals, which represents only for-profit
hospitals, also issued a bulletin urging its members to speak out against the
plan. ''Hospitals have already agreed to the largest voluntary provider
contribution to pay for health reform -- $155 billion in Medicare and Medicaid
cuts,'' the group wrote on its Web site.

''Any Medicare buy-in would invariably lead to crowd out of the private health
insurance market, placing more people into Medicare,'' the federation said.
ROBERT PEAR and DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: December 11, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


