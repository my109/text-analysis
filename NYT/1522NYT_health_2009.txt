                   Copyright 2010 The New York Times Company


                             1522 of 2021 DOCUMENTS


                               The New York Times

                           February 18, 2010 Thursday
                              Late Edition - Final

Time To Party Like It's 1854

BYLINE: By GAIL COLLINS

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 27

LENGTH: 778 words


The Conservative Political Action Conference begins Thursday in Washington.
Glenn Beck is scheduled to give the keynote speech. Stephen Baldwin is slated to
preside over a special nighttime youth event. As always, the right wing is great
at producing hot talk shows and terrible at attracting hot actors.

The workshops and panels range from ''Is It Time for a Catholic Tea Party?'' to
''Getting Started in Hollywood.'' But the one that caught my eye was ''When All
Else Fails: Nullification and State Resistance to Federal Tyranny.''

How many of you out there thought we had settled the question of whether states
have the right to nullify federal laws during the Lincoln administration? Can I
see a show of hands?

It's civil war deja vu. The trick in conservative circles today is to see how
furious you can get about Washington's encroachment onto states rights without
quite falling over the edge into Fort Sumter.

The 10th Amendment to the Constitution, which gives the states all powers not
delegated to the federal government, is all the rage. (The Second Amendment is
so 2008.) Its passionate fans, who are inevitably starting to be referred to as
''tenthers,'' interpret the amendment as pretty much restricting the federal
government to military matters. They feel the health care reform bill is
unconstitutional. Perhaps also Social Security.

It's hard for the Conservative Political Action Conference, which was the home
of the right-wing fringe a decade or so ago, to keep ahead of the game. The
kickoff event for the conference on Wednesday was a trek to Mount Vernon for the
solemn signing of an extremely bland eulogy to federalism and the founding
fathers by conservative dignitaries who appeared old enough to have dined there
with George and Martha.

Mitt Romney, who won the conference's presidential straw poll last year after a
Herculean lobbying effort, is back with a new book in which he warns that if
America doesn't change its ways it could wind up becoming the ''France of the
21st century.''

We all know that Americans would hate to spend the next 90 years enveloped in
serious wine and universal health care. But think about how much more
threatening Romney's warning must be for the French. Where are they supposed to
go in this new world order? Do you think France would rather be the Latvia of
the 21st century or the Finland?

Romney, who is good-looking, wealthy and blessed with a lovely family, is,
unsurprisingly, not actually very angry. He would like to be president and run
the country like a business, but he is not the kind of guy who is in mourning
for the Articles of Confederation.

Romney's most dramatic recent moment came when he was attacked by a fellow
passenger on a flight home from the Olympics in Vancouver. A spokesman said
Romney, who was not injured, was ''physically assaulted'' when he asked a man to
raise his seat to an upright position before takeoff.

Perhaps the assailant mistook Romney for a flight attendant.

The attacker was taken off the plane but not charged, and Romney's spokesman
said there was no indication he recognized his victim. But maybe he did. Perhaps
he was an irritable Democrat. Or maybe he was an animal lover, still seething
over the fact that Romney once drove his family to Canada for a vacation with
their Irish setter, Seamus, strapped to the roof of the car.

But let's agree right here and now that this is a bad idea. Not the dog on the
roof. Although that, too, is really, really undesirable. But I was thinking of
the attack.

I digress. About the  Conservative Political Action Conference. Some of the
tenthers' favorite stars were too busy to show up. Sarah Palin -- whose husband
once flirted with the Alaska secessionists -- declined. Gov. Rick Perry of Texas
-- who cuddled up to the Texas secession movement in 2008 -- is home running for
re-election and wowing the crowd at a Tenth Amendment Town Hall. His strongest
challenger for the Republican nomination appears to be a woman who told Glenn
Beck that she had an open mind about whether there was any American government
involvement in the 9/11 attack.

The news media, as Beck said to Katie Couric, is ''a sound-bite world -- a
really nasty place to live.'' I would like to think that this is just because we
happen to be at a moment in history when the country has a huge number of TV,
radio and Internet outlets fighting to be loud enough and shrill enough to get
noticed.

That's what things were like with newspapers at the end of the 19th century, and
I cannot tell you what Grover Cleveland went through. But if that's the
explanation, this, too, shall pass. Like the Articles of Confederation.

URL: http://www.nytimes.com

LOAD-DATE: February 18, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


