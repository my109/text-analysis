                   Copyright 2009 The New York Times Company


                             273 of 2021 DOCUMENTS


                               The New York Times

                            August 8, 2009 Saturday
                              Late Edition - Final

Merck And Schering-Plough Shareholders Back Merger

BYLINE: By THE ASSOCIATED PRESS

SECTION: Section B; Column 0; Business/Financial Desk; BUSINESS BRIEFING COMPANY
NEWS; Pg. 2

LENGTH: 144 words


Shareholders of both Merck and Schering-Plough were nearly unanimous in voting
on Friday to support a merger of the two drug makers. By acquiring the smaller
Schering-Plough for $41.1 billion, Merck, the world's eighth-biggest drug maker
in terms of prescription medicine sales, will jump to No. 2 worldwide, just
behind Pfizer. The new Merck would have about $42.4 billion in annual sales. The
companies already are partners on the blockbuster cholesterol drugs Vytorin and
Zetia. Schering-Plough's sizable biotech unit will help Merck's sagging drug
development pipeline. The new company expects to slash costs -- including
roughly 15,000 jobs -- to maintain profit as the industry deals with increasing
competition from generics and the unknown impact of health care reform.  The
deal still needs approval from the regulators in the United States and
elsewhere.

URL: http://www.nytimes.com

LOAD-DATE: August 8, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO

DOCUMENT-TYPE: Brief

PUBLICATION-TYPE: Newspaper


