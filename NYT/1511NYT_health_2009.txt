                   Copyright 2010 The New York Times Company


                             1511 of 2021 DOCUMENTS


                               The New York Times

                            February 15, 2010 Monday
                              Late Edition - Final

Obama Stays Course, But the Winds Change

BYLINE: By JOHN HARWOOD

SECTION: Section A; Column 0; National Desk; THE CAUCUS; Pg. 10

LENGTH: 861 words


In an interview, Barack Obama declared: ''I am a pro-growth, free market guy. I
love the market. I think it is the best invention to allocate resources and
produce enormous prosperity for America or the world that's ever been
designed.''

That was in June 2008, as Senator Obama headed toward the Democratic
presidential nomination. In tone, those remarks matched President Obama's
statement last week that he would not ''begrudge'' multimillion-dollar bonuses
to ''savvy'' executives at Goldman Sachs and JPMorgan Chase.

At times in the 20 intervening months, Mr. Obama's rhetoric has shifted. Those
shifts have attracted criticism from conservatives, Wall Street executives or
his own liberal base.

But the record indicates that those shifts pale compared with the underlying
constancy in Mr. Obama's stated goals through huge events: the Wall Street
crisis and subsequent bailout, his election, recovery from recession and rising
joblessness, legislative gains and electoral losses.

''When you're dealing with an economic crisis, it's hard to achieve elegant
communications,'' said David Axelrod, Mr. Obama's top strategist. Yet, Mr.
Axelrod said, ''when people ask me what is the most salient quality I have seen
in Obama, the one that strikes me the most is his consistency.''

What has changed more profoundly is the reaction to Mr. Obama's agenda -- by
independents seeking results, Republicans eyeing a comeback and Democrats
fearing that their moment is slipping away.

Bipartisan Tack

In that June 2008 interview, Mr. Obama also said the market had ''gotten out of
balance'' with the needs of average Americans. To right it, he called for new
financial regulations and a ''long-term growth'' agenda for the economy,
policies that guide his domestic agenda today: an overhaul of the health care
system, new energy policy and higher education standards.

Three months later, when the financial crisis threatened Wall Street, Mr. Obama
repeated in another interview his commitment to that agenda. With Congress
moving toward a $700 billion bailout, he argued, ''we can't simply do that and
go back to business as usual.''

When President-elect Obama began assembling his economic team, some Republicans
and business leaders saw evidence of greater centrism. They found it in his
appointment of advisers like Lawrence H. Summers, President Bill Clinton's
Treasury secretary, as director of the National Economic Council.

In a third interview, in January 2009, President-elect Obama welcomed bipartisan
cooperation and said, ''Republicans have a lot to offer'' on economic policy.
Today, Mr. Obama has reason to assert that his agenda has in fact incorporated
Republican approaches, even as he has failed to attract Republican political
support.

His health care plan, fighting for survival on Capitol Hill, is grounded in a
requirement that individuals buy insurance coverage, creation of an ''exchange''
marketplace in which policies are sold and subsidies for those who cannot afford
them. That resembles the plan enacted in Massachusetts by Gov. Mitt Romney, a
possible Republican presidential contender in 2012.

The hot-button provision of Mr. Obama's energy plan is a cap-and-trade system to
limit carbon emissions in hopes of curbing climate change. Senator John McCain,
Republican of Arizona, embraced that concept in his 2008 presidential campaign.

Neel T. Kashkari, a top official in President George W. Bush's Treasury
Department, said Mr. Obama's actions in administering the 2008 financial bailout
were ''very consistent'' with how a Republican administration would have handled
them.

Shifting Views

The bailout itself, by linking public anger at Washington and Wall Street, has
generated the strongest political winds of Mr. Obama's tenure. And they have
bent the administration's approach and rhetoric.

Some presidential advisers believe that Mr. Obama, famous for his cool demeanor,
blew too hot when he denounced Wall Street bonuses as ''obscene'' in proposing a
new tax on large financial institutions.

Others believe that his softer comments about bonuses last week, while
consistent with many of his past statements, were ill suited to his current
imperatives in pushing for new financial regulations and drawing sharper
contrasts with Republicans.

Whatever the cause, public perceptions of Mr. Obama have indeed shifted in ways
that go beyond simple erosion in his approval ratings.

In a CNN poll last March, 58 percent called his policies ''just about right.''
Last month, the same question found that the proportion responding ''too
liberal'' or ''not liberal enough'' had both increased, while the proportion
responding ''just about right'' dropped to 42 percent.

Mr. Obama's aides blame that shift largely on the hair-trigger reactions of
Washington's political and media culture. The president has repeatedly
challenged that culture's focus on short-term results, though it remains unclear
whether he will succeed.

''Washington focuses on the next five minutes,'' said Anita Dunn, the former
White House communications director. ''If he were to communicate one thing more
clearly, it is that that's not what he's doing.''

URL: http://www.nytimes.com

LOAD-DATE: February 15, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: President Obama's stated goals have changed little.  (PHOTOGRAPH
BY J. SCOTT APPLEWHITE/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


