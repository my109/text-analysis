                   Copyright 2009 The New York Times Company


                             673 of 2021 DOCUMENTS


                               The New York Times

                          September 24, 2009 Thursday
                              Late Edition - Final

What's On Today

BYLINE: By KATHRYN SHATTUCK

SECTION: Section C; Column 0; The Arts/Cultural Desk; Pg. 9

LENGTH: 683 words


9 P.M. (13, 49) PBS SPECIAL REPORT ON HEALTH CARE REFORM Three PBS news programs
band together to parse health care reform in this 90-minute report. ''Now on
PBS'' examines the ways in which reform may change the lives of Americans,
especially baby boomers who have their own coverage but are also responsible for
aging parents and grown children. ''Nightly Business Report'' investigates the
cost of and controversies surrounding employer-provided health care. And ''Tavis
Smiley'' focuses on childhood obesity, particularly among minorities, and offers
solutions to the crisis.

7 A.M. (NBC) TODAY Mackenzie Phillips discusses her memoir, ''High on Arrival,''
and her relationship with her father, John Phillips of the Mamas and the Papas.

8 A.M. (Fox Business Network) G-20 SUMMIT COVERAGE Peter Barnes reports from the
G-20 Summit in Pittsburgh on Thursday and Friday.

11 A.M. (ABC) THE VIEWJoseph Fiennes talks about his new series,
''FlashForward''; the ladies check in on Elisabeth Hasselbeck, who gave birth to
her third child in August.

4 P.M. (ABC); 7 P.M. (55) THE OPRAH WINFREY SHOWJay-Z and Barbra Streisand are
the guests.

8 P.M. (ABC) FLASHFORWARD The world's population glimpses the future when it
blacks out for 2 minutes 17 seconds and then chronicles it -- where else? --
online. Sometimes it's pretty, sometimes it's not. And sometimes it's not there.
Joseph Fiennes, John Cho and Sonya Walger star in this new series.

9 P.M. (CBS) CSI: CRIME SCENE INVESTIGATION Jorja Fox returns as Sara Sidle just
in time to help solve the case of an actress who is killed in a suspicious car
collision.

9 P.M. (National Geographic) 10 THINGS YOU DIDN'T KNOW ABOUT AVALANCHES Iain
Stewart, a Scottish geologist, crosses several mountain ranges and glaciers as
he presents a historic overview of avalanches, including one that wiped out
18,000 people in just a few minutes and another that held the key to an aviation
mystery.

9 P.M. (ABC) GREY'S ANATOMY The hospital staff mourns George's death in this
two-hour Season 6 premiere. Meredith (Ellen Pompeo) and Derek (Patrick Dempsey)
consummate their marriage with increasing frequency, and in new and unusual
locations, while Cristina (Sandra Oh, above) and Owen (Kevin McKidd) abstain
from sex on the advice of Dr. Wyatt (Amy Madigan).

9 P.M. (BBC America) SKINS The friendship between Katie (Megan Prescott) and
Effy (Kaya Scodelario) is fraying, Pandora (Lisa Backwell) is sleeping with both
Cook (Jack O'Connell) and Thomas (Merveille Lukeba), and Emily (Kathryn
Prescott) and Naomi (Lily Loveless) refuse to talk about what happened between
them. How to ease tensions? A drive to a forest to consume mushrooms seems like
a laugh -- until gunshots ring out.

9 P.M. (Food Network) EXTREME CUISINE WITH JEFF CORWIN Mr. Corwin travels to
Morocco, where he fishes for gigantic conger eel off the coast of Essaouira,
milks a camel to make cheese and a rancid butter, and stands in goat dung to dig
for argon nuts to make ''liquid gold.'' Later he hikes up the High Atlas
Mountains to a Berber village, where he cooks a ram in an underground oven.

9 P.M. (NBC) THE OFFICE Toby (Paul Lieberstein) and Dwight (Rainn Wilson, right)
go undercover to investigate the worker's comp claim made by Darryl (Craig
Robinson).

10 P.M. (CBS) THE MENTALIST Patrick Jane (Simon Baker) and Teresa Lisbon (Robin
Tunney) learn that the Red John case has been reassigned to a new team from the
California Bureau of Investigation led by the no-nonsense Sam Bosco (Terry
Kinney).

10 P.M. (Sundance) BRICK CITY The filmmakers Marc Levin and Mark Benjamin and
the executive producer Forest Whitaker continue their examination of the urban
renewal of Newark. In this penultimate episode a circus arrives in town, but
Mayor Cory A. Booker has other shenanigans to deal with as he tries to resolve a
feud between the city's police director and chief. Meanwhile Bloods members
receive some tough news, and Jayda faces old assault charges in court.

10 P.M. (FX) IT'S ALWAYS SUNNY IN PHILADELPHIA The gang takes a road trip to the
Grand Canyon. KATHRYN  SHATTUCK

URL: http://www.nytimes.com

LOAD-DATE: September 24, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS

PUBLICATION-TYPE: Newspaper


