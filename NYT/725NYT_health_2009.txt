                   Copyright 2009 The New York Times Company


                             725 of 2021 DOCUMENTS


                               The New York Times

                            October 1, 2009 Thursday
                              Late Edition - Final

A Race in Cardiology

BYLINE: By BARRY MEIER

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 1034 words


A race is on to develop the potentially next big thing in heart surgery: a
replacement valve that can be implanted through thin tubes known as catheters
rather than by traditional open-heart surgery.

The contest pits two major companies, Edwards Lifesciences and Medtronic.
Analysts estimate a market for the product that could exceed $1.5 billion within
six years. But if the valves catch on, their benefits for the nation's aging
population could be substantial -- even if the impact on the nation's health
care bill may be hard to calculate.

The new valves -- which make it possible to repair the heart without the rigors
of chest-opening surgery -- have been available in Europe for about 18 months,
with sales of about $100 million split about evenly last year between the two
companies. While doctors say that the early results have been promising,  they
add that it is too early to gauge the technology's long-term benefits.

In the United States, the devices are currently being tested in older,
critically ill patients in clinical trials. Many medical experts and financial
analysts predict that if the devices live up to their makers' claims, they could
revolutionize valve replacement,  a common heart operation, and extend the lives
of thousands of frail patients who are not now considered candidates for
surgery.

The development of the new valves makes it clear that even as Congress debates
ways to control the nation's skyrocketing health costs,  medical technology
continues to advance in ways that may drive up health care spending even further
with costly new procedures that can enable more people to live longer.  Most
heart valve replacements involve people of Medicare age.

The race between Edwards and Medtronic involves one of the heart's four valves
-- the aortic valve, which controls blood flow into the aorta from the heart.
Aortic valves were involved in the vast majority of the estimated 95,000
open-heart surgeries in the country last year to replace a diseased valve with
a new one, according to Millennium Research, a consulting group.

An estimated 20,000 people die annually from valve-related disease, including
those too sick to withstand surgery. The new valves from Edwards and Medtronic
would be meant to enable more such patients to have life-saving valve
replacements.

The new units, known as percutaneous heart valves, are implanted through
catheters, in much the same way that artery-opening stents are. A catheter
holding a compressed replacement valve is inserted through a small incision in
the groin or, if blood vessels are blocked, through an opening near the ribs.
The catheter is then snaked through a vessel into the heart and the new valve is
released, essentially compressing the diseased tissue and moving into its place.

But the new technology would not be inexpensive.

The current cost of heart valve replacement surgery is about $50,000, a sum that
includes surgical fees, hospital costs and the price of a replacement valve,
which sells for about $5,000.  By contrast, the new valves currently sell for up
to $30,000. Both Edwards and Medtronic, though, say that the higher prices for
the valves would be offset by lower costs from anticipated shorter hospital
stays.

Edwards, which recently enrolled the last of about 1,000 patients in a major
trial of its valve that is already under way, has a significant head start. Its
device, if successful in that study and subsequently approved by the Food and
Drug Administration, could be on the market in the United States in two years.

Medtronic entered the race only in April, by purchasing CoreValve, a privately
held company, for $700 million. Although the company hopes to start a major
trial next summer, it is still discussing the study's design with the F.D.A.

''It is a two-valve race, with Edwards in the lead in the U.S.,'' said Kristen
Stewart, a medical device industry analyst with Credit Suisse.

If the new valves prove beneficial in the sickest of patients, the next big
question will be whether they should be used in patients who are now considered
suitable candidates for open-heart surgery. Many heart specialists, pointing to
the rapid proliferation of other expensive medical technologies, are urging
caution. They point out that the success rate of conventional open-heart surgery
is extremely high and that the replacement valves now in use are highly durable,
lasting 10 to 15 years.

With only a few years of data on the new valves, it remains uncertain how long
they can last. That is why the studies so far are focusing mainly on people who
are not candidates for conventional surgery.

''We are willing to put in a valve that will not last as long in a patient who
is at high surgical risk,'' said Dr. Robert Guyton, the chief of cardiothoracic
surgery at Emory University in Atlanta, who is involved in the Edwards trial.

He and other doctors also say that the technology of implanting heart valves
through catheters is still highly experimental. While complication rates are
coming down as doctors gain experience, the procedure still can sometimes damage
the heart or cause strokes.

''It's like baseball, you either hit a home run or you strike out,'' Dr. Lars G.
Svensson, a heart surgeon at the Cleveland Clinic who is also participating in
the Edwards trial.

Such risks may matter little to patients suffering from aortic stenosis, or a
narrowing of the valve caused by the hardening of fat deposits. Once patients
begin to experience that condition's symptoms, including shortness of breath and
dizziness, they can die in two or three years if the valve is not replaced.

That was the situation facing Henry Tipton, a 97-year-old retired naval officer
who lives in Sterling, Va. About three years ago, Mr. Tipton's doctor told him
he was too old for open-heart surgery and his outlook was bleak. Then the doctor
heard about the Edwards trial, which was just starting.

''I really had no alternative, so I decided to go ahead and take a chance,''
said Mr. Tipton in a recent telephone interview from Utah where he was traveling
with family members. ''Within one day of the implant of the artificial valve, I
felt completely recovered.''

URL: http://www.nytimes.com

LOAD-DATE: October 1, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Henry Tipton, 97, was told by his doctor that he could not
endure open-heart surgery. So he participated in the trial for a new valve from
Edwards and now feels ''completely recovered.''(PHOTOGRAPH BY PRISCILLA T.
O'DONNELL)(pg. B6)  DRAWINGS: An illustration, above, shows how a new valve from
Edwards Lifesciences is inserted. The valve is shown below.(DRAWINGS BY EDWARDS
LIFESCIENCES)

PUBLICATION-TYPE: Newspaper


