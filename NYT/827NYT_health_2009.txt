                   Copyright 2009 The New York Times Company


                             827 of 2021 DOCUMENTS


                               The New York Times

                            October 16, 2009 Friday
                              Late Edition - Final

Undermining an Argument

BYLINE: By ANDREW POLLACK

SECTION: Section A; Column 0; National Desk; GENERIC DRUGS; Pg. 18

LENGTH: 246 words


One way Congress hopes to lower health care spending is by authorizing
''biosimilars.'' Those, essentially, would be generic versions of biotechnology
drugs, medicines, like the cancer drug Avastin, that currently can cost tens of
thousands of dollars a year.

But a commentary published online this week by The New England Journal of
Medicine says the current provisions in the health care legislation would do
little to reduce prices of such drugs. That is because the new rules would not
allow the copycat drugs to be sold until the original biotechnology drug had
been on the market for 12 years. Such a delay, the authors argue, would
discourage would-be competitors.

The public ''would not get the benefit of the enhanced price competition'' that
generic drugs are meant to offer, according to the authors, who urge that
Congress shorten the market protection for an original drug to five years.

The commentary was written by Dr. Jerry Avorn and Dr. Aaron S. Kesselheim, both
of Harvard, and by Alfred B. Engelberg, a philanthropist who made his fortune as
a lawyer challenging pharmaceutical company patents.

Consumer groups, insurers, labor unions and the Obama administration are
expected to push for a shorter period as the health care legislation moves
forward. They face an uphill battle because the 12-year provision, designed to
let biotech companies recoup their research costs, was approved by big margins
in House and Senate committees. ANDREW POLLACK

URL: http://www.nytimes.com

LOAD-DATE: October 16, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


