                   Copyright 2009 The New York Times Company


                             1254 of 2021 DOCUMENTS


                               The New York Times

                          December 23, 2009 Wednesday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 681 words


International

JORDAN'S KING UNDERCUTS

Prodemocracy Image

In recent days, King Abdullah II of Jordan, who is seen in the West as being
among the most enlightened Middle East leaders, has dismissed the prime
minister, dissolved Parliament and postponed legislative elections. The moves
demonstrate the leadership's continued intention to manipulate and suppress the
political process, former officials and political commentators said. PAGE A8

KYRGYZSTAN LEADER ACCUSED

Opposition politicians in Kyrgyzstan blamed the president, Kurmanbek Bakiyev,
for the death of a prominent opposition journalist, saying that he was
escalating his efforts to eliminate dissent in the country. PAGE A6

National

SENATE LAYS GROUNDWORK

For Final Health Care Vote

The Senate voted 60 to 39 on three steps leading up to a final vote Thursday
morning on sweeping health care legislation. Democrats remained united in favor
of the bill, while Republicans remained united in opposition. PAGE A18

FOCUSING ON LOCAL CONCERNS

Setti Warren will be inaugurated next Friday as the first African-American mayor
of Newton, Mass. And while he does not rule out a run for higher office -- his
fans include President Obama, who called last month to congratulate him, and
Bill Clinton, his former boss -- Mr. Warren must first succeed in prosaic
municipal politics. PAGE A16

New York

STATE REPUBLICANS LOOK

For A-Lister for Senate Bid

The decision by Rudolph W. Giuliani to stay in the private sector, and the
apparent lack of interest on the part of George E. Pataki, leave the Republican
Party in recruiting-and-rebuilding mode precisely when it could use a popular
figure to challenge the vulnerable Senator Kirsten E. Gillibrand in her bid for
election. PAGE A25

Business

STRIPPED FORECLOSURES

Often Stymie the Police

Several states hit hard by the foreclosure crisis have no specific criminal
prohibition against stripping fixtures from a property before foreclosure.
Mortgage contracts prohibit such behavior, but violating those provisions is a
civil matter, which has stymied the police.  PAGE B1

RECOVERY REMAINS SLUGGISH

The fragile housing sector showed signs of firming up, according to a report
from the National Association of Realtors. But a separate report showed that the
recovery was weaker than expected in the third quarter, held back by slow
business construction and dwindling inventories. PAGE B1

OPEC TO MAINTAIN TARGETS

The Organization of the Petroleum Exporting Countries agreed to maintain its
production targets, an expression of satisfaction that prices are high enough to
ensure sufficient revenues but not suppress a nascent recovery. PAGE B3

Sports

IN THE N.F.L., KICKERS

Are in a Slump

A year after field-goal accuracy was the highest in N.F.L. history, and after a
decade of steadily increasing precision, kickers are surprisingly in a slump,
and their woeful outings are coming at the worst time of the season, as teams
are preparing for the playoffs. PAGE B10

Arts

A FILM WHOSE PLOT

Turns on Words

The new Romanian film ''Police, Adjective'' is a story of law enforcement with a
special interest in grammar. Its climactic scene is not a chase or a shootout,
but rather a tense, suspenseful session of dictionary reading. Review by A. O.
Scott. PAGE C1

A GLORIOUS 'MESSIAH'

Anthony Tommasini writes that Handel's ''Messiah'' bears its overexposure
amazingly well. And when presented with musical skill, as it was at Carnegie
Hall by the Musica Sacra Chorus and Orchestra, it emerges once again as a work
of distinction. PAGE C1

Dining

AT LA GRENOUILLE,

Pleasures From the Past

La Grenouille, the last great French restaurant in New York , turned 47 on
Saturday. Sam Sifton writes that the restaurant has kept alive the quiet
opulence of the traditional haute cuisine. PAGE D1

Obituaries

JOHN EDWIN SMITH, 88

A prominent philosopher and author, he tackled large questions in his work about
the nature of truth from a pragmatic, pluralistic and specifically American
perspective.  PAGE B15

Op-ed

THOMAS FRIEDMAN PAGE A27

MAUREEN DOWD PAGE A27

URL: http://www.nytimes.com

LOAD-DATE: December 23, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


