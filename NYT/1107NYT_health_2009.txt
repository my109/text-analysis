                   Copyright 2009 The New York Times Company


                             1107 of 2021 DOCUMENTS


                               The New York Times

                            December 1, 2009 Tuesday
                              Late Edition - Final

Debate on Two Amendments

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 21

LENGTH: 285 words


The Senate opened debate on major health care legislation Monday, including the
first two amendments in what is expected to be a weeks-long blizzard of proposed
changes to the bill.

Under an agreement between Democrats and Republicans, each side was allowed one
amendment Monday, with votes to be held perhaps on Tuesday afternoon.

Senator Barbara A. Mikulski, Democrat of Maryland, proposed enhanced screenings
and other preventive care for women at low or no cost to patients. The proposal
would require insurance companies to offer the services without co-payments, and
Ms. Mikulski said it would cost the federal government about $1 billion over 10
years.

The measure would probably include screenings for cervical cancer and
post-partum depression, as well as annual tests for heart disease and diabetes.
It would also require insurers to provide annual mammograms, even for women
under 50 -- a matter of contention after a federal advisory board recently
recommended against routine mammograms in women younger than 50.

''We don't mandate that you have a mammogram at age 40,'' Ms. Mikulski said.
''But if your doctor says you need one, my amendment says you are going to get
one.''

Senator John McCain of Arizona offered the first Republican amendment: a
proposal to remove provisions intended to slow the rise in government spending
on Medicare. Republicans say those provisions will lead to dangerous cuts in
medical care for Americans over age 65. Democrats say the savings can be
achieved by eliminating wasteful spending.

''Slashing Medicare by nearly $500 billion,'' Mr. McCain said, ''to create a new
federal health care entitlement is not health care reform.''

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: December 1, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


