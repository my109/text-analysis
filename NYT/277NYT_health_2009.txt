                   Copyright 2009 The New York Times Company


                             277 of 2021 DOCUMENTS


                               The New York Times

                            August 8, 2009 Saturday
                              Late Edition - Final

Health Care Hullabaloo

BYLINE: By CHARLES M. BLOW

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 19

LENGTH: 441 words


One of the most frustrating aspects of the health care debate is that the people
who most want reform   are the most apathetic about it.

According to a CNN/Opinion Research poll released on Wednesday, nearly 8 in 10
Democrats said that they favor ''Barack Obama's plan to reform health care,''
compared with just 19 percent of Republicans.

Yet, according to a Gallup poll released last week, only 47 percent of Democrats
said that they had a good understanding of the issues involved in the current
health care debate. Fifty-eight percent of the Republicans polled said that they
understood.

Furthermore, a Pew Research Center poll released on Thursday found that
Democrats were the least likely to say that they were following the debate over
health care reform ''very closely.'' Only 42 percent of Democrats said that they
were, compared with 45 percent of the independents and 56 percent of the
Republicans polled.

And while Democrats are sitting it out, Republicans are storming in.

According to the CNN poll, nearly half of those opposing current plans on health
care reform said that they were very likely to attend a town hall meeting. Only
37 percent of those who support the plans were very likely to attend.

Not only are anti-reformists showing up, they're terrorizing legislators with
their tomfoolery when they do. Blinded by fear and passion, armed with
misinformation and misplaced anger, they descend on these meetings and hoot and
holler in an attempt to shut down the debate rather than add to it.

I must say that this says more about them than it does about any forthcoming
legislation. Belligerence is the currency of the intellectually bankrupt.

Trapped in their vacuum of ideas, too many Republicans continue to display an
astounding ability to believe utter nonsense, even when faced with facts that
contradict it.

A Daily Kos/Research 2000 poll released last Friday found that 28 percent of
Republicans don't believe that Barack Obama was born in the United States and
another 30 percent are still ''not sure.'' That's nearly 6 out of 10 Republicans
refusing to accept a basic truth. Then again, this shouldn't surprise me.
According to a Gallup poll released last summer, 6 in 10 Republicans also said
they thought that  humans were created, in their present form, 10,000 years ago.

Let's face it: This is no party of Einsteins. Really, it isn't. A Pew poll last
month found that only 6 percent of scientists said that they were Republicans.

Democrats should be leading this discussion. Instead, they're losing control of
it. That's unfortunate because the debate is too important to be hijacked by
hooligans.

URL: http://www.nytimes.com

LOAD-DATE: August 8, 2009

LANGUAGE: ENGLISH

GRAPHIC: CHART: How closely are people following the health care debate?
(Source: The Pew Research Center for the People and the Press)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


