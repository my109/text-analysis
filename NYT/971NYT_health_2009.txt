                   Copyright 2009 The New York Times Company


                             971 of 2021 DOCUMENTS


                               The New York Times

                           November 10, 2009 Tuesday
                              Late Edition - Final

The Ban on Abortion Coverage

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 34

LENGTH: 608 words


When the House narrowly passed the health care reform bill on Saturday night, it
came with a steep price for women's reproductive rights. Under pressure from
anti-abortion Democrats and the United States Conference of Catholic Bishops,
lawmakers added language that would prevent millions of Americans from buying
insurance that covers abortions -- even if they use their own money.

The restrictions would fall on women eligible to buy coverage on new health
insurance exchanges. They are a sharp departure from current practice, an
infringement of a woman's right to get a legal medical procedure and an
unjustified intrusion by Congress into decisions best made by patients and
doctors.

The anti-abortion Democrats behind this coup insisted that they were simply
adhering to the so-called Hyde Amendment, which bans the use of federal dollars
to pay for almost all abortions in a number of government programs. In fact,
they reached far beyond Hyde and made it largely impossible to use a
policyholder's own dollars to pay for abortion coverage.

The bill brought to the floor already included a careful compromise that should
have satisfied reasonable legislators on both sides of the abortion issue. The
vast majority of people expected to buy policies on the new exchanges would pay
part of the premium and receive government tax credits to pay for the rest. The
compromise would have prohibited the use of the tax subsidies to pay for almost
all abortions, but it would have allowed the segregation and use of premium
contributions and co-payments to pay for such coverage. A similar approach
allows 17 state Medicaid programs to cover abortions using only state funds, not
federal matching funds.

Yet neither the Roman Catholic bishops nor anti-abortion Democrats were willing
to accept this compromise. They insisted on language that would ban the use of
federal subsidies to pay for ''any part'' of a policy that includes abortion
coverage.

If insurers want to attract subsidized customers, who will be the great majority
on the exchange, they will have to offer them plans that don't cover abortions.
It is theoretically possible that insurers could offer plans aimed only at
nonsubsidized customers, but it is highly uncertain that they will find it
worthwhile to do so.

In that case, some women who have coverage for abortion services through
policies bought by small employers could actually lose that coverage if their
employer decides to transfer its workers to the exchange. Ultimately, if larger
employers are permitted to make use of the exchange, ever larger numbers of
women might lose abortion coverage that they now have.

The restrictive language allows people to buy ''riders'' that would cover
abortions. But nobody plans to have an unplanned pregnancy, so this concession
is meaningless. It is not clear that insurers would even offer the riders since
few people would buy them.

The highly restrictive language was easily approved by a 240-to-194 vote and
incorporated into the overall bill, which squeaked through by a tally of 220 to
215. It was depressing evidence of the power of anti-abortion forces to override
a reasonable compromise. They were willing to scuttle the bill if they didn't
get their way. Outraged legislators who support abortion rights could also have
killed the bill but sensibly chose to keep the reform process moving ahead.

The fight will resume in the Senate, where the Finance Committee has approved a
bill that incorporates the compromise just rejected by the House. We urge the
Senate to stand strong behind a compromise that would preserve a woman's right
to abortion services.

URL: http://www.nytimes.com

LOAD-DATE: November 10, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


