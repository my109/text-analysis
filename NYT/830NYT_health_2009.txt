                   Copyright 2009 The New York Times Company


                             830 of 2021 DOCUMENTS


                               The New York Times

                            October 16, 2009 Friday
                              Late Edition - Final

In Saying No, G.O.P. Sees More Pros Than Cons

BYLINE: By JACKIE CALMES

SECTION: Section A; Column 0; National Desk; POLITICAL MEMO; Pg. 18

LENGTH: 1007 words

DATELINE: WASHINGTON


The numbers are striking: Of the 217 Republicans in the House and the Senate,
only one, Senator Olympia J. Snowe of Maine, has publicly supported a health
care overhaul along the lines President Obama seeks.

The Republicans' opposition is a remarkable display of the unity emerging
against the broader Obama agenda as a dangerous expansion of government. That
stance is popular with, even demanded by, the party's narrowed conservative
base.

But it also exposes Republicans to criticism that they have become political
obstructionists with no policy agenda of their own. And that could keep them
from extending their appeal to the centrist voters who are essential to
rebuilding the party's strength nationally.

Republicans' naysaying on health care, after their nearly unanimous opposition
to Mr. Obama's economic stimulus package, has already drawn rare rebukes from an
array of prominent party figures outside Capitol Hill, who say the party should
be for something, not just against. Among the critics have been three former
Senate Republican leaders: Bob Dole, Bill Frist and Howard H. Baker Jr.

Congressional Republicans, however, are certain that the politics are on their
side. Dismissing Democrats' attacks on them as ''the party of no,'' they point
to polls and other signs indicating that high unemployment and deficits have
created vast unease with Mr. Obama's agenda as the 2010 midterm elections
approach.

''We're the party of know: k-n-o-w,'' said Representative Pete Sessions of
Texas, chairman of House Republicans' campaign committee.

''We know a lot about the Democrats' plans, and we think it's a bad way to go,''
Mr. Sessions added. ''Theirs is about taxing and spending and destroying jobs.''

Some other party strategists are optimistic about the prospect of Republican
gains next year, perhaps enough gains to reclaim a House majority.

''I just don't think that there's a downside to voting no -- I really don't,''
said Vin Weber, a former Republican congressman from Minnesota. ''That's quite
aside from whether you should or shouldn't, or whether the country needs it or
doesn't need it. The basic rule is you rarely pay a price at the polls for being
against something.''

Republican incumbents ''have far more to lose,'' he  said, ''by having the
Republican base conclude that they're just throwing in the towel and
compromising on a big-government agenda.''

After recent defeats, Republicans are down to 40 members in the Senate and 177
in the House, or 40 percent in each chamber. They are largely reduced to the
party's base of mostly Southern and rural states and beholden both to the
conservative activists there and to the cable television celebrities those
activists follow.

Few centrists remain. And since many centrists have been defeated by
conservatives in party primaries, the survivors -- or any Republicans
considering compromise -- operate in fear of similar challenges.

Senator Arlen Specter of Pennsylvania switched parties this year to avoid a
Republican primary fight. In Utah, Senator Robert F. Bennett is unexpectedly
facing a strong intraparty challenge, largely because he sponsored a bipartisan
health plan.

Associates say Charles E. Grassley of Iowa, the Senate Finance Committee's
senior Republican, who is also up for re-election,  might have cut a health
deal with the committee chairman, Senator Max Baucus of Montana, a past ally,
had Mr. Grassley not been unnerved by conservatives' warnings back home.

Even Mr. Dole, one of the Republicans recently critical of their own party, said
of his friend Mr. Grassley, ''You can't expect him to commit political
suicide.''

While Mr. Grassley has denied that politics plays a role, his opposition to
health care proposals has helped bolster his support among Iowa Republicans,
according to a recent poll for The Des Moines Register. Yet his longtime
popularity among Democrats and independents dropped by 24 and 11 percentage
points, respectively, compared with the results from a poll in April.

Last week Mr. Dole and Tom Daschle, a former Senate Democratic leader, issued a
statement applauding Congress's progress on  health care.  The two men, along
with Mr. Baker, another of the onetime Senate Republican leaders, had proposed a
health plan of their own as a means of encouraging bipartisanship at the
Capitol, but Mr. Baker declined to join in the statement after a current
Republican senator urged him not to. Mr. Dole said that he too had gotten a
call, his from Senator Mitch McConnell of Kentucky, the current Republican
leader, but that he had not returned it.

National polls hold some warnings for Republicans. One, by CBS News last week,
found that 69 percent of Americans say the party is not serious about health
care reform. Another, for the nonpartisan Pew Research Center, found that even
62 percent of the Democratic initiative's opponents say Republicans should try
to improve it, not kill it. That finding ''implies that there might not be a big
political windfall'' for Republicans in opposing the measure, said Andrew Kohut,
president of the Pew center.

More important than national polls, Mr. Weber countered, are polls in states
where Republicans hope to regain ground next year -- in the South, the rural
Midwest and the Mountain West -- that he said were much more supportive.

Republicans say their hostility to Mr. Obama on health care is no different from
Democrats' opposition in 2005 to President George W. Bush's proposal to partly
privatize Social Security. But Republicans, who had majorities in Congress then,
were unsupportive as well in that case. Further, while most Democrats disagreed
with Mr. Bush's view that Social Security faced a financial crisis, both parties
agree that the health care system needs overhaul.

Republicans ''all stand up and say they're for health care reforms, so why don't
they do something about it?'' Mr. Dole said. He said he held out hope that they
would, ''because I don't believe they could absorb just across-the-board being
against everything.''

URL: http://www.nytimes.com

LOAD-DATE: October 16, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


