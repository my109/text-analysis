                   Copyright 2009 The New York Times Company


                             113 of 2021 DOCUMENTS


                               The New York Times

                              July 6, 2009 Monday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 693 words


International

OBAMA RAISES CONCERNS

About Freedoms in Russia

Before his trip to Moscow, intended to  repair strained relations with Russia,
President Obama vowed not to sacrifice American support for greater freedom in
Russia and questioned the politically charged prosecution of a prominent Russian
businessman. PAGE A8

INFLUENCING IRAQI POLITICS

With most U.S. troops now on large bases outside the cities, America's
day-to-day involvement in Iraqi life has vanished. And behind the high walls of
the American Embassy in Baghdad, diplomats are casting about to find a new
formula to influence politics in Iraq.  PAGE A4

MEXICO'S MIDTERM VOTE

In a climate of frustration with politics-as-usual, Mexicans voted Sunday in
midterm elections that are likely to turn on President Felipe Calderon's
handling of the global economic crisis and his crackdown on drug trafficking
cartels.  PAGE A7

National

MAINERS LEAN ON SENATORS

In Health Care Debate

In Maine, there have been widespread and intense efforts to sway the votes of
Senators Olympia Snowe and Susan Collins, moderate Republicans who could provide
crucial support for the Democratic health care plan. PAGE A10

EXPLAINING PALIN DECISION

Lt. Gov. Sean Parnell of Alaska said that Gov. Sarah Palin's decision to resign
was largely prompted by the personal legal costs of the ethics investigations
against her. PAGE A10

Washington Ex-Mayor Arrested A11

New York

STATE TO INCREASE

Welfare Subsidies

New York City's welfare recipients will see an increase in the subsidy for a
typical family of three to $321 a month, from $291, city and state officials
said.  PAGE A14

Business

AN 'ARMS RACE'

For Internet Security

The competition between Symantec and McAfee, the two leading makers of computer
security software, has become fiercer as both companies vie for millions of
global followers willing to donate a steady sum every year for protection
against online threats.   PAGE B1

REVISITING THE LIGHT BULB

A law that set tough efficiency standards that no traditional incandescent bulb
on the market could meet has inadvertently breathed new innovation into Thomas
Edison's light bulb.  PAGE B1

POST APOLOGIZES FOR 'SALONS'

The publisher of The Washington Post apologized to the paper's readers after a
controversy erupted over the company's plans to organize ''salons,'' or
sponsored meetings with influential Washington officials and the paper's
reporters.  PAGE B2

CURIOSITY ABOUT APPLE CHIEF

Is prurience, and not legitimate financial concerns, driving most people's
interest in the health of Steve Jobs of Apple? David Carr, The Media Equation.
PAGE B1

Britons Wary of ESPN B4

Chrysler Fills Rest of Board B2

Sports

THE LIFE AND DEATH

Of Steve McNair

Most Tennessee Titans fans, or National Football League fans, would never have
guessed that Steve McNair's life would end like this: violently, under
compromising circumstances. William C. Rhoden, Sports of The Times.  PAGE D1

A GREAT FIND IN PANAMA

Twenty years ago, Herb Raybourn scouted a young shortstop in Panama named
Mariano Rivera, and felt that he had the mechanics to be a productive major
league pitcher. PAGE D3

CHALLENGING THE BEST

In the Wimbledon men's final, Andy Roddick finally attached himself to Roger
Federer as an opponent to remember. Harvey Araton, Sports of The Times. PAGE D7

6 Red Sox in All-Star Game D2

Arts

ABSOLUTELY FREE,

If You Can Afford It

Ellen Ruppel Shell's ''Cheap'' and Chris Anderson's ''Free'' are both books
about how and why people respond to low prices, but they arrive at very
different conclusions. Review by Janet Maslin.  PAGE C1

A TIGHT BOX OFFICE BATTLE

''Ice Age: Dawn of the Dinosaurs'' was in a virtual tie for first place at the
weekend box office with ''Transformers: Revenge of the Fallen'' -- each sold an
estimated $43 million in tickets over the three-day weekend. PAGE C1

Review: Boogie Rican Blvd. C2

Obituaries

JOE BOWMAN, 84

Known as the Straight Shooter and the Master of Triggernometry, he dazzled
audiences with his fancy gunplay and sharpshooting with pistol and rifle. PAGE
B9

Op-ed

PAUL KRUGMAN PAGE A19

ROSS DOUTHAT PAGE A19

URL: http://www.nytimes.com

LOAD-DATE: July 6, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


