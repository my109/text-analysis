                   Copyright 2009 The New York Times Company


                              91 of 2021 DOCUMENTS


                               The New York Times

                              June 28, 2009 Sunday
                              Late Edition - Final

The Pitfalls Of the Public Option

BYLINE: By N. GREGORY MANKIW.

N. Gregory Mankiw is a professor of economics at Harvard. He was an adviser to
President George W. Bush.

SECTION: Section BU; Column 0; Money and Business/Financial Desk; ECONOMIC VIEW;
Pg. 5

LENGTH: 958 words


IN the debate over health care reform, one issue looms large: whether to have a
public option. Should all Americans have the opportunity to sign up for
government-run health insurance?

President Obama has made his own preferences clear. In a letter to Senators
Edward M. Kennedy of Massachusetts and Max Baucus of Montana, the chairmen of
two key Senate committees, he wrote: ''I strongly believe that Americans should
have the choice of a public health insurance option operating alongside private
plans. This will give them a better range of choices, make the health care
market more competitive, and keep insurance companies honest.''

Even if one accepts the president's broader goals of wider access to health care
and cost containment, his economic logic regarding the public option is hard to
follow. Consumer choice and honest competition are indeed the foundation of a
successful market system, but they are usually achieved without a public
provider. We don't need government-run grocery stores or government-run gas
stations to ensure that Americans can buy food and fuel at reasonable prices.

An important question about any public provider of health insurance is whether
it would have access to taxpayer funds. If not, the public plan would have to
stand on its own financially, as private plans do, covering all expenses with
premiums from those who signed up for it.

But if such a plan were desirable and feasible, nothing would stop someone from
setting it up right now. In essence, a public plan without taxpayer support
would be yet another nonprofit company offering health insurance. The
fundamental viability of the enterprise does not depend on whether the employees
are called ''nonprofit administrators'' or ''civil servants.''

In practice, however, if a public option is available, it will probably enjoy
taxpayer subsidies. Indeed, even if the initial legislation rejected them, such
subsidies would be hard to avoid in the long run. Fannie Mae and Freddie Mac,
the mortgage giants created by federal law, were once private companies. Yet
many investors believed -- correctly, as it turned out -- that the federal
government would stand behind Fannie's and Freddie's debts, and this perception
gave these companies access to cheap credit. Similarly, a public health
insurance plan would enjoy the presumption of a government backstop.

Such explicit or implicit subsidies would prevent a public plan from providing
honest competition for private suppliers of health insurance. Instead, the
public plan would likely undercut private firms and get an undue share of the
market.

President Obama might not be disappointed if that turned out to be the case.
During the presidential campaign, he said, ''If I were designing a system from
scratch, I would probably go ahead with a single-payer system.''

Of course, we are not starting from scratch. Because many Americans are happy
with their current health care, moving immediately to a single-payer system is
too radical a change to be politically tenable. But for those who see
single-payer as the ideal, a public option that uses taxpayer funds to tilt the
playing field may be an attractive second best. If the subsidies are big enough,
over time more and more consumers will be induced to switch.

Which raises the question: Would the existence of a dominant government provider
of health insurance be good or bad?

It is natural to be skeptical. The largest existing public health programs --
Medicare and Medicaid -- are the main reason that the government's long-term
finances are in shambles. True, Medicare's administrative costs are low, but it
is easy to keep those costs contained when a system merely writes checks without
expending the resources to control wasteful medical spending.

A dominant government insurer, however, could potentially keep costs down by
squeezing the suppliers of health care. This cost control works not by fostering
honest competition but by thwarting it.

Recall a basic lesson of economics: A market participant with a dominant
position can influence prices in a way that a small, competitive player cannot.
A monopoly -- a seller without competitors -- can profitably raise the price of
its product above the competitive level by reducing the quantity it supplies to
the market. Similarly, a monopsony -- a buyer without competitors -- can reduce
the price it pays below the competitive level by reducing the quantity it
demands.

This lesson applies directly to the market for health care. If the government
has a dominant role in buying the services of doctors and other health care
providers, it can force prices down. Once the government is virtually the only
game in town, health care providers will have little choice but to take whatever
they can get. It is no wonder that the American Medical Association opposes the
public option.

To be sure, squeezing suppliers would have unpleasant side effects. Over time,
society would end up with fewer doctors and other health care workers. The
reduced quantity of services would somehow need to be rationed among competing
demands. Such rationing is unlikely to work well.

FAIRNESS is in the eye of the beholder, but nothing about a government-run
health care system strikes me as fair. Squeezing providers would save the rest
of us money, but so would a special tax levied only on health care workers, and
that is manifestly inequitable.

In the end, it would be a mistake to expect too much from health insurance
reform. A competitive system of private insurers, lightly regulated to ensure
that the market works well, would offer Americans the best health care at the
best prices.

The health care of the future won't come cheap, but a public option won't make
it better.

URL: http://www.nytimes.com

LOAD-DATE: June 28, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID G. KLEIN)

PUBLICATION-TYPE: Newspaper


