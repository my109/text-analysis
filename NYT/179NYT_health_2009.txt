                   Copyright 2009 The New York Times Company


                             179 of 2021 DOCUMENTS


                               The New York Times

                              July 20, 2009 Monday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1111 words


International

NETANYAHU'S TALK OF PEACE

Finds Few Believers

In the weeks since Israel's prime minister, Benjamin Netanyahu, finally accepted
the principle of a Palestinian state, with qualifications, there has been deep
skepticism about his sincerity. PAGE A9

16 DIE IN HELICOPTER CRASH

Sixteen civilians were killed when a transport helicopter working for the
NATO-led military coalition in Afghanistan crashed as it tried to take off from
the military base at Kandahar. PAGE A4

ISRAEL REJECTS U.S. CALL

Prime Minister Benjamin Netanyahu of Israel rejected an American call to hold
off on a planned Jewish housing development in East Jerusalem, saying Israel's
sovereignty over the disputed city could not be challenged. PAGE A9

HONDURAS TALKS STALL

Talks between negotiators for Manuel Zelaya, the deposed Honduran president, and
the de facto government that exiled him resumed for a second day, but faltered
as the government refused to give ground on restoring him to power. PAGE A8

CHINA MAY EASE CHARGES

Australia's foreign minister said that China's investigation into whether four
employees of the British-Australian mining giant Rio Tinto stole state documents
may be treated as a commercial case rather than an espionage case. PAGE A9

COUP LEADER WINS ELECTION

Nearly a year after seizing power in a military coup that ousted Mauritania's
first freely elected leader, Mohamed Ould Abdel Aziz won the presidency on
Sunday in a vote his opponents denounced as a fraudulent ''electoral coup.''
PAGE A6

National

HEALTH BILL MIGHT DIRECT

Tax Money to Abortion

An Obama administration official refused to rule out the possibility that
federal tax money might be used to pay for abortions under proposed health care
legislation. PAGE A10

THE HEALTH CARE STRUGGLE

The question facing Senate Democrats and the White House is whether to push
their health care legislation on a party-line vote, or seek Republican
cooperation. PAGE A11

NEW USE FOR FOOD STAMPS

The federal government has made hundreds of thousands of terminals available for
electronic transfer of benefits at farmers' markets, making it easier to use
food stamps. PAGE A10

New York

DEMOCRATS FIRE BACK

At Bloomberg Over Schools

In the increasingly acrimonious battle over mayoral control of New York City's
public schools, 10 senators, all of them Democrats, held an hourlong news
conference, ostensibly to demand that Mayor Michael R. Bloomberg make some
concessions before they consider extending his control. PAGE A13

Metropolitan Diary A14

Business

CIT NEARS LOAN DEAL

The CIT Group, one of the nation's leading lenders to small and midsize
businesses, was close to a deal with some of its major bondholders to help it
avert a bankruptcy filing through a $3 billion emergency loan, according to
people briefed on the matter. PAGE B1

TAKING CELLPHONES GLOBAL

Despite years of dabbling in overseas markets, Japan's cellphone makers have
little presence beyond the country's shores. Some of the country's best minds in
the field of handheld electronics have come together to debate how Japanese
cellphones can go global. PAGE B1

REDUCING HIGH-TECH ARMS

The Obama administration began scaling back the Future Weapons Systems program,
an ambitious project to develop networks of high-tech vehicles, drones and
robotic sensors, as part of a broader effort to overhaul expensive weapons
contracts and focus more on fighting insurgencies. PAGE B1

CRONKITE'S VOICE, OFF THE AIR

A recording of Walter Cronkite's deep Midwestern voice announcing the ''CBS
Evening News With Katie Couric,'' was retired by network executives, who
concluded that they would stop playing the voice-over when he died. PAGE B6

PROMISING TRIAL FOR DRUG

A medicine to treat lupus has proved effective in a large clinical trial, which
could pave the way for approval of the first new treatment for the disease in
more than 40 years. PAGE B3

MAKING YOUR OWN MAGAZINE

A start-up called Printcasting has a new twist on print publishing that lets
readers pick which articles they want in their magazine and then print it
themselves. PAGE B7

CHANGES AT INTERVIEW

Interview magazine, searching for stability after a year and a half of turnover
and power struggles, will announce this week that it has a new president and a
new editor in chief. PAGE B6

Sports

A HANGOUT FOR GAY ATHLETES

At the Olympics

At every Olympics, there are temporary, carefully designed hangouts for athletes
and officials from various countries. Pride House at the coming Vancouver
Olympics is believed to be the first such house for gay and lesbian athletes and
their friends and families. PAGE D1

REMEMBERING A BIG UPSET

The old boys from the United States Military Academy brought back their coach,
Paul Dietzel, and his wife, Anne, to West Point to commemorate one of the great
upsets of college football -- Army 11, Navy 8 -- in November 1964. George
Vecsey, Sports of the Times. PAGE D2

BROTHERS AS PRO ATHLETES

Toney and Harry Douglas are set to become only the sixth set of brothers to play
in the N.B.A. and N.F.L. PAGE D2

Arts

A FONT OF FACTS,

But a Desert for Photos

Few high-quality photographs, particularly of celebrities, make it onto
Wikipedia. This is because the site runs only pictures with the most permissive
Creative Commons license, which allows anyone to use an image, for commercial
purposes or not, as long as the photographer is credited. PAGE C1

LIKE 'TOP CHEF,' BUT FOR ART

A new Bravo reality show will try to do for the contemporary art world what the
cable channel has done for the worlds of fine cuisine and fashion: Discover
unknowns with the talent to command the attention of both a television audiences
and a serious audience in the creative field to which they aspire. PAGE C1

ENDING PROM'S SEGREGATION

The HBO documentary ''Prom Night in Mississippi'' takes an anthropological look
at a high school in Charleston, Miss., where the senior prom had been segregated
since 1970, but was integrated last year. PAGE C3

A PARKING LOT RAVE

Each summer the Warped Tour traverses the country, surviving through big-tent
optimism and style agnosticism. But 2009 will be remembered as the year the
Warped Tour transmogrified into a rave. Review by Jon Caramanica. PAGE c3

CELEBRATING MANDELA

Dozens of musicians attended Mandela Day, a benefit concert at Radio City Music
Hall for 46664, Mr. Mandela's organization for AIDS and H.I.V. prevention.
Review by Jon Pareles. PAGE C5

Obituaries

HENRY ALLINGHAM, 113

One of Britain's last three surviving veterans of World War I and an iconic
figure to many in Britain, he was officially recognized as Britain's oldest man.
PAGE A14

URL: http://www.nytimes.com

LOAD-DATE: July 20, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


