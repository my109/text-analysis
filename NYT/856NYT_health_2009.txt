                   Copyright 2009 The New York Times Company


                             856 of 2021 DOCUMENTS


                               The New York Times

                            October 20, 2009 Tuesday
                              Late Edition - Final

1,502 Pages of Details

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; SPECIFIC LANGUAGE; Pg. 22

LENGTH: 233 words


The Senate Finance Committee posted the full version of its health care bill on
the committee Web site on Monday. (A link is provided on the Prescriptions
blog.) The legislation provides a blueprint for ''affordable, quality health
care for all Americans'' and for ''other purposes.''

And at 1,502 pages, there are more than a few other purposes.

But a quick skim reveals few surprises, and the ending is not particularly
explosive. Just some fairly indecipherable legalese setting forth the effective
date of an obscure tax provision related to ''loans for qualified investments in
therapeutic discovery projects in lieu of tax credits.''

Senate Democratic leaders are now working to combine this bill -- possibly by
the end of the week -- with an 839-page health measure that the Health,
Education, Labor and Pensions Committee approved in July.

Once the two Senate bills are combined, the completed legislative language will
let the Congressional Budget Office develop a more precise cost estimate of the
legislation. The Finance Committee bill was projected at $829 billion over 10
years, while the health committee's bill has not yet had a full work-up by the
budget office.

For those interested in tracking the Finance Committee bill's status, it is
available through the Library of Congress at thomas.loc.gov. Look for S. 1796,
the America's Healthy Future Act.

DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: October 20, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING

PUBLICATION-TYPE: Newspaper


