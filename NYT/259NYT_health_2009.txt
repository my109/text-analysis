                   Copyright 2009 The New York Times Company


                             259 of 2021 DOCUMENTS


                               The New York Times

                             August 4, 2009 Tuesday
                              Late Edition - Final

Speculation Prompts Obama to Renew Vow of No Tax Increase on Middle Class

BYLINE: By PETER BAKER

SECTION: Section A; Column 0; National Desk; Pg. 12

LENGTH: 508 words

DATELINE: WASHINGTON


The White House tried Monday to douse speculation that it might raise taxes on
the middle class in violation of President Obama's campaign promise, just a day
after two of his top economic advisers left the door open to such a move to rein
in spiraling deficits.

Mr. Obama told his economic team in a meeting at the White House that he
intended to stand by his promise not to increase taxes on families making less
than $250,000, aides said. He then sent his spokesman out to repeat that message
in front of the television cameras.

''The president made a commitment in the campaign. He's clear about that
commitment, and he's going to keep it,'' said Robert Gibbs, the White House
press secretary.

The renewal of the promise came a day after Treasury Secretary Timothy F.
Geithner and Lawrence H. Summers, director of the National Economic Council,
both refused to rule out tax increases on the middle class  while discussing
ways to pare the deficit. The two were speaking on separate Sunday morning talk
shows, venues where administration officials are usually well prepared on the
official line before appearing.

''It's never a good idea to absolutely rule things out no matter what,'' Mr.
Summers said on ''Face the Nation'' on CBS. Mr. Geithner, on ABC's ''This Week
With George Stephanopoulos,'' said, ''We can't make these judgments yet about
exactly what it's going to take'' to tame the deficit.

Conservative critics interpreted those comments as laying the groundwork for
trying to wriggle out of Mr. Obama's campaign pledge.

''Obama should fire Geithner and Summers,'' said Grover Norquist, president of
Americans for Tax Reform, a group that opposes tax increases. They ''went on
national television and implied the president lied his way into office and that
he is open to raising taxes.''

The developments come at a time when the White House and Congressional
Democrats, trying to figure out how to pay for expanding health care coverage,
are considering proposals to increase taxes on the wealthiest Americans. Some
critics from the left have suggested that Mr. Obama should not limit tax
increases to the rich so that a broader cross section of Americans would be
invested in the new health care system, as they are in Social Security and
Medicare.

But the White House is trying to fend off attacks portraying Mr. Obama  as a
tax-and-spend liberal. Mr. Gibbs said that he had read the transcripts from
Sunday's shows ''a few times'' to study what had been  said and that the
president had made a point of reminding Mr. Geithner and Mr. Summers of his
position, but was not scolding them.

''We talked about it as an issue,'' Mr. Gibbs said, but added, ''This wasn't a,
you know, like 'school is in' type of thing.''

Mr. Gibbs seemed exasperated at repeated questions on the matter at his daily
briefing.

''If you don't trust what I'm going to tell you, I don't know why we do this,''
he said finally.

Asked why Mr. Geithner and Mr. Summers did not repeat the president's campaign
promise, he said, ''They left it to me.''

URL: http://www.nytimes.com

LOAD-DATE: August 4, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


