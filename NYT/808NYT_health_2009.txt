                   Copyright 2009 The New York Times Company


                             808 of 2021 DOCUMENTS


                               The New York Times

                            October 13, 2009 Tuesday
                              Late Edition - Final

A Preview of the Floor Fight

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 20

LENGTH: 275 words


It is pick-'em odds on Senator Olympia J. Snowe, Republican of Maine, when the
Senate Finance Committee meets Tuesday to vote on health care legislation.

Ms. Snowe, the only Republican who has expressed willingness to support the
bill, has not offered any clues on whether she will vote for or against. Even
Washington's most seasoned political bookmakers are not venturing a guess.

Because Democrats hold a 13-to-10 majority on the panel, there is no question
that the bill is headed to the Senate floor. Yet a number of other interesting
details bear watching before the vote.

The committee, which plans to convene at 10 a.m., will let senators quiz the
director of the Congressional Budget Office, Douglas W. Elmendorf, and the chief
of staff of the Joint Committee on Taxation, Thomas A. Barthold, on their cost
analysis of the bill.

The Q-and-A session may be wonky, but senators will try to elicit answers to
help build their case, for or against the legislation, with the American public.

Also listen carefully to the committee's top Republican, Senator Charles E.
Grassley of Iowa, for insight into some of the more serious G.O.P. reservations.
And pay attention to Senator Jon Kyl of Arizona, the voice of the Republican
leadership on the committee, for a preview of the floor fight that lies ahead.

On the Democratic side, watch for clues that liberals are looking at a
compromise on a government-run insurance plan once the bill reaches the floor.
Take note, too, when Senator Ron Wyden, Democrat of Oregon, talks about the need
to give Americans more choices. Experts and the public seem to agree with him.
DAVID M. HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: October 13, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


