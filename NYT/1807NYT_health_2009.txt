                   Copyright 2010 The New York Times Company


                             1807 of 2021 DOCUMENTS


                               The New York Times

                             March 30, 2010 Tuesday
                              Late Edition - Final

Corrections

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 77 words


A Congressional Memo article on Thursday about the decision by Democrats to pass
a health care bill that did not include the government-run insurance plan known
as the public option misstated the number of new customers that private health
insurance companies are expected to gain under the new law. It is 16 million,
not 30 million. (The Congressional Budget Office estimates that about 30 million
to 32 million uninsured people will be covered by the new law.)

URL: http://www.nytimes.com

LOAD-DATE: March 30, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Correction

PUBLICATION-TYPE: Newspaper


