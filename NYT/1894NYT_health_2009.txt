                   Copyright 2010 The New York Times Company


                             1894 of 2021 DOCUMENTS


                               The New York Times

                            April 17, 2010 Saturday
                              Late Edition - Final

Call and Response on the State of the Black Church

BYLINE: By SAMUEL G. FREEDMAN.

E-mail: sgfreedman@nytimes.com

SECTION: Section A; Column 0; National Desk; ON RELIGION; Pg. 12

LENGTH: 1063 words


In the first decade of the American nation, a former slave turned itinerant
minister by the name of Richard Allen found himself preaching to a growing
number of blacks in Philadelphia. He came to both a religious and organizational
revelation. ''I saw the necessity,'' he later wrote, ''of erecting a place of
worship for the colored people.''

Allen's inspiration ultimately took the forms of Bethel African Church, founded
in 1794, and the African Methodist Episcopal denomination, established in 1799.
As much as it can be dated to anything, the emergence of a formal
African-American Christianity can be dated to Allen's twin creations.

Over more than two centuries since then, the Black Church has become a proper
noun, a fixture, a seeming monolith in American society. Its presence is as
prevalent as film clips of the Rev. Dr. Martin Luther King Jr. delivering the
''I Have a Dream'' speech and contestants on ''American Idol'' indulging in the
gospel style of melisma.

In the conventional wisdom that accompanied the popular imagery, the Black
Church was regarded by insider and outsider, by ally and opponent, as a fount of
progressive politics expressed through the prophetic tradition of Moses, Amos,
Isaiah and Jesus.

Now a young scholar has taken a rhetorical wrecking ball to the monolith, and
the reverberations are rippling through religious and academic circles of
African-Americans. To mix the metaphor, the broader public has been allowed to
eavesdrop on the theological equivalent of a black barbershop, a place of
glorious disputation that is usually kept out of white earshot.

The debate took off in February when The Huffington Post published an essay by
Eddie S. Glaude Jr. , a professor of religion at Princeton, under the
deliberately provocative headline ''The Black Church Is Dead.''

''When I came up with the title, I said, 'Lord, what am I doing?' '' Professor
Glaude, 41, recalled in a telephone interview this week. ''And as I was thinking
that, I hit the send key. With the understanding that I would be in the
firestorm.''

Early in the obituary, Professor Glaude declared, ''The idea of this venerable
institution as central to black life and as a repository for the social and
moral conscience of the nation has all but disappeared.'' He added later, ''The
idea of a black church standing at the center of all that takes place in a
community has long since passed away.''

Professor Glaude argued that many black churches espouse conservative politics,
especially on social issues, and have failed to address current liberal causes
like health care reform. Ministries devoted to self-help or the so-called health
and wealth gospel, some led by whites, draw black followers.

In large measure, Professor Glaude explained in the interview, he wrote the
essay in response to two recent developments. The election of Barack Obama, a
black Christian, as president has complicated if not blunted the black church's
traditional role of confronting the establishment, ''speaking truth to power.''
The social conservatism of some black churches meanwhile figured prominently in
the ballot measure against same-sex marriage in California and an anti-abortion
billboard campaign in the Atlanta area.

There have been internal criticisms of the black church almost as long as there
has been a black church. A 19th-century bishop of the A.M.E. denomination
scorned call-and-response praise songs as ''cornfield ditties.'' E. Franklin
Frazier, the eminent black sociologist, depicted the church as an obstacle to
assimilation. Malcolm X ridiculed Christianity as ''the white man's religion.''

All those precedents notwithstanding, Professor Glaude's jeremiad brought on the
predicted firestorm. A panel of leading scholars of African-American religion
published responses on the Religion Dispatches Web site. Professor Glaude
debated another one of his peers, Prof. Josef Sorett of Columbia University, on
bloggingheads.tv. Private e-mail accounts sizzled with contention.

And only some of the criticism dealt with Professor Glaude's thesis. A fair
amount assailed his very right to criticize. As a born Roman Catholic without a
church membership presently, and as a faculty member in a privileged university,
Professor Glaude was vulnerable to attack from the mainstream of working-class,
African-American Protestants.

''I am sick and tired,'' went an e-mail message from the Rev. Dr. J. Alfred
Smith Sr., pastor emeritus of Allen Temple Baptist Church in Oakland, Calif.,
''of black academics who are paid by rich, powerful ivy league schools, who have
access to the microphone and the ear of the press pontificating about the health
of black churches.'' The e-mail message continued, ''None of these second- or
third-generation black academics talk to us in the trenches. They are too
elitist to talk to us.''

Lawrence H. Mamiya, a professor of religion at Vassar and co-author of the
seminal history ''The Black Church in the African American Experience,'' levied
similar complaints, albeit in less strident language. ''Theologians and
philosophers like Eddie Glaude don't go to black churches,'' Professor Mamiya
said in a telephone interview. ''They haven't been out in the field. And unless
you're in the field, you can't see what's happening.'' (Professor Glaude's
scholarly specialty is the philosophy of religion; he is not a social scientist
regularly engaged in field work.)

Among his own generation of scholars, though, Professor Glaude has received
plenty of credit for stirring discussion. ''It causes us to complicate how we
think about African-American Protestantism,'' said Jonathan L. Walton, 36, a
professor of religious studies at the University of California, Riverside. ''The
term 'black church' is in so many ways unsubstantiated. So this debate is very
healthy.''

One of Professor Glaude's forebears in criticism, Prof. James H. Cone of Union
Theological Seminary in New York, appreciated a certain paradox.

''Eddie Glaude is doing the black church a service,'' said Professor Cone, 71,
the author of several books of black liberation theology. ''By saying it's dead,
he's challenging the black church to show it's alive. But the black church, like
any institution, does not like criticism from outside the family. It wants to be
prophetic against society, but it does not want intellectuals to be prophetic
against it.''

URL: http://www.nytimes.com

LOAD-DATE: April 17, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Eddie S. Glaude Jr., a religion professor at Princeton, found
himself ''in the firestorm'' with his essay ''The Black Church Is Dead.''
(PHOTOGRAPH BY MIKE GREENLAR/THE POST-STANDARD)

PUBLICATION-TYPE: Newspaper


