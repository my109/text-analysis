                   Copyright 2009 The New York Times Company


                             528 of 2021 DOCUMENTS


                               The New York Times

                          September 9, 2009 Wednesday
                              Late Edition - Final

Our One-Party Democracy

BYLINE: By THOMAS L. FRIEDMAN

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 29

LENGTH: 838 words


Watching both the health care and climate/energy debates in Congress, it is hard
not to draw the following conclusion: There is only one thing worse than
one-party autocracy, and that is one-party democracy, which is what we have in
America today.

One-party autocracy certainly has its drawbacks. But when it is led by a
reasonably enlightened group of people, as China is today, it can also have
great advantages. That one party can just impose the politically difficult but
critically important policies needed to move a society forward in the 21st
century. It is not an accident that China is committed to overtaking us in
electric cars, solar power, energy efficiency, batteries, nuclear power and wind
power. China's leaders understand that in a world of exploding populations and
rising emerging-market middle classes, demand for clean power and energy
efficiency is going to soar. Beijing wants to make sure that it owns that
industry and is ordering the policies to do that, including boosting gasoline
prices, from the top down.

Our one-party democracy is worse. The fact is, on both the energy/climate
legislation and health care legislation, only the Democrats are really playing.
With a few notable exceptions, the Republican Party is standing, arms folded and
saying ''no.'' Many of them just want President Obama to fail. Such a waste. Mr.
Obama is not a socialist; he's a centrist. But if he's forced to depend entirely
on his own party to pass legislation, he will be whipsawed by its different
factions.

Look at the climate/energy bill that came out of the House. Its sponsors had to
work twice as hard to produce this breakthrough cap-and-trade legislation. Why?
Because with basically no G.O.P. representatives willing to vote for any price
on carbon that would stimulate investments in clean energy and energy
efficiency, the sponsors had to rely entirely on Democrats -- and that meant
paying off coal-state and agriculture Democrats with pork. Thank goodness, it is
still a bill worth passing. But it could have been much better -- and can be in
the Senate. Just give me 8 to 10 Republicans ready to impose some price on
carbon, and they can be leveraged against Democrats who want to water down the
bill.

''China is going to eat our lunch and take our jobs on clean energy -- an
industry that we largely invented -- and they are going to do it with a managed
economy we don't have and don't want,'' said Joe Romm, who writes the blog,
climateprogress.org.

The only way for us to match them is by legislating a rising carbon price along
with efficiency and renewable standards that will stimulate massive private
investment in clean-tech. Hard to do with a one-party democracy.

The same is true on health care. ''The central mechanism through which Obama
seeks to extend coverage and restrain costs is via new 'exchanges,' insurance
clearinghouses, modeled on the plan Mitt Romney enacted when he was governor of
Massachusetts,'' noted Matt Miller, a former Clinton budget official and author
of ''The Tyranny of Dead Ideas.'' ''The idea is to let individuals access group
coverage from private insurers, with subsidies for low earners.''

And it is possible the president will seek to fund those subsidies, at least in
part, with the idea John McCain ran on -- by reducing the tax exemption for
employer-provided health care. Can the Republicans even say yes to their own
ideas, if they are absorbed by Obama? Without Obama being able to leverage some
Republican votes, it is going to be very hard to get a good plan to cover all
Americans with health care.

''Just because Obama is on a path to give America the Romney health plan with
McCain-style financing, does not mean the Republicans will embrace it -- if it
seems politically more attractive to scream 'socialist,' '' said Miller.

The G.O.P. used to be the party of business. Well, to compete and win in a
globalized world, no one needs the burden of health insurance shifted from
business to government more than American business. No one needs immigration
reform -- so the world's best brainpower can come here without restrictions --
more than American business. No one needs a push for clean-tech -- the world's
next great global manufacturing industry -- more than American business. Yet the
G.O.P. today resists national health care, immigration reform and wants to just
drill, baby, drill.

''Globalization has neutered the Republican Party, leaving it to represent not
the have-nots of the recession but the have-nots of globalized America, the
people who have been left behind either in reality or in their fears,'' said
Edward Goldberg, a global trade consultant who teaches at Baruch College. ''The
need to compete in a globalized world has forced the meritocracy, the
multinational corporate manager, the eastern financier and the technology
entrepreneur to reconsider what the Republican Party has to offer. In principle,
they have left the party, leaving behind not a pragmatic coalition but a group
of ideological naysayers.''

URL: http://www.nytimes.com

LOAD-DATE: September 9, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


