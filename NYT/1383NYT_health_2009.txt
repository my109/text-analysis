                   Copyright 2010 The New York Times Company


                             1383 of 2021 DOCUMENTS


                               The New York Times

                           January 20, 2010 Wednesday
                              Late Edition - Final

Centrist, And Yet Not Unified

BYLINE: By DAVID LEONHARDT.

E-mail: leonhardt@nytimes.com

SECTION: Section B; Column 0; Business/Financial Desk; ECONOMIC SCENE; Pg. 1

LENGTH: 1255 words


So what should happen with health reform now?

The  stunning victory of  Scott Brown, the Massachusetts Republican who will
have Ted Kennedy's old Senate seat,  suggests that public opinion has turned
against the proposal. It's hard to  know exactly how Democrats  will respond.
But given the sudden uncertainty over health reform's  fate, this does seem to
be  an important time to boil down its  substance.

Here's my attempt: The bills before Congress are politically partisan and
substantively bipartisan.

What does that mean? The first part is obvious. All 60  Senate Democrats and
independents voted for  the bill, and all 40  Republicans  voted against  it.
The second part is the counterintuitive one. Yet it's true.

The current versions of health reform are the product of decades of debate
between Republicans and Democrats. The bills are more conservative than  Bill
Clinton's  1993 proposal. For that matter, they're more conservative than
Richard Nixon's 1971 plan, which would have had the federal government provide
insurance to people who didn't get it through their job.

Today's Congressional Republicans have made the strategically reasonable
decision to describe  President Obama's health care plan, like almost every
other part of his agenda,  as radical and left wing. And the message seems to be
at least partly working, based on polls and the Massachusetts surprise. But a
smart political strategy isn't the same thing as accurate policy analysis.

The better  way to describe the Obama agenda, I think, is that it's ambitious
(even radical) in its scope and sharply different in direction from the
Reagan-Bush era, but mostly moderate in terms of how far it goes on any single
issue.

Mr. Obama wants to undo George W. Bush's high-income tax cuts, but would keep
the basic Reagan tax structure intact. The administration is trying to
re-regulate financial markets, but has rejected the sweeping ideas favored by
the former Federal Reserve chairman Paul Volcker, British regulators and many
liberals. The pattern is especially clear on Afghanistan and Iraq.

Now, a centrist  approach isn't necessarily the best one  --  no matter how good
it may sound to call yourself a centrist. Sometimes, Republicans are right about
an issue (whether the welfare system was broken) and sometimes Democrats are
(whether to respond to an economic crisis with fiscal stimulus or a Hooverite
approach).

Maybe the country would be better off with a big-government health care plan,
like a Medicare for all. Or maybe we'd be better off with a free-market version,
in which people shopped for their own plans in an open marketplace. Those are
interesting enough arguments. They also make it clear that the bills before
Congress are not particularly radical.

A little history is useful here. The first modern attempt at health reform, as
you've probably heard, came from Harry Truman. After World War II, he proposed a
government insurance plan that would cover everyone. Republicans and the
American Medical Association labeled the plan ''socialistic''  --  which, in
some ways, it was.

Opponents instead called for expanding the private insurance system. Nixon, then
a young California representative, and others suggested government subsidies for
people who couldn't afford insurance, as Paul Starr explains in his Pulitzer
Prize-winning book, ''The Social Transformation of American Medicine.'' But the
socialism critique was strong enough to defeat Truman's plan without need for
compromise.

The next push came from John F. Kennedy and Lyndon B. Johnson, who tried to
cover only the elderly. Critics cried socialism about Medicare, too. ''Behind it
will come other federal programs that will invade every area of freedom as we
have known it in this country,'' as Reagan, who was then working as the American
Medical Association's spokesman, said in a widely circulated speech.  This time,
though, big Congressional majorities and sympathy for the elderly  let  the
Democrats prevail.

Once Nixon was president, the focus switched from expanding access to
controlling costs, as you might expect with a Republican. He favored giving
doctors incentives to set up prepaid group practices, which had the potential to
provide better, cheaper care than the  fee-for-service system. Ted  Kennedy
often said he regretted not making a deal with Nixon on health reform.

The current bills, for better and worse, are akin to  a negotiated settlement to
this six-decade debate.  It would try to end our status as the only rich country
with tens of millions of uninsured people, as liberals have long urged. And it
would do so using private insurers and government subsidies, as conservatives
prefer. (I realize that some liberals argue that a more liberal bill would have
fared better, but the history of the health reform  --  not to mention this
country's conservative instincts  --  offers reason for doubt.)

On cost control, the bill is similarly centrist. In 1993, Mr. Clinton pushed for
putting a cap on the growth of  insurance premiums --  an idea similar to having
a national health budget, which   conservative governments in other countries
have done. Today's Democrats saw that move as too radical. Instead, they have
borrowed Nixon's old push for prepaid group practices, which are now called
accountable care organizations.

Together, the cost-control measures are serious enough that the Congressional
Budget Office estimates they would save the government $1 trillion in the next
20 years, over and above the cost of covering the uninsured. Some experts remain
doubtful of these projections. Others, though, think the budget office is
underestimating the savings, as it has with past Medicare changes.

The one big conservative idea that's largely missing is malpractice reform.  But
the White House said several times that it was willing to negotiate on this
issue. And think about it: Rahm Emanuel, the Obama chief of staff, likes to say
the only thing that's not negotiable is success. Don't you think Mr. Obama would
have gladly taken some heat from trial lawyers in exchange for passing health
reform with bipartisan support and making himself look like a transformational
leader?

The obvious question, then, is how the current bill could have inspired such
skepticism from voters.

The unified Republican message is part of the answer. So is the fact that Mr.
Obama never found a strong, consistent way to sell the bill.  That said, health
reform was never going to be easy.

Something like 90 percent of voters already have insurance. Many imagine that
they will never lose it. Many people even believe  they don't pay for their
insurance, because the money comes out of their paycheck before they see it.
(They do pay in lost income.) Polls also show that Americans are more aware of
our medical system's strengths than its  weaknesses (like needlessly high error
rates). As for Medicare being on course to break the bank  --  voters rarely get
excited about future fiscal problems.

So health reform was probably destined to inspire more fear than hope. It's been
that way since Truman.

In the wake of Mr. Brown's victory, the decision facing Democrats is not whether
to start with a blank slate and try to write a bill based on both liberal health
care ideas and conservative ones. They've already tried that.

The decision is whether to  expand insurance and try to control costs,  despite
the political risks, or whether that project will  once again be put off  until
another day.

URL: http://www.nytimes.com

LOAD-DATE: January 20, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


