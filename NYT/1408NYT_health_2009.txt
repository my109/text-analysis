                   Copyright 2010 The New York Times Company


                             1408 of 2021 DOCUMENTS


                               The New York Times

                           January 23, 2010 Saturday
                              Late Edition - Final

Senate Plan Pushed

BYLINE: By MICHELLE ANDREWS

SECTION: Section A; Column 0; National Desk; A PLEA TO THE HOUSE; Pg. 12

LENGTH: 209 words


Forty-seven health policy experts sent a letter on Friday to the House speaker,
Nancy Pelosi, and the chairmen of three key House committees urging the House to
adopt the Senate's health care legislation.

Signers included economists and political scientists from across the liberal and
moderate political spectrum who have been involved in the legislative effort.

The letter notes that the two bills have many elements in common, including
provisions mandating that everyone have insurance, prohibiting insurance denials
because of health status, creating insurance exchanges and offering subsidies to
help people buy insurance.

While acknowledging that the Senate bill is imperfect, the letter argues that
differences between the House and Senate versions can be adjusted through the
budget reconciliation process.

Timothy Jost, a professor of health law and health policy at Washington & Lee
University who organized the letter signing, said in an interview that other
options under discussion, like starting over from scratch or putting together a
less comprehensive bill, were not feasible. ''The whole thing hangs together,''
he said. ''You can't just pull out a little piece here and there and think the
whole thing will work.''

MICHELLE ANDREWS

URL: http://www.nytimes.com

LOAD-DATE: January 23, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


