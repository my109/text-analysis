                   Copyright 2009 The New York Times Company


                             351 of 2021 DOCUMENTS


                               The New York Times

                           August 19, 2009 Wednesday
                         The New York Times on the Web

More Voices in the Health Care Debate

SECTION: Section ; Column 0; Editorial Desk; LETTERS; Pg.

LENGTH: 281 words


To the Editor:

Re '' 'Public Option' in Health Plan May Be Dropped'' (front page, Aug. 17):

Once more, sensible legislation is being defeated by the well-organized attacks
of the right wing.

Until something is done about the overwhelming media monopoly of the rich and
powerful, almost no attempt to control costs or fight corruption has a
likelihood of success. The media need only fill the airwaves with carefully
worded disinformation, and they can win over half the public to almost any
position, no matter how absurd.

Len Fosberg  Hamburg, Germany, Aug. 17, 2009

To the Editor:

''Health Reform and Small Business'' (editorial, Aug. 13) cuts through the
rhetoric to present a clear-eyed view of why health care reform is vital for the
continued success of America's small businesses. Opinion research we've
conducted in 16 states over the last several months echoes this conclusion and
shows that small-business owners understand the current crisis and are eager for
reform.

Contrary to the Sturm und Drang heard at town hall meetings across the nation,
health care reform will not kill small business. In fact, our recently released
economic analysis, based on modeling done by the economist Jonathan Gruber,
shows that health care reform will benefit small businesses: it will save money
and jobs, and allow them to hone their competitive edge.

Those shouting the loudest are fighting progress with fear. Cooler heads must
prevail. Small-business owners want reform because it will bolster their bottom
line and keep the backbone of our economy healthy and thriving.

John Arensmeyer  Founder and Chief Executive   Small Business Majority
Sausalito, Calif., Aug. 13, 2009

URL: http://www.nytimes.com

LOAD-DATE: August 19, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


