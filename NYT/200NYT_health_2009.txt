                   Copyright 2009 The New York Times Company


                             200 of 2021 DOCUMENTS


                               The New York Times

                             July 23, 2009 Thursday
                              Late Edition - Final

White House Official Links Health Care Plan to Fiscal Balance

BYLINE: By GERRY SHIH

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 5

LENGTH: 639 words


A comprehensive overhaul of the health care system, eliminating inefficiencies
and wasteful practices, will be crucial in correcting the nation's unsustainable
fiscal excesses, the White House budget director said on Wednesday. The Obama
administration is increasing its pressure on Congress to act before the August
recess.

Speaking before an audience at the Council on Foreign Relations in New York,
Peter R. Orszag, the White House budget director, sought support for President
Obama's health care agenda. The president has already expended considerable
political capital to push through a $787 billion economic recovery package and,
most recently, to eliminate additional money for the F-22 fighter jet program.

Mr. Orszag opened his talk by urging patience with the administration's stimulus
spending plan. But he soon focused on health care, arguing that while
overhauling the system would raise costs temporarily, the policies would
ultimately become at least ''deficit neutral'' and help restore fiscal balance.

The drive to overhaul the health care system is ''further along than it's been
in decades, with an array of allies that was once unthinkable,'' Mr. Orszag
said, dismissing news reports of rising political challenges on Capitol Hill.

''If we fail to do more to move toward a high-value, low-cost health care
system,'' he said, ''we will be on an unsustainable fiscal path, no matter what
else we do.''

But Mr. Orszag warned several times that the president would reject any bill
that worsened the long-term fiscal situation, pledging that ''at worst, we are
going to be deficit neutral and meet the moral imperative of covering more
people.''

Whether the president can push his health care initiatives through Congress now
is seen as a critical measure of how well he has maintained political momentum.

Mr. Orszag's speech came hours before the president gave an extended prime-time
news conference in Washington in which his health care agenda was a central
theme.

In order to bring down long-term costs for health care, Mr. Orszag called for
the creation of an independent commission that would measure the efficiency of
specific medical providers and practices.

The existing problem of inflated costs, he said, was caused by a ''distinct lack
of information about what works and what doesn't, producing huge variations in
the quality and cost of care.''

He cited cost and efficiency discrepancies between top-tier service providers in
different parts of the country. Mr. Orszag said that during the last six months
of life, Medicare patients at the New York University Langone Medical Center
spent an average of 31 days in the hospital, while the average was 12 days at
the Stanford Medical Center in California.

''East Coast versus the West Coast  --  or East Side versus the West Side?'' he
said. ''If anything, it seems that higher-cost hospitals and regions provide
lower-quality care, not higher-quality care.''

Mr. Orszag said that an Independent Medicare Advisory Commission, a panel of
''politically insulated'' health experts who would set Medicare reimbursement
rates, could track advances in medical technology and promote  efficiency.

But Mr. Orszag conceded that the road ahead was long.

''We don't have enough knowledge to change from a fee-for-service to
fee-for-value,'' he said. ''But moving more decisions into the hands of medical
professionals and out of the political process will enable us to continually
update the system.''

At one point during the question-and-answer session, a man took the microphone
to ask those in attendance whether they believed the benefits of a health care
overhaul would outweigh the costs, and he drew a mixed response.

Glenn Hubbard, the dean of the Columbia Business School and moderator of the
event, quickly moved on to the next question.

URL: http://www.nytimes.com

LOAD-DATE: July 23, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Peter R. Orszag said that high medical costs were partly because
of a lack of information about effective treatments.(PHOTOGRAPH BY RICHARD
DREW/ASSOCIATED PRESS)

PUBLICATION-TYPE: Newspaper


