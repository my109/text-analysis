                   Copyright 2010 The New York Times Company


                             1345 of 2021 DOCUMENTS


                               The New York Times

                           January 14, 2010 Thursday
                              Late Edition - Final

Editors' Note

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 105 words


Several articles in the past year about the government's plans to overhaul the
health care system quoted Jonathan Gruber, identifying him as a professor of
economics at M.I.T. On Friday, Professor Gruber confirmed reports that he is
also a paid consultant on health care issues for the Department of Health and
Human Services. Had editors been aware of his government work, the articles
would have disclosed this relationship to readers.

An article on Saturday about union opposition to a proposed tax on health
insurance did initially mention Professor Gruber's government ties, but the
point was deleted during the editing process.

URL: http://www.nytimes.com

LOAD-DATE: January 14, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


