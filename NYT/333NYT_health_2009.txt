                   Copyright 2009 The New York Times Company


                             333 of 2021 DOCUMENTS


                               The New York Times

                             August 16, 2009 Sunday
                              Late Edition - Final

An Expat Goes for a Checkup

BYLINE: By SARAH LYALL.

Sarah Lyall, a London correspondent for The Times, is author of ''The Anglo
Files: A Field Guide to the British.''

SECTION: Section WK; Column 0; Week in Review Desk; Pg. 1

LENGTH: 1297 words

DATELINE: LONDON


-- There are times when, viewed from afar, American political discourse looks
like nothing more than a huge brawl conducted by noisy, ill-informed
polemicists. This is one of them, as Britain found last week when the renowned
physicist Stephen Hawking was, bizarrely, drawn into the raucous debate over the
health care proposals of President Obama and Congress.

Mr. Hawking, 67, has Lou Gehrig's disease, is paralyzed, speaks through a voice
synthesizer and needs a great deal of medical attention. He also lives in
Britain. This makes him a spectacularly unfortunate choice to pick as an example
of the evils of the National Health Service, which has provided free health care
-- to him, and to millions of other people here -- for 61 years.

But that is what Investor's Business Daily did on Aug. 3, in an editorial
opposing Mr. Obama's proposals by accusing him of wanting to institute an
N.H.S.-style system in America. Mr. Hawking ''wouldn't have a chance in the
U.K.,'' the newspaper declared, because the health service would declare his
life ''essentially worthless.''

The paper printed a correction, and Mr. Hawking issued a statement saying that,
actually, the health service had helped keep him alive.

Debates about health care are often personal. Policy is full of statistics --
mortality rates, spending per capita, cost of drugs, length of hospital stays --
and full of hysterical predictions of what disasters change will bring. But in
discussing which system is best, patients turn to their own experiences and
those of their families, friends and acquaintances. People believe in the
hospital and doctors where they had good outcomes; they deplore those that have
let them down.

As an American who now lives in Britain, occasionally writes about the health
service, and uses public and private medicine here (as well as back home,
occasionally), I have seen firsthand the arguments from all sides. Certainly, as
someone who in the 1980s paid $333 to have an emergency room doctor at
Georgetown University Hospital remove a piece of toilet paper from my ear after
I had unsuccessfully tried to use it as an earplug, I applaud a system that is
free.

Founded in 1948 during the grim postwar era, the National Health Service is
essential to Britain's identity. But Britons grouse about it, almost as a
national sport. Among their complaints: it rations treatment; it forces people
to wait for care; it favors the young over the old; its dental service is
rudimentary at best; its hospitals are crawling with drug-resistant superbugs.

All these things are true, sometimes, up to a point.

The N.H.S. is great at emergency care, and great at pediatric care. My children
have enjoyed thorough treatment for routine matters -- vaccines, eye tests and
the like. A friend who had cancer received the same drugs and the same
treatment, I was assured, as she would have in the United States. When,
heartbreakingly, she died, her family was not left with tens of thousands of
dollars of outstanding bills, or with the prospect of long, bitter fights with
hardened insurance companies.

But there are limits. Without an endless budget, the N.H.S. does have to ration
care, by deciding, for instance, whether drugs that might add a few months to
the life of a terminal cancer patient are worth the money. Its hospitals are not
always clean. It is bureaucratic. Its doctors and nurses are overworked.
Patients sometimes are treated as if they were supplicants rather than
consumers. Women in labor are advised to bring their own infant's diapers and
their own cleaning products to the hospital. Sick people routinely have to wait
for tests or for treatment.

Because resources are finite and each region allocates care differently, waiting
times can vary widely from place to place. So can treatment, as in the United
States, regardless of how it is paid for.

Limited in what treatments they can offer, doctors sometimes fail to advise
patients of every option available -- or every possible complication. American
doctors, conversely, often seem strangely alarmist about your future and
overeager to prescribe more expensive treatment.

After I had my first baby, Alice, a National Health nurse came to my house
regularly to weigh and examine her and to lecture me about breast-feeding. It
was all free, a matter of course. The baby seemed to have mild eczema, but the
nurse was relaxed about it. When I took Alice, at 3 months, to New York and we
went to a Park Avenue pediatrician for a minor, unrelated complaint, the doctor
seemed unaccountably exercised.

''This baby has ECZEMA!'' he said accusingly, pointing to a few reddish spots.
''What are you doing about it?''

Britons are well aware of the limitations of their system. But do they
appreciate having the N.H.S. held up by Americans on the right as Exhibit A in
discussions of the complete failure of socialized medicine? Do they want to hear
that it is ''Orwellian,'' that it is a breeding ground for terrorism, or that,
in the words of Senator Charles Grassley, Republican of Iowa, it would refuse to
treat everyone from ''Granny'' to Senator Edward M. Kennedy?

No, they do not.

Like squabbling family members who band together against outside criticism,
Britons have reacted to the barrage of American attacks on the N.H.S. with
collective nationalist outrage.

A new Twitter campaign, ''We Love the NHS,'' has become one of the most popular
topics on the site, helped by Prime Minister Gordon Brown himself, as well as
the leader of the opposition Conservative Party, David Cameron.

Mr. Brown's eyesight was saved by a National Health surgeon after a rugby
accident when he was in college; Mr. Cameron's 6-year-old son, Ivan, who died in
February, was severely disabled and received loving care from the service.

The Twitter campaign is full of testimonials from recipients of successful
treatment for brain abscesses, complicated pregnancies, mangled toes, liver
disease, hernias, car accidents, nervous breakdowns, cancer -- you name it.

For me, the health service was a godsend when my husband suffered a severe
stroke in the 1990s. He got exemplary critical care; I did not get a bill. It
was only in the aftermath -- when I learned that, unusually in Britain, my
husband's job came with private health insurance -- that I came to realize what
it could and could not do. A little over one in 10 Britons have some sort of
private supplemental insurance; others pick and choose when to use the N.H.S.
and when to pay out of pocket for the top specialists or speedier care.

Told my husband needed a sophisticated blood test from a particular doctor, I
telephoned her office, only to be told there was a four-month wait.

''But I'm a private patient,'' I said.

''Then we can see you tomorrow,'' the secretary said.

And so it went. When it came time for my husband to undergo physical
rehabilitation, I went to look at the facility offered by the N.H.S. The
treatment was first rate, I was told, but the building was dismal: grim, dusty,
hot, understaffed, housing 8 to 10 elderly men per ward. The food was inedible.
The place reeked of desperation and despair.

Then I toured the other option, a private rehabilitation hospital with
air-conditioned rooms, private bathrooms and cable televisions, a
state-of-the-art gym, passably tasty food and cheery nurses who made a cup of
cocoa for my husband every night before bed.

We chose the private hospital, where the bills would be paid in their entirety
by insurance. My husband lived there for nearly two months. We saw the other
patients only when they were in the gym for treatment when my husband was. Most
of them seemed to be from rich countries in the Middle East. Perhaps they were
the only ones who could afford to pay.

URL: http://www.nytimes.com

LOAD-DATE: August 16, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: CARED FOR: The physicist Stephen Hawking is a defender of
Britain's National Health. (PHOTOGRAPH BY KIMBERLY WHITE/REUTERS)(pg. WK6)
DRAWING (DRAWING BY YAREK WASZUL) (pg.WK1)

PUBLICATION-TYPE: Newspaper


