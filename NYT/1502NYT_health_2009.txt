                   Copyright 2010 The New York Times Company


                             1502 of 2021 DOCUMENTS


                               The New York Times

                           February 13, 2010 Saturday
                              Late Edition - Final

Fears of a 'Backroom Deal'

BYLINE: By DAVID M. HERSZENHORN

SECTION: Section A; Column 0; National Desk; Pg. 13

LENGTH: 373 words


House Republican leaders on Friday demanded that House and Senate Democrats halt
their efforts to resolve differences between the versions of major health care
legislation adopted by each chamber late last year.

In a letter to Speaker Nancy Pelosi and the Senate majority leader, Harry Reid
of Nevada, the House Republican leaders said that the efforts by Democrats to
reach a compromise plan would undermine negotiations at a bipartisan meeting
scheduled by President Obama for Feb. 25.

Mr. Obama has said that he wants to be able to present a unified Democratic
health care proposal at the meeting and then debate the major provisions with
Republicans and independent experts to see if they have better ideas.

But in their letter, House Republicans suggested that the work by Democrats to
resolve their differences would amount to a ''backroom deal among the White
House and Democratic leaders'' that would ''make a mockery of the president's
stated desire to have a 'bipartisan' and 'transparent' dialogue.''

The letter was signed by the House Republican leader, Representative John A.
Boehner of Ohio; the party whip, Representative Eric Cantor of Virginia; and the
Republican conference chairman, Representative Mike Pence of Indiana.

It was the latest sign that Republicans are increasingly anxious that Mr. Obama
and the Democrats will score a political victory with the bipartisan meeting.

The Republicans have demanded that Democrats discard the health care bills
adopted by the House and the Senate and start the process over. Mr. Obama and
the Democrats have refused, and they called on Republicans to negotiate on a
combined version of the bills already adopted.

By raising questions about the Democrats' motives, Republicans seem to be
anticipating that Mr. Obama will move quickly after the meeting to win passage
of a revised health care bill, and they appear to want to blunt any effort by
the president to suggest that Republicans offered few workable alternatives.

The White House issued invitations to the bipartisan meeting on Friday to senior
lawmakers in Congress, including the top Democrat and Republican on each of the
Congressional committees with jurisdiction over health care. DAVID M.
HERSZENHORN

URL: http://www.nytimes.com

LOAD-DATE: February 13, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


