                   Copyright 2010 The New York Times Company


                             1362 of 2021 DOCUMENTS


                               The New York Times

                            January 17, 2010 Sunday
                              Late Edition - Final

Smoke Screen

BYLINE: By RANDY COHEN

SECTION: Section MM; Column 0; Magazine Desk; THE ETHICIST; Pg. 19

LENGTH: 719 words


My son was dropped from our family's employee-sponsored health insurance shortly
after graduating from college in May. While filling out the application for a
new policy, he asked me how to answer a question about his marijuana use in the
past year. I said, ''Honestly.'' He checked a box indicating he smoked very
occasionally and was denied coverage. Now he is uninsured while countless
pot-smoking liars have coverage. My husband thinks I gave our son foolish
advice. Do you agree?  M.H., MONTCLAIR, N.J.

In this situation, there is no good advice. Some problems are simply not
amenable to an honorable individualist solution, offering a choice only between
disheartening alternatives.

Honesty may not always be the best policy -- and, by the way, do these pants
make me look fat? -- but we rely on the trustworthiness of those we do business
with. Were your son to lie on that form, he'd do his small part to erode that
trust. And yet it's hard to see how he'd harm the insurance company. Few dire
health consequences result from sporadic youthful pot-smoking or even occasional
adult pot-smoking. It is impertinent of the insurer to act on information that
is medically insignificant.

And so, were I filling out that form, I'd lie without remorse. (All right, with
some remorse. Accompanied by resentment. I blame my upbringing. And my inept,
albeit imaginary, therapist.) But I could not advise my child to lie -- even an
older child, even to an insurance company. I would feel a parental duty to teach
integrity and encourage civic engagement. So I would urge him to supply an
honest answer on that form and write an urgent letter to his elected
representatives, particularly those working on health care reform. The real
solution here is to guarantee access to medical care to all people, not just
those pot-smoking liars.

UPDATE: The son appealed the decision. The company remained adamant but said he
could reapply in a year. M. H. says she believes it was giving him a nod and a
wink, hinting that next year her son should simply lie. The parents were able to
get him back on his father's policy for $500 a month.

I work in the reference department of a large public library system. A caller
sought someone to do some research, for which he was willing to pay. I told him
that the library does not provide research services and described the materials
we have that he should investigate. However, he was calling from a thousand
miles away and could not come in. Could I have undertaken this paid research on
my own time? Isn't this similar to a teacher who moonlights as a tutor? NAME
WITHHELD, CALIFORNIA

If doing such research is not a function of your day job and you tackle this
commission after hours, then there's no harm in it. It is akin to your taking a
weekend job to paint this guy's apartment or walking his (paint-spattered) dog.
It is also pertinent that he phoned from out of state. Even if your library
provided extensive research services, he'd be geographically unable to drop by
and avail himself of those services, and hence would need to hire a local
researcher. There's no reason that the local researcher can't be you.

You would be wrong to accept outside payments simply for doing your job. That
would present a conflict of interest, an incentive to slack off during the
workday in the hope of charging the punters -- sorry, patrons -- for similar
tasks after hours. And you'd cultivate the misleading expectation in those
patrons that they must slip you some cash to receive the ordinary library
services to which they are entitled.

As you suggest, schools face a related problem: they do not want teachers to
have a financial interest in ill-educating their students, who would then need
pricey tutoring, conveniently available from those very teachers. A solution
some New York private schools have adopted is to forbid teachers to provide paid
tutoring to any of their own students. A resourceful countermeasure some
teachers could adopt: Those at one school refer student-customers to colleagues
at another.

e-mail queries to ethicist@nytimes.com, or send them to the Ethicist, The New
York Times Magazine, 620 Eighth Avenue, New York, N.Y. 10018, and include a
daytime phone number. podcasts of the Ethicist are now available at NYTimes.com,
iTunes and Yahoo.com.

URL: http://www.nytimes.com

LOAD-DATE: January 17, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY MATTHEW WOODSON)

DOCUMENT-TYPE: Question

PUBLICATION-TYPE: Newspaper


