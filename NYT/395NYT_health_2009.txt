                   Copyright 2009 The New York Times Company


                             395 of 2021 DOCUMENTS


                               The New York Times

                             August 24, 2009 Monday
                         The New York Times on the Web

Lieberman Suggests Health Care Reform May Have to Wait

BYLINE: By JOSEPH BERGER and DERRICK HENRY

SECTION: Section ; Column 0; National Desk; Pg.

LENGTH: 1196 words


Senator Joseph I. Lieberman of Connecticut on Sunday urged the Obama
administration to consider postponing overhauling the  health care system and
instead work on smaller chunks of the issue until the economy improves.

''I'm afraid we've got to think about putting a lot of that off until the
economy's out of recession,'' Mr. Lieberman said on CNN's ''State of the
Union.'' ''There's no reason we have to do it all now, but we do have to get
started. And I think the place to start is cost health delivery reform and
insurance market reforms.''

Mr. Lieberman's comments could further complicate Democratic efforts to get a
health care overhaul passed in Congress. They had been depending on the
independent senator to support their efforts, even though he often aligns with
Republicans.

In the Senate, 60 votes would be needed to overcome any Republican  filibuster,
and the  Democrats control 60 votes, including those of two independents.
However, Senators Edward M. Kennedy of Massachusetts and Robert C. Byrd of West
Virginia have been ill. On top of that, moderate Democrats have signaled that
they might not support every proposal.

Mr. Lieberman credited Mr. Obama for taking on the heath care issue, but noted
that ''he took it on at a very difficult time that was not of his making.'' The
senator said he believed  health care reform debate has intensified partly
because  of the general  nervousness over the economy.

Also Sunday, Senator John McCain said that one way for Democrats and Republicans
to reach a compromise would be for Mr. Obama to abandon a government-run
insurance program for the nation's 49 million uninsured.

Interviewed on ABC's ''This Week with George Stephanopoulos'' Mr. McCain, the
Arizona Republican and 2008 presidential candidate, said that a public plan
would cause many Americans to lose their private health insurance. Although he
was not asked to explain, opponents of a government-run plan have previously
warned that many employers would scuttle private insurance for their workers if
such a program were enacted.

''Certainly if you have it, you shouldn't have to lose it,'' Mr. McCain said of
private insurance. ''But under the president's plan, you would have to lose it
in my view because of the government option. I believe that one of the
fundamentals for any agreement would be that the president abandon the
government option.''

Last week, Kathleen Sebelius, the Secretary of Health and Human Services, said
that a government-run plan was not essential to an overhaul, a concession to
United States senators who say such a plan could not win the backing of a
majority of the crucial Senate Finance Committee. The focus shifted to
discussion of nonprofit medical co-operatives, which are popular in some
Midwestern states and more palatable to conservatives. But outrage by liberals
supporting a government plan has made the fate of that provision unclear.

Senator Charles E. Schumer, the New York Democrat and a member of the Finance
committee, said on NBC's ''Meet the Press'' that President Obama ''believes
strongly in a public option.''

He argued that a government plan was imperative because it would introduce
competition in the industry. In 40 of the 50 states, he said, as few as two
companies rule the private health insurance market. A government program, he
said, ''doesn't have to make a profit or merchandise as much, so its costs are
probably 20 percent lower.''

But on the same show, Senator Orrin G. Hatch, the Utah Republican and a member
of the same finance panel, predicted that ''tens of millions of people will go
into the government plan'' against their will. And, he added, the problem with
government plans like Medicare is they are not sufficiently financed. Medicare,
he said, will go bankrupt within 10 years.

''The costs of the government plan will be astronomical,'' he said. ''Keep in
mind, in Medicare they pay doctors 20 percent less, they pay hospitals 30
percent less. Guess where those costs are transferred? They're transferred to
the people who have private health insurance, and the average private health
insurance policy goes up about $1,800 a year just to pay for what the government
fails to pay for in their current government plan.''

But on CBS's ''Face the Nation,'' Howard B. Dean, former governor of Vermont and
former chair of the Democratic National Committee, said a government program
would be far cheaper than any private alternatives. Mr. Dean said that only 80
percent of the revenue of private insurance companies goes to medical care while
the rest becomes profits for investors in the insurance company and for costs
like administration. With Medicare, he said, 96 percent of revenue ends up being
spent on actual health care.

Senator McCain said President Obama is as much to blame as Republicans for the
paralysis on health care legislation because ''the president has not come
forward with a plan of his own.'' He lamented the absence of Senator Kennedy,
who is ailing with brain cancer, from the Congressional debate. Senator Kennedy,
he said, has had ''a unique way'' of getting Senators ''sitting down at the
table and making the right concessions.''

While Mr. McCain rejected the term ''death panels'' -- deployed by his running
mate, Sarah Palin -- but he said that the language in some bills would have
created boards to decide what procedures would be allowed for the terminally
sick and dying.

''Doesn't that open the door to the possibility of rationing?'' he asked.

Senator Charles E. Grassley of Iowa, the ranking Republican on the Finance
Committee, explained on ''Face the Nation'' why language about paying for
end-of-life counseling had to be taken out of a health care bill the committee
was reviewing in March.

''We were not going to have any of this end-of-life stuff because it scares
people,'' he said.

On Fox News Sunday, Jim Towey, director of faith-based initiatives during the
administration of George W. Bush, said end-of-life counseling is already taking
place for soldiers returning from Iraq and Afghanistan. He criticized the
Veterans Administration for using a booklet called ''Your Life, Your Choices''
to help injured veterans decide which medical treatments they would choose. On
one page, he said, veterans are asked ''What makes your life worth living?'' and
some of the choices include confinement to a wheelchair, placement in a nursing
home and severely burdening a family's finances Such questions, he said, were
''kind of pushing people toward a predetermined conclusion.''

The booklet, he charged, was written by a proponent of assisted suicide during
the Bush administration but he said he managed to block its use in 2007. Last
month, he said, the Veterans Administration reinstated it.

Responding on the same show, Tammy Duckworth, an assistant secretary of veteran
affairs, insisted the booklet was indeed pulled off the shelves in 2007 and that
the Obama administration has been revising it and telling medical practitioners
not to use it. But she described the booklet as ''a tool'' and pointed out that
Mr. Towey has a competing book on end-of-life discussions that veterans can
purchase for $5.

URL: http://www.nytimes.com

LOAD-DATE: August 24, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


