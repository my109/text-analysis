                   Copyright 2010 The New York Times Company


                             1861 of 2021 DOCUMENTS


                               The New York Times

                             April 8, 2010 Thursday
                              Late Edition - Final

California: Man Accused of Threatening Pelosi

BYLINE: By NYT

SECTION: Section A; Column 0; National Desk; NATIONAL BRIEFING WEST; Pg. 21

LENGTH: 63 words


Federal agents arrested a San Francisco man on Wednesday in connection with
threats against Speaker Nancy Pelosi for her role in winning adoption of health
care legislation, officials said. Charges were not disclosed. The man, Gregory
Guisti, 48, is scheduled to appear in court Thursday. On Tuesday, a man was
charged with threatening to kill Senator Patty Murray of Washington.

URL: http://www.nytimes.com

LOAD-DATE: April 8, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Brief

PUBLICATION-TYPE: Newspaper


