                   Copyright 2010 The New York Times Company


                             1844 of 2021 DOCUMENTS


                               The New York Times

                             April 3, 2010 Saturday
                              Late Edition - Final

An Article of Faith

BYLINE: By CHARLES M. BLOW

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 17

LENGTH: 466 words


Since signing the health care reform bill, President Obama has been traipsing
about the country trying to sell it. It's not really working for him.

According to a CBS News poll released on Friday, President Obama's approval
rating on health care sank to a personal low: 34 percent. (His overall approval
rating in the poll was also a new low for him: 44 percent.)

This is in large part because of Republican recalcitrance. The left loves him.
The right not so much. Actually, not at all. According to a Quinnipiac
University poll released last week, Obama's job approval rating among
Republicans was a measly 9 percent. On health care, his approval rating was an
even-more-measly 7 percent.

Why? The Apostles of Anger in their echo chamber of fallacies have branded him
the enemy. This has now become an article of faith. Obama isn't just the enemy
of small government and national solvency. He's the enemy of liberty.

This underscores the current fight for the soul of this country. It's not just a
tug of war between left and right. It's a struggle between the mind and the
heart, between evidence and emotions, between reason and anger, between what we
know and what we believe.

This conflict was captured in a tit-for-tat between Obama and Rush Limbaugh. In
an interview with CBS this week, Obama complained about the ''vitriol'' coming
from the likes of Limbaugh: ''I think the vast majority of Americans know that
we're trying hard, that I want what's best for the country.''

Limbaugh shot back on Friday, ''I and most Americans do not believe President
Obama is trying to do what's best for the country.''

And there it was. Obama's language focused on what people ''know,'' or should
know. He seems to find comfort in the empirical nature of knowledge. It's
logical. Limbaugh's language focused on what he thinks people ''believe.''
Beliefs are a more complicated blend of facts, or lies, and faith. And, they can
exist beyond  the realm of the rational.

This focus on faith has allowed people like Limbaugh to mislead and manipulate
large swaths of the right.

According to another Quinnipiac poll released last week, Republicans were far
more likely than Democrats to say that they follow public affairs most of the
time. But how? They listen to people like Limbaugh, and they're more likely than
others to watch Fox News.

But invectives are not information. For example, a poll released on Wednesday by
the Pew Research Center found that most Republicans say that they still don't
understand how the new health care reform will affect them and their family.

They don't know what it means, but they believe it's bad. Rush & Co. said so. In
the vacuum of confusion and misinformation, they strum their fears and feed
their anxiety. And, by worrying, their faith is made perfect.

URL: http://www.nytimes.com

LOAD-DATE: April 3, 2010

LANGUAGE: ENGLISH

GRAPHIC: CHARTS: A Lack of Understanding: How well do you feel you understand
how the new health care reform law will affect you and your family? (Source: The
Pew Research Center)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


