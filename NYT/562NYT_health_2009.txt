                   Copyright 2009 The New York Times Company


                             562 of 2021 DOCUMENTS


                               The New York Times

                          September 12, 2009 Saturday
                              Late Edition - Final

Inside The Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1111 words


International

RARE UGANDAN UNREST CENTERS ON LOCAL KING

Rioters swept through the Ugandan capital, Kampala, in an unusual burst of
mayhem that witnesses say has killed at least 10 people. Residents said the
riots were set off by accusations that the police harassed an official of a
local, mostly ceremonial, king. PAGE A4

A BAD CHECK IN DUBAI

A rising number of people have been sent to prison for going into debt in Dubai,
where bouncing a check is a criminal offense. That fact has begun raising
concern that the legal system needs to be modernized. PAGE A4

SOUTH KOREA'S NEW EXPORT

A South Korean real estate mogul is shipping an unusual export: the Korean
alphabet. She is taking the alphabet to places where native people lack written
systems to record their languages. PAGE A4

N. KOREA TRIMS DEMANDS

North Korea, in its latest conciliatory gesture toward South Korea, dropped its
demand for a big wage increase for its workers at the joint industrial park that
it operates with the South, officials said. PAGE A5

LIFE FOR EX-LEADER OF TAIWAN

Chen Shui-bian, the former president of Taiwan, and his wife were convicted of
corruption and sentenced to life in prison. PAGE A5

IRAN'S LEADER GIVES WARNING

Iran's supreme leader, Ayatollah Ali Khamenei, warned opposition leaders of a
''harsh response'' if they continued speaking out against the government, a move
that analysts said opened the way for their possible arrest. PAGE A7

2 ROCKETS FIRED INTO ISRAEL

After at least two rockets were fired from southern Lebanon and hit open areas
in northern Israel, the Israeli military returned fire. No injuries were
reported. PAGE A7

Obituaries

LARRY GELBART, 81

A writer, his caustic wit was behind the enduring success of the television
series ''M*A*S*H,'' Broadway hits like ''A Funny Thing Happened on the Way to
the Forum'' and film comedies like ''Tootsie.'' PAGE A15

National

INDUSTRY IS FOR REFORM, EXCEPT IF IT HURTS PROFITS

The top lobbyists for every major sector of the health care industry insist they
are behind the Obama administration's plan to overhaul health care. But each
group is also working quietly to scuttle or reshape some element the overhaul
that might hurt profits.  PAGE A11

COAST GUARD DRILL STIRS FEAR

On the anniversary of the terrorist attacks of Sept. 11, and with President
Obama nearby, a mock radio call about gunfire on the Potomac River -- the
transmission was part of a training exercise -- roused worldwide fears. PAGE A8

PITTSBURGH SUED FOR PERMITS

Six groups that want to demonstrate during the coming Group of 20 meeting in
Pittsburgh filed a federal lawsuit claiming they were being denied the right to
protest. PAGE A9

HOUSE MAY CENSURE HECKLER

Unless he apologizes on the House floor, Democrats plan to pursue a resolution
admonishing Representative Joe Wilson, a Republican from South Carolina, for
calling President Obama a liar during the president's speech. PAGE A9

DOLE PUSHES FOR HEALTH PLAN

When former Senator Bob Dole was the Republican minority leader, he helped kill
President Bill Clinton's health care plan. This year, Mr. Dole, 86, who left the
Senate in 1996, is working to keep Republicans at the negotiating table.  PAGE
A11

REVIEW OF PATIENT DEATHS

The Orleans Parish district attorney said he was conducting an ''evaluation'' of
the patient deaths at Memorial Medical Center in New Orleans after Hurricane
Katrina. An investigation was not reopened.  PAGE A12

2010 Census Drops Acorn A12

Beliefs: Truce in Abortion Battle A12

New York

MUSEUM TO SHOW PHOTOS OF THE 9/11 HIJACKERS

The museum being built at the site of ground zero may also include printed
quotations from the videos the 19 men made before the attacks. PAGE A14

Obama Honors Victims of 9/11 A14

Big City: Rooftop Eden in Peril A13

Business

IKEA TRIES TO BUILD CASE AGAINST BRIBERY IN RUSSIA

As Ikea opened stores across Russia, the company rented generators instead of
paying bribes demanded by local utility companies. Ikea executives took great
pride in their creative solution. But Russian graft may have proved harder to
break than an Ikea coffee table. PAGE B1

IT'S MARKET SEE, MARKET DO

In the last year, stock markets all over the globe went up and down nearly in
unison. Off the Charts, by Floyd Norris. PAGE B1

MISTRIAL IN BONE DRUG CASE

A federal judge in the first jury trial involving the bone drug Fosamax declared
a mistrial on Friday after jurors started sending him desperate hand-scrawled
notes saying they were deadlocked over a verdict. PAGE B1

PCS FOR SALE, IN ENGLISH

The ''fact tag'' sits next to PCs, bewildering one potential buyer after
another. It trumpets things like DDR2 RAM and the robust L2 cache sizes of Core
2 Duos. But PC makers are abandoning the once cherished fact tag. PAGE B1

CRITICISM OF G.M. DEAL

The ink was barely dry on General Motors' agreement to cede control of its
European operations before Belgian officials reacted angrily to expectations
that an Opel plant in Antwerp would be closed while German jobs are protected.
PAGE B3

DISPUTE OVER PAPER LABEL

The F.S.C. label -- a judgment by the nonprofit Forest Stewardship Council of
whether a wood or paper product is environmentally friendly -- is being
challenged by a rival certification system supported by the paper and timber
industry. PAGE B3

Washington Post Executive Quits B2

Guilty Plea in Vast I.D. Theft B2

Wholesale Inventories Fell 1.4% B2

Sports

THE FIRST LADY WILL HELP CHICAGO'S 2016 OLYMPIC BID

President Obama will not go to Denmark next month to help bring the 2016
Olympics to Chicago (he says he has to deal with some health care issue).
Chicago's consolation prize will be Michelle Obama. PAGE B8

Arts

ENSURING THE MARQUEE SHINES BRIGHT ENOUGH

Ostensibly, film festivals are all about film. But the big ones, like the
Toronto International Film Festival, get their energy from stars. So a veritable
army was employed to arrange George Clooney's presence in Toronto. PAGE C1

HOW HE'LL MEET THE EMMYS

Neil Patrick Harris was such a hit hosting the Tonys he was hired to host the
Emmys. After that, there's only the big one left, he said: ''the Telemundo
awards.'' PAGE C1

A COLD WAR HISTORY

''The Hawk and The Dove'' -- a new book on two architects of America's cold war
strategy -- could only have been written by Nicholas Thompson, the hawk's
grandson. PAGE C1

NEW DETAILS ON ROOSEVELT

Newly found papers may indicate that Jewish leaders asked President Franklin D.
Roosevelt to bomb the largest Nazi death camp, Auschwitz-Birkenau.  PAGE C2

Op-ed

BOB HERBERT PAGE A17

GAIL COLLINS PAGE A17

CHARLES M. BLOW PAGE A17

URL: http://www.nytimes.com

LOAD-DATE: September 12, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


