                   Copyright 2010 The New York Times Company


                             1722 of 2021 DOCUMENTS


                               The New York Times

                             March 23, 2010 Tuesday
                              Late Edition - Final

Questions From Readers

SECTION: Section A; Column 0; National Desk; PRESCRIPTIONS MAKING SENSE OF THE
HEALTH CARE DEBATE; Pg. 19

LENGTH: 970 words


The landmark health care overhaul passed by the House will provide medical
coverage to tens of millions of uninsured Americans and will have an impact on
nearly everyone who already has insurance. Readers, understandably, have
questions about it all.

On Monday, Ron Lieber, Tara Siegel Bernard, Jennifer Saranow Schultz and Natasha
Singer of The New York Times answered readers' queries online. Here are edited
versions of the questions and answers:

QuestionI am a 25-year-old Peace Corps volunteer with no health insurance. Once
this bill is signed, will I have the right to be put on my parents' insurance?
Or must I already be on the account for the extension to apply? -- Shaun Wood,
Seattle

Answer According to Sara Collins, a vice president at the Commonwealth Fund, a
nonprofit, nonpartisan foundation focused on improving the health care system,
you can be covered as a dependent on your parents' health insurance plans until
you turn 26. Second, she believes that the legislation does not specify that you
have to be on your parents' insurance continuously for the extension to apply.
But you are not eligible if you have the option of getting insurance through
your job. Finally, the provision goes into effect six months after enactment.

Q.After my mother's premiums went up, she did not renew her coverage. She is
uninsured. Will this bill help her immediately? -- HC, Clewiston, Fla.

A. In the short term, the legislation would create a temporary insurance program
for people who have pre-existing medical conditions and have been uninsured for
at least six months. If your mother meets both those conditions, she will be
able to sign up for the so-called high-risk program and receive subsidized
premiums.

The premiums have not been established yet. But according to the Kaiser Family
Foundation, they will be ''established for a standard population,'' and an older
person would not be required to pay more than four times as much as a younger
person.

Besides premiums, the maximum your mother would have to pay out of pocket (for
things like co-payments or deductibles) would be capped at $5,950 (or $11,900
for families). The program would be established within 90 days of the
legislation's enactment.

If your mother does not have a pre-existing condition, she will have to wait
until 2014 for subsidized coverage.

Q.Is there anything to limit the insurance companies from extreme rate hikes on
individually bought policies, particularly in the immediate future? -- AVC, N.M.

A. In 2014, people who earn up to 400 percent of the poverty level (or $88,200
for a family of four) would not have to pay more than 9.5 percent of their
income on premiums. People with low incomes could pay as little as 3 percent.
The government would help subsidize the rest.

Large group plans that spend more than 15 percent of your premiums on items
other than medical costs (or more than 20 percent in the case of individual and
small group plans) must provide a rebate to consumers beginning in 2011. Details
are still fuzzy.

The legislation would immediately create a process for the review of increases
in health care premiums and would require plans to justify those increases,
according to the Kaiser Family Foundation. Once the state-run insurance
exchanges are up and running, states would be required to report premium
increases and recommend whether any plans should be excluded due to unjustified
premium increases.

Q. My husband and I are both 1099 contractors. We have health insurance, but it
is 100 percent out of pocket. Are we going to see any cash savings, or is the
reform just protecting options for our family? -- Madelyn Gillespie Driver

A. Come 2014, you and your husband will be able to shop for insurance through
the state-run exchanges, regardless of your health status.

How much you may save would depend on how much you earn. Individuals who earn
more than about $43,000 will be required to pay the full premiums, as would
couples who earn more than about $58,000. People below those thresholds would be
eligible for subsidies on both their premiums and out-of-pocket costs, which
would also be capped.

Q.What effect does the new health care bill have on Medicare Advantage plans? --
Mary, Hanover PA

A. Federal subsidies to the private Medicare Advantage plans -- which cover
about 25 percent of Medicare participants and cost the government more, on
average, than traditional Medicare -- would be cut beginning in 2012. Over the
next several years, the formula for reimbursing insurers will be reduced to
bring them in line with traditional Medicare.

''Plans' payments will be tied to their ability to deliver quality care on
measures that include customer service and clinical measures related to the care
provided to people with chronic conditions,'' said Paul Precht, director of
policy and communications at the Medicare Rights Center.

Plans will be required to spend at least 85 percent of the revenue they receive
from premiums and government subsidies on medical claims, Mr. Precht  added.

Starting in 2015, the two most popular Medigap plans -- C and F -- will be
altered for new enrollees so that they have some ''nominal'' cost sharing (they
now cover all deductibles and co-insurance) to encourage ''appropriate use'' of
physician services.

Q.How much mental health coverage will this provide? Will I be able to get
coverage for a therapist or psychiatrist? -- E.M.

A. If you are buying on the individual market, the state-based insurance
exchanges will offer different tiers of benefit packages, and all of the plans
will include a base package of benefits, which must include some mental health
coverage, said Ms. Collins of the Commonwealth Fund. The offerings in basic
plans will probably be more extensive than those available in the individual
market now.

URL: http://www.nytimes.com

LOAD-DATE: March 23, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: House members collected autographs on copies of the health bill.
(PHOTOGRAPH BY  STEPHEN CROWLEY/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


