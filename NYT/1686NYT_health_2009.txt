                   Copyright 2010 The New York Times Company


                             1686 of 2021 DOCUMENTS


                               The New York Times

                             March 19, 2010 Friday
                              Late Edition - Final

A Hands-On Approach To Health Care Reform

BYLINE: By STEPHEN HOLDEN

SECTION: Section C; Column 0; Movies, Performing Arts/Weekend Desk; MOVIE REVIEW
'REPO MEN'; Pg. 8

LENGTH: 858 words


To audiences starved for quasi-medical gore, the gruesome ''Repo Men'' should
help fill the void left by ''Nip/Tuck,'' the recently concluded FX series whose
weekly ritual centerpiece showed the slicing of flesh (with perky musical
accompaniment) in the cause of beauty . The blood count is higher in ''Repo
Men,'' a dystopic satire, adapted from Eric Garcia's novel ''The Repossession
Mambo'' (Mr. Garcia wrote the screenplay with Garrett Lerner) and set 20 years
in the future in a generically grungy megalopolis.

An omnivorous company called the Union, which has developed synthetic,
life-extending prostheses -- known as artiforgs -- for every body part markets
these mechanisms to consumers using the aggressive scare tactics of door-to-door
insurance salesmen. There is one horrible catch. The implants are prohibitively
expensive (upwards of $600,000 an item), and hardly anyone can afford them.

Should a recipient fall too far behind in his payments, the company's repo men,
using hand-held body scanners to identify embedded bar codes and due dates,
ambush the hapless debtors. Wielding scalpels, these corporate bounty hunters,
who work on commission, reclaim company property that will then be sold to
another sucker.

The concept of ''Repo Men'' has elements of ''The Matrix'' movies, the conehead
episode of the original ''Star Trek'' series, ''Surrogates'' and Steven
Spielberg's infinitely deeper and more humane ''A.I.'' But most of all, it
parallels ''Repo! The Genetic Opera,'' a cult movie musical that opened in
several cities two years ago.

The most colorful character in Mr. Spielberg's masterwork, Gigolo Joe, was
played by Jude Law, who stars in ''Repo Men'' as Remy, one of the corporation's
most gung-ho bounty hunters. Remy may be human, but he comes across as a figure
almost as robotic as Gigolo Joe, minus Joe's ever-chipper wit. Playing Remy, Mr.
Law lacks Joe's spasmodically jerky body language, but he is just as cold a
fish, and flaunts the same cocksure attitude. Should anyone choose to remake ''A
Clockwork Orange,'' the role of Alex seems ready-made for him.

Remy has a grimly unhappy wife, Carol (Carice van Houten), who loathes what he
does, but an adoring young son. When Carol leaves him, Remy doesn't seem to care
much about her departure; his boy is another matter.

All is going swimmingly for Remy and his lumbering, possibly sociopathic partner
(and childhood best friend), Jake (Forest Whitaker), until Remy suffers a
near-fatal accident on the job. While reclaiming the artificial heart of a
musician named T-Bone (the rapper RZA) during a mixing session, he is knocked
unconscious from an electric shock that might be one of T-Bone's improvised
defenses against repossession. Emerging from a coma, Remy finds to his chagrin
that he has been implanted with an artificial heart.

Once back on the job, he discovers he has lost his taste for the work. His hands
shake; his confidence is shattered. His impatient boss, Frank (Liev Schreiber in
his coldly hearty ''Glengarry Glen Ross'' mode), gives Remy a few days to
collect himself. When Remy fails to produce, you can see Frank's solution coming
from miles away.

As a recession-era satire, ''Repo Men'' strikes a very bitter chord. In today's
climate of widespread home foreclosures in which most banks refuse to modify the
terms of mortgages, the movie offers a grotesque funhouse reflection of this
scarily Darwinian environment. Then there is the matter of escalating health
care costs and the insurance industry's resistance to reform. We are halfway
there already.

It may have been a shrewd business decision by the film's director, Miguel
Sapochnik, to treat the story as a nasty, comic thriller. But when, after a
certain point,  ''Repo Men'' subsumes its satire to strenuous action sequences,
it loses its edge and turns into a chase movie of no special distinction. It
also becomes sentimental, as the technically heartless Remy suddenly gains his
soul and sets out to destroy the Union.

Mr. Law, buffed up for his role, emerges as a flailing, kicking and shooting
one-man army: a bantam-weight Rambo. His partner in rebellion, Beth (Alice
Braga), is a depressed nightclub singer (her signature song is ''Cry Me a
River'') with an almost entirely prosthetic body. When Beth remarks to Remy that
at least her lips are real, it is an invitation for their first kiss, at which
point ''Repo Men'' becomes a product as synthetic as an artiforg.

''Repo Men'' is rated R (Under 17 requires accompanying parent or adult
guardian). The movie is extremely gory.

REPO MEN

Opens on Friday  nationwide.

Directed by Miguel Sapochnik; written by Eric Garcia and Garrett Lerner, based
on the novel ''The Repossession Mambo'' by Mr. Garcia; director of photography,
Enrique Chediak; edited by Richard Francis-Bruce; music by Marco Beltrami;
production designer, David Sandefur; costumes by Caroline Harris; produced by
Scott Stuber; released by Universal Pictures. Running time: 1 hour 51 minutes.

WITH: Jude Law (Remy), Forest Whitaker (Jake), Liev Schreiber (Frank), Alice
Braga (Beth), RZA (T-Bone) and Carice van Houten (Carol).

URL: http://www.nytimes.com

LOAD-DATE: March 19, 2010

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Jude Law plays Remy, who accidentally shocks himself and turns
his life upside down in the futuristic film ''Repo Men.'' (PHOTOGRAPH BY KERRY
HAYES/UNIVERSAL PICTURES)

DOCUMENT-TYPE: Review

PUBLICATION-TYPE: Newspaper


