                   Copyright 2009 The New York Times Company


                             992 of 2021 DOCUMENTS


                               The New York Times

                           November 12, 2009 Thursday
                              Late Edition - Final

Rep. Kennedy And Bishop In Bitter Rift On Abortion

BYLINE: By ABBY GOODNOUGH

SECTION: Section A; Column 0; National Desk; Pg. 16

LENGTH: 627 words


Representative Patrick J. Kennedy of Rhode Island was to meet Thursday with
Thomas J. Tobin, the Roman Catholic bishop of Providence, and perhaps start
healing a bitter rift over whether health care legislation now before Congress
should restrict abortion coverage.

Instead, they postponed the meeting, and Bishop Tobin stepped up his public
rebuke of Mr. Kennedy, accusing him Wednesday of ''false advertising'' for
describing himself as a Catholic and saying he should not receive holy communion
because he supports using taxpayer money for abortions.

''If you freely choose to be a Catholic, it means you believe certain things,
you do certain things,'' Bishop Tobin said on WPRO, a Providence radio station.
''If you cannot do all that in conscience, then you should perhaps feel free to
go somewhere else.''

The United States Conference of Catholic Bishops has lobbied forcefully against
including federal financing for abortion in the health care legislation, and
Bishop Tobin, who has led the Catholic Church in Rhode Island since 2005, has
been a vocal participant.

His conflict with Mr. Kennedy -- an unusually personal example of the pressure
Catholic bishops are bringing to bear on the health care debate -- started last
month, when Mr. Kennedy, a Democrat in his eighth term, questioned why the
church had vowed to fight any health care bill that did not explicitly ban the
use of public money for abortions.

In an interview with Cybercast News Service on Oct. 21, Mr. Kennedy said he
could not understand ''how the Catholic Church could be against the biggest
social justice issue of our time,'' adding that its stance was fanning ''flames
of dissent and discord.''

The next day, Bishop Tobin called the comments ''irresponsible and ignorant of
the facts'' and Mr. Kennedy ''a disappointment'' to the church.

Mr. Kennedy, a son of the late Senator Edward M. Kennedy, then agreed to meet
with the bishop, but said his discord with the church hierarchy ''does not make
me any less of a Catholic.''

When the House approved its version of the legislation last Saturday, Mr.
Kennedy voted for the bill but opposed an amendment, ultimately adopted, that
restricted abortion coverage.

Bishop Tobin, in a letter publicly released Monday, called Mr. Kennedy's support
of abortion rights ''a deliberate and obstinate act of the will'' that was
''unacceptable to the Church and scandalous to many of our members.''

''It's not too late for you to repair your relationship with the Church,'' he
wrote, ''redeem your public image, and emerge as an authentic 'profile in
courage,' especially by defending the sanctity of human life for all people,
including unborn children.''

Mr. Kennedy declined an interview request, and on Tuesday he told reporters in
Providence that he would not comment on the bishop's letter.

''I had initially agreed to a meeting with him,'' Mr. Kennedy said, ''provided
we would not debate this in public in terms of my personal faith. But
unfortunately he hasn't kept to that agreement, and that's very disconcerting to
me.''

The battle is being waged nearly three months after Mr. Kennedy's father died of
brain cancer and received a Catholic funeral despite his longtime conflict with
the church over abortion rights and other issues. After the senator's death, his
family made public a letter he had written to Pope Benedict XVI. ''I have always
tried to be a faithful Catholic,'' he wrote.

In Wednesday's radio interview, Bishop Tobin said he still hoped to have a
private conversation with Representative Kennedy, who, he said, has a chance to
win the church's acceptance.

''It's not too late for the congressman to redeem his image,'' the bishop said,
''and to embrace the church and the teachings of the church.''

URL: http://www.nytimes.com

LOAD-DATE: November 12, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Bishop Thomas J. Tobin

PUBLICATION-TYPE: Newspaper


