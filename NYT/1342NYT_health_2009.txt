                   Copyright 2010 The New York Times Company


                             1342 of 2021 DOCUMENTS


                               The New York Times

                           January 13, 2010 Wednesday
                         The New York Times on the Web

Interview With Harold Ford Jr.

SECTION: Section ; Column 0; Metropolitan Desk; Pg.

LENGTH: 5425 words


Following are edited excerpts from Monday's interview with Harold E.  Ford  Jr.,
conducted by Michael Barbaro, a political reporter for The New York Times, at
New York University's campus in Manhattan:

Q. Why are you thinking about jumping into this race? What is your rationale for
possibly being a candidate for United States Senate from New York?

A. I ran for office 14 years ago in Tennessee, for Congress. And my appetite for
public policy and changing it, and not only being a part of the conversation,
but affecting it in a positive way, never diminished after 10 years in Congress.
I've stayed active and engaged in the national conversation, in a far more
limited way than I was when I was in elected office, through my role as chairman
of the Democratic Leadership Council, and even my role as a pundit, initially at
Fox News -- I was there for a year and then switched to MSNBC about a year
after. I stayed engaged and involved in the conversation. So that never
diminished in any way.

The race for office in New York is not something that was first mentioned last
week. Upon really moving here, and spending time here, I have had a number of
people inquire whether New York would be a place that I would consider pursuing
a political career. Some of the names that we have talked about, across the
board, up and down the board, to cabdrivers I have seen, who ask if I have given
some thought to running for office in New York. It's been a cross-section of
people.

In the last year, as I have watched the health-care debate, as I have watched
the last 6 to 12 months, the reaction to the jobs numbers, and most recently the
increased attention around national security issues, which don't come and go,
but just become more apparent and urgent, combined with President Obama's
election, my appetite to be part of the process in a more meaningful and
tangible way increased, combined with the talk from many from across the city,
for that matter. Most of it has been concentrated in the New York City area.
People have been encouraging, asking me if I would consider a run from here.

And, candidly, as you know, I have maintained my Tennessee residence for the
first year I was here. I taught in Vanderbilt University. I spent my time at
Merrill Lynch here. I had an office in Nashville. One of the articles noted, I
continued to vote in Tennessee. I even gave some thought to pursuing the
governor's race in Tennessee. The race was I think this year. I decided against
that in '08, that I would not do that.

Q: Why?

A: I was living here with my wife  --  we were not married yet. But shortly
after the race in November, I moved here. This is the woman I wanted to spend
the rest of my life with. We had got engaged. We dated. I wanted to be with her.
And so I moved here. And moved is such a legal term. I was not a resident here
yet until just last year, because I did not spend the requisite number of days
here  --  being that the majority of time I still spent commuting between here
and Nashville, and spending time in Tennessee.

This race has become even more interesting, because as I learn and become more
integrated into the city, and appreciate the kind of elected representation that
New York and New York State has enjoyed, the kind of independent-minded senators
and, for that matter, mayor, and governor and other positions over the years
that you have had here  --  I should say the kinds of people who have held
office here.

So I thought more and more about the issues and become more and more integrated
into the city, into philanthropic life  -- not in a big way, but some of the
organizations I have contributed to and are now a part of  --  be it the Prep
for Prep program, the Council on Foreign Relations.

My wife  --  she lived here for five years. New York has, in many ways, become
home.

And, again, you combine the central issues confronting the country, which
confront New York in a more meaningful and tangible way, arguably, than other
cities and locals around the country.

I often have said to people that there are really two cities in the country
where the outlook is always forward-looking  -- there is never really a
backward-looking tendency. My banking work has taken me out to Palo Alto, what
is commonly called Silicon Valley. And you sense out there is always a
forward-looking outlook. And New York City. There is a desire to allow people to
become a part of something that is so great and so big.

Q. You spoke about independence. You are running against somebody. It's not an
open seat  --

A. The seat was  --  the person who held it was appointed. As a matter of fact,
I contributed to her shortly after she was appointed.

Q. Why did you do that?

A. Well, I was asked by a good friend of mine. I have known Kirsten.

Q. Who was the friend?

A. A donor, a friend of hers. I don't want to introduce him into it.... I was
asked by a friend to make a contribution. Kirsten was a Blue Dog member of the
House. We never served together. And I like Kirsten. I have nothing personally
against her, even as this process unfolds. I have no personal ill will toward
the senator or her family.

Q. But a donation suggests you approve of the way she's led, no?

A. She had only been a senator for two days, when I made my contribution.

Q. That's precise.

A. One of the things you will find as we talk is that I have a decent-size
memory for dates and people. One of the reasons I remember this. It was at the
St. Regis Hotel. I had been on ''Morning Joe'' that morning. And our mutual
friend asked if I would attend the fund-raiser. I said, absolutely.

Q. What are her challenges?

A. There are some differences that I have with the senator on a few fronts -- as
it relates to the city and as it relates to the state, and to national issues.
One, I am a firm believer on the economy and national security.

As it relates to the economy, you have to be far more aggressive than the
senator, or the Congress, has been, around job creation. I think tax cuts for
employers, particularly small businesses, is critically important right now.

As it relates to big issues: I think there ought to be a huge-tax cut bill for
business people, not only in New York but across the country.

Health care: I don't know how you add the kind of burdens you add in the
Medicaid program to cities like this, to states like this, who send far more
money to Washington than they sent back. That conversation has been lost. And
the leadership in Washington from some representing New York has been lacking.
This is not a personal affront at Kirsten; it's just the reality.

The other reality is that Kirsten was appointed to the seat. She is not the
incumbent. New Yorkers have never had a chance to vote for her. She has never
stood on the ballot before.

So if we look at some of these questions, it's important to acknowledge that
fact. Second, as it relates to health care, I am for the bill, as I stated. But
I wouldn't support the bill with the Stupak amendment if it came to the House
floor. I wouldn't vote for a bill that would add additional debt and burden to
my home state, which would be New York.

If I were the senator of the state, there is no way I could support a bill that
would add the kind of burdens that have been projected to add what it does to
New York City and New York  State. We should not do it to any state.

Q. You would oppose the health-care overhaul as it now stands?

A. I couldn't support a health-care bill that places the kind of burdens on New
York State that this one does. I am for health-care reform, and I think there is
a way to correct this, and understand, Michael, I have a relationship with
MSNBC, and I have talked about the increased taxes on business and increased
burdens placed on states. At a time when projections show states like New York
face the kinds of projected deficits, it does not seem smart to place that kind
of extra burden on them at this moment.

Q. You raised the issue of independence. It sounds like you are saying Kirsten
Gillibrand is not independent enough from leaders in Washington and Albany. Can
you explain what you mean?

A. One thing is clear: If I am elected senator from New York, Harry Reid will
not instruct me how to vote. He may try, and would be my leader and I would work
with him. But I would be beholden to voters in New York. And you can count, for
certain, that I was sure of what I voted for before I voted for it. I think I
read some comments where at least exchanges between some elected officials in
the city and Kirsten Gillibrand, she didn't quite understand what was in the
health-care bill. And if you are going to cast a vote in favor of a massive
reform bill, you ought to know what is in it.

I want health-care reform to pass. The president has invested a great amount of
political energy and political capital. Democrats and some Republicans, a few,
have invested as well. It would be very hard to vote for the bill that had the
kind of anti-abortion language that came out of the House.

Q. Has Kirsten been a good senator? Can you grade her for me?

A. Obviously, I am thinking of running against her. It would certainly be hard
to give a grade that would be a glowing one. If the grade were a glowing one, I
would not be having this conversation with you.

Q. Can you grad her?

A: I am giving thought to running. You can interpret what the grade would be.

Q. If you had to rank  -- and sorry for these academic exercises  -- but if you
had to rank your seriousness about running for Senate, on a 1-to-10 scale?

A. Again, I wouldn't be having the conversation with you if there were not a
level of seriousness about this. This all happened  -- the seriousness and the
rapidity and, for that matter, the speed in which this happened  --  I mean, we
are talking one week. This was eight days ago that this really picked up
momentum and a kind of seriousness.

I laid out a timetable in your first article of 45 days.

After the article appeared in your newspaper, that you wrote, the response I
have gotten is overwhelming, from different categories, the spectrum of
political leaders, people involved in politics, people representing different
social and income classes in the city, be it the cabdriver on the way down here,
who had positive things to say, and wanted to take a picture with me before I
got out of the taxi, to people who are business leaders and leaders in the
entertainment industry and media industry based here.

I am very seriously considering this. I haven't made a decision. But I wouldn't
be sitting here unless I were. You all have reported some of the people I have
met with, the conversations.

Q. Has Harry Reid called you?

A. No.

Q. Can you tell me about your conversation with Senator Schumer?

A. I am not going to get into that. I think that the characterizations in the
press have been what they are and I have not objected to them.

Q. I am not here to quiz you or try to embarrass you, but I do want to ask you
about your life and travels in New York. So here goes. Have you traveled all
five boroughs?

A. I will tell you what I did. I was able to do it.  Kelly had a  -- Chief
Kelly, Commissioner Kelly  -- invited, I guess, business people in the city,
including Sir. Harold Evans, in my group. We spent the afternoon with the
special operations force, and so I had the chance to helicopter to various areas
in the boroughs. The only place I have not spent considerable time is Staten
Island.

Q. Have you been to Staten Island?

A. I landed there in the helicopter, so I can say yes.

Q. I asked you about your travel within New York State. What is the most
northern place you have been in the state?

A. I will tell you where I have been. To Buffalo. I have been to Syracuse. I am
scheduled to go to Rochester at the end of the month, to speak at the University
of Rochester. It's on the schedule now. Things may change over the next few
weeks. I am scheduled to be in Albany to be at the Coalition of Black and Latino
Elected Officials.

Q. So I have heard.

A. That is also  --  one is Feb. 7, one is Feb. 14.

Q. What subways do you use in the city?

A. Near my house, I take the 6, if I am uptown  --  I come down  --  I take the
6, 5 or the 4.

Q. Do you commute by subway or car?

A. Normally I commute, it's easier, in the morning, because I spend time at
MSNBC, generally  --  I don't really take the subway very often unless it's
wintertime and I can't get a taxi on Fifth, because I stumble out. I normally
take a take a taxi now on Fifth. My office is at 42nd and Fifth. I walk a lot.
In the morning, I am going to the office, it takes me four or five minutes. I am
on at 6 a.m. NBC will sometimes, if not all the time, send a car, and I shoot
right up Sixth Avenue. The two corridors are easy to get back and forth to.

Q. Jets or Giants?

A. I had breakfast about every morning when I am in town, or I should say,
several mornings, at the Regency. I see my friends the Tisches. Steve Tisch is
my close personal friend. I have been to more Giants games. I spent the
holidays, I had lunch over the holidays with Woody Johnson. We met for the first
time. I am happy for his team.

Q. Baseball?

A. I am a Yankees fan. I should say  --  have been to more Yankees games than
Mets games. I have not been to the new Citi Field yet. I was at the old stadium
for the Mets. I made several playoff games for the Yankees. This coming season,
with classmates from Penn, who already have a group I am buying into.

Q. You have a car in the city?

A. Yes, I do. I have a driving license from my old state. I have a Ford Escape.
It's an '08.

Q. I want to talk about executive compensation. You've spent the last few years
in finance in New York. Should executive compensation be capped?

A. Should it be capped? I am not a believer that it should be capped. What I do
believe is that shareholders should have greater say. I am not in opposition to
some of the measures that have come before Congress  -- and there ought to be
some nonbinding votes that ought to take place. I think there ought to be more
transparency around a lot of these issues for shareholders to understand what
people are being paid. You can look back  --  I have been on ''Morning Joe''
talking about this, so there is a record here and I am not running away from it.

Q. In 2008 and 2009, a lot of the large Wall Street firms, including Merrill
Lynch and Bank of America, got federal TARP money and a lot of them lost money
that year. But most of the major banks still gave their employers bonuses.
Should they have?

A. Well, let's make a distinction. There was a lot of talk about A.I.G. TARP was
not a bailout. It was essentially a loan. The bank I worked for paid their money
back, along with some of the others, recently, B of A paid theirs back. There
are a bunch of factors that got these companies into the situation they were in.
Candidly, I joined the firm in '07, but my role in terms of helping develop
those kinds of products was not that extensive. I want to make sure we are clear
on that front.

I am for TARP. I think TARP worked. That is another distinction between Kirsten
and myself. I mean, No. 1, if you are going to represent New York state, in New
York City, one of your largest industries, how it is that financial services  --
how can you be against insuring that the lifeblood of your city and of your
state survive, not just the bonuses that may have gone to the largest
executives, the people who clean the building, sell food in the building, the
companies that provide transportation to those who work in the industry, not to
mention the restaurants in the city, and not to mention, the schools, the
libraries the firehouses  --  I live next door to a firehouse, the police
stations.

TARP has worked  --  it is working. We are going to get our money back as
taxpayers.

Q. Understood, but my question was about bonuses. Should they have been given
out in these two years?

A. The pay czar worked out a reasonable arrangement. And where those
arrangements are now worked out, you have other actions that took place,
including by the attorney general of this great state. There are some
institutions where the government took an ownership stake. Should the government
be allowed to dictate to private institutions, you shouldn't be allowed to pay
this person X, this person Y, this person Z? I actually don't like that. I
actually think I am not too far from where the president is. I don't think the
government should dictate what they pay. That said, shareholders ought to be
able to see what financial institution, or any public institution, is paying
their top executives. It ought to be in line with performance. These
institutions were able to show that the work these people did measured up to the
amount they were getting  -- absolutely. But ultimately, they have to answer to
shareholders.

Q. Did you do any lobbying for the bank?

A. No.

Q. During the financial crisis, did you work with people on the Hill to help
Merrill or Bank of America navigate the situation?

A: No, that would be lobbying. See, as a former member of Congress, what trips
up lobbying is different than what you or David were to do. [He is referring to
David Goldin, a spokesman for Mr. Ford.] I cannot interface with lawmakers.
There are certain things that would trip it up right away. I was not a
registered lobbyist and never lobbied.

Q. Let's talk about gay marriage. You know your record very well, but to quickly
remind you, you voted to ban same sex marriage, with the Federal Marriage
Amendment, twice.

A. I can say up until 2003, most organizations and national organization that
had an office in Washington dedicated to fighting for equality for Americans, I
enjoyed broad support and big support from them. The marriage votes drove my
ratings down considerably, and arguably rightly so. I have been a supporter of
civil unions. My opponent raised the issue on the campaign trail in Tennessee.

As the presidential race unfolded, one of the things I recognized during the
campaign: My position on same-sex marriage resembles President Obama's over the
years. Frankly, up until maybe a year ago, that of the senior senator in the
state, Senator Schumer, who was opposed to same-sex marriage.

Q. Where are you now?

A. I am for gay marriage. Or same-sex marriage. I don't want to say it the wrong
way. I think people are sensitive to it. I have been painted as being this
right-wing zealot on choice. Nothing could be further from the truth. I think
there are legitimate questions around my support for  --

Q. Let's focus on your two votes to ban same-sex marriage. Can you explain that?
Walk me through that.

A. The last three years, think about what has transpired. How many states have
either courts and or legislatures that have declared same-sex marriage is
acceptable in their states? There has been a robust debate.

I don't think it's a great leap to go from civil unions to gay marriage  -- I
may be in the minority in believing that. But I don't think there is. Long
before I arrived in New York, my commitment to issues of fairness and equality
are clear and obvious and unmistakable. And in light of that, and consistent
with that, according the same rights that a couple were married, versus the
rights provided by civil unions, I don't believe the difference is that great. I
understand that in certain communities it's not viewed on equal footing. But my
change, or my maturation to that point....

Q. What changed for you?

A. Understand, I did not start at zero and get to 10. I started at 8. This is my
point: I think some of the press accounts of my record have been distorted or
just been wrong. People make it sound as if  --  let's go back to the votes in
the Congress.

Q. Do you regret those votes, then?

A: I have been in politics for 14 years. I was elected back in 1996 ... over the
14 years, have I learned and have I listened? Absolutely. Understand, Michael, I
did not go from zero to 10. I was for civil unions and believed strongly that
the flow of benefits and protections that would be provided in a civil union for
same-sex couples, the decisions that have to be made, when health hardships are
faced, when economic hardships are faced, I wanted all of those protections. I
never strayed from them. It was just the issue of marriage, that particularly
over the last three years, I have come to understand differently.

Q. Let's talk about abortion.

A. I want more jobs in New York.....

Q. And we are going to get to that. But first, we know you describe yourself as
''pro-life.''

A. No, no. Let's be clear.

Q. O.K., walk me through this.

A. I was not going to cede the language of life to the National Right to Life
Committee. When asked questions when I was on the ballot, I have often opened
answers, especially in areas outside of urban areas, outside of Memphis, outside
of Nashville, Chattanooga and Knoxville, making clear, that if the Republicans
are so pro-life, which I am, why is it we are denying benefits to veterans? Why
is it we are denying equal pay treatment to National Guardsmen. Why is it we are
denying special tax treatments to  employees who take 9- and 12-month absences
to serve their country but they can't count on their paychecks? ... If we are so
pro-life, why aren't we doing these things? So it was put in that frame. No. 2,
the National Right to Life Committee  -- I may be off by one or two points, I
don't think I ever received higher than a 25 percent voting record. No. 3, they
never gave me a penny. No. 4, my voting record in the 10 years in the Congress,
was three or four years, was 100 percent. [He refers to his record from NARAL
Pro-Choice America.]

I know somebody somewhat cynically suggested that perhaps there were not votes
on abortion those years. But if that were true, the entire Congress would have
gotten  100 percent. That did not happen. You can go and check.

My recollection: Every time I was on the ballot in Tennessee, I received a
contribution from organizations that would certainly be in line with being
pro-choice or supporting a woman's right to choice.

I accept the criticism coming from some in the city who are supporters of
Senator Gillibrand who are aligned with NARAL. They have every right not to
support me, and I hope to have an audience with them, to introduce myself.

To say that I am pro life is just wrong. I am personally pro-choice and
legislatively pro-choice.

Q. O.K.  Let's discuss a few votes. You voted to ban partial-birth abortion and
require consent for minors who want an abortion. Do you stand by those votes?

A. I do. I hope the Lord blesses my wife and I with a daughter some day. And if
my daughter  -- if a situation befalls her where she has to make that choice, I
would love to know and I stand by that vote.

Q. You would love to know what?

A. If my daughter, my 15-year-old daughter, is taken to the hospital, to get an
abortion, I would hope as a minor, I would know it. If she can't get into a
movie to see an R or NC-17 movie, at 15, I would hope that I would know if she
was going to the doctor to have this kind of procedure performed. Not to give
you my answer. But if I said yea or nay. I support my family. But I would want
to know that. So that is why I voted in favor of parental consent.

Q. Partial-birth abortion. You have objections to it?

A. I do.

A. And I still want more jobs for New York City.

Q. O.K., let's talk about jobs. How do you create jobs in New York State?

A. A couple of things. One, you have to be mindful of not undermining the
opportunity for growth and initiatives you have in the city and the state. The
financial services industry is an important part of the economic engine for the
state. I think some of the reforms that have been proposed in Washington,
especially the ones committed to transparency and calling for capital reserves
on the part of some of the financial institutes, will provide some of protection
we are looking for as well as increased say-so on the part of shareholders.

Two, I think immediately, we need to lower the corporate tax rate from 35 to 25
percent. We need to make clear that dividends and capital-gains taxes not go up
-- we do not want to see decisions made that are rash. We should have a broad
payroll-tax cut. A three month payroll-tax holiday for businesses and workers.
Finally, I am for the extension of unemployment benefits. And I am for the
increased infrastructure spending that some of my colleagues in Washington want
to engage in. I believe we have to set up an official way to it, through a
national infrastructure bank, so money flows to projects that make the most
sense and produce the most jobs.

Q. What else should I know about your vision for the state economy?

A. The clash for clunkers program that worked. I am a huge supporter for cash
for caulkers  -- which allows people to make improvement for energy efficient in
their homes. We should do the same for Americans purchasing appliances and
computers and for that matter, new air-conditioner and heating units. I would
provide an even greater break for those who buy U.S.-made products. Look at
China  --  there was a greater tilt toward home-grown products, which helps
their economy.

Q. Has Senator Gillibrand proposed economic stimulus plans that you oppose?

A. I haven't heard her talk about it at all. Maybe you have evidence of it; I
don't. If I were the U.S. Senator, I would talk about it.

Q. Can I briefly talk about immigration? I want to talk about illegal
immigrants, a topic of enormous importance to Mayor Bloomberg. You voted for
legislation that would allow local enforcement agencies to investigate and
arrest illegal immigrants. I am sure you are familiar with the debate around
that bill, how it changes the relationship between cops and the people they are
supposed to protect. People fear they will be reported as illegal, and get
deported. The secure fence act of 2006, which sealed the border. Can you explain
these positions, and do you stand by them?

A. No. 1, put this in perspective: In the campaign in '06, I argued for what was
then, the McCain-Kennedy legislation, which really worked to provide a path to
citizenship, which you could probably go back and find the language. I supported
that, and was criticized for it.

No. 2, I know that Mayor Bloomberg and I differed back then. We don't differ
now. I have come to better understand the issue. At the time, empowering local
enforcement to do what federal law enforcement was not doing seemed to make
sense in my state. Mayors and sheriffs and police chiefs across the state
complained bitterly about it and there was no effort at the federal effort, or
very little, to address the problem. It had been in some ways negligent to not
think about this. In the long term, Michael, the answer is a broad immigration
reform. If I am elected to the Senate, we will work towards that. Before
2003-2004, my votes in the Congress, at least the grading by anti-immigrant
advocacy groups, they gave me  --  one of them, the group FAIR  -- gave me a
zero percent rating. In 2003-2004, Americans for Better Immigration, which is a
great name for a group that wants to restrict immigration, rated me at 8
percent. So in '05 and '06, as this issue became more urgent, and the concerns
expressed across the jurisdiction I was running to represent, it was obvious
that we needed some other measures in place. Those votes were never meant, in my
mind, to be the long-term answer to this challenge. The long-term answer is to
deal with, to rally around a broad immigration reform bill that provides a safe
understandable and legal way to citizenship.

Q. Where you talk about an evolution, closer to mayor Bloomberg's position, that
is on the law enforcement issue.

A. Yeah, the law enforcement issue.

Q. What about the secure fence on the border?

A: Even if we had a fence now, we are not going to stop it. In a lot of ways,
it's a stopgap, to say, we have to do more. I don't depart from what I said at
the start. We need a broad comprehensive immigration reform bill.

Q. Guns. Let's talk about this issue.

A: I never got an A rating, like my opponent  --  would-be opponent  --  has
enjoyed. I don't own them. I do shoot them, and I shoot them at things that
can't shoot back. And will continue to do that. And by that, I want to be clear,
I don't mean children. I have done a little bird hunting in my day.

Q. Are you a frequent hunter?

A. I would say, occasional, recreational hunter.

Q. Where do you hunt?

A. I have done it in Arkansas, and in Florida.

Q. D.C. and New York have done a lot to restrict gun laws, and they are among
the toughest in the country. You opposed D.C.'s effort to put restrictions on
guns  --  one of the measures you opposed would require a gun at home to be
unloaded and disassembled. Why do that?

A. I don't think I did oppose that part. That was part of a broader bill...
well, I take your word for it. I am a supporter of the Second Amendment and
think that people ought to have the right to own weapons, guns and firearms,
with certain responsibilities attached to them.

One of the reasons I never scored above a B with the N.R.A.  -- my intent was
never to -- it speaks to my independent-minded approach  -- I am a member of the
N.R.A. But I don't send money or contribute to those efforts that I think are
bad, and when I disagree with them, I push back.

Q. Should a city like New York have the right to restrict guns?

A. The answer is yes. All of Mayor Bloomberg and Mayor Booker's efforts in the
region, I support wholeheartedly. [He is referring to Mayor Cory A. Booker of
Newark.]

Q: So what about D.C., where that was put to the test?

A. It ... My understanding of that, I don't recall it, we vote for 1,000 pieces
of legislation  -- read me the bill again.

Q. It was a restriction on guns, that would require federal approval, because
D.C. is a ward of Congress, that made firearms at homes be unloaded,
disassembled and so on.

A. People have the right to own a gun, and you meet all of the qualifications,
why would I then require that the gun be disassembled? I didn't support that.
That's different from what, I believe, Mayor Bloomberg and Booker are doing. The
number of handguns on the street here and back where I represented in Congress,
the proliferation of gun violence in the 90s and the early 2000s, it certainly
precipitated action on the part of the Congress. ... my opponent in this race,
has been endorsed by the N.R.A. and enjoyed A ratings. That is not the reason I
am giving consideration to running. Her A rating from the N.R.A. I supported
background checks, waiting 72 hours.

We have fundamental difference on independence. We have a difference on the
level, the kind and the stature of advocacy New Yorkers deserve. And we have
some honest differences on some issues.

Q. Is Senator Gillibrand's fund-raising a concern to you, the $5 million or so
she has on hand, and $7 million raised?

A. It's a number I don't have. But it doesn't make me flinch.

Q. You ran for Senate in '06. You talked about running in '99 for Senate against
Frist. You flirted with running for governor of Tennessee. You ran against
Pelosi for minority leader in the House. Now are you thinking of running for
Senate from New York. My question is, and it's provocative, I admit: Are you a
little impetuous when it comes to politics, and running for office? Is that
fair?

A. It's totally unfair. I don't know of anyone in any state in the country,
who's been elected to any office, that at some point, when offices open up that
are higher than the one they hold, that people don't speculate whether that
person will run or not. In 1999, in Tennessee, when Bill Frist  -- remember, he
won in '94  --  the party was looking for the most formidable opponent. I was
approached by leaders of my party, members of Congress, to think about it and
ultimately, I decided not to. I don't think that is any different from Chuck
Schumer, who gave thought to running for governor here, maybe once or twice or
before. I don't think it's any different than any member of Congress in this
district. Carolyn Maloney has given thought to running for Senate. ... I don't
think it's a label that impetuous seems a little bizarre. Do I have an appetite
for public policy? Yes. Do I have an appetite for having my record distorted?
No. I don't know who welcomes that kind of scrutiny.

URL: http://www.nytimes.com

LOAD-DATE: January 13, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Interview

PUBLICATION-TYPE: Newspaper


