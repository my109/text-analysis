                   Copyright 2010 The New York Times Company


                             1427 of 2021 DOCUMENTS


                               The New York Times

                            January 26, 2010 Tuesday
                              Late Edition - Final

Don't Give Up Now

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL; Pg. 22

LENGTH: 1004 words


It would be a terrible mistake for Democrats to abandon comprehensive health
care reform just because voters in the Massachusetts Senate race last week
decided that they liked the Republican, Scott Brown, more than the Democrat,
Martha Coakley.

There is no question that without a filibuster-proof majority it will be a lot
harder to pass a bill. But it should not be impossible if Congressional
Democrats and the White House show courage and creativity. Health care reform is
too important to throw away, and it is not too late to persuade voters that it
is in their interest.

Congress is achingly close to passing legislation that would cover most
uninsured Americans and provide much more security for all Americans --
guaranteeing that if they lose their jobs they will be able to buy affordable
policies and can't be denied coverage because of pre-existing conditions.

If the Democrats quit now, so close to the goal line, the opportunity for
large-scale reform could be lost for years. Meanwhile, the number of uninsured,
currently more than 46 million, will keep going up and the cost of health care
will continue to soar.

Many panicky Democrats see Mr. Brown's win as proof that angry voters will
punish them in November if they press ahead with reform. We believe that is a
misreading of what happened and what's possible.

Ms. Coakley ran an inept campaign. And the White House hasn't done enough to
address voters' profound and legitimate fears about losing their jobs and their
homes. But President Obama and Congressional Democrats have also clearly failed
to explain why reform will make Americans' lives more secure -- not less.

What makes this all the more frustrating is that Massachusetts, which adopted
its own very similar health care reform in 2006, is a compelling example of both
the benefits and popularity of the effort.

A poll taken in Massachusetts after the election by The Washington Post, the
Henry J. Kaiser Family Foundation and the Harvard School of Public Health found
that a surprising 68 percent of those who had voted said that they supported
their own state's plan, including slightly more than half of those who had voted
for Mr. Brown.

Mr. Brown, who promised to block reform in Washington, voted for his state's
program in 2006 and did not campaign against it this year. Instead, he argued
that since Massachusetts' citizens already have coverage, why should they help
pay to expand coverage elsewhere.

That cynical I've-got-mine argument doesn't make a lot of sense --  even in
Massachusetts. The Senate bill would funnel additional money into the
Massachusetts program and federal efforts to rein in costs should ultimately
benefit all of the states.

Democrats should take another look at what really happened in Massachusetts and
then summon the nerve to enact comprehensive reform. They must make clear to
voters that they have little to fear. Even the mandate requiring everyone to buy
insurance doesn't kick in until 2014. And they must make clear that reform
offers immediate gains, especially for middle-class Americans.

Once the bill becomes law, many dependent children could remain on their
parents' policies until age 26. Insurers would no longer be able to set lifetime
caps on the amount they will pay for health care. Children could not be denied
coverage because of pre-existing conditions. The gap in drug coverage for
Medicare beneficiaries, known as the doughnut hole, would begin to close. And
small businesses would immediately get tax credits to cover their employees.

Recent polls show that the public is divided, with more opposing the bills than
favoring them. The negatives have been driven up by critics' distortions about a
supposed government takeover of medicine and the tawdry deal-making necessary to
win 60 votes to overcome a Republican filibuster in the Senate.

Still, a recent national poll by the Henry J. Kaiser Family Foundation found
that large shares of people became more supportive when told about such
provisions as tax credits for small businesses that offer coverage, exchanges
where people could choose among competing policies, and rules against denying
coverage.

We are hearing a lot of talk in Washington, including from President Obama,
about possibly paring down the current bills -- to cover many fewer of the
uninsured and focus instead on reeling in the worst abuses of the insurance
industry and reining in health care costs. That could be difficult technically;
many of the parts are not easy to disentangle without undermining their
effectiveness. And the politics on Capitol Hill -- where the Republicans are
determined to oppose pretty much anything President Obama endorses -- are
unlikely to get easier.

The most promising path forward would be for House Democrats to pass the Senate
bill as is and send it to the president for his signature. That would allow the
administration and Congress to pivot immediately to job creation and other
economic issues. The Senate bill is not perfect, but it would expand coverage to
94 percent of all citizens and legal residents by 2019, reduce the deficit for
decades to come, and create pilot programs to move the medical system toward
better care at lower costs.

Rank-and-file House Democrats apparently won't accept the Senate bill without
modification. So Congressional leaders are looking for ways to commit both the
House and the Senate to changes  -- such as better subsidies to make insurance
more affordable -- that could be approved through parallel ''budget
reconciliation'' legislation that could be approved by a simple majority in both
the Senate and House.

This is a once-in-a-generation chance. President Obama must explain to the
American people why reform is essential to their health and security and this
nation's future. And he must insist that Congress finish the job.

This editorial is a part of a comprehensive examination of the debate over
health care reform. You can read all of these articles at:
nytimes.com/editorials.

URL: http://www.nytimes.com

LOAD-DATE: January 26, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


