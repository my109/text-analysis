                   Copyright 2010 The New York Times Company


                             1734 of 2021 DOCUMENTS


                               The New York Times

                             March 23, 2010 Tuesday
                              Late Edition - Final

Two Rallies

BYLINE: By LAWRENCE DOWNES

SECTION: Section A; Column 0; Editorial Desk; EDITORIAL NOTEBOOK; Pg. 28

LENGTH: 442 words


A few hundred Tea Party-types clustered on the south end of the Capitol on
Sunday, trying to kill health care reform, fouling the crisp spring air with
shouts of violence and loathing.

Instead of pitchforks, they hoisted revolting signs. Some showed Barack Obama as
a whiteface Joker, and some as Mao and Hitler. The Democrats were traitors and
vermin; ''government'' was an evil beast. ''Kill the bill!,'' the people
chanted, cheering House Republicans who came out to the balcony now and then to
feel the hate.

The eruption had an underground source, ugly and not always unspoken. A huge
spray-painted banner acknowledged it. A ponytailed guy held it up, advising
''All Tea Party'' on what to do ''if u hear a racial slur'': step away, point,
boo, take the person's picture and post it on the Web.

The ones who had hurled racist and antigay epithets and spat at congressmen
earlier in the day were lying low in the late afternoon. Now the crowd screamed,
''We want Stupak!'' Not to hail Representative Bart Stupak's staunch
antiabortion convictions, but to take his head off for supporting the bill.

Kill the bill. It throbbed in the ears, like an infection. I escaped it at the
other end of the National Mall, at a rally that far eclipsed the Tea Party in
size, hopefulness and decency.

Many tens of thousands of immigrants and allies were pressing for immigration
reform. It's an issue for which they have marched and waited, marched and waited
--  their hopes dashed repeatedly. Sunday's rally was a demand for action.

''We've listened quietly. We've asked politely,'' said Representative Luis
Gutierrez, an Illinois Democrat. ''We've turned the other cheek so many times
our heads are spinning.''

If anyone has reason to fear government, it is immigrants like those at the
rally, which Mr. Obama addressed via a jumbo TV screen. The government has
violently invaded their lives, broken into homes, torn parents from children and
sent them away to distant prisons. They have law-scoffing sheriffs and brutal
employers and unjust laws aiming just at them.

This is a fear the Kill-the-Billers will never know. No matter how darkly they
loathe Medicare, unemployment insurance or Social Security, the safety net is
theirs for life.

It's usually best to avoid depicting life in black-white contrast. Not this
time. Here were two rallies: one good, one loathsome. One hopeful, one paranoid.
One trying to repair how Washington works for all America, and one looking to
break it so the system can go on failing.

Kill the bill! Si, se puede! Same beat, different drums. I'll take the one that
rings with patience and hope. Sounds more American.

URL: http://www.nytimes.com

LOAD-DATE: March 23, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Editorial

PUBLICATION-TYPE: Newspaper


