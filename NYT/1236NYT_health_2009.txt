                   Copyright 2009 The New York Times Company


                             1236 of 2021 DOCUMENTS


                               The New York Times

                            December 21, 2009 Monday
                              Late Edition - Final

Buried in Health Bill, Very Specific Beneficiaries

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 1

LENGTH: 1277 words

DATELINE: WASHINGTON


Buried in the deal-clinching health care package that Senate Democrats unveiled
over the weekend is an inconspicuous proposal expanding Medicare to cover
certain victims of ''environmental health hazards.''

The intended beneficiaries are identified in a cryptic, mysterious way:
individuals exposed to environmental health hazards recognized as a public
health emergency in a declaration issued by the federal government on June 17.

And who might those individuals be? It turns out they are people exposed to
asbestos from a vermiculite mine in Libby, Mont.

For a decade, Senator Max Baucus, Democrat of Montana, has been trying to get
the government to help them. He is in a position to deliver now because he is
chairman of the Finance Committee and a principal author of the health care
bill.

Working for a 21st consecutive day, the Senate on Sunday pushed toward a  final
vote on Christmas Eve on the bill, which would provide health insurance for more
than 30 million Americans. Democrats said on Saturday that they had secured the
60th vote needed to pass the bill, and a 60-to-40 procedural vote early Monday
morning was the first in a series testing their ability to maintain party unity
on the issue.

David Axelrod, a senior adviser to President Obama, appeared on television talk
shows on Sunday with other White House aides in an effort to reframe the debate
and to rally Democrats around the bill. Despite polls that show declining public
support for the measure, Mr. Axelrod said it would prove to be popular once
people learned more about it.

''People understand we're on the doorstep of doing something really historic
that will help the American people and strengthen our country for the long
run,'' he said.

Mr. Axelrod said the provisions benefiting specific states, like Nebraska, and
favored constituencies were a natural part of the legislative process.

''Every senator uses whatever leverage they have to help their states,'' Mr.
Axelrod said on the CNN program ''State of the Union.'' ''That's the way it has
been. That's the way it will always be.''

Republicans said they would resist the legislation with every tool available,
and they denounced the deal struck on Saturday. ''This process is not
legislation,'' said Senator Tom Coburn, Republican of Oklahoma, referring to a
variety of special-interest provisions.  ''This process is corruption. It's a
shame the only way we can come to a consensus in this country is to buy votes.''

Mr. Baucus defended the assistance for those affected by the asbestos site in
his state. ''The people of Libby were poisoned and have been dying for more than
a decade,'' he said. ''New residents continue to get sick all the time. Public
health tragedies like this could happen in any town in America. We need this
type of mechanism to help people when they need it most.''

Items were inserted into the bill by the Senate majority leader, Harry Reid,
Democrat of Nevada, to get or keep the support of various lawmakers. He needs
support from all 60 members of his caucus to overcome a Republican filibuster
and pass the bill by his self-imposed Christmas deadline.

Senator Ben Nelson, Democrat of Nebraska, was the critical final Democrat to
endorse the bill. He obtained  tighter restrictions on insurance coverage of
abortion, and additional Medicaid money and other benefits for his state.

Another item in the package would increase Medicare payments to hospitals and
doctors in any state where at least 50 percent of the counties are ''frontier
counties,'' defined as those having a population density less than six people
per square mile.

And which are the lucky states? The bill gives no clue. But the Congressional
Budget Office has determined that Montana, North Dakota, South Dakota, Utah and
Wyoming meet the criteria.

Another provision would give $100 million to an unnamed ''health care facility''
affiliated with an academic health center at a public research university in a
state where there is only one public medical and dental school.

Senators and their aides said on Sunday that they were not sure who would
qualify for this money or who had requested it.

Dr. Atul Grover, the chief lobbyist for the Association of American Medical
Colleges, said he believed that Commonwealth Medical College, a new school in
Scranton, Pa., was a likely candidate.

Reached at home on Sunday, Dr. Robert M. D'Alessandri, the president of the
medical school, said initially, ''We meet the conditions'' in the Senate
proposal. But then he said he was not so sure.

The Senate health bill, like one passed by the House last month, would impose
tough new restrictions on referrals of Medicare patients by doctors to hospitals
in which the doctors have financial interests. The package assembled by Mr. Reid
would provide exemptions to a small number of such hospitals, including one in
Nebraska.

Under the original Senate bill, doctor-owned hospitals could qualify for this
exemption if they were certified as Medicare providers  by Feb. 1, 2010. Mr.
Reid's proposal would move the deadline to Aug. 1, 2010.

Molly Sandvig, executive director of Physician Hospitals of America, which
represents doctor-owned hospitals, said the change would benefit Bellevue
Medical Center, scheduled to open next year in Bellevue, Neb.

Under the proposal, Ms. Sandvig said, ''doctor-owners can continue to refer
Medicare patients to the hospital'' in eastern Nebraska.

''Senator Nelson has always been a friend to our industry,'' she said. ''But
doctor-owned hospitals in other states were not so fortunate. They would not
meet the Aug. 1 deadline.''

Another provision of the bill would increase Medicare payments to certain
''low-volume hospitals'' treating limited numbers of Medicare patients. Senator
Tom Harkin, Democrat of Iowa and chairman of the Senate health committee, said
this ''important fix'' would help midsize Iowa hospitals in Grinnell, Keokuk and
Spirit Lake.

Another item in Mr. Reid's package specifies the data that Medicare officials
should use in adjusting payments to hospitals to reflect local wage levels. The
officials can use certain new data only if it produces a higher index and
therefore higher Medicare payments for these hospitals.

Senate Democrats said this provision would benefit hospitals in Connecticut and
Michigan.

Mr. Reid's proposal also provides additional money to several states  to help
pay for the expansion of Medicaid to cover many childless adults and parents who
did not previously qualify.

Senate Democrats said Saturday that the cost would probably be less than $100
million over 10 years. But the Congressional Budget Office said Sunday that the
cost of this provision, benefiting Massachusetts, Nebraska and Vermont, ''is
approximately $1.2 billion over the 2010-2019 period.'' Massachusetts and
Vermont have been leaders in providing health insurance to their residents.

Nebraska, with help from Mr. Nelson,  won a particularly generous arrangement
under which the federal government would indefinitely pay the full cost of
covering certain low-income people added to the Medicaid rolls under the bill.

Republicans derided this provision as the ''Cornhusker kickback.'' And they said
it was typical of the favors Democrats had given to Mr. Nelson and a handful of
other senators.

''You've got to compliment Ben Nelson for playing 'The Price is Right,' '' said
Senator Richard M. Burr, Republican of North Carolina. ''He negotiated a
Medicaid agreement for Nebraska that puts the federal government on the hook
forever. Not for six years, not for 10 years. This isn't the Louisiana Purchase;
this is the Nebraska windfall.''

URL: http://www.nytimes.com

LOAD-DATE: December 21, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senators Max Baucus, left, Harry Reid and Christopher J. Dodd at
a news conference on Saturday. Mr. Reid, the majority leader, needed all 60
members of his caucus to back the health bill. (PHOTOGRAPH BY BRENDAN SMIALOWSKI
FOR THE NEW YORK TIMES) (A20) CHART: How the Health Care Proposals Compare:
Senate Democrats said Saturday that they had reached an agreement on sweeping
legislation to overhaul the nation's health care system. The House approved its
health care bill in November. The proposals are broadly similar but differ on
some major issues, like a new government insurance plan and abortion. Once the
Senate passes its bill, a House-Senate conference committee will try to iron out
the differences to produce a compromise. A look at some key differences between
the proposals: (A20)

PUBLICATION-TYPE: Newspaper


