                   Copyright 2009 The New York Times Company


                             720 of 2021 DOCUMENTS


                               The New York Times

                          September 30, 2009 Wednesday
                              Late Edition - Final

SECTION: Section A; Column 0; National Desk; WE THE PATIENTS; Pg. 20

LENGTH: 352 words


The Senate Finance Committee debated Tuesday, and then voted down, amendments by
Senators  John D. Rockefeller IV of West Virginia and Charles E. Schumer of New
York, both Democrats, which had proposed two different approaches to a
government-run public insurance option that would be offered in competition with
commercial insurance plans. The committee's chairman, Senator Max Baucus,
Democrat of Montana, was among those voting against the amendments.

In response to live coverage on the Prescriptions blog by Katharine Q. Seelye
and news reports during the day by David M. Herszenhorn, readers of the blog
posted hundreds of comments. Here is a sampling, in many cases condensed from
longer postings.

A simple ''Medicare for all'' plan would solve the problems of health care in
the fairest way: it is no accident that every civilized country on earth has
some form of single-payer, and we need and deserve no less. At a minimum, we
must have the public option: without it, any ''reform'' is just Potemkin health
care. -- Susan

The only real problem with the plan that I have is that small ''government-run''
part. -- Lewis B. Sckolnick

We have public highways, public water and sewer systems, public health
institutions, National Institutes that publicly fund vast amounts, biomedical
research-national programs to promote IT and software development, and yet: All
of the benefits of the aforementioned ''public'' options are not being addressed
in the rhetorical babble. Let's get real about getting our money's worth from
all of the above. Let's create a real National Health Care system -- Jonathan
Eberle

A public option would lead to less competition, higher costs and ultimately
fewer choices when the government drives the private insurers out of business
(which is the ultimate goal of the far left). -- Jerry

I wish the public could take away Sen. Max Baucus' health insurance, and then
tell him he needs emergency open-heart surgery that would leave him $100,000 in
debt as he makes only $25,000 as a factory worker. Maybe then he would redraft
his health insurance bill. -- Rusty Shackleford

URL: http://www.nytimes.com

LOAD-DATE: September 30, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


