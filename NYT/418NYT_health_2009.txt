                   Copyright 2009 The New York Times Company


                             418 of 2021 DOCUMENTS


                               The New York Times

                            August 27, 2009 Thursday
                              Late Edition - Final

China Announces a System For Voluntary Organ Donors

BYLINE: By MICHAEL WINES

SECTION: Section A; Column 0; Foreign Desk; Pg. 4

LENGTH: 592 words

DATELINE: BEIJING


China has inaugurated a voluntary organ donor program, hoping to overhaul a
system that now harvests a vast majority of its organs from black-market sellers
and executed prisoners and leaves millions of ailing people without hope of
getting transplants.

The new program, run by the national Red Cross Society with help from China's
Health Ministry, was reported by the state-run English-language newspaper China
Daily on Wednesday. The newspaper quoted the vice minister of health, Huang
Jiefu, as saying the goal was to create an organ donation system that ''will
benefit patients regardless of social status and wealth.''

At least one million people in China need organ transplants each year, but only
about 10,000 receive them, according to government statistics. Dr. Huang said
that most of those organs -- as high as 65 percent, by some estimates -- were
taken from death-row inmates after their executions.

China does not publicly report execution figures, but Amnesty International
estimates that 1,718 prisoners were put to death in 2008, the highest number of
any nation. The government said last month that it planned to cut the number of
executions to ''an extremely small number'' by changing criminal laws and
issuing more suspended death sentences.

The practice of harvesting organs from executed Chinese convicts has been widely
reported in the past, although it was only confirmed in 2005, by Dr. Huang, at a
medical conference in Manila. The government has routinely denied other
allegations that prisoners' organs regularly found their way to the black
market, often for sale to wealthy foreigners, and that executions were sometimes
scheduled to coincide with the need for a specific organ.

At a news conference in Shanghai held Wednesday to unveil the new organ donation
system, a transplant surgeon, Qian Jianmin, chief transplant surgeon with the
Shanghai Huashan Hospital, was quoted by the newspaper as saying that the
removal of organs from convicts was sometimes subject to corruption. Dr. Huang
was quoted as saying that prisoners must give written consent for their organs
to be used in transplants and that their rights are protected.

Nevertheless, he said, inmates ''are definitely not a proper source for organ
transplants.''

Patients also receive some organs, like livers and kidneys, from living
relatives and loved ones. But many other organs are bought on the black market
by those rich enough to afford them, despite a law that bans organ trafficking.

A black-market kidney can cost up to 200,000 renminbi, or about $29,300, the
newspaper stated, plus transplant costs. Because so many people are desperate to
receive transplants, corruption is a serious problem, and citizens can wait
years before being matched with donors, if a donor is ever found.

It is almost unheard of for Chinese citizens to volunteer to donate their organs
after death. Only about 130 people have pledged to donate their organs since
2003, the newspaper stated, quoting Chen Zhonghua, a professor at Tongji
Hospital's Institute of Organ Transplantation in Shanghai.

Dr. Chen recently told the southern China newspaper Nanfang Daily that
transplant efforts were hampered not only by a lack of donors but also by the
lack of a system to match organs with a person in need. As a result, he said,
only a fraction of viable organs are used.

The new system will start as an experimental program in 10 cities, including
Shanghai, and later will be rolled out nationwide. The details of the system are
still being worked out, officials said.

URL: http://www.nytimes.com

LOAD-DATE: August 27, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


