                   Copyright 2009 The New York Times Company


                             1203 of 2021 DOCUMENTS


                               The New York Times

                            December 18, 2009 Friday
                              Late Edition - Final

Unions Spurn Senate Bill

BYLINE: By KATHARINE Q. SEELYE

SECTION: Section A; Column 0; National Desk; LABOR'S COOL RECEPTION; Pg. 27

LENGTH: 250 words


The A.F.L.-C.I.O. and the Service Employees International Union, two of the
nation's biggest labor organizations and strong allies of the Obama
administration, have withheld their endorsements of the Senate health care bill
now that the Senate has dropped the public option.

Instead, they are vowing to renew their fight for a public option -- a
government-run insurance program -- and for other provisions once the Senate
approves its final bill and negotiators begin melding it with the House bill,
which does provide for the option.

Andy Stern, the service employees' president, told reporters in a conference
call on Thursday that the Senate ''had done all it's going to do.''

It is time for the Senate ''to send this bill on to conference, where the real
work needs to be done,'' Mr. Stern said.

The A.F.L.-C.I.O. took the same tack, calling the Senate bill ''inadequate'' and
''too kind to the insurance industry.'' Richard L. Trumka, its president, said
in a statement that his union would work to lower the cost of health care and
hold insurance companies accountable. The House bill, he said, ''is the model
for genuine health care reform.''

The statements underscored the deepening divisions within the Democratic Party
base. Most senators in the Democratic caucus backed the new bill.

At the same time, two consumer advocacy groups, Families USA and the United
States Public Interest Research Groups, endorsed the Senate bill, saying the
good  outweighed the bad. KATHARINE Q. SEELYE

URL: http://www.nytimes.com

LOAD-DATE: December 18, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


