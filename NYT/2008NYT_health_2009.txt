                   Copyright 2010 The New York Times Company


                             2008 of 2021 DOCUMENTS


                               The New York Times

                             May 27, 2010 Thursday
                              Late Edition - Final

4 Guilty Pleas in Incident at Senator's Office

BYLINE: By CAMPBELL ROBERTSON

SECTION: Section A; Column 0; National Desk; Pg. 19

LENGTH: 322 words


The conservative provocateur James O'Keefe and three other men pleaded guilty in
federal court on Wednesday to a misdemeanor in a scheme in which they posed as
telephone repairmen in Senator Mary L. Landrieu's New Orleans office.

Magistrate Daniel Knowles III, who cited the defendants' potential as
investigative journalists though he was critical of this incident, sentenced Mr.
O'Keefe, 25, to three years of probation, 100 hours of community service and a
$1,500 fine. As Mr. O'Keefe was considered the ringleader, his fellow
defendants, Joseph Basel, Stan Dai and Robert Flanagan, were given lesser
sentences of two years of probation, 75 hours of community service and $1,500
fines.

On Jan. 25, two of the men entered the office of Ms. Landrieu, a Democrat,
pretending to be telephone repairmen, one of them wearing a hidden video camera
on his hard hat. Mr. O'Keefe was also in the office pretending to wait for a
friend, but secretly recording the interaction, and another man was waiting
outside. All four were arrested and eventually charged with entering federal
property under false pretenses, a misdemeanor. The charge carries a maximum term
of six months in prison and a $5,000 fine.

Mr. O'Keefe has said they were in the office because of reports that Ms.
Landrieu's constituents had trouble reaching her office during the health care
debate, though a spokesman for the senator said that her voice mail and that of
several other senators were jammed at the time by an unusually high volume of
calls.

Mr. O'Keefe, whom the judge described as ''extremely talented,'' gained fame for
secretly recording conversations with workers for Acorn, the community
organizing group. In at least one video, Acorn workers advised a conservative
activist who was posing as a prostitute how to conceal her criminal activities
in the course of trying to buy a house. The heavily edited videos severely
damaged Acorn's reputation.

URL: http://www.nytimes.com

LOAD-DATE: May 27, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


