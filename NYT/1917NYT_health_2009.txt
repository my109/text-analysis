                   Copyright 2010 The New York Times Company


                             1917 of 2021 DOCUMENTS


                               The New York Times

                            April 21, 2010 Wednesday
                              Late Edition - Final

Everybody Loves A Winner

BYLINE: By THOMAS L. FRIEDMAN.

Maureen Dowd is off today.

SECTION: Section A; Column 0; Editorial Desk; OP-ED COLUMNIST; Pg. 27

LENGTH: 850 words


I've been thinking about President Obama's foreign policy lately, but first, a
golf tip: I went to Dave Pelz's famous short-game school this winter to improve
my putting and chipping, and a funny thing happened --  my long game got better.
It brings to mind something that happened to Obama. The president got health
care reform passed, and it may turn out to be his single most important foreign
policy achievement.

In politics and diplomacy, success breeds authority and authority breeds more
success. No one ever said it better than Osama bin Laden: ''When people see a
strong horse and a weak horse, by nature they will like the strong horse.''

Have no illusions, the rest of the world was watching our health care debate
very closely, waiting to see who would be the strong horse --  Obama or his
Democratic and Republican health care opponents? At every turn in the debate,
America's enemies and rivals were gauging what the outcome might mean for their
own ability to push around an untested U.S. president.

It remains to be seen whether, in the long run, America will be made physically
healthier by the bill's passage. But, in the short run, Obama definitely was
made geopolitically healthier.

''When others see the president as a winner or as somebody who has real
authority in his own house, it absolutely makes a difference,'' Defense
Secretary Robert Gates said to me in an interview. ''All you have to do is look
at how many minority or weak coalition governments there are around the world
who can't deliver something big in their own country, but basically just teeter
on the edge, because they can't put together the votes to do anything
consequential, because of the divided electorate.'' President Obama has had ''a
divided electorate and was still able to muscle the thing through.''

When President Dmitri Medvedev of Russia spoke by phone with Obama the morning
after the health care vote  -- to finalize the New Start nuclear arms reduction
treaty  --  he began by saying that before discussing nukes, ''I want to
congratulate you, Mr. President, on the health care vote,'' an administration
official said. That was not just rank flattery. According to an American
negotiator, all throughout the arms talks, which paralleled the health care
debate, the Russians kept asking: ''Can you actually get this ratified by the
Senate'' if an arms deal is cut? Winning passage of the health care bill
demonstrated to the Russians that Obama could get something hard passed.

Our enemies surely noticed, too. You don't have to be Machiavelli to believe
that the leaders of Iran and Venezuela shared the barely disguised Republican
hope that health care would fail and, therefore, Obama's whole political agenda
would be stalled and, therefore, his presidency enfeebled. He would then be a
lame duck for the next three years and America would be a lame power.

Given the time and energy and political capital that was spent on health care,
''failure would have been unilateral disarmament,'' added Gates. ''Failure would
have badly weakened the president in terms of dealing with others  --  his
ability to do various kinds of national security things. ... You know, people
made fun of Madeleine [Albright] for saying it, but I think she was dead on:
most of the rest of the world does see us as the 'indispensable nation.' ''

Indeed, our allies often complain about a world of too much American power, but
they are not stupid. They know that a world of too little American power is one
they would enjoy even less. They know that a weak America is like a world with
no health insurance  --  and a lot of pre-existing conditions.

Gen. James Jones, the president's national security adviser, told me that he
recently met with a key NATO counterpart, who concluded a breakfast by
congratulating him on the health care vote and pronouncing: ''America is back.''

But is it? While Obama's health care victory prevented a power outage for him,
it does not guarantee a power surge. Ultimately, what makes a strong president
is a strong country  --  a country whose underlying economic prowess, balance
sheet and innovative capacity enable it to generate and project both military
power and what the political scientist Joe Nye calls ''soft power'' -- being an
example that others want to emulate.

What matters most now is how Obama uses the political capital that health care's
passage has earned him. I continue to believe that the most important foreign
policy issue America faces today is its ability to successfully engage in nation
building  --  nation building at home.

Obama's success in passing health care and the bounce it has put in his step
will be nothing but a sugar high if we can't get our deficit under control,
inspire a new generation of start-ups, upgrade our railroads and Internet and
continue to attract the world's smartest and most energetic immigrants.

An effective, self-confident president with a weak country is nothing more than
a bluffer. An effective, self-confident president, though, at least increases
the odds of us building a stronger country.

URL: http://www.nytimes.com

LOAD-DATE: April 23, 2010

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


