                   Copyright 2009 The New York Times Company


                             599 of 2021 DOCUMENTS


                               The New York Times

                           September 15, 2009 Tuesday
                              Late Edition - Final

One Injury, 10 Countries: A Journey in Health Care

BYLINE: By ABIGAIL ZUGER, M.D.

SECTION: Section D; Column 0; Science Desk; BOOKS; Pg. 1

LENGTH: 1127 words


With all due respect to the seminar room, the boardroom, the hearing room and
the Oval Office, a better vantage point than any of them for evaluating and
redesigning our health care system is the hospital room (window bed, please).

The chair next to the bed isn't bad, either.

Some of us perch on one or the other almost every day, observing the tangled
mess that is our current system and mentally designing a dozen better
alternatives. But for those who wind up in bed or a chair only when tragedy
strikes, T. R. Reid's new book provides an excellent substitute perspective.

Mr. Reid, a veteran foreign correspondent for The Washington Post, knows from
personal experience that there are indeed a dozen better alternatives.
International postings from London to Japan familiarized him with many of the
world's health care systems. Then a chronic shoulder problem offered the
opportunity for an unusually well-controlled experiment: Mr. Reid decided to
present his stiff shoulder for treatment around the world.

One shoulder, 10 countries. Admittedly it's a gimmick, but what saves the book
from slumping into a sack of anecdotes like Michael Moore's 2007 documentary
''Sicko'' is a steel backbone of health policy analysis that manages to trap
immensely complicated concepts in crystalline prose.

''The Healing of America'' blends subjective and objective into a seamless
indictment of our own disastrous system, an eloquent rebuttal against the
arguments used to defend it, and appealing alternatives for fixing it.

Mr. Reid starts with a methodical clarification of terms. First: universal
health care. Far from a single socialized system, the various plans other
countries use to cover all their residents are quite distinct. Some are as
private as our own, and most offer considerably more in the way of choice.

In Japan, and many European countries, private health insurers -- all of them
nonprofit -- finance visits to private doctors and private hospitals through a
system of payroll deductions.

In Canada, South Korea and Taiwan, the insurer is government-run and financed by
universal premiums, but doctors and hospitals are private.

In Britain, Italy, Spain and most of Scandinavia, most hospitals are
government-owned, and a tax-financed government agency pays doctors' bills.

In poor countries around the world, private commerce rules: residents pay cash
for all health care, which generally means no health care at all.

Similarly, what Americans often consider a single unique system of health care
is an illusion: we exist in a sea of not-so-unique alternatives. Like the
citizens of Germany and Japan, workers in the United States share insurance
premiums with an employer. Like Canadians, our older, destitute and disabled
citizens see private providers with the government paying. Like the British,
military veterans and Native Americans receive care in government facilities
with the government paying the tab. And like the poor around the world, our
uninsured pay cash, finagle charity care,  or stay home.

Our archipelago of plans means that those safe on a good island with good
insurance can be delighted with the system, even as millions of invisible fellow
citizens tread water or drown offshore. It means that those on a mediocre island
are stuck there. It also means that not one single piece of the infrastructure
-- like record keeping, drug pricing and administrative costs -- can be
streamlined across islands in any meaningful way. Hence the expense, the
inequity and the tragedy.

When Mr. Reid presents his shoulder to his own orthopedist in Colorado, the
doctor is quick to recommend a shoulder replacement. It will cost his insurer
tens of thousands of dollars (assuming it agrees to pay), with unknown
co-payments for him. Risks include all those of major surgery; benefits include
a restored golf swing.

The same shoulder gets substantially different reactions elsewhere in the world.

In France, a general practitioner sends him to an orthopedist (out-of-pocket
consultation fee: $10) who recommends physical therapy, suggests an easily
available second opinion if Mr. Reid really wants that surgery, and notes that
the cost of the operation will be entirely covered by insurance (waiting time
about a month).

In Germany, the operation is his for the asking the following week, for an
out-of-pocket cost of about $30.

In London, a cheerful general practitioner tells Mr. Reid to learn to live with
his shoulder. No joint replacement is done in Britain without disability far
more serious than his to justify the expense and the risks, and if his golf game
is that important, he can go private and foot the bill himself.

In Japan, the foremost orthopedist in the country (waiting time for an
appointment, less than a day) offers a range of possible treatments, from
steroid injections to surgery, all covered by insurance. (''Think about it, and
call me.'')

In an Ayurvedic hospital in India, a regimen of meditation, rice, lentils and
massage paid for entirely out of pocket, $42.85 per night, led to ''obvious
improvement in my frozen joint,'' Mr. Reid writes, adding, ''To this day, I
don't know why it happened.''

But the comparative merits of different orthopedic philosophies are secondary
here: Mr. Reid's attention is focused on a meticulous deconstruction of the
history and philosophy of the policy decisions behind them.

Among health policy narratives, this book's clarity, comprehensiveness and
readability are exceptional, and its bottom line is a little different from
most. Instead of rationalization and hand-wringing, Mr. Reid offers an array of
possible solutions for our crisis. As the proverb goes (it is a favorite among
policy wonks): ''To find your way in the fog, follow the tracks of the oxcart
ahead.'' We have plenty of reasonable paths to follow.

And plenty of reasons to follow them. Mr. Reid's underlying message of hope does
not preclude an intensely satisfying quotient of moral outrage at the worst
casualties of our system as it stands.

One is the uninsured working person, too rich for Medicaid, too poor for a
standard insurance policy, at first too proud to acknowledge disability, and
then too sick for the process that a formal declaration of disability requires.
These are the people who die of treatable illness in our country.

And then there is the insured working person who discovers, with surprise, that
health insurance is a for-profit industry, that the industry term for payment is
''medical loss'' and that the process of extracting payment for a dire health
condition can turn into a bizarre game of ''catch me if you can.''

A person's last days can be spent in any number of ways. But on the phone
pleading with an insurer, that's only in America.

URL: http://www.nytimes.com

LOAD-DATE: September 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: (pg.D1)
 TRAVELED: The author T. R. Reid had his sore shoulder examined around the
world. (PHOTOGRAPH BY KATAYAMA KISHIN) (pg. D6)

DOCUMENT-TYPE: Review

PUBLICATION-TYPE: Newspaper


