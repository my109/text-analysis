                   Copyright 2009 The New York Times Company


                             458 of 2021 DOCUMENTS


                               The New York Times

                             August 31, 2009 Monday
                              Late Edition - Final

Inside the Times

SECTION: Section A; Column 0; Metropolitan Desk; Pg. 2

LENGTH: 1230 words


International

IRANIAN PANEL TO OVERSEE

Investigations Into Unrest

Political analysts said that conservative rivals of President Mahmoud
Ahmadinejad of Iran have appointed a committee to supervise investigations into
the unrest that swept the nation after he claimed a landslide victory in the
disputed presidential election in June, in yet another challenge to his drive to
consolidate power. PAGE A4

GABON'S VOTERS HEAD TO POLLS

After more than four decades under the unchallenged rule of one of Africa's last
''Big Men,'' the late El Hadj Omar Bongo Ondimba, citizens in Gabon voted for a
new president, and many expressed a keen desire for a break from the grip of the
Bongo family. PAGE A7

National

FIGHTING HEALTH OVERHAUL, AND PROUD OF IT

Senator Jim DeMint, the South Carolina Republican who predicted that President
Obama's effort to overhaul the health care system would become his ''Waterloo,''
is doing his best to make that happen. PAGE A12

A FOCUS ON REVITALIZATION

On the fourth anniversary of the hurricane that redefined its future, New
Orleans is no longer talking about mere recovery. The talk is about
revitalization, a buzzword and an idea that drives the new energy in the city,
carried by an intensity and an idealism that would have bordered on indecent in
the city's charmingly carefree old days. PAGE A8

TIGHTENING LEED RULES

Builders covet LEED certification -- it stands for Leadership in Energy and
Environmental Design -- as a way to gain tax credits, attract tenants, charge
premium rents and project an image of environmental responsibility. But the gap
between design and construction and how some buildings actually perform, led the
program to announce that it would begin collecting information about energy use
from all the buildings it certifies. PAGE A8

New York

CONCERN IS HIGH THAT MOB

May Seek Cut of Stimulus

Federal and state investigators who track organized crime believe that mob
figures have geared up to take advantage of the swift and enormous cash influx
from the federal stimulus.  PAGE A13

A CAFE FROZEN IN TIME

The decision to close the Cafe des Artistes, the storied New York City
restaurant on West 67th Street that was saddled with steady losses and a union
lawsuit, was also the death of an intrinsic part of old New York.  PAGE A14

The Commissioner's Neckties A13

Business

TRIBUNE CHIEF MAY REMAIN

For Company's Cleanup

The Tribune Company may emerge from bankruptcy this fall with much of its
current top management intact, but it remains unclear whether that might include
the top dog himself, Samuel Zell, the chairman and chief executive who took the
company private.  PAGE B1

STILL CHALLENGING GOOGLE

For nearly a decade, Qi Lu has played a leading role in building Yahoo's
Internet search and advertising technologies. He left Yahoo 14 months ago, but
finds himself once again leading the charge against Google for Microsoft.  PAGE
B1

PAPERS TO FILE FOR BANKRUPTCY

Freedom Communications, owner of The Orange County Register and 30 other
newspapers, is expected to file for bankruptcy this week under a plan that will
hand its publications to its lenders, people briefed on the matter said. PAGE B3

WELCOME TO REAPPLY

Nearly 200 employees of The Journal News, a Westchester daily owned by Gannett,
were told that jobs were being redefined, that they all would need to reapply
for the new positions and that by the time the music stopped, 70 of them would
be without jobs. David Carr, The Media Equation.  PAGE B1

PROTESTS OF FILM'S EDIT

The DVD for the HBO documentary ''Thrilla in Manila,'' about the rivalry between
Muhammad Ali and Joe Frazier and their climactic third fight, includes cuts from
one crucial scene. It is evidence, some say, of whitewashing the film to portray
Ali in the most flattering light.  PAGE B5

CHINA TRADE DISPUTE ENDS

China's choice to comply with the first World Trade Organization decision
against it, on auto parts, would make little difference to General Motors, a
senior company executive said. PAGE B3

SEEKING A BAN ON TEXTING

An organization of state highway safety officials plans to call for a ban on
texting while driving, joining a growing chorus of legislators and safety
advocates endorsing such a policy. PAGE B4

Arts

A DAUGHTER'S MEMOIR,

A Mother's Anguish

In her memoir ''Lies My Mother Never Told Me,'' Kaylie Jones remembers the
supposedly charmed life she led as the daughter of a stellar writer, James
Jones, as well as the turmoil wrought by her mother's alcoholism.  PAGE C1

RECONSIDERING A PIONEER

Paul Bowles was an elusive figure throughout his long life, and he remains
nearly as elusive now. A decade after his death, his achievements as a writer,
as well as the flaws that plague even his best work, are worth re-examining,
Dwight Garner writes. Page C1

TROUBLES MADE PUBLIC

Julie Myerson, a high-profile novelist and occasional television commentator,
was pilloried in her native Great Britain for writing about her teenage son's
descent into drug addiction in the memoir ''The Lost Child: A Mother's Story.''
The book is set to be released by Bloomsbury in the United States, and Ms.
Myerson is bracing for another round of recriminations.  PAGE C1

A WORDY TAKE ON LOVE

The repertory troupe Shakespeare & Company decided to take on John Patrick
Shanley's ''Dreamer,'' an extreme example of what good acting is often said to
be about: externalizing the internal. Review by Ben Brantley. PAGE C1

EMPHASIS ON THE NEO

The Locrian Chamber Players performed a number of neo-Romantic string works
Thursday evening at Riverside Church -- none more than a decade old -- in a
concert that was also to some extent a tribute to John Kreckler, one the
ensemble's founders, who died in June. Review by Allan Kozinn. PAGE C3

HONORING BIRD AND JACKSON

Charlie Parker and Michael Jackson share a birthday, and so the long wave of
this summer's Jackson tributes rolled through the annual Charlie Parker Jazz
Festival on Saturday. Review by Ben Ratliff  PAGE C3

Arts, Briefly C2

Obituaries

HYMAN BLOOM, 96

A mystical and reclusive painter, he was for a brief time in the 1940s and '50s
regarded as a precursor to the Abstract Expressionists and one of the most
significant American artists of the post-World War II era. PAGE A17

SHEILA LUKINS, 66

She was an owner of the Silver Palate food shop and an author of four Silver
Palate cookbooks, and helped usher in the new American cooking of the 1980s.
PAGE A17

Sports

A QUICK STUDY

On the Drag Strip

After giving up a budding career as a sprinter, Antron Brown, a 33-year-old
native of Chesterfield, N.J., quickly progressed in the world of drag racing,
and currently sits in first place in the standings for the 7,000-horsepower
top-fuel dragsters.  PAGE D2

NEW WAYS TO LURE FANS

In an effort to fill seats during difficult economic times, marketing officials
for N.B.A. teams are stretching their imaginations in an effort to get fans to
stretch their entertainment dollars.  PAGE D6

FEELING THE TICKET CRUNCH

Saturday night's preseason game between the Giants and Jets was the last between
the New York teams in their current Meadowlands stadium before they move next
door to a new stadium. But as the teams sell seats for both stadiums, they face
economic forces that daunt even big-ticket sports franchises in major markets
during a severe recession.  PAGE D1

URL: http://www.nytimes.com

LOAD-DATE: August 31, 2009

LANGUAGE: ENGLISH

DOCUMENT-TYPE: Summary

PUBLICATION-TYPE: Newspaper


