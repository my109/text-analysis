                   Copyright 2010 The New York Times Company


                             1310 of 2021 DOCUMENTS


                               The New York Times

                           January 6, 2010 Wednesday
                              Late Edition - Final

Democratic Senator Won't Run Again

BYLINE: By DAVID M. HERSZENHORN and CARL HULSE

SECTION: Section A; Column 0; National Desk; Pg. 15

LENGTH: 457 words

DATELINE: WASHINGTON


Senator Byron L. Dorgan, Democrat of North Dakota, announced abruptly on Tuesday
that he would not seek re-election this year -- a clear sign of the difficulties
Democrats will face in defending their large Congressional majorities in the
midterm elections.

Mr. Dorgan, who is completing his third term, has been regarded for months as
one of the most vulnerable Democratic incumbents. Gov. John Hoeven, a popular
Republican, has been weighing a run for Senate, and recent polls indicate he
would easily defeat Mr. Dorgan, who nonetheless remains well-regarded by voters.

His announcement came only hours before word spread that a  Democratic senator
considered even more vulnerable than Mr. Dorgan, Christopher J. Dodd of
Connecticut, had decided not to seek re-election.

Mr. Dorgan's decision increases both the likelihood that Mr. Hoeven, a
three-term governor will pursue the open Senate seat and that Democrats will
lose it.

For now, Democrats nominally control 60 seats in the Senate, which is the
precise number needed to overcome Republican filibusters. The major health care
legislation, for instance, is advancing only because all 60 members of the
Democratic caucus, including two independents, have united in support of it.

Democratic incumbents also face serious challenges in Arkansas, Colorado,
Connecticut  and Pennsylvania, among other states, while Senator Harry Reid, the
party leader, is in his own tough fight for re-election in Nevada.

At the same time, Democrats have some opportunity to pick up Republican seats
because of retirements in Florida, Missouri, New Hampshire and Ohio.

Mr. Dorgan has grown increasingly pessimistic about his prospects for
re-election in recent weeks as he traveled around the state and met with
constituents, and he did not hide his sentiment that the Congress and the Obama
administration erred in putting too much emphasis on the health care overhaul
and not enough on job-creating economic programs.

But in a statement released Tuesday he said his decision was based on a desire
to engage in other pursuits after more than 30 years in public service.

''Although I still have a passion for public service and enjoy my work in the
Senate, I have other interests and I have other things I would like to pursue
outside of public life,'' Mr. Dorgan said, adding that he was interested in
writing, teaching and working on energy policy in the private sector.

Senate Republicans quickly hailed the news of Mr. Dorgan's retirement as a sign
of their brightening political prospects.

''This development is indicative of the difficult environment and slumping
approval ratings that Democrats face,'' said Brian Walsh, a spokesman for the
National Republican Senatorial Committee.

URL: http://www.nytimes.com

LOAD-DATE: January 6, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


