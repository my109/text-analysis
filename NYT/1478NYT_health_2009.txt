                   Copyright 2010 The New York Times Company


                             1478 of 2021 DOCUMENTS


                               The New York Times

                            February 5, 2010 Friday
                              Late Edition - Final

Stalled Health Care Bill Leaves Drug Makers in Regulatory Limbo

BYLINE: By DUFF WILSON

SECTION: Section B; Column 0; Business/Financial Desk; Pg. 1

LENGTH: 1218 words


With the possible demise of health care legislation, getting back to business as
usual may not be the best thing for the nation's drug makers.

After all, in return for the prospect of tens of millions of newly insured
customers and a large degree of regulatory certainty, the pharmaceutical
industry had agreed to pay a relatively small price: $8 billion a year in
discounts and fees. It was a modest compromise for an industry with $246 billion
in prescription drug sales last year.

But now, with the health care overhaul on a back burner in Washington and
possibly dead for this year, drug makers are getting a sinking sense of how a
piecemeal public policy future might look  for them.

President Obama's proposed budget this week, for example, includes a plan he
alluded to in last week's State of the Union address: a new tax on profits from
some patents and other intangible assets parked in overseas tax havens by
American companies.

For drug makers, which are among those most likely to be affected by such a tax,
the president's  proposal ''pretty much came out of the blue,'' said Martin A.
Sullivan, an economist formerly with the Treasury Department and Joint Committee
on Taxation.

Another expert agreed. ''Pharma's one that would really get hit,'' said Erik
Gordon, a professor at the Ross School of Business at the University of
Michigan. ''That's a biggie.''

The industry giant Pfizer, for example, which has said that 88 percent of its
$56 billion in income from 2004 through 2008 originated overseas, could be
subject to the corporate 35 percent tax rate on at least some of its foreign
profit in the future if the president's proposal goes through.

Although the tax idea faced a cool reception from Democratic and Republican
leaders of the Senate Finance Committee, it highlighted the risks to the drug
industry of unpredictable election-year politics. It is a time when
bolt-from-the-blue challenges may await drug makers, compared   with the
carefully mapped future they had worked out with the White House and the Senate
under the health care legislation.

As if to reinforce the continuing business challenges that drug makers face,
regardless of carrots and sticks from Washington, Pfizer issued an earnings
report and financial forecast this week that disappointed Wall Street. The
report, on Wednesday, has helped send its shares down 6.3 percent in the last
two days.

Pfizer lowered its 2010 and 2012 forecasts, citing the rising dollar and the
sale or spinoff of some assets, including animal health products and an H.I.V.
drug. The company also noted the coming patent expirations on some of its
blockbuster drugs, including Lipitor, as well as growing competition from
lower-priced generics.

Several of the other big American drug companies reported a mixed bag of
results last week. Johnson & Johnson  posted a rare yearly decline in sales and
gave a disappointing 2010 profit forecast. Bristol-Myers Squibb's earnings per
share beat analysts' estimates, helped by a lower-than-expected tax rate. Eli
Lilly profit fell just short of expectations,  and its shares declined as
investors also showed concern about expirations of its drug patents.

The other big American drug maker, Merck,  plans to report on Feb. 16.

None of those drug makers had forecast effects from a health care overhaul by
Washington. But all of them had basically endorsed  the Senate bill.

Just last month, before  the Republican candidate Scott Brown's victory in a
special election in Massachusetts upended the Democrats' supermajority in the
Senate, Pfizer's chief executive, Jeffrey B. Kindler, told analysts that the
Senate health care bill was ''largely consistent with the principles''  that
were important to his industry.

The drug industry had forecast lower revenue in the early years of its
cost-savings deal, in which it agreed to pay $80 billion in rebates and fees
over a decade. But its revenue and profit were expected to more than make up the
difference later in that period  as more than 30 million uninsured people began
receiving health and drug coverage.

And just as important, the industry had received assurances from the Obama
administration and some critical Congressional Democrats that long-feared
proposals that might have popular  appeal -- including government negotiation of
Medicare drug prices and allowing the import of cheaper drugs from Canada --
would be tabled in return for drug makers' support of the rest of the health
care overhaul. And some industry experts say any notions of an offshore tax had
also been taken off the table.

As part of the deal, the drug industry had also pledged and spent more than $100
million on TV advertising in support of the overhaul effort.

It is unlikely that the drug industry will run ads promoting President Obama's
offshore tax proposal. Drug makers are fighting it through a Washington-based
business coalition called Promote America's Competitive Edge, which includes
many other multinationals.

The group argues that lower foreign tax rates create a level playing field for
American companies and international competitors and, ultimately, save jobs in
the United States.

Mr. Sullivan, the former Treasury economist who now writes about tax issues,
said that all the big drug makers engage in offshore tax sheltering of one sort
or another. And the portion of overseas profit and revenue has been growing in
recent years.

''Typically when a pharmaceutical company develops a new drug, it transfers it
to a holding company in a tax haven like Bermuda or the Cayman Islands, usually
on very favorable terms,'' he said. ''There's a tremendous amount of income
taken out of the U.S. and put into the tax haven. This proposal seems targeted
to just that type of situation.''

Pfizer has reported that 58 percent of its revenue came from overseas in 2008,
compared with 39 percent a decade earlier. For other drug markets, overseas
revenue in 2008 amounted to 46 percent for Lilly, 44 percent for Merck and 42
percent for Bristol-Myers.

Professor Gordon of  the University of Michigan, who studies pharmaceutical
companies, said the president's new plan to tax ''excessive'' offshore profits
had surprised the industry and revealed the risks of a piecemeal health policy.

''The industry thought that it got some uncertainty off the table,'' Professor
Gordon said. ''They're not thrilled about what happened in Massachusetts. It's
not working to their advantage.''

Some analysts, meanwhile, say parts of the $80 billion, 10-year  deal could come
back to bite the drug industry -- if it is held to some of the cost pledges
without receiving the offsetting political protection.

The pact,  for instance, committed the industry to support an increase in the
rebate the government receives on Medicaid drugs  -- to 23 percent, up from 15
percent currently.

In the current deficit-cutting environment, that discount may still look
attractive to lawmakers. And it could conceivably be enacted by Congress at any
time without being part of an industry quid pro quo. ''No question that's
alive,'' said Seamus Fernandez, pharmaceutical industry analyst for the health
care investment bank Leerink Swann.

But the overall health care reform package, he added, is ''either dead or
severely hobbled.''

URL: http://www.nytimes.com

LOAD-DATE: February 5, 2010

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


