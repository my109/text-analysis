                   Copyright 2009 The New York Times Company


                             790 of 2021 DOCUMENTS


                               The New York Times

                           October 10, 2009 Saturday
                              Late Edition - Final

It's Time for the Annual Task of Choosing Your Insurance Plan

BYLINE: By LESLEY ALDERMAN

SECTION: Section B; Column 0; Business/Financial Desk; PATIENT MONEY; Pg. 6

LENGTH: 1172 words


IT'S that time of year again. In a few weeks, if you have health benefits
through work, you will probably receive e-mail messages and brochures from your
employer about your benefit options for 2010.

Once again you will have to decide whether you want to switch to a new health
plan. Or finally enroll in a flexible spending account. Or take that biometric
test your company has been urging you toward. (More on that below.)

If, like many people, you typically default to your current options, I have one
word of advice: Don't. Health insurance costs will be higher, and doing nothing
could potentially cost you hundreds of dollars.

My job today is to highlight the main trends this year in employee health
benefits. Over the next few weeks, my colleague Walecia Konrad and I plan to
delve into more specific elements of open enrollment, including the pros and
cons of high-deductible health plans and how to select a new health insurer.

This year, as you face higher prices on insurance premiums, brand-name
prescriptions and more, your employer will be nudging you to take more interest
in managing your health and your health insurance. Sometimes those nudges might
feel more like shoves.

What you probably won't see are many major changes to your health plan's overall
design. That is mainly because employers are waiting to see what shakes out from
the health care debate in Washington before they take any big cost-saving steps,
says Mike Miele, president of the benefits consulting firm, GBS Healthcare
Analytics in Princeton, N.J.

But here's a rundown on the changes you probably can expect.

YOU'LL PAY MORE -- AGAIN Health care costs keep going up and up, and the slow
economy and rocky job market have not helped.

Last year, the recession jolted many employees into get-well-quick frenzies.
Fearing layoffs -- and the loss of health coverage -- employees got check-ups,
teeth cleanings and various long-deferred procedures at a higher-than-normal
pace.

That use-or-lose stampede translated into a spike in medical costs for employers
-- which they are now passing on to those still fortunate enough to have jobs,
according to Joan Smyth, a principal with Mercer, a health and benefits
consulting firm.

If you are in an H.M.O. -- a health maintenance organization --  or P.P.O., a
preferred-provider organization, your out-of-pocket costs, including premiums
and co-payments, will probably rise by about 10 percent in the coming year.

In addition, many companies will raise deductibles by $50 to $100 -- some even
more.

DEPENDENTS? DEPENDS ...  Even if your spouse or other dependents have been on
your health plan, be sure to look for changes in the coverage rules. Some
companies are raising premiums for working spouses -- or even eliminating
coverage for spouses and dependents.

FEWER CHOICES Many large companies with employees in a number of states are
consolidating their health plans into one or two national carriers, rather than
various regional ones.

In the same vein, some companies will jettison inefficient H.M.O.'s and either
replace them with more efficient ones or get rid of them altogether, replacing
them with lower-cost plans, either P.P.O.'s or high-deductible plans.

PUSHING HIGH-DEDUCTIBLES More employers will be nudging workers toward
high-deductible health plans because they cost less -- about 20 percent less
than a P.P.O. or an H.M.O. -- and they force employees to take more
responsibility for their health care.

Here's a mini-refresher on how high-deductible plans work: Typically, they have
a high  annual deductible,  of $2,500 to as much as $5,000. Many employers may
put $500 to $1,000 into a health savings account for you to offset that high
limit.

If you spend all the money in the health savings account, you then have to dip
into your own pocket to pay your medical bills until you exhaust the deductible.
But if you don't spend the money, it stays in the account for next year's
expenses.

To push workers toward high-deductible plans, employers may raise the premiums
on P.P.O.'s and H.M.O.'s even more than the underlying cost increase might
warrant.

GOING GENERIC In another move to steer employees toward lower-cost health care,
many employers will increase the co-payment on brand-name medications, while
making generics more affordable. Some plans, in lieu of co-payments, may even
require that you pay a percentage of the cost of brand-name drugs.

Let's say you now make a $30 co-payment when filling a Nexium or Lipitor
prescription. Next year, your co-payment could rise to $40 -- or even higher if
your plan requires you to pay a percentage  of the actual cost.

At the same time, though, some plans may lower the cost of maintenance drugs
(say, for asthma) that have been proved to reduce overall health costs,
according to Tom Billet, a senior consultant at Watson Wyatt, a benefits
consulting firm.

WELLNESS AND MORE Prepare to be doctored. Employers are determined to make you
healthier and, thereby, less expensive to insure.

''Employers are not backing away from wellness programs,'' Mr. Billet said.
''They are a core component of their long-term strategy to control costs and
improve productivity of their work force.''

Your employer will probably suggest that you (and your spouse if she or he is
covered on your plan) fill out a detailed health risk questionnaire, including
your height and weight and your family history for several diseases. The company
might even reward you with cash or some other incentive for doing so.

Many employers are also strongly urging their workers to get biometric tests --
blood work-ups that identify whether a person is  at risk for diseases like
diabetes or atherosclerosis.

The results of these tests and exams are sent to your insurer or a third-party
administrator, and your employer is not supposed to have access to the
information.

BEWARE THE AUDIT As another cost-saving strategy, more companies are conducting
audits to ensure that only eligible dependents are covered by their health
plans.

You may get an e-mail message or letter that asks you to verify the ages of your
children or the marital status of you and your covered spouse. Completing the
audit may be as simple as calling an 800 number and answering some prompts, or
it may be as complicated (and annoying) as sending in copies of birth and
marriage certificates to your insurer.

Remember, most policies cover your children only until age 19, or 23 if they are
in college.

Why are companies going to all this trouble? Audits routinely reveal that 5 to 8
percent of covered individuals are actually ineligible, according to Mr. Miele,
who expects his audit business to double in 2010.

Typically, the issue is one of confusion rather than fraud. ''Employers have not
done a great job of explaining who is covered,'' says Mr. Miele, who adds that
most of the people who get ''kicked out'' are college grads still living at
home. You can also expect to see much more specific coverage guidelines in 2010.

URL: http://www.nytimes.com

LOAD-DATE: October 10, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Benefits personnel at Biogen Idec checking a brochure outlining
options for this year's open-enrollment period. As costs rise, employees need to
evaluate their health plan choices carefully. (PHOTOGRAPH BY JODI HILTON FOR THE
NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


