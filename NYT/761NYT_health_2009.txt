                   Copyright 2009 The New York Times Company


                             761 of 2021 DOCUMENTS


                               The New York Times

                            October 6, 2009 Tuesday
                              Late Edition - Final

A Texas-Sized Health Care Failure

BYLINE: By CAPPY McGARR.

Cappy McGarr, the president of a private equity firm, was the chairman of the
Texas Insurance Purchasing Alliance from 1993 to 1995.

SECTION: Section A; Column 0; Editorial Desk; OP-ED CONTRIBUTOR; Pg. 31

LENGTH: 787 words

DATELINE: Dallas


THE Senate Finance Committee has for the moment rejected the idea of creating a
public health insurance plan. It's difficult to see how Americans will be able
to find good, affordable health insurance without one. But if we are to go
forward without a public option, it is more important than ever to make sure
that we get another part of health reform right: the exchanges, where it is
envisioned that small businesses and people without employer-sponsored insurance
could shop for policies of their own.

Back in the 1990s, I was the founding chairman of Texas' state-run purchasing
alliance -- an exchange, essentially -- which ultimately failed. There are
lessons to be learned from that experience, as well as the similar failures of
other states to create useful exchanges.

The Texas Insurance Purchasing Alliance, created by the Texas Legislature in
1993, was meant to help small businesses, which often cannot afford coverage for
their employees. (More than half of all uninsured Americans work for small
businesses.) Small businesses are charged higher rates -- on average 18 percent
higher than those paid by large companies. And their administrative costs, built
into those premiums, are typically as high as 25 percent of the premium,
compared to only 10 percent for big companies.

Our system pooled small employers into purchasing groups large enough to obtain
the lower wholesale insurance rates that big companies get.

Initially, the alliance worked exactly as planned. Sixty-three percent of the
businesses that participated were able to offer their employees health coverage
for the first time. The alliance offered small businesses a low-cost, nonprofit
option: our administrative arrangements did away with the high marketing costs
that insurers pass on to small businesses. And we didn't charge higher rates to
firms with older or less healthy workers. This in turn led other insurers,
outside the alliance, to lower their prices. We did all this not by creating a
government bureaucracy, but by relying on the private sector.

Nevertheless, six years after the program got off the ground, it folded. Many
factors contributed to our failure. Some elements of the program, like the
restriction it put on the size of eligible companies (only employers with 50 or
fewer employees could join), proved unpopular. In addition,  the governor who
helped create the alliance, Ann Richards,  was replaced in 1995 by George W.
Bush, who did not consider it a priority.

Most important, though, our exchange failed not because it wasn't needed, and
not because the concept wasn't sound, but because it never attained a large
enough market share to exert significant clout in the Texas insurance market.
Private insurance companies, which could offer small-business policies both
inside and outside the exchange, cherry-picked relentlessly, signing up all the
small businesses with generally healthy employees and offloading the bad risks
-- companies with older or sicker employees -- onto the exchange. For the
insurance companies, this made  business sense. But as a result, our exchange
was overwhelmed with people who had high health care costs, and too few healthy
people to share the risk. The premiums we offered rose significantly. Insurance
on the exchange was no longer a bargain, and employers began backing away.
Insurance companies, too, began leaving the alliance.

Texas wasn't the only state to see its insurance exchange fail. Florida and
North Carolina were also unsuccessful. And California, which had the first
exchange (established in 1992) and the largest market, shut its doors in 2006.
All these state exchanges failed for the same reason: cherry-picking by insurers
outside the exchange.

If Congress now creates new exchanges,  as seems increasingly likely,  it must
prevent this phenomenon by setting two national rules: Insurers have to accept
everyone and have  to charge everyone the same rates  regardless of  health
status.

Such rules would force insurers to spread risk. But enforcement would also be
difficult. Every aspect of health insurance -- from the rules for underwriting
and setting premiums to the marketing of policies -- would need to be monitored
stringently to prevent companies from steering all bad risks to the exchanges.

It would be smarter for Congress to revisit the idea of creating a public plan
that could provide an attractive choice for consumers and real competition for
private insurers, to give them the incentive to offer good coverage at
affordable prices.

But without a public plan, tough rules and restrictions on insurance companies
will be essential. Otherwise,  Americans will never be able to count on good,
affordable health care.

URL: http://www.nytimes.com

LOAD-DATE: October 6, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO (PHOTOGRAPH BY KELLY BLAIR)

DOCUMENT-TYPE: Op-Ed

PUBLICATION-TYPE: Newspaper


