                   Copyright 2009 The New York Times Company


                             734 of 2021 DOCUMENTS


                               The New York Times

                             October 2, 2009 Friday
                              Late Edition - Final

Snowe Fine After Tumble

BYLINE: By DAVID M. HERSZENHORN and JACKIE CALMES

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 226 words


Senator Olympia J. Snowe of Maine, the only Republican on the Senate Finance
Committee who has expressed any willingness to support the health care
legislation, may be the most closely watched figure in the committee's debating
sessions.

As Thursday afternoon's proceedings wore on, Ms. Snowe got up to leave the dais
-- senators frequently walk in and out -- and apparently tripped and fell. Aides
helped gather her papers, and she quickly left the hearing room. A spokesman for
Ms. Snowe said she  was just fine.

After a few awkward minutes, Senator Max Baucus, Democrat of Montana and the
committee chairman, said Ms. Snowe appeared to have stumbled over someone's foot
jutting into the aisle.

Presumably, it was a foot from her own party. Ms. Snowe fell at the end of the
staff row on the Republican side -- a metaphor, maybe, for the efforts by some
of her  Republican colleagues to cut the legs out from under her bipartisan
negotiations.

But so far Ms. Snowe has not wavered from her centrist course. And she has
warned Mr. Baucus that she will not vote for any legislation that she has not
had a chance to  review fully.

So Mr. Baucus has made clear that even if he met his goal of completing the
public debate by late Thursday or early Friday, a final committee vote would not
come until next week. DAVID M. HERSZENHORN and JACKIE CALMES

URL: http://www.nytimes.com

LOAD-DATE: October 2, 2009

LANGUAGE: ENGLISH

PUBLICATION-TYPE: Newspaper


