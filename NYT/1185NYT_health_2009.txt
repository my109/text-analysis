                   Copyright 2009 The New York Times Company


                             1185 of 2021 DOCUMENTS


                               The New York Times

                           December 15, 2009 Tuesday
                              Late Edition - Final

How Much Will Health Reform Cost?

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 40

LENGTH: 848 words


To the Editor:

In ''Can We Afford It?'' (editorial, Dec. 13), you remark that Republican
critics oppose the currently proposed health care reform on the grounds that the
nation cannot afford to add this new trillion-dollar entitlement in tough
economic times.

Here we should recall that in 2003, barely out of the recession of 2000-2,
President George W. Bush and his then Republican Congress passed the Medicare
Modernization Act of 2003, a huge new entitlement.

Unlike serious attempts to make the current health reform deficit-neutral,
however, the entire cost of the Medicare drug bill simply has been deficit-
financed since the inception of the program in 2006. Current projections are
that the legislation will add in excess of $1 trillion to the federal deficit
over the period 2010 to 2019.

Foreigners have bought many of the new United States Treasury bonds sold to
finance this largess. Our children will have to service these bonds and
ultimately redeem them.

There is something quite untoward for people who went along with that glaring
fiscal irresponsibility to now belatedly discover the virtue of fiscal probity.

Uwe E. Reinhardt    Princeton, N.J., Dec. 13, 2009

The writer is a professor of political economy at Princeton University.

To the Editor:

As a dermatologist in practice for 30 years, I read ''Can We Afford It?'' with
interest. You state that the House and the Senate have a lot of ideas that could
bring down costs over time. To the contrary, all of the ideas you cited would
actually increase costs to me and my patients.

Electronic records are expensive, take far too much time away from patient care
and don't have any influence on the number of tests I do.

Standard claims forms and automated claims processing have been around for years
and have reduced costs as much as they are going to.

As for replacing fee-for-service with fixed payments for a year's worth of
service, I'll retire before falling for this boondoggle. How else other than
fee-for-service are you going to pay me for caring for patients who have such
diverse needs as the kid I see once every few years for a wart to the elderly
person with a melanoma that requires half a day in the operating room and
follow-up visits every few days or weeks?

Your readers should ask themselves what it would be like trying to see a doctor
when that doctor has already been paid for seeing you and would have a strong
financial incentive not to see you again.

Rick Parkinson    Provo, Utah, Dec. 13, 2009

To the Editor:

You acknowledge that for many, $1 trillion is unimaginably more than we can pay
for expansion of health care, yet as you point out, it's less than the $4
trillion unfunded Bush tax cuts.

Let's look at it another way:

Over 10 years, $1 trillion is $100 billion a year. Given an estimated 308
million people in the United States, that's $325 a year per person. The $325
breaks down to $27 a month, or roughly $6 a week, or less than 90 cents a day
for each of us, man, woman and child, for 10 years, during which we would be
providing health insurance to over 30 million people not covered today.

Yes, health care reform is fiendishly complex and expensive. Can we afford a
trillion dollars? Yes.

Ben Hardy    Bloomfield, Conn., Dec. 13, 2009

To the Editor:

You seem to be looking past the reality and track record of health care reform
finances when you posit that the country can, in fact, afford the current
legislative initiative.

Yes, Congress estimates that the proposed legislation will cost in the
neighborhood of $1 trillion over 10 years with a net reduction of the deficit of
about $100 billion to $200 billion. So what's the problem? Congressional
budgetary ineptitude.

Sally C. Pipes, president of the Pacific Research Institute, had it right when
she argued that with the advent of Medicare in 1965: ''The cost was about $3
billion and it was projected to cost $12 billion in 1990. Well in fact it cost
$107 billion. Last year it cost $427 billion; it's estimated in 2017 to cost
$884 billion.''

Just imagine what a similarly misestimated $1 trillion program will actually
cost just 10 years out -- $10 trillion?

Ronald F. LaValle    Patchogue, N.Y., Dec. 13, 2009

The writer is a former associate director at the University Hospital and Medical
Center at Stony Brook.

To the Editor:

Re ''Lieberman Says He Can't Back Current Health Bill'' (news article, Dec. 14):
Senate Democrats should force Senator Joseph I. Lieberman, independent of
Connecticut, and his Republican allies to filibuster the health care bill
throughout the holiday season rather than letting them hold the American people
hostage to their tactics.

Moreover, the filibusters should be shown live in public hospital emergency
rooms throughout the United States, so that the uninsured people waiting for
primary care can understand exactly who is preventing them from obtaining
affordable health insurance. (Better yet, make the filibusters take place in
emergency rooms, but the Capitol Police might object.)  Jonathan I. Ezor    West
Hempstead, N.Y., Dec. 14, 2009

URL: http://www.nytimes.com

LOAD-DATE: December 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY THOMAS POROSTOCKY)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


