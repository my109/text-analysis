                   Copyright 2009 The New York Times Company


                             860 of 2021 DOCUMENTS


                               The New York Times

                            October 20, 2009 Tuesday
                              Late Edition - Final

Basic Medicare Premium to Rise 15% Next Year

BYLINE: By ROBERT PEAR

SECTION: Section A; Column 0; National Desk; Pg. 22

LENGTH: 676 words

DATELINE: WASHINGTON


The basic Medicare premium will shoot up next year by 15 percent, to $110.50 a
month, federal officials said Monday.

The increase means that monthly premiums would top $100 for the first time, a
stark indication of the rise in medical costs that is driving the debate in
Congress about a broad overhaul of the health care system.

About 12 million people, or 27 percent of Medicare beneficiaries, will have to
pay higher premiums or have the additional amounts paid on their behalf. The
other 73 percent will be shielded from the increase because, under federal law,
their Medicare premiums cannot go up more than the increase in their Social
Security benefits, and Social Security officials announced last week that there
would be no increase in benefits in 2010 because inflation had been extremely
low.

Kathleen Sebelius, the secretary of health and human services, urged the Senate
to approve a bill, already passed by the House, to block the scheduled increase
in Medicare premiums.

''We are in tremendously difficult economic times, and seniors are being hit
particularly hard,'' Ms. Sebelius said. ''The last thing seniors need right now
is a substantial increase in their Medicare premiums, and many seniors will see
such an increase if no action is taken.''

Among those who face higher premiums next year are new Medicare beneficiaries,
high-income people and those whose Medicare premiums are paid by Medicaid.
Premiums can be as high as $353.60 a month, or more than $4,200 a year, for
Medicare beneficiaries who file tax returns with adjusted gross income greater
than $214,000 for an individual or $428,000 for a couple.

The higher premiums will impose ''an additional and significant burden'' on
states, which help pay Medicaid costs, along with the federal government.

The House bill was passed, 406 to 18, on Sept. 24. Among those who voted against
it was the Democratic leader, Representative Steny H. Hoyer of Maryland, who
said he saw no need to help multimillionaires at a time when the nation was
struggling to rein in entitlement programs.

While lawmakers considered whether to freeze Medicare premiums, a handful of
senators met behind closed doors on Monday to work out a compromise health care
bill to cover the uninsured.

One participant, Senator Max Baucus, Democrat of Montana and chairman of the
Finance Committee, said senators were considering new ideas to finesse
disagreements over whether the government should offer its own health insurance
plan, in competition with private insurers.

Under one proposal, he said, the government would create a public plan, but
states could ''opt out'' if they wanted to devise and operate their own
insurance programs.

The meeting, convened by the Senate majority leader, Harry Reid, Democrat of
Nevada, included Mr. Baucus and Senator Christopher J. Dodd, Democrat of
Connecticut, who presided over the health committee when it approved a sweeping
health care bill in July.

Mr. Reid said he hoped to take a compromise bill to the Senate floor early next
month. But that assumes rapid progress in negotiations and a quick analysis by
the Congressional Budget Office, to confirm whether the 10-year cost of the bill
is under the $900 billion ceiling set by President Obama.

The Finance Committee approved a detailed outline of a sweeping health care bill
last week. Mr. Baucus formally introduced the bill, a 1,502-page document, on
Monday.

The Senate is debating a separate bill to prevent deep cuts in Medicare payments
to doctors. The bill would not offset any of the costs, estimated at $247
billion over the next 10 years.

Senator Lamar Alexander of Tennessee, the No. 3 Republican in the Senate, said:
''Of course, we need to fix doctors' reimbursement. But it needs to be paid for.
We can't just add a quarter-trillion dollars to the national debt.''

Democrats said the bill simply recognized political reality. In recent years,
they said, Congress has repeatedly stepped in to prevent cuts in Medicare
payments to doctors, and it is likely to do so in the future.

URL: http://www.nytimes.com

LOAD-DATE: October 20, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTO: Senator Max Baucus, chairman of the Finance Committee, on his
way to a meeting Monday to discuss health care legislation.(PHOTOGRAPH BY LUKE
SHARRETT/THE NEW YORK TIMES)

PUBLICATION-TYPE: Newspaper


