                   Copyright 2009 The New York Times Company


                             1274 of 2021 DOCUMENTS


                               The New York Times

                            December 27, 2009 Sunday
                              Late Edition - Final

IV. Health Care

BYLINE: By KEVIN SACK

SECTION: Section WK; Column 0; Week in Review Desk; THE YEAR IN PICTURES 2009;
Pg. 6

LENGTH: 261 words


Who knew that health policy, with its bloodless considerations of guaranteed
issue and adverse selection, could excite such fury? Public option, you say? Now
those are fighting words.

By August, when members of Congress made the mistake of returning home, the
health care debate had transformed into a screaming proxy war over the role of
government in the age of Obama.The rancor at town-hall-style meetings
temporarily obscured the focus ofPresident Obama's top domestic priority:
covering most of the 46 million uninsured while slowing the bankrupting growth
of medical costs.

But while the Democrats missed self-appointed deadlines, neither did they fold,
at least not completely. With Mr. Obama stoking momentum at key junctures, the
House passed a sweeping bill in November and the more conservative Senate
followed suit just before Christmas. After working to attract Republican
support, Senate leaders eventually resolved to go it alone. To corral the 60
votes he needed, the majority leader,Harry Reid of Nevada, negotiated
enticements that made power brokers of a succession of moderates, nudging the
bill toward the center.

House and Senate conferees now must blend their bills into one that is projected
to cost nearly $900 billion over 10 years. There is broad agreement on
fundamentals like requiring individuals to have insurance, forcing insurers to
cover pre-existing conditions, vastly expanding Medicaid and providing subsidies
for premiums. One fight that still rages: the public option, which exists in the
House bill but not in the Senate's.

URL: http://www.nytimes.com

LOAD-DATE: December 27, 2009

LANGUAGE: ENGLISH

GRAPHIC: PHOTOS: Emily Love, 64, was comforted before receiving dental treatment
in Inglewood, Calif. Thousands waited in long lines at a free medical clinic set
up by the nonprofit Remote Area Medical.(PHOTOGRAPH BY RUTH FREMSON, THE NEW
YORK TIME)
Hamid Karzai, the Afghan president, campaigning for re-election in Dar-e-Kayan
in a remote valley in the central highlands to get support from the Ismailis, a
Shiite sect (PHOTOGRAPH BY TYLER HICKS, THE NEW YORK TIMES)
Marines trying to clear out Taliban militants from strongholds in southern
Afghanistan ran to safety after an I.E.D. blast. Two soldiers were
killed.(PHOTOGRAPH BY MANPREET ROMANA, AGENCE FRANCE-PRESSE - GETTY IMAGES)
An aid worker distributed water in the Turkana region of Kenya, an area
particularly hard hit by a severe drought.(PHOTOGRAPH BY JEHAD NGA FOR THE NEW
YORK TIMES)
Supporters of the ousted the Honduran president, Manuel Zelaya, raised their
hands before advancing soldiers during a protest in Tegucigalpa.(PHOTOGRAPH BY
DARIO LOPEZ-MILLS ASSOCIATED PRESS)
Hong Kong closed the city's primary schools after local swine flu cases were
discovered.(PHOTOGRAPH BY MIKE CLARKE, AGENCE FRANCE-PRESSE - GETTY)
 Pakistani soldiers held Taliban fighters and suspected supporters in a large
campaign in the country's Swat Valley.(PHOTOGRAPH BY TYLER HICKS, THE NEW YORK
TIMES)

PUBLICATION-TYPE: Newspaper


