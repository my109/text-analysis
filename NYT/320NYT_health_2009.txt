                   Copyright 2009 The New York Times Company


                             320 of 2021 DOCUMENTS


                               The New York Times

                            August 15, 2009 Saturday
                              Late Edition - Final

A Flood of Ideas About Health Care

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 18

LENGTH: 1172 words


To the Editor:

Re ''10 Steps to Better Health Care,'' by Atul Gawande, Donald Berwick, Elliott
Fisher and Mark McClellan (Op-Ed, Aug. 13):

The writers touch on a significant but generally overlooked issue in the heated
and often nasty debate about health care reform. They refer to pockets around
the nation where health care costs are not wildly out of control while the
quality of care is maintained at acceptable levels.

It makes economic and moral sense for our legislators and President Obama to
explore incorporating the ideas cited in this article as they try to work out a
comprehensive legislative tool to improve a system that is too costly and
doesn't ensure coverage of a large segment of our population.

Using their examples to develop working models of reform can only result over
time in the delivery of better quality care and improved medical outcomes.

Alan Safron    Woodcliff Lake, N.J., Aug. 13, 2009

To the Editor:

''10 Steps to Better Health Care'' makes clear that a major problem in reducing
costs is the fee-for-service model, whereby physicians are paid for the often
expensive tests and procedures they provide. Psychologists would say that unless
these ''contingencies of reinforcement'' are changed, it will be hard to control
costs.

The writers note that when the incentive for fees is eliminated through
salaries, costs are significantly reduced. With medical students typically
graduating with a debt of $140,000 for their education, the incentive to
generate such excessive fees is set in place.

One obvious way to change all of this is for the government to pay for doctors'
medical education if they accept a salaried position.

Paul M. Wortman    East Setauket, N.Y., Aug. 13, 2009

The writer is professor emeritus of social and health psychology at Stony Brook
University.

To the Editor:

President Obama and the Democrats in Congress have made a significant mistake in
trying to resolve all of the nation's health care problems at the same time. The
issue has two distinct, albeit interrelated, aspects: the high cost of providing
health care and the high cost of insurance.

A more rational approach would have been to attack these aspects separately,
with reducing health care costs having the higher priority.

As noted in ''10 Steps to Better Health Care,'' there are prototypes available
(for example, the Cleveland Clinic) that could serve as models for redefining
how health care is efficiently provided.

Government incentives for the medical profession to restructure itself along the
lines of these models should be the focus of any bill the president signs. One
incentive could be a drastic reduction in, or even elimination of, taxes paid by
clinics and groups structured along these lines.

A bill designed to accomplish this result could be written in fewer than 100
pages and in terms easily understood by mainstream Americans.

Fred Lobbin    Columbia, Md., Aug. 13, 2009

To the Editor:

The writers have several ideas for improving patient outcomes and keeping costs
down.

But what is an improved patient outcome? Is it a high success rate for surgery?
Is it fast service? Is it a low rate of medical errors? Is it a high remission
rate for cancers?

How does one judge whether a high-risk and high-reward procedure yields a good
''patient outcome'' if some patients die and some recover completely?

The best answers to those questions will come when all adults can make their own
decisions about which outcomes are best for them. While today's system falls
short of this ideal, every proposal in Congress gives less decision-making power
to the individual.

There is a lot we can learn from the wisdom of crowds, but I worry that the
wisdom of Congress will win the day.

Jeff Baird    Chicago, Aug. 13, 2009

To the Editor:

Re ''Getting to the Source of the 'Death Panel' Rumor'' (front page, Aug. 14):

The lack of thoughtful debate on health care reform is breathtaking. How
conservatives can spin discussions of end-of-life care into ''death panels'' is
mind-boggling.

Even worse is the fact that under the current system insurance companies are
already acting the part by deciding who receives treatment and what type. Yet
those same angry mobs attacking these end-of-life proposals are not taking up
arms against the status quo.

Ann D. Bagchi    East Brunswick, N.J., Aug. 14, 2009

To the Editor:

As a teacher of a course on death and author of a book on bereavement, I have
carefully read Section 1233 (''Advance Care Planning Consultation'') of the
proposed health care bill.

It is a compassionate and coherent statement about helping Americans to
understand their choices. We are obsessed with the avoidance of dying and the
denial of death.

As a former hospice volunteer, I was frustrated that every individual I worked
with died so quickly because physicians and family members waited too long to
initiate the supportive program that a hospice offers. Section 1233 can help in
many ways to make dying less painful and frightening.

Jack D. Spiro    Richmond, Va., Aug.  14, 2009

The writer is the author of ''A Time to Mourn: Judaism and the Psychology of
Bereavement.''

To the Editor:

''Thousands Wait in Line for Health Care That's Free'' (news article, Aug. 13)
demonstrates quite well why we need a single-payer system in this country.

It also illustrates why we need reforms in our health care system that make
access to health care independent of our job status and our financial status. No
one should have to be treated in an arena or at a fairground.

I would be happy to see the money spent on my benefits go to finance a
single-payer system that covers health and dental care. It's time for our
elected representatives to do what is right: make sure that every citizen in
this country has access to decent care, both health and dental.

A. L. Alterman    Hastings-on-Hudson, N.Y., Aug. 13, 2009

To the Editor:

One phrase used by more than a few people in the debate on health care reform is
that ''our health care system is the envy of the world.'' Which world is that?

A recent Harris/Decima Poll in Canada, the country that probably knows our
system the best, found a 10-to-1 majority who believe their system is better
than ours. And Harris Polls in France and Britain found that most people there
believe that their systems are ''the envy of the world.''

Humphrey Taylor    Chairman, The Harris Poll    New York, Aug. 13, 2009

To the Editor:

A neighbor asked me how I felt about President Obama's health care reform
effort. My answer was that I had no answer. I'm confused. Aside from insiders,
does anyone truly understand it?

Despite all the wild absurdities and tumultuous emotions, we're going to have
health care legislation. But what will be in it?

At 93, I've enjoyed Medicare for 28 years. No complaints.

Why can't everyone have Medicare, which has performed admirably since 1965,
instead of a whole new complicated bureaucracy that will still have to be
tested?

Jack E. Cohen     Hewlett, N.Y., Aug. 13, 2009

URL: http://www.nytimes.com

LOAD-DATE: August 15, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY ANTHONY RUSSO)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


