                   Copyright 2010 The New York Times Company


                             1484 of 2021 DOCUMENTS


                               The New York Times

                            February 7, 2010 Sunday
                              Late Edition - Final

Why Politics Is Stuck In the Middle

BYLINE: By TYLER COWEN.

Tyler Cowen is a professor of economics at George Mason University.

SECTION: Section BU; Column 0; Money and Business/Financial Desk; ECONOMIC VIEW;
Pg. 5

LENGTH: 978 words


MARKET competition, under the proper circumstances, has the power to make a
business better serve its customers. Cellphone companies, for example,   compete
via cheaper prices, clearer connections and better apps. Political competition,
though no less vigorous, is conducted on very different terms --  and often ends
up stifling innovation instead of encouraging it.

When viewed through an economist's lens, the quest for voter approval helps
explain some recent developments  in domestic politics, including the stalling
of health care reform and the proposed freeze on the federal government's
discretionary spending.

Economists approach political competition with a simple but potent hypothesis
called the ''median voter theorem.'' Anthony Downs, a senior fellow at the
Brookings Institution, proposed the idea in his 1957 book, ''An Economic Theory
of Democracy.'' Essentially, the idea is this: Any politician who strays too far
from voters at the philosophical center  will soon be out of office.

In fact, there is a dynamic that pushes politicians to embrace the preferences
of the typical or ''median'' voter, who sits squarely in the middle of public
opinion. A significant move to either the left or the right would open the door
for a rival to take a more moderate stance, win the next election and change the
agenda. Politicians will respond to this dynamic, whether they are power-seeking
demagogues or more benevolent types who use elected office to help the world.

When it comes to the big issues, voters at the midpoint  usually get the
policies, if not always the exact outcomes, they want. In the federal budget,
the largest line items include Social Security, Medicare, Medicaid and military
spending --   all very popular programs. The interest on the national debt is
mounting because we don't like paying higher taxes now for all those benefits,
so our government borrows to postpone the pain.

Upon his election, President Obama stepped into a world already full of
political constraints. He won the White House and significant Democratic
majorities in both the House and the Senate -- yet even if American voters were
tired of the Republican Party, it's not clear that their underlying opinions had
changed very much.

Correctly or not, most Americans have failed to embrace the Democratic health
care plans. And ever since the Republicans won the special Senate election in
Massachusetts, even the Democrats in Congress have stalled on the legislation.
It now appears that much of the initial support was thin.

Senate Democrats, for instance, could overcome a Republican filibuster through a
parliamentary process known as reconciliation, but they are waiting, evidently
out of fear that voters aren't with them on this issue.

Many people are increasingly worried about deficits. That may have led Mr. Obama
to announce a freeze on nonmilitary discretionary spending, and yet this freeze
refuses to target major, popular budget items  like Social Security. The public
seems to want the self-image of being tough on spending without giving up the
goodies. President Obama may well know better, but he is doing his best to
oblige, if only to prevent a Republican landslide this November.

The point here is not to belittle or praise the president, but to point out that
his hands are tied. The biggest leftward move in American economic policy
occurred during the Roosevelt and Truman years, when the Democrats had the upper
hand for five consecutive presidential terms. Because of depression and war,
people were looking for real change. Competitive forces in politics were
relatively weak, and the Democrats had the chance to make their policies stick.

The Supreme Court's recent ruling on campaign spending also comes into clearer
focus through the median voter theorem.  The court ruled that the government may
not ban political spending by corporations in candidate elections. Critics fear
that the political influence of corporations will grow, but some academic
specialists in campaign finance aren't so sure.

For all the anecdotal evidence, it's hard to show statistically that money has a
large and systematic influence on political outcomes. That is partly because
politicians cannot stray too far from public opinion. (In part, it is also
because interest groups get their way on many issues by supplying an
understaffed Congress with ideas and intellectual resources, not by running ads
or making donations.) It is quite possible that the court's decision won't
affect election results very much.

Of course, the median voter theorem is far from a complete explanation of
politics. Sometimes politicians lead public opinion and talk voters into
accepting new ideas, as when President Bill Clinton promoted Nafta. And voters
often favor conflicting or contradictory policies, like wanting to pull troops
out of Iraq but also not wanting Iraq to explode into chaos.

Finally, most people  aren't very well informed about politics and can be
downright irrational or stubborn, which is another reason that political
competition isn't always as beneficial as economic competition.

THE median voter theorem doesn't predict that the legacy of the Obama
administration will be a wash. But it does imply that we might find the most
important achievements in areas that don't always linger on the front page. For
instance, the president's ideas on education, which involve accountability and
charter schools and pay for performance, may please the American public and thus
make their way into policy. And because  education transforms the knowledge and
interests of the median voter for generations to come, such acceptance could
make for a lot of other improvements.

If you're looking for change to believe in, and change that will last, the odds
are best when political competition is pushing the world in your direction.

URL: http://www.nytimes.com

LOAD-DATE: February 7, 2010

LANGUAGE: ENGLISH

GRAPHIC: DRAWING (DRAWING BY DAVID G. KLEIN)

PUBLICATION-TYPE: Newspaper


