                   Copyright 2009 The New York Times Company


                              52 of 2021 DOCUMENTS


                               The New York Times

                              June 19, 2009 Friday
                              Late Edition - Final

The Rationing of U.S. Medical Care

SECTION: Section A; Column 0; Editorial Desk; LETTERS; Pg. 26

LENGTH: 960 words


To the Editor:

David Leonhardt (''Limits in a System That's Sick,'' Economic Scene, June 17)
makes no mention of the most egregious form of medical care rationing -- the
decisions by insurance companies to pay or not to pay for medical procedures,
decisions that are routinely based on cost rather than on sound medical
practice.

Health care reform will get nowhere until the public understands that the
insurance companies are the ones currently making the rationing decisions. The
objection to a government-run plan -- that it would compromise the relationship
of doctor and patient -- overlooks the fact that the relationship is already
severely compromised by the insurance companies.

Howard Nenner  Whately, Mass., June 17, 2009

To the Editor:

Let's not forget that health care is also already rationed by socioeconomic
status: the very poor are covered by government plans, and the very rich can
afford whatever treatment they want. It's we folks in the middle who are left
holding the bag.

Ted Claire  Berkeley, Calif., June 17, 2009

To the Editor:

David Leonhardt is correct that health care in the United States is rationed.
Our present system reflects a flawed value system of society that does not
recognize the value of the time, judgment and advice of a fully trained
primary-care physician. Payment is for actions taken. This is different from the
other learned professions.

Thoughtful, conscientious evaluation and management require more time and fewer
actions than third parties pay for. This changes good primary-care doctors into
referral centers to specialists. Paying for time and not just actions is a
prerequisite for meaningful reform.

The cultural desire to know what is wrong immediately is another cause of higher
costs. Immediacy requires myriad tests and imaging done quickly. Careful
observation for a time can be just as effective, safer and less costly. Another
prerequisite for lowering costs is for our national culture to develop some
patience.

Marcus M. Reidenberg  New York, June 17, 2009

The writer, a medical doctor, is a professor of pharmacology, medicine and
public health at Weill Cornell Medical College.

To the Editor:

''Doctors and the Cost of Care'' (editorial, June 14) unfairly blames
''profligate physician behavior'' for high medical spending in the United
States. Contrary to what the Dartmouth researchers claimed, regions with high
Medicare per capita spending are not necessarily wasting money.

Medicare spending per capita is an inaccurate proxy for overall medical
spending. Atul Gawande, in his New Yorker article cited in the editorial, relies
on the Dartmouth Medicare data to identify McAllen, Tex., as the town with the
highest per capita medical spending next to Miami.

But there are reasons other than physician behavior that explain Medicare costs
in McAllen. It is a border town and cares for many newcomers who are uninsured
and can't pay. Therefore costs are shifted to insurance programs like Medicare.

In addition, to the extent actual medical spending per capita is high in
McAllen, Dr. Gawande points out other reasons. McAllen's population drinks 60
percent more than average and has a 38 percent obesity rate.

Most doctors put their patients first, and many doctors provide free treatment
when a patient cannot pay. What a shame doctors are being vilified in the name
of reform.   Betsy McCaughey   New York, June 15, 2009

The writer is chairwoman of the Committee to Reduce Infection Deaths and a
former lieutenant governor of New York State.

To the Editor:

I was disturbed to see your editorial suggest that the blame for ''ever rising
premiums'' falls primarily on physicians. Let's give credit where credit is due.

Between 2000 and 2007, the 10 largest publicly traded insurance companies
increased their profits 428 percent, from $2.4 billion to $12.9 billion,
according to Securities and Exchange Commission filings.

During the same period, the number of insurers fell by nearly 20 percent,
largely because of a huge wave of mergers that led to stunning consolidation.
And premiums increased by more than 87 percent, rising four times faster than
the average American's wages.

Today, 95 percent of American insurance markets qualify as tight oligopolies. As
in so many industries, blind reliance on free-market forces has failed the
American public.

Clearly, doctors bear a responsibility to curb costs. But the real culprits are
the middlemen who, after years of lax regulation, now have such a tight grip on
the market that they can -- and do -- charge whatever they want.

David A. Balto  Washington, June 14, 2009

The writer is a senior fellow with the Center for American Progress.

To the Editor:

Since its founding, Mayo Clinic has paid physicians with salaries to avoid
financial conflicts of interest in clinical decision-making, and to promote
multi-disciplinary coordination of care. Less well known, but important to
Mayo's early growth, was its principle of billing based on patients' means.

Using a sliding scale, wealthier patients paid more than poorer patients did for
the same care. Such means adjustment increased patient volumes, which
strengthened Mayo's expertise and efficiency, benefiting rich and poor alike.

Health reform warrants similar means-adjustment principles. Consumer-owned
cooperatives could buy high-deductible, low-premium insurance policies, and pay
below-deductible bills, using means-adjusted withdrawals from participants'
health savings accounts. Such a plan would improve outcomes and value more
effectively than bureaucratic price-setting would.

Randall C. Walker  Rochester, Minn., June 15, 2009

The writer is a medical doctor at the Division of Infectious Diseases, Mayo
Clinic College of Medicine.

URL: http://www.nytimes.com

LOAD-DATE: June 19, 2009

LANGUAGE: ENGLISH

GRAPHIC: DRAWING: (DRAWING BY TED McGRATH)

DOCUMENT-TYPE: Letter

PUBLICATION-TYPE: Newspaper


