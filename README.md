# Text Analysis using R

These materials accompany the DUPRI Text Analysis training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* text_analysis.Rmd - Rmarkdown HTML version of training code
* text_analysis.html - HTML report of training for reference
* Text Analysis.Rproj - RStudio project file
* DailyKos_health.csv - Daily Kos blog post data
* NYT - Contains _New York Times_ health care articles
* topic_model.RData - RData file containing topic model
* topic_number_diagnostics.RData- RData file containing diagnostics to determine number of topics for topic model
